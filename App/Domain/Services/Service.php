<?php

namespace App\Domain\Services;

use Library\Events\Event;
use Library\Events\EventManager;
use Library\Log\Log;
use Library\Transformer\Transformer;

abstract class Service
{
    /**
     * @var EventManager
     */
    private $eventManager;

    /**
     * @var Transformer
     */
    protected $transformer;

    /**
     * @var Log
     */
    protected $log;

    /**
     * @var array
     */
    protected $paths = [];

    public function __construct(EventManager $eventManager, Transformer $transformer, Log $log)
    {
        $this->eventManager = $eventManager;
        $this->transformer = $transformer;
        $this->log = $log;
        if (is_null(getenv('PROJECTS_ROOT')))
        {
            throw new \Exception('PROJECTS_ROOT key is not set.');
        }

        $this->paths = ['projectsRoot' => getenv('PROJECTS_ROOT')];
        $this->paths['pyramidRoot'] = $this->paths['projectsRoot'].'pyramid/';
        $this->paths['scriptsRoot'] = $this->paths['pyramidRoot'].'Storage/Scripts/';
        $this->paths['backendTemplateRoot'] = $this->paths['scriptsRoot'].'.pyramid-init/backend_template/';
        $this->paths['frontendTemplateRoot'] = $this->paths['scriptsRoot'].'.pyramid-init/frontend_template/';

        $this->paths['setupApacheScript'] = $this->paths['scriptsRoot'].'setup-apache';
        $this->paths['removeApacheScript'] = $this->paths['scriptsRoot'].'remove-apache-config';

        $this->paths['apacheDefaultConfigFile'] = $this->paths['scriptsRoot'].'.pyramid-init/apache/default.dev.conf';
    }

    /**
     * @param Event $event
     */
    protected function fireEvent(Event $event)
    {
        $this->eventManager->fire($event);
    }
}