<?php

namespace App\Domain\Services\Server;

use App\Domain\Services\CurrentProjectService;
use Library\Http\Response;
use App\Domain\Utils\FileUtils;

class DatabaseService extends CurrentProjectService
{
    public function fetch()
    {
        $schema = require $this->currentProject->projectRoot().'/Config/Schema/schema.php';
        return new Response(Response::STATUS_OK, $schema);
    }

    public function addType(string $name)
    {
        $schema = json_decode(file_get_contents($this->currentProject->projectRoot().'/Config/Datamapper/schema.json'), true);
        if (isset($schema[$name]))
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Type with name '.$name.' already exists.');
        }

        $schema[$name] = [];
        FileUtils::writeFile($this->currentProject->projectRoot().'/Config/Datamapper/schema.json', json_encode($schema, JSON_PRETTY_PRINT | JSON_FORCE_OBJECT));

        $script = $this->currentProject->projectRoot().'/engine';
        $cmd = 'php '.$script.' schema:sync 2>&1';
        exec($cmd, $output);

        return new Response(Response::STATUS_OK, $output);
    }

    public function removeType(string $name)
    {
        $schema = json_decode(file_get_contents($this->currentProject->projectRoot().'/Config/Datamapper/schema.json'), true);
        if (!isset($schema[$name]))
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Type with name '.$name.' does not exist.');
        }

        unset($schema[$name]);
        file_put_contents($this->currentProject->projectRoot().'/Config/Datamapper/schema.json', json_encode($schema, JSON_PRETTY_PRINT | JSON_FORCE_OBJECT));

        $script = $this->currentProject->projectRoot().'/engine';
        $cmd = 'php '.$script.' schema:sync 2>&1';
        exec($cmd, $output);

        return new Response(Response::STATUS_OK, $output);
    }

    public function addField(string $typeName, $field)
    {
        $schema = json_decode(file_get_contents($this->currentProject->projectRoot().'/Config/Datamapper/schema.json'), true);
        if (!isset($schema[$typeName]))
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Type with name '.$typeName.' does not exist.');
        }

        if (isset($schema[$typeName][$field['name']]))
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Field with name '.$fieldName.' already exists.');
        }

        $schema[$typeName][$field['name']] = [
            'type' => $field['type']
        ];
        file_put_contents($this->currentProject->projectRoot().'/Config/Datamapper/schema.json', json_encode($schema, JSON_PRETTY_PRINT | JSON_FORCE_OBJECT));

        $script = $this->currentProject->projectRoot().'/engine';
        $cmd = 'php '.$script.' schema:sync 2>&1';
        exec($cmd, $output);

        return new Response(Response::STATUS_OK, $output);
    }

    public function removeField(string $typeName, string $fieldName)
    {
        $schema = json_decode(file_get_contents($this->currentProject->projectRoot().'/Config/Datamapper/schema.json'), true);
        if (!isset($schema[$typeName]))
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Type with name '.$typeName.' does not exist.');
        }

        if (!isset($schema[$typeName][$fieldName]))
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Field with name '.$fieldName.' does not exist.');
        }

        unset($schema[$typeName][$fieldName]);
        file_put_contents($this->currentProject->projectRoot().'/Config/Datamapper/schema.json', json_encode($schema, JSON_PRETTY_PRINT | JSON_FORCE_OBJECT));

        $script = $this->currentProject->projectRoot().'/engine';
        $cmd = 'php '.$script.' schema:sync 2>&1';
        exec($cmd, $output);

        return new Response(Response::STATUS_OK, $output);
    }
}