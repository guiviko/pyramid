<?php

namespace App\Domain\Services\Server;

use App\Domain\Services\CurrentProjectService;
use Library\Http\Response;

class ModelService extends CurrentProjectService
{
    public function fetch()
    {
        return new Response(Response::STATUS_OK);
    }
}