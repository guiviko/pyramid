<?php

namespace App\Domain\Services\Server;

use App\Domain\Services\CurrentProjectService;
use Library\Http\Response;
use Library\Routing\RouteBuilder;

class RouteService extends CurrentProjectService
{
    public function fetch()
    {
        $routeBuilder = new RouteBuilder($this->currentProject->routesFile());

        $routes = [];
        foreach ($routeBuilder->getRoutes()->toArray() as $route)
        {
            list($controllerPath, $action) = explode('@', $route->action());

            $namespaces = explode('\\', $controllerPath);
            array_shift($namespaces);
            array_shift($namespaces);
            array_shift($namespaces);
            $controller = array_pop($namespaces);

            $routes[] = [
                'uri' => $route->uri(),
                'method' => $route->methods()[0],
                'namespaces' => $namespaces,
                'controller' => $controller,
                'action' => $action
            ];
        }

        return new Response(Response::STATUS_OK, $routes);
    }

    public function create($method, $uri, $controller, $action, $service)
    {
        // add to routes.php
        $this->addToRoutes($method, $uri, $controller, $action);

        // make request
        $this->createRequest($controller, $action);

        // make controller
        $this->createController($controller);

        // add controller method
        $this->addToController($controller, $action, $service);

        // add service method
        $this->addToService($action, $service);

        return new Response(Response::STATUS_OK);
    }

    private function addToRoutes($method, $uri, $controller, $action)
    {
        $content = file_get_contents($this->currentProject->routesFile());

        $namespaces = explode('\\', $controller);
        $controllerName = $namespaces[sizeof($namespaces) - 1];
        $namespaces[sizeof($namespaces) - 1] = str_replace('Controller', '', $controllerName);

        // check if already there
        if (strpos($content, $controllerName.'@'.$action) !== false)
        {
            return;
        }

        $level = -1;
        $lastOffset = 0;
        foreach ($namespaces as $namespace)
        {
            $level++;
            preg_match('/group\(\[\'namespace\' => \''.ucfirst($namespace).'.*\n\s*({)/sU', $content, $matches, PREG_OFFSET_CAPTURE);
            if (sizeof($matches) > 0)
            {
                $lastOffset = $matches[1][1] + 2;
                continue;
            }

            $indent = '';
            for ($i = 0; $i < $level; $i++)
            {
                $indent .= '    ';
            }

            if ($level == sizeof($namespaces) - 1)
            {
                $str = $indent.'$this->group([';
            }
            else
            {
                $str = $indent.'$this->group([\'namespace\' => \''.ucfirst($namespace).'\', ';
            }

            $str .= '\'prefix\' => \'/'.lcfirst($namespace).'\', ';
            $str .= '\'as\' => \''.lcfirst($namespace).'\'], ';
            $str .= 'function()'.PHP_EOL;
            $str .= $indent.'{'.PHP_EOL;
            $add = strlen($str);
            $str .= PHP_EOL;
            $str .= $indent.'});'.PHP_EOL;

            $content = substr($content, 0, $lastOffset).$str.substr($content, $lastOffset);

            $lastOffset += $add;
        }

        $indent = '';
        for ($i = 0; $i < $level + 1; $i++)
        {
            $indent .= '    ';
        }

        $uriParts = explode('/', $uri);
        $uriLeaf = $uriParts[sizeof($uriParts) - 1];

        $str = $indent.'$this->'.strtolower($method).'(\''.($uri[strlen($uri) - 1] == '/' ? '' : '/'.$uriLeaf).'\', ';
        $str .= '\''.$controllerName.'@'.$action.'\');';
        if (substr($content, $lastOffset)[0] != PHP_EOL)
        {
            $str .= PHP_EOL;
        }

        $content = substr($content, 0, $lastOffset).$str.substr($content, $lastOffset);

        file_put_contents($this->currentProject->routesFile(), $content);
    }

    private function createRequest($controller, $action)
    {
        $namespaces = explode('\\', $controller);
        $namespaces[sizeof($namespaces) - 1] = str_replace('Controller', '', $namespaces[sizeof($namespaces) - 1]);

        $dirs = $this->currentProject->requestsRoot();
        foreach ($namespaces as $namespace)
        {
            $namespace = ucfirst($namespace);
            $dirs .= '/'.$namespace;

            if (!file_exists($dirs))
            {
                mkdir($dirs);
                exec('sudo '.self::fixAccessScript.' '.$dirs.' guiviko');
            }
        }

        $file = $dirs.'/'.ucfirst($action).'Request.php';
        if (file_exists($file))
        {
            return;
        }

        $content = file_get_contents(self::templateDir.'/server/requests/basic.php');

        if (sizeof($namespaces) > 0)
        {
            $str = '';
            foreach ($namespaces as $namespace)
            {
                $str .= '\\'.ucfirst($namespace);
            }

            $content = str_replace('{namespaces}', $str, $content);
        }
        else
        {
            $content = str_replace('{namespaces}', '', $content);
        }

        $content = str_replace('{className}', ucfirst($action).'Request', $content);

        file_put_contents($file, $content);
        exec('sudo '.self::fixAccessScript.' '.$file.' guiviko');
    }

    private function createController($controller)
    {
        $namespaces = explode('\\', $controller);
        $controllerName = ucfirst(array_pop($namespaces));

        $dirs = $this->currentProject->controllersRoot();
        foreach ($namespaces as $namespace)
        {
            $namespace = ucfirst($namespace);
            $dirs .= '/'.$namespace;

            if (!file_exists($dirs))
            {
                mkdir($dirs);
                exec('sudo '.self::fixAccessScript.' '.$dirs.' guiviko');
            }
        }

        $file = $dirs.'/'.$controllerName.'.php';
        if (file_exists($file))
        {
            return;
        }

        $content = file_get_contents(self::templateDir.'/server/controllers/basic.php');

        if (sizeof($namespaces) > 0)
        {
            $str = '';
            foreach ($namespaces as $namespace)
            {
                $str .= '\\'.ucfirst($namespace);
            }

            $content = str_replace('{namespaces}', $str, $content);
        }
        else
        {
            $content = str_replace('{namespaces}', '', $content);
        }

        $content = str_replace('{className}', $controllerName, $content);

        file_put_contents($file, $content);
        exec('sudo '.self::fixAccessScript.' '.$file.' guiviko');
    }

    private function addToController($controller, $action, $service)
    {
        $file = $this->currentProject->controllersRoot().'/'.str_replace('\\', '/', $controller).'.php';
        $content = file_get_contents($file);

        $serviceName = explode('\\', $service);
        $serviceName = ucfirst($serviceName[sizeof($serviceName) - 1]);
        $serviceName = substr($serviceName, 0, -4);

        $namespaces = explode('\\', $controller);
        $namespaces[sizeof($namespaces) - 1] = str_replace('Controller', '', $namespaces[sizeof($namespaces) - 1]);

        // check if already there
        if (strpos($content, 'function '.$action.'(') !== false)
        {
            return;
        }

        $offset = strrpos($content, '}') - 1;

        $str = '';
        if ($content[$offset - 1] == '}')
        {
            $str .= PHP_EOL.PHP_EOL;
        }
        $str .= '    public function '.$action.'('.ucfirst($action).'Request $request';
        if (!is_null($service) && $service != '' && $service != 'none')
        {
            $str .= ', '.$serviceName.' $service';
        }

        $str .= ')'.PHP_EOL;
        $str .= '    {'.PHP_EOL;
        $str .= '        return $service->'.$action.'();'.PHP_EOL;
        $str .= '    }';

        $content = substr($content, 0, $offset).$str.substr($content, $offset);

        // request import
        preg_match('/use\s+[a-zA-Z0-9\\\\]+'.ucfirst($action).'Request;/', $content, $matches);
        if (sizeof($matches) == 0)
        {
            preg_match_all('/use\s+[a-zA-Z0-9\\\\]+(;)/', $content, $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
            $matches = end($matches);
            $requestOffset = $matches[1][1] + 2;

            $requestStr = 'use App\\Http\\Requests\\'.implode('\\', $namespaces).'\\'.ucfirst($action).'Request;'.PHP_EOL;

            $content = substr($content, 0, $requestOffset).$requestStr.substr($content, $requestOffset);
        }

        // service import
        preg_match('/use\s+[a-zA-Z0-9\\\\]+'.$serviceName.';/', $content, $matches);
        if (sizeof($matches) == 0)
        {
            preg_match_all('/use\s+[a-zA-Z0-9\\\\]+(;)/', $content, $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
            $matches = end($matches);
            $serviceOffset = $matches[1][1] + 2;

            $serviceStr = 'use App\\Domain\\Services\\'.substr($service, 0, -4).';'.PHP_EOL;

            $content = substr($content, 0, $serviceOffset).$serviceStr.substr($content, $serviceOffset);
        }

        file_put_contents($file, $content);
    }

    public function addToService($action, $service)
    {
        $file = $this->currentProject->serverServicesRoot().'/'.str_replace('\\', '/', $service);
        $content = file_get_contents($file);

        // check if already there
        if (strpos($content, 'function '.$action.'(') !== false)
        {
            return;
        }

        $offset = strrpos($content, '}') - 1;

        $str = '';
        if ($content[$offset - 1] == '}')
        {
            $str .= PHP_EOL.PHP_EOL;
        }
        $str .= '    public function '.$action.'()'.PHP_EOL;
        $str .= '    {'.PHP_EOL;
        $str .= '        return new Response(Response::STATUS_OK);'.PHP_EOL;
        $str .= '    }';

        $content = substr($content, 0, $offset).$str.substr($content, $offset);

        // response import
        if (strpos($content, 'use Library\\Http\\Response;') === false)
        {
            preg_match_all('/use\s+[a-zA-Z0-9\\\\]+(;)/', $content, $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
            $matches = end($matches);
            $responseOffset = $matches[1][1] + 2;

            $requestStr = 'use Library\\Http\\Response;'.PHP_EOL;

            $content = substr($content, 0, $responseOffset).$requestStr.substr($content, $responseOffset);
        }

        file_put_contents($file, $content);
    }
}