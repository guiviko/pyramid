<?php

namespace App\Domain\Services\Server;

use App\Domain\Services\CurrentProjectService;
use Library\Http\Response;

class ServicesService extends CurrentProjectService
{
    public function fetch()
    {
        $services = $this->findServices($this->currentProject->serverServicesRoot());

        $abstract = [];
        $concrete = [];
        foreach ($services as $service)
        {
            if ($service['isAbstract'])
            {
                $abstract[] = $service;
                continue;
            }

            $concrete[] = $service;
        }

        return new Response(Response::STATUS_OK, [
            'abstract' => $abstract,
            'concrete' => $concrete
        ]);
    }

    public function create($name, $parent)
    {
        $namespaces = explode('\\', $name);
        $serviceName = ucfirst(array_pop($namespaces));

        $shouldExtend = !is_null($parent) && $parent != '' && $parent != 'undefined';

        $content = file_get_contents(self::templateDir.'/server/services/'.($shouldExtend ? 'extends.php' : 'basic.php'));

        if (sizeof($namespaces) > 0)
        {
            $str = '';
            foreach ($namespaces as $namespace)
            {
                $str .= '\\'.ucfirst($namespace);
            }

            $content = str_replace('{namespaces}', $str, $content);
        }
        else
        {
            $content = str_replace('{namespaces}', '', $content);
        }

        $content = str_replace('{className}', $serviceName, $content);

        if ($shouldExtend)
        {
            $parentNamespaces = explode('\\', $parent);
            $parentName = ucfirst(array_pop($parentNamespaces));
            $parentName = substr($parentName, 0, strlen($parentName) - 4);

            $str = '';
            foreach ($parentNamespaces as $namespace)
            {
                $str .= '\\'.ucfirst($namespace);
            }
            $str .= '\\'.$parentName;

            $content = str_replace('{parentNamespaces}', $str, $content);

            $content = str_replace('{parentName}', $parentName, $content);
        }

        $dirs = $this->currentProject->serverServicesRoot();
        foreach ($namespaces as $namespace)
        {
            $dirs .= '/'.$namespace;

            if (!file_exists($dirs))
            {
                mkdir($dirs);

                exec('sudo '.self::fixAccessScript.' '.$dirs.' guiviko');
            }
        }

        file_put_contents($dirs.'/'.$serviceName.'.php', $content);

        exec('sudo '.self::fixAccessScript.' '.$dirs.'/'.$serviceName.'.php'.' guiviko');

        return new Response(Response::STATUS_OK);
    }

    private function findServices($directory, $level = 0)
    {
        $services = [];

        $directoryParts = explode('/', $directory);
        $prefix = '';
        for ($i = sizeof($directoryParts) - $level; $i < sizeof($directoryParts); $i++)
        {
            $prefix .= $directoryParts[$i].'\\';
        }

        foreach (scandir($directory) as $file)
        {
            if ($file == '.' || $file == '..')
            {
                continue;
            }

            if (substr($file, -11) == 'Service.php')
            {
                $services[] = $this->processServiceFile($directory.'/'.$file, $prefix.$file);
                continue;
            }

            if (is_dir($directory.'/'.$file))
            {
                $services = array_merge($services, $this->findServices($directory.'/'.$file, $level + 1));
            }
        }

        return $services;
    }

    private function processServiceFile($file, $name)
    {
        $content = file_get_contents($file);
        $isAbstract = strpos($content, 'abstract class') !== false;
        $extends = '';
        if (strpos($content, 'extends ') !== false)
        {
            for ($i = strpos($content, 'extends ') + 8; $i < strlen($content); $i++)
            {
                if ($content[$i] == PHP_EOL)
                {
                    break;
                }

                $extends .= $content[$i];
            }
        }

        return [
            'name' => $name,
            'isAbstract' => $isAbstract,
            'extends' => $extends
        ];
    }
}