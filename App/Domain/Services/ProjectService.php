<?php

namespace App\Domain\Services;

use App\Domain\Models\Projects\Module;
use App\Domain\Models\Projects\Page;
use App\Domain\Models\Projects\Project;
use App\Domain\Models\Projects\Component;
use App\Domain\Models\Projects\ComponentTemplate;
use App\Domain\Utils\FileUtils;
use Library\Http\Response;

class ProjectService extends CurrentProjectService
{
    public function fetch()
    {
        return new Response(Response::STATUS_OK, $this->transformer->of(Project::class)
            ->transform($this->dm->findAll(Project::class)->toArray()));
    }

    public function create(string $name)
    {
        if (is_null($name) || strlen($name) == 0)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Project name is missing or invalid.');
        }

        $projectRoot = $this->paths['projectsRoot'].$name;
        if (file_exists($projectRoot))
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Project directory already exists.');
        }

        // copy backend files
        if (!FileUtils::copyRecursive($this->paths['backendTemplateRoot'], $projectRoot))
        {
            $this->removeProjectFiles($projectRoot);

            return new Response(Response::STATUS_BAD_REQUEST, 'Could not copy backend files.');
        }

        // setup apache
        exec('sudo '.$this->paths['setupApacheScript'].' '.$name.' '.$this->paths['apacheDefaultConfigFile'].' 2>&1');

        // run composer

        // copy frontend files
        if (!FileUtils::copyRecursive($this->paths['frontendTemplateRoot'], $projectRoot))
        {
            $this->removeProjectFiles($projectRoot);

            return new Response(Response::STATUS_BAD_REQUEST, 'Could not copy backend files.');
        }

        // add project to database
        try
        {
            $project = new Project($name);
            $this->dm->persist($project);
            $this->currentProject->setProject($project);

            $appModule = new Module($project, 'app', '', '', $this->currentProject->clientRoot());
            $this->dm->persist($appModule);

            $this->dm->flush();

            $template = $this->dm->find(ComponentTemplate::class, 1);
            $pageComponent = new Component($template, 'index.page', $this->currentProject->pagesRoot().'/index', 'm:app:'.$appModule->getId(),
                                           $template->emptyConfig(), $template->blockAreas(), $template->metadata());
            $this->dm->persist($pageComponent);
            $indexPage = new Page($appModule, $pageComponent, 'index', '', 'm:app:'.$appModule->getId(),
                $this->currentProject->pagesRoot().'/index');
            $this->dm->persist($indexPage);

            $appModule->pages()->add($indexPage);
            $project->modules()->add($appModule);

            $this->dm->flush();
        }
        catch (\Exception $e)
        {
            $this->removeProjectFiles($projectRoot);

            $this->removeApacheConfig($name);

            return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not save project.');
        }

        return new Response(Response::STATUS_OK);
    }

    public function destroy()
    {
        try
        {
            $this->removeProjectFiles($this->paths['projectsRoot'].$this->currentProject->project()->name());
            $this->dm->delete($this->currentProject->project());
            $this->removeApacheConfig($this->currentProject->project()->name());

            $this->dm->flush();
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not remove project.'.$e->getMessage());
        }

        return new Response(Response::STATUS_OK);
    }

    private function removeProjectFiles(string $path)
    {
        FileUtils::removeDirectoryRecursive($path);
    }

    private function removeApacheConfig(string $name)
    {
        exec('sudo '.$this->paths['removeApacheScript'].' '.$name.' 2>&1');
    }
}