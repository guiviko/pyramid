<?php

namespace App\Domain\Services;

use App\Domain\Project\CurrentProject;
use App\Domain\Project\Editors\Client\ClientProjectEditor;
use App\Domain\Project\Editors\Server\ServerProjectEditor;
use Library\DataMapper\DataMapper;
use Library\Events\EventManager;
use Library\Log\Log;
use Library\Transformer\Transformer;

abstract class CurrentProjectService extends Service
{
    /**
     * @var CurrentProject
     */
    protected $currentProject;

    /**
     * @var ClientProjectEditor
     */
    protected $clientEditor;

    /**
     * @var ServerProjectEditor
     */
    protected $serverEditor;

    /**
     * @var DataMapper
     */
    protected $dm;

    public function __construct(EventManager $eventManager, Transformer $transformer, DataMapper $dm, Log $log,
                                CurrentProject $currentProject, ClientProjectEditor $clientEditor,
                                ServerProjectEditor $serverEditor)
    {
        parent::__construct($eventManager, $transformer, $log);

        $this->currentProject = $currentProject;
        $this->clientEditor = $clientEditor;
        $this->serverEditor = $serverEditor;
        $this->dm = $dm;
    }
}