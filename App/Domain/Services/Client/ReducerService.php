<?php

namespace App\Domain\Services\Client;

use App\Domain\Processors\Client\ActionProcessor;
use App\Domain\Processors\Client\ReducerProcessor;
use App\Domain\Processors\Client\SelectorProcessor;
use App\Domain\Services\CurrentProjectService;
use App\Domain\Utils\FileUtils;
use Library\Events\EventManager;
use Library\Http\Response;
use Library\Log\Log;
use Library\Transformer\Transformer;

class ReducerService extends CurrentProjectService
{
    /**
     * @var ReducerProcessor
     */
    private $reducerProcessor;

    /**
     * @var SelectorProcessor
     */
    private $selectorProcessor;

    /**
     * @var ActionProcessor
     */
    private $actionProcessor;

    public function __construct(EventManager $eventManager, Transformer $transformer, Log $log)
    {
        parent::__construct($eventManager, $transformer, $log);

        $this->reducerProcessor = new ReducerProcessor($this->currentProject, $this->pyrConfig);
        $this->selectorProcessor = new SelectorProcessor($this->currentProject, $this->pyrConfig);
        $this->actionProcessor = new ActionProcessor($this->currentProject, $this->pyrConfig);
    }

    public function fetch()
    {
        $reducers = [];
        foreach (FileUtils::scanFilesRecursive($this->currentProject->reducerRoot(), 'reducer.ts') as $file)
        {
            $reducers[] = [
                'name' => $file
            ];
        }

        return new Response(Response::STATUS_OK, $reducers);
    }

    public function create(string $name)
    {
        $name = lcfirst($name);

        $this->reducerProcessor->addReducer($name);
        $this->reducerProcessor->addToIndex($name);
        $this->selectorProcessor->addSelector($name);
        $this->selectorProcessor->addToIndex($name);
        $this->actionProcessor->addActionFile($name);
        $this->actionProcessor->addToIndex($name);

        return new Response(Response::STATUS_OK);
    }
}