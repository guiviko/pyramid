<?php

namespace App\Domain\Services\Client;

use App\Domain\Models\Projects\Module;
use App\Domain\Services\CurrentProjectService;
use Library\Http\Response;

class ModuleService extends CurrentProjectService
{
    public function fetch()
    {
        try
        {
            $modules = $this->buildModules($this->currentProject->project()->appModule(), $this->currentProject->project()->modules());

            return new Response(Response::STATUS_OK, $modules);
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not retrieve modules.'.$e->getMessage());
        }
    }

    private function buildModules($root, $modules, $level = 1, $uriPrefix = '')
    {
        $fullUri = $uriPrefix.($root->uri() == null ? '' : $root->uri());
        $pages = [];
        foreach ($root->pages() as $page)
        {
            $pages[] = [
                'id' => $page->getId(),
                'name' => $page->name(),
                'uriPrefix' => $fullUri,
                'uri' => $page->uri(),
                'dotChain' => $page->dotChain(),
                'component' => [
                    'id' => $page->component()->getId(),
                ]
            ];
        }

        $data = [
            [
                'id' => $root->getId(),
                'name' => $root->name(),
                'uriPrefix' => $uriPrefix,
                'uri' => $root->uri(),
                'dotChain' => $root->dotChain(),
                'pages' => $pages,
                'level' => $level,
                'isLeaf' => false,
                'isOpen' => true
            ]
        ];

        foreach ($modules as $module)
        {
            $parts = explode('#', $module->dotChain());
            if (sizeof($parts) != $level || $parts[$level - 1] != 'm:'.$root->name().':'.$root->getId())
            {
                continue;
            }

            $data = array_merge($data, $this->buildModules($module, $modules, $level + 1, $root->uri()));
        }

        if (sizeof($data) == 1)
        {
            $data[0]['isLeaf'] = true;
        }

        return $data;
    }

    public function create(string $name, $parentId, string $uri)
    {
        $name = lcfirst($name);

        try
        {
            $parentModule = $this->dm->find(Module::class, $parentId);
            if (is_null($parentModule))
            {
                return new Response(Response::STATUS_BAD_REQUEST, 'Could not find parent module.');
            }

            $directoryPath = $parentModule->isRoot()
                ? $this->currentProject->moduleRoot().'/'.$name
                : $parentModule->directoryPath().'/'.$name;

            $dotChain = $parentModule->dotChain() == '' ? '' : $parentModule->dotChain().'#';
            $dotChain .= 'm:'.$parentModule->name().':'.$parentModule->getId();
            $module = new Module($this->currentProject->project(), $name, $uri, $dotChain, $directoryPath);
            $this->dm->persist($module);

            $this->clientEditor->module()->create($module, $parentModule);

            $this->dm->flush();

            return new Response(Response::STATUS_OK, ['id' => $module->getId()]);
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not create module.');
        }
    }

    public function destroy($moduleId)
    {
        try
        {
            $module = $this->dm->find(Module::class, $moduleId);
            if (is_null($module))
            {
                return new Response(Response::STATUS_BAD_REQUEST, 'Could not find module');
            }

            $dots = explode('#', $module->dotChain());
            $parentId = explode(':', $dots[sizeof($dots) - 1])[2];
            $parent = $this->dm->find(Module::class, $parentId);
            if (is_null($parent))
            {
                return new Response(Response::STATUS_BAD_REQUEST, 'Could not find parent module.');
            }

            $this->dm->delete($module);

            foreach ($module->pages() as $page)
            {
                $this->clientEditor->page()->destroy($page);
            }

            $this->clientEditor->module()->destroy($module, $parent);

            $this->dm->flush();

            return new Response(Response::STATUS_OK);
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not remove module');
        }
    }
}