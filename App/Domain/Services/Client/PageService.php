<?php

namespace App\Domain\Services\Client;

use App\Domain\Models\Projects\Component;
use App\Domain\Models\Projects\ComponentTemplate;
use App\Domain\Models\Projects\Module;
use App\Domain\Models\Projects\Page;
use App\Domain\Services\CurrentProjectService;
use Library\Http\Response;
use Exception;

class PageService extends CurrentProjectService
{
    public function fetch($pageId)
    {
        try
        {
            $page = $this->dm->find(Page::class, $pageId);
            if (is_null($page))
            {
                return new Response(Response::STATUS_BAD_REQUEST, 'Could not find page.');
            }

            return new Response(Response::STATUS_OK, [
                'id' => $page->getId(),
                'name' => $page->name(),
                'uri' => $page->uri(),
                'dotChain' => $page->dotChain(),
                'content' => [
                    'nextId' => 3,
                    1 => [
                        'id' => 1,
                        'isRoot' => true,
                        'parentId' => null,
                        'blocks' => [2],
                        'flexBasis' => 100,
                        'width' => 100,
                        'height' => 100,
                        'flexDirection' => 'row'
                    ],
                    2 => [
                        'id' => 2,
                        'isRoot' => false,
                        'parentId' => 1,
                        'blocks' => [],
                        'flexBasis' => 100,
                        'width' => 100,
                        'height' => 100,
                        'flexDirection' => 'row'
                    ]
                ]
            ]);
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not find page.');
        }
    }

    public function create($parentId, string $name, string $uri)
    {
        try
        {
            $parentModule = $this->dm->find(Module::class, $parentId);
            if (is_null($parentModule))
            {
                return new Response(Response::STATUS_BAD_REQUEST, 'Could not find parent module.');
            }

            $directoryPath = $this->currentProject->pagesRoot().'/';
            $directoryPath .= $parentModule->isRoot()
                ? $name
                : substr($parentModule->directoryPath(), strpos($parentModule->directoryPath(), '/modules/') + 9).'/'.$name;
            $dotChain = $parentModule->dotChain() == '' ? '' : $parentModule->dotChain().'#';
            $dotChain .= 'm:'.$parentModule->name().':'.$parentModule->getId();

            $template = $this->dm->findOneBy(ComponentTemplate::class, [['name', '=', 'empty']]);
            if (is_null($template))
            {
                return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Component template does not exist.');
            }
            $component = new Component($template, $name.'.page', $directoryPath, $dotChain, $template->emptyConfig(),
                $template->blockAreas(), $template->metadata());
            $this->dm->persist($component);

            $page = new Page($parentModule, $component, $name, $uri, $dotChain, $directoryPath);
            $this->dm->persist($page);

            $this->clientEditor->page()->create($page);

            $this->dm->flush();

            return new Response(Response::STATUS_OK, [
                'id' => $page->getId(),
                'name' => $page->name(),
                'uriPrefix' => '',
                'uri' => $page->uri(),
                'dotChain' => $page->dotChain(),
                'component' => [
                    'id' => $page->component()->getId(),
                ]
            ]);
        }
        catch (Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not create page.');
        }
    }

    public function destroy($pageId)
    {
        try
        {
            $page = $this->dm->find(Page::class, $pageId);
            if (is_null($page))
            {
                return new Response(Response::STATUS_BAD_REQUEST, 'Could not find page.');
            }

            $this->dm->delete($page);

            $this->clientEditor->page()->destroy($page);

            $this->dm->flush();

            return new Response(Response::STATUS_OK);
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not remove page.');
        }
    }
}