<?php

namespace App\Domain\Services\Client;

use App\Domain\Models\Projects\Action;
use App\Domain\Models\Projects\ActionGroup;
use App\Domain\Models\Projects\StateField;
use App\Domain\Project\CurrentProject;
use App\Domain\Project\Editors\Client\ClientProjectEditor;
use App\Domain\Project\Editors\Server\ServerProjectEditor;
use App\Domain\Services\ProjectService;
use Library\DataMapper\DataMapper;
use Library\Events\EventManager;
use Library\Http\Response;
use Library\Log\Log;
use Library\Transformer\Transformer;

class ActionService extends ProjectService
{
    public function fetch()
    {
        try
        {
            $groups = $this->currentProject->project()->getActionGroups()->toArray();
            $res = [];
            foreach ($groups as $group)
            {
                $actions = [];
                foreach ($group->getActions() as $action)
                {
                    $actions[] = [
                        'id' => $action->getId(),
                        'name' => $action->getName(),
                        'map' => $action->getMap()
                    ];
                }

                $fields = [];
                foreach ($group->getStateFields() as $field)
                {
                    $fields[] = [
                        'id' => $field->getId(),
                        'name' => $field->getName(),
                        'type' => $field->getType()
                    ];
                }

                $res[] = [
                    'id' => $group->getId(),
                    'name' => $group->getName(),
                    'actions' => $actions,
                    'stateFields' => $fields
                ];
            }
            return new Response(Response::STATUS_OK, $res);
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, $e->getMessage());
        }
    }

    public function createGroup($data)
    {
        if ($data['name'] == '')
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Group name cannot be empty.');
        }

        $group = new ActionGroup($data['name'], $this->currentProject->project());
        $this->dm->persist($group);
        $this->currentProject->project()->getActionGroups()->add($group);

        try
        {
            $this->dm->flush();

            $this->clientEditor->action()->addActionFile($data['name']);
            $this->clientEditor->reducer()->addReducerFile($data['name']);
            $this->clientEditor->effect()->addEffectFile($data['name']);
            $this->clientEditor->service()->addServiceFile($data['name']);

            return new Response(Response::STATUS_OK);
        }
        catch (\Exception $e)
        {
            if (!is_null($group->getId()))
            {
                $this->dm->delete($group);
                $this->dm->flush();
            }

            return new Response(Response::STATUS_BAD_REQUEST, $e->getMessage());
        }
    }

    public function createAction($data)
    {
        try
        {
            $group = $this->dm->find(ActionGroup::class, $data['groupId']);
            if (is_null($group))
            {
                return new Response(Response::STATUS_BAD_REQUEST, 'Could not find group.');
            }

            if ($data['name'] == '')
            {
                return new Response(Response::STATUS_BAD_REQUEST, 'Action name cannot be empty.');
            }

            $action = new Action($data['name'], $group);
            $action->setMap([
                'nodes' => [
                    [
                        'id' => 1,
                        'title' => 'action',
                        'friendlyName' => 'Action',
                        'posX' => 50,
                        'posY' => 200,
                        'noHeaderInSlot' => true,
                        'slots' => [
                            ['direction' => 'out', 'name' => 'dispatch']
                        ]
                    ]
                ],
                'lastNodeId' => 1
            ]);
            $this->dm->persist($action);
            $group->getActions()->add($action);

            $this->clientEditor->action()->addAction($group->getName(), $action->getName());

            $this->dm->flush();

            return new Response(Response::STATUS_OK);
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, $e->getMessage());
        }
    }

    public function createStateField($data)
    {
        try
        {
            $group = $this->dm->find(ActionGroup::class, $data['groupId']);
            if (is_null($group))
            {
                return new Response(Response::STATUS_BAD_REQUEST, 'Could not find group.');
            }

            if ($data['name'] == '')
            {
                return new Response(Response::STATUS_BAD_REQUEST, 'Field name cannot be empty.');
            }

            $stateField = new StateField($data['name'], $data['type'], $group);
            $this->dm->persist($stateField);
            $group->getStateFields()->add($stateField);

            $this->addField($data, $group);

            $this->dm->flush();

            return new Response(Response::STATUS_OK);
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, $e->getMessage());
        }
    }

    private function addField($data, $group)
    {
        switch ($data['type'])
        {
            case 'list':
                $this->clientEditor->reducer()->addField($group->getName(), $data['name'], 'any', '{}');
                $this->clientEditor->reducer()->addField($group->getName(), $data['name'].'Ids', 'any', '[]');
                break;
        }
    }
}