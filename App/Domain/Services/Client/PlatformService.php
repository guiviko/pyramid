<?php

namespace App\Domain\Services\Client;

use App\Domain\Models\Projects\Action;
use App\Domain\Models\Projects\Component;
use App\Domain\Models\Projects\ComponentTemplate;
use App\Domain\Models\Projects\ControlTemplate;
use App\Domain\Models\Projects\Module;
use App\Domain\Processors\Client\BlockAreaProcessor;
use App\Domain\Services\CurrentProjectService;
use App\Domain\Utils\FileUtils;
use App\Domain\Utils\StringUtils;
use Library\Http\Response;

class PlatformService extends CurrentProjectService
{
    public function fetch($componentId)
    {
        try
        {
            $component = $this->dm->find(Component::class, $componentId);
            if (is_null($component))
            {
                return new Response(Response::STATUS_BAD_REQUEST, 'Could not find component.');
            }

            $allActions = $this->getAllActions();
            $allReducers = $this->getAllReducers();
            $allSelectors = $this->getAllSelectors();
            $outputs = $this->getComponentOutputs($component, $allActions, $allReducers);

            return new Response(Response::STATUS_OK, [
                'id' => $component->getId(),
                'name' => $component->name(),
                'templateName' => $component->template()->name(),
                'dotChain' => $component->dotChain(),
                'config' => $component->config(),
                'blockAreas' => $component->blockAreas(),
                'metadata' => $component->metadata(),
                'outputs' => $outputs,
                'allActions' => $allActions,
                'allReducers' => $allReducers,
                'allSelectors' => $allSelectors
            ]);
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not find component.'.$e->getMessage());
        }
    }

    private function getComponentOutputs(Component $component, array $allActions, array &$allReducers)
    {
        $content = file_get_contents($component->directoryPath().'/'.$component->name().'.ts');

        preg_match_all('/@Output\(\)\s+(\w+)/', $content, $matches, PREG_OFFSET_CAPTURE);

        $outputs = [];
        $counter = 0;
        foreach ($matches[1] as $match)
        {
            $counter++;
            $prefix = '??';
            foreach ($allActions as $actionGroup => $actions)
            {
                foreach ($actions as $action)
                {
                    if ($action == $match[0])
                    {
                        $prefix = $actionGroup;
                        break 2;
                    }
                }
            }

            $connections = [];
            foreach ($allReducers as &$reducerField)
            {
                foreach ($reducerField['actions'] as $action)
                {
                    if ($action['prefix'] == $prefix && $action['suffix'] == $match[0])
                    {
                        $connections[] = [
                            'from' => 'o-'.$counter,
                            'to' => $reducerField['id']
                        ];

                        $reducerField['isConnected'] = true;

                        break;
                    }
                }
            }

            $outputs[] = [
                'id' => 'o-'.$counter,
                'name' => $prefix.'::'.$match[0],
                'prefix' => $prefix,
                'suffix' => $match[0],
                'connections' => $connections
            ];
        }

        return $outputs;
    }

    private function getAllReducers()
    {
        $allReducers = [];

        $counter = 0;
        foreach (FileUtils::scanFiles($this->currentProject->reducerRoot(), '.reducer.ts') as $reducerFile)
        {
            $counter++;
            $reducerGroup = substr($reducerFile, 0, strpos($reducerFile, '.reducer.ts'));

            $content = FileUtils::readFile($this->currentProject->reducerRoot().'/'.$reducerFile);
            $functionRegex = '/const\s([a-zA-Z0-9\-_]+)\s=\s\(.*{.*};/Us';
            $actionRegex = '/case\s([a-zA-Z0-9\-_]+\.[a-zA-Z0-9\-_]+):/';

            preg_match_all($functionRegex, $content, $matches);
            for ($i = 0; $i < sizeof($matches[0]); $i++)
            {
                preg_match_all($actionRegex, $matches[0][$i], $actionMatches);
                $actions = [];
                foreach ($actionMatches[1] as $actionMatch)
                {
                    $parts = explode('.', $actionMatch);
                    $actions[] = [
                        'prefix' => lcfirst(substr($parts[0], 0, strpos($parts[0], 'Actions'))),
                        'suffix' => StringUtils::constantToCamelCase($parts[1])
                    ];
                }

                $allReducers[] = [
                    'id' => 'r-'.$counter.$i,
                    'name' => $reducerGroup.'::'.$matches[1][$i],
                    'prefix' => $reducerGroup,
                    'suffix' => $matches[1][$i],
                    'actions' => $actions,
                    'isConnected' => false
                ];
            }
        }

        return $allReducers;
    }

    private function getAllSelectors()
    {
        $allSelectors = [];

        $counter = 0;
        foreach (FileUtils::scanFiles($this->currentProject->selectorRoot(), '.selectors.ts') as $selectorFile)
        {
            $counter++;
            $selectorGroup = substr($selectorFile, 0, strpos($selectorFile, '.selectors.ts'));
            $content = FileUtils::readFile($this->currentProject->selectorRoot().'/'.$selectorFile);

            preg_match_all('/(get\w+)\(\)\s{/', $content, $matches);
            for($i = 0; $i < sizeof($matches[0]); $i++)
            {
                $allSelectors[] = [
                    'id' => 's-'.$counter.$i,
                    'prefix' => $selectorGroup,
                    'suffix' => $matches[1][$i]
                ];
            }
        }

        return $allSelectors;
    }

    private function getAllActions()
    {
        $allActions = [];

        foreach (FileUtils::scanFiles($this->currentProject->actionRoot(), 'actions.ts') as $actionFile)
        {
            $actionGroup = substr($actionFile, 0, strpos($actionFile, '.actions.ts'));
            preg_match_all('/([0-9a-zA-Z_\-]+)\(.*\):\s?Action\s?{/', FileUtils::readFile($this->currentProject->actionRoot().'/'.$actionFile), $matches);
            foreach ($matches[1] as $action)
            {
                $allActions[$actionGroup][] = $action;
            }
        }

        return $allActions;
    }

    public function fetchTemplates()
    {
        try
        {
            return new Response(Response::STATUS_OK, $this->transformer
                ->of(ComponentTemplate::class)
                ->transform($this->dm->findAll(ComponentTemplate::class)->toArray()));
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not retrieve templates.');
        }
    }

    public function fetchFormTemplates()
    {
        try
        {
            return new Response(Response::STATUS_OK, $this->transformer
                ->of(ControlTemplate::class)
                ->transform($this->dm->findAll(ControlTemplate::class)->toArray()));
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not retrieve templates.');
        }
    }

    public function fetchReducers()
    {
        $files = scandir($this->currentProject->reducerRoot());
        $reducers = [];
        foreach ($files as $file)
        {
            if ($file == '.' || $file == '..' || $file == 'index.ts')
            {
                continue;
            }

            $parts = explode('.', $file);
            if (sizeof($parts) != 3)
            {
                continue;
            }

            $reducers[] = $this->parseReducer($file);
        }

        return new Response(Response::STATUS_OK, $reducers);
    }

    private function parseReducer($file)
    {
        $content = FileUtils::readFile($this->currentProject->reducerRoot().'/'.$file);

        return [
            'name' => explode('.', $file)[0],
            'fields' => $this->parseReducerFields($content)
        ];
    }

    private function parseReducerFields($content)
    {
        preg_match('/interface\s\w+State\s{[\r\n]+(.*)}/sU', $content, $matches);
        if (sizeof($matches) == 0)
        {
            return [];
        }

        $fieldStrings = explode(PHP_EOL, $matches[1]);
        $fields = [];
        foreach ($fieldStrings as $fieldString)
        {
            if (trim($fieldString) == '')
            {
                continue;
            }

            $fields[] = $this->parseReducerField($content, $fieldString);
        }

        return $fields;
    }

    private function parseReducerField($content, $fieldString)
    {
        $parts = explode(':', $fieldString);
        $name = trim($parts[0]);
        $type = trim(str_replace(';', '', $parts[1]));
        
        preg_match('/const\s'.$name.'\s=\s.*=>\s{\s+switch\s\(action.type\)\s{\s(.*\s+default:.*})/sU', $content, $matches);

        return [
            'name' => $name,
            'type' => $type,
            'actions' => $this->parseReducerFieldActions($matches[1])
        ];
    }

    private function parseReducerFieldActions($caseContent)
    {
        preg_match_all('/case\s(\w+)Actions.(\w+):/sU', $caseContent, $matches, PREG_SET_ORDER);
        if (sizeof($matches) == 0)
        {
            return [];
        }

        $actions = [];

        for ($i = 0; $i < sizeof($matches); $i++)
        {
            $actions[] = $this->parseReducerFieldAction($caseContent, trim($matches[$i][1]), trim($matches[$i][2]));
        }

        return $actions;
    }

    private function parseReducerFieldAction($caseContent, $actionGroup, $actionName)
    {
        preg_match('/case\s'.$actionGroup.'Actions\.'.$actionName.':\s+(.*)\s+(?:case|default)/sU', $caseContent, $matches);

        $lines = explode(PHP_EOL, $matches[1]);
        foreach ($lines as &$line)
        {
            $substrCount = 0;
            $max = strlen($line) < 12 ? strlen($line) : 12;
            for ($i = 0; $i < $max; $i++)
            {
                if ($line[$i] == ' ')
                {
                    $substrCount++;

                    continue;
                }

                break;
            }

            if ($substrCount > 0)
            {
                $line = substr($line, $substrCount);
            }
        }

        return [
            'actionGroup' => lcfirst($actionGroup),
            'actionName' => StringUtils::constantToCamelCase($actionName),
            'content' => implode(PHP_EOL, $lines)
        ];
    }

    public function addComponent($parentComponentId, $blockAreaId, $blockId, $templateId, $name)
    {
        try
        {
            return $this->addComponentStep1($parentComponentId, $blockAreaId, $blockId, $templateId, $name);
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not update block.'.$e->getMessage());
        }
    }

    private function addComponentStep1($parentComponentId, $blockAreaId, $blockId, $templateId, $name)
    {
        $parentComponent = $this->dm->find(Component::class, $parentComponentId);
        if (is_null($parentComponent))
        {
            return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not find component.');
        }

        $template = $this->dm->find(ComponentTemplate::class, $templateId);
        if (is_null($template))
        {
            return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not find template.');
        }

        return $this->addComponentStep2($parentComponent, $blockAreaId, $blockId, $template, $name);
    }

    // create the component in the database
    private function addComponentStep2($parentComponent, $blockAreaId, $blockId, $template, $name)
    {
        $config = $template->emptyConfig();
        $config['groups'][0]['rows'][0]['fields'][0]['value'] = $name;
        $childComponent = new Component($template, $name,
                                        $this->currentProject->componentRoot().'/'.$name,
                                        $parentComponent->dotChain().'#c:'.$parentComponent->name().':'.$parentComponent->getId(),
                                        $config, $template->blockAreas(), $template->metadata());
        $this->dm->persist($childComponent);

        $this->dm->flush();

        return $this->addComponentStep3($parentComponent, $childComponent, $blockAreaId, $blockId, $template, $name);
    }


    // find the block area index and set the component in the block
    private function addComponentStep3($parentComponent, $childComponent, $blockAreaId, $blockId, $template, $name)
    {
        $blockAreas = $parentComponent->blockAreas();
        $blockAreaIndex = -1;
        for ($i = 0; $i < sizeof($blockAreas); $i++)
        {
            if ($blockAreas[$i]['id'] == $blockAreaId)
            {
                $blockAreaIndex = $i;
                break;
            }
        }
        if ($blockAreaIndex == -1)
        {
            return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not find block area.');
        }

        $blockAreas[$blockAreaIndex]['blockMap'][$blockId]['content']['component'] = [
            'templateId' => $template->getId(),
            'id' => $childComponent->getId(),
            'name' => $name,
            'icon' => $template->icon()
        ];
        $parentComponent->setBlockAreas($blockAreas);

        return $this->addComponentStep4($parentComponent, $childComponent, $blockAreaId, $blockAreaIndex, $blockId, $template, $name);
    }

    // Adding child component to the file tree
    private function addComponentStep4($parentComponent, $childComponent, $blockAreaId, $blockAreaIndex, $blockId, $template, $name)
    {
        // Finding the leaf module
        $leafModuleId = 0;
        $parts = explode('#', $childComponent->dotChain());
        for ($i = sizeof($parts) - 1; $i >= 0; $i--)
        {
            $subParts = explode(':', $parts[$i]);
            if ($subParts[0] == 'm')
            {
                $leafModuleId = $subParts[2];
                break;
            }
        }
        if ($leafModuleId == 0)
        {
            $this->dm->delete($childComponent);
            $this->dm->flush();
            return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not find module of new component.');
        }

        $leafModule = $this->dm->find(Module::class, $leafModuleId);
        if (is_null($leafModule))
        {
            $this->dm->delete($childComponent);
            $this->dm->flush();
            return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not find module of new component.');
        }

        foreach (scandir($this->currentProject->componentRoot()) as $file)
        {
            if ($file == $childComponent->name())
            {
                $this->dm->delete($childComponent);
                $this->dm->flush();
                return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Component already exists.');
            }
        }

        $this->clientEditor->component()->create($childComponent, $leafModule);

        return $this->addComponentStep5($parentComponent, $childComponent, $blockAreaId, $blockAreaIndex, $blockId, $template);
    }

    private function addComponentStep5($parentComponent, $childComponent, $blockAreaId, $blockAreaIndex, $blockId, $template)
    {
        $filePath = $parentComponent->directoryPath().'/'.$parentComponent->name().'.html';
        $html = FileUtils::readFile($filePath);
        $html = $this->clientEditor->document()->addComponent($childComponent, $html, $blockAreaId, $blockId);
        FileUtils::writeFile($filePath, $html);

        $this->dm->flush();

        return new Response(Response::STATUS_OK, $parentComponent->blockAreas()[$blockAreaIndex]);
    }

    public function splitVertically($parentComponentId, $blockAreaId, $blockId, $value)
    {
        try
        {
            $parentComponent = $this->dm->find(Component::class, $parentComponentId);
            if (is_null($parentComponent))
            {
                return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not find component.');
            }

            $blockAreas = $parentComponent->blockAreas();
            $blockAreaIndex = -1;
            for ($i = 0; $i < sizeof($blockAreas); $i++)
            {
                if ($blockAreas[$i]['id'] == $blockAreaId)
                {
                    $blockAreaIndex = $i;
                    break;
                }
            }
            if ($blockAreaIndex == -1)
            {
                return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not find block area.');
            }

            $blockAreaProcessor = new BlockAreaProcessor($blockAreas[$blockAreaIndex]);
            $blockAreaProcessor->splitVertically($blockId, $value);

            $blockAreas[$blockAreaIndex] = $blockAreaProcessor->getBlockArea();
            $parentComponent->setBlockAreas($blockAreas);

            //$this->clientEditor->document()->update($parentComponent);

            $this->dm->flush();

            return new Response(Response::STATUS_OK, $blockAreaProcessor->getBlockArea());
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not split vertically.');
        }
    }

    public function splitHorizontally($parentComponentId, $blockAreaId, $blockId, $value)
    {
        try
        {
            $parentComponent = $this->dm->find(Component::class, $parentComponentId);
            if (is_null($parentComponent))
            {
                return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not find component.');
            }

            $blockAreas = $parentComponent->blockAreas();
            $blockAreaIndex = -1;
            for ($i = 0; $i < sizeof($blockAreas); $i++)
            {
                if ($blockAreas[$i]['id'] == $blockAreaId)
                {
                    $blockAreaIndex = $i;
                    break;
                }
            }
            if ($blockAreaIndex == -1)
            {
                return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not find block area.');
            }

            $blockAreaProcessor = new BlockAreaProcessor($blockAreas[$blockAreaIndex]);
            $blockAreaProcessor->splitHorizontally($blockId, $value);

            $blockAreas[$blockAreaIndex] = $blockAreaProcessor->getBlockArea();
            $parentComponent->setBlockAreas($blockAreas);

            $this->dm->flush();

            return new Response(Response::STATUS_OK, $blockAreaProcessor->getBlockArea());
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not split horizontally.');
        }
    }

    public function dropControlInForm($componentId, $controlId)
    {
        try
        {
            $component = $this->dm->find(Component::class, $componentId);
            if (is_null($component))
            {
                return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not find component.');
            }

            $template = $this->dm->find(ControlTemplate::class, $controlId);
            if (is_null($template))
            {
                return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not find control template.');
            }

            $labelTemplate = $this->dm->find(ControlTemplate::class, 1);
            if (is_null($labelTemplate))
            {
                return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not find label template.');
            }

            $metadata = $component->metadata();

            $metadata['columns'][0]['rows'][] = [
                'controls' => [
                    [
                        'id' => 1,
                        'width' => '30',
                        'widthUnit' => '%',
                        'config' => $labelTemplate->config()
                    ],
                    [
                        'id' => $controlId,
                        'width' => '30',
                        'widthUnit' => '%',
                        'config' => $template->config()
                    ]
                ]
            ];

            $component->setMetadata($metadata);

            $this->dm->flush();

            return new Response(Response::STATUS_OK, $component->metadata());
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not drop control in form.');
        }
    }

    public function dropControlInRow($componentId, $controlId, $rowIndex)
    {
        try
        {
            $component = $this->dm->find(Component::class, $componentId);
            if (is_null($component))
            {
                return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not find component.');
            }

            $template = $this->dm->find(ControlTemplate::class, $controlId);
            if (is_null($template))
            {
                return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not find control template.');
            }

            $metadata = $component->metadata();
            if ($rowIndex + 1 > sizeof($metadata['columns'][0]['rows']))
            {
                return new Response(Response::STATUS_BAD_REQUEST, 'Row with index '.$rowIndex.' does not exist.');
            }

            $metadata['columns'][0]['rows'][$rowIndex]['controls'][] = [
                'id' => $controlId,
                'config' => $template->config()
            ];

            $component->setMetadata($metadata);

            $this->dm->flush();

            return new Response(Response::STATUS_OK, $component->metadata());
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not drop control in form.');
        }
    }

    public function dropExistingControlInForm($componentId, $rowIndex, $controlIndex)
    {
        try
        {
            $component = $this->dm->find(Component::class, $componentId);
            if (is_null($component))
            {
                return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not find component.');
            }

            $labelTemplate = $this->dm->find(ControlTemplate::class, 1);
            if (is_null($labelTemplate))
            {
                return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not find label template.');
            }

            $metadata = $component->metadata();
            if ($rowIndex + 1 > sizeof($metadata['columns'][0]['rows']))
            {
                return new Response(Response::STATUS_BAD_REQUEST, 'Row with index '.$rowIndex.' does not exist.');
            }

            if (sizeof($metadata['columns'][0]['rows'][$rowIndex]['controls']) == 1)
            {
                return new Response(Response::STATUS_OK, $component->metadata());
            }

            $existingControl = $metadata['columns'][0]['rows'][$rowIndex]['controls'][$controlIndex];
            $metadata['columns'][0]['rows'][] = [
                'controls' => [
                    [
                        'id' => 1,
                        'config' => $labelTemplate->config()
                    ],
                    $existingControl
                ]
            ];
            array_splice($metadata['columns'][0]['rows'][$rowIndex]['controls'], $controlIndex, 1);

            $component->setMetadata($metadata);

            $this->dm->flush();

            return new Response(Response::STATUS_OK, $component->metadata());
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not drop control in form.');
        }
    }

    public function deleteControl($componentId, $rowIndex, $controlIndex)
    {
        try
        {
            $component = $this->dm->find(Component::class, $componentId);
            if (is_null($component))
            {
                return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not find component.');
            }

            $metadata = $component->metadata();
            if ($rowIndex + 1 > sizeof($metadata['columns'][0]['rows']))
            {
                return new Response(Response::STATUS_BAD_REQUEST, 'Row with index '.$rowIndex.' does not exist.');
            }

            if ($controlIndex + 1 > sizeof($metadata['columns'][0]['rows'][$rowIndex]['controls']))
            {
                return new Response(Response::STATUS_BAD_REQUEST, 'Control with index '.$controlIndex.' does not exist.');
            }

            if (sizeof($metadata['columns'][0]['rows'][$rowIndex]['controls']) == 1)
            {
                array_splice($metadata['columns'][0]['rows'], $rowIndex, 1);
            }
            else
            {
                array_splice($metadata['columns'][0]['rows'][$rowIndex]['controls'], $controlIndex, 1);
            }

            $component->setMetadata($metadata);

            $this->dm->flush();

            return new Response(Response::STATUS_OK, $component->metadata());
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not delete control.');
        }
    }

    public function reorderFormRows($componentId, $firstRowIndex, $secondRowIndex)
    {
        try
        {
            $component = $this->dm->find(Component::class, $componentId);
            if (is_null($component))
            {
                return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not find component.');
            }

            $metadata = $component->metadata();

            $rows = &$metadata['columns'][0]['rows'];
            if (sizeof($rows) < $secondRowIndex + 1)
            {
                return new Response(Response::STATUS_BAD_REQUEST, 'Row index out of bounds.');
            }

            $temp = $rows[$firstRowIndex];
            $rows[$firstRowIndex] = $rows[$secondRowIndex];
            $rows[$secondRowIndex] = $temp;

            $component->setMetadata($metadata);

            $this->dm->flush();

            return new Response(Response::STATUS_OK, $component->metadata());
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not drop control in form.');
        }
    }

    public function updateControlConfig($componentId, $controlRowIndex, $controlIndex, $config)
    {
        try
        {
            $component = $this->dm->find(Component::class, $componentId);
            if (is_null($component))
            {
                return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not find component.');
            }

            $metadata = $component->metadata();

            $rows = &$metadata['columns'][0]['rows'];
            $rows[$controlRowIndex]['controls'][$controlIndex]['config'] = $config;

            $component->setMetadata($metadata);

            $this->dm->flush();

            return new Response(Response::STATUS_OK, [
                'controlRowIndex' => $controlRowIndex,
                'controlIndex' => $controlIndex,
                'config' => $config
            ]);
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not update control config.');
        }
    }

    public function updateComponentConfig($componentId, $groupIndex, $rowIndex, $fieldIndex, $value)
    {
        try
        {
            $component = $this->dm->find(Component::class, $componentId);
            if (is_null($component))
            {
                return new Response(Response::STATUS_INTERNAL_SERVER_ERROR, 'Could not find component.');
            }

            $config = $component->config();
            $field = &$config['groups'][$groupIndex]['rows'][$rowIndex]['fields'][$fieldIndex];
            $field['value'] = $value;
            $component->setConfig($config);

            $this->clientEditor->template()->editor($component->template()->name())
                ->process($component, $groupIndex, $rowIndex, $fieldIndex, $value);

            $this->dm->flush();

            return new Response(Response::STATUS_OK, [
                'groupIndex' => $groupIndex,
                'rowIndex' => $rowIndex,
                'fieldIndex' => $fieldIndex,
                'value' => $value
            ]);
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not update component config.'.$e->getMessage());
        }
    }

    public function addOutput($componentId, string $name, string $actionName)
    {
        try
        {
            $component = $this->dm->find(Component::class, $componentId);
            if (is_null($component))
            {
                return new Response(Response::STATUS_BAD_REQUEST, 'Could not find component.');
            }

            $this->clientEditor->action()->addActionFile($name);
            $this->clientEditor->action()->addAction($name, $actionName);
            $this->addOutputChainRecursive($name, $actionName, $component, '');

            return new Response(Response::STATUS_OK);
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not add output. '.$e->getMessage());
        }
    }

    private function addOutputChainRecursive(string $name, string $actionName, Component $component, string $childSelector)
    {
        $dotChainParts = explode('#', $component->dotChain());
        $dotChainPart = $dotChainParts[sizeof($dotChainParts) - 1];
        $parentDotParts = explode(':', $dotChainPart);

        if ($childSelector != '')
        {
            if ($parentDotParts[0] == 'm')
            {
                $this->clientEditor->component()->addOutputPageHtml($component, $childSelector, $actionName);
            }
            else
            {
                $this->clientEditor->component()->addOutputChainHtml($component, $childSelector, $actionName);
            }
        }

        if ($parentDotParts[0] == 'm')
        {
            $this->clientEditor->page()->addDispatch($component, $name, $actionName);

            return;
        }
        else
        {
            $this->clientEditor->component()->addOutputTs($component, $actionName);
        }

        $parent = $this->dm->find(Component::class, $parentDotParts[2]);
        if (is_null($parent))
        {
            throw new \Exception('Could not find parent component.');
        }

        return $this->addOutputChainRecursive($name, $actionName, $parent, StringUtils::camelCaseToDash($component->name()));
    }

    public function connectActionToReducer(string $componentId, string $actionGroup, string $actionName, string $reducerGroup, string $reducerField)
    {
        try
        {
            $this->clientEditor->reducer()->addActionToField($actionGroup, $actionName, $reducerGroup, $reducerField);

            return new Response(Response::STATUS_OK);
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not connect action to reducer.'.$e->getMessage());
        }
    }

    public function addReducerField(string $componentId, string $reducerGroup, string $reducerField, string $type, string $default)
    {
        try
        {
            $this->clientEditor->reducer()->addField($reducerGroup, $reducerField, $type, $default);

            return new Response(Response::STATUS_OK);
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not connect action to reducer.'.$e->getMessage());
        }
    }

    public function saveReducerContent(string $reducerName, string $fieldName, string $actionGroup, string $actionName, string $content)
    {
        try
        {
            $this->clientEditor->reducer()->saveContent($reducerName, $fieldName, $actionGroup, $actionName, $content);

            return new Response(Response::STATUS_OK);
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not save reducer content.'.$e->getMessage());
        }
    }

    public function disconnectOutputFromReducer(string $reducerGroup, string $reducerField, string $actionGroup, string $actionName)
    {
        try
        {
            $this->clientEditor->reducer()->disconnectOutput($reducerGroup, $reducerField, $actionGroup, $actionName);

            return new Response(Response::STATUS_OK);
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not save reducer content.'.$e->getMessage());
        }
    }

    public function addInput($componentId, string $group, string $inputName) 
    {
        try
        {
            $component = $this->dm->find(Component::class, $componentId);
            if (is_null($component))
            {
                return new Response(Response::STATUS_BAD_REQUEST, 'Could not find component.');
            }

            $this->addInputChainRecursive($group, $inputName, $component, '');

            return new Response(Response::STATUS_OK);
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not add input.'.$e->getMessage());
        }
    }

    private function addInputChainRecursive(string $group, string $inputName, Component $component, string $childSelector)
    {
        $dotChainParts = explode('#', $component->dotChain());
        $dotChainPart = $dotChainParts[sizeof($dotChainParts) - 1];
        $parentDotParts = explode(':', $dotChainPart);

        if ($childSelector != '')
        {
            if ($parentDotParts[0] == 'm')
            {
                $this->clientEditor->component()->addInputPageHtml($component, $childSelector, $inputName);
            }
            else
            {
                $this->clientEditor->component()->addInputChainHtml($component, $childSelector, $inputName);
            }
        }

        if ($parentDotParts[0] == 'm')
        {
            $this->clientEditor->page()->addSelection($component, $group, $inputName);

            return;
        }
        else
        {
            $this->clientEditor->component()->addInputTs($component, $inputName);
        }

        $parent = $this->dm->find(Component::class, $parentDotParts[2]);
        if (is_null($parent))
        {
            throw new \Exception('Could not find parent component.');
        }

        return $this->addInputChainRecursive($group, $inputName, $parent, StringUtils::camelCaseToDash($component->name()));
    }

    public function map($actionId)
    {
        try
        {
            $action = $this->dm->find(Action::class, $actionId);
            if (is_null($action))
            {
                return new Response(Response::STATUS_BAD_REQUEST, 'Could not find action.');
            }

            return new Response(Response::STATUS_OK, ['groupId' => $action->getActionGroup()->getId(), 'actionId' => $actionId, 'map' => $action->getMap()]);
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not process update request.'.$e->getMessage());
        }
    }

    public function update($data, $actionId)
    {
        try
        {
            $action = $this->dm->find(Action::class, $actionId);
            if (is_null($action))
            {
                return new Response(Response::STATUS_BAD_REQUEST, 'Could not find action.');
            }

            $map = $action->getMap();

            foreach ($data['updates'] as $update)
            {
                switch ($update['action'])
                {
                    case 'createNode':
                        $nextId = isset($map['lastNodeId']) ? $map['lastNodeId'] + 1 : 1;
                        $map['nodes'][] = $this->createNode($update['nodeInfo'], $nextId, $action);
                        $map['lastNodeId'] = $nextId;
                        break;
                    case 'updateNode':
                        $node = $this->getNodeById($map, $update['data']['id']);
                        if (is_null($node))
                        {
                            return new Response(Response::STATUS_BAD_REQUEST, 'Invalid node id.');
                        }
                        $node['posX'] = $update['data']['posX'];
                        $node['posY'] = $update['data']['posY'];
                        $map = $this->replaceNode($map, $node);
                        break;
                    case 'connectNodes':
                        if (!isset($update['data']['to']))
                        {
                            $update['data']['to'] = $this->findConnectionTarget($update['data']['from'], $map['nodes'][sizeof($map['nodes']) - 1]);
                        }
                        $fromNode = $this->getNodeById($map, $update['data']['from']['nodeId']);
                        $fromNode['slots'][$update['data']['from']['slotId']]['connections'][] = $update['data'];
                        $map = $this->replaceNode($map, $fromNode);
                        $toNode = $this->getNodeById($map, $update['data']['to']['nodeId']);
                        if ($update['data']['to']['slotId'] != -1)
                        {
                            $toNode['slots'][$update['data']['to']['slotId']]['connections'][] = $update['data'];
                            $map = $this->replaceNode($map, $toNode);
                        }
                        else
                        {
                            $toNode['headerConnections'][] = $update['data'];
                            $map = $this->replaceNode($map, $toNode);
                        }
                        $this->connectNodes($fromNode, $toNode, $update['data'], $action);
                        break;
                    case 'setRequestPayload':
                        $this->setRequestPayload($update['data'], $action);
                        break;
                }
            }

            $action->setMap($map);
            $this->dm->flush();

            return new Response(Response::STATUS_OK);
        }
        catch (\Exception $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not process update request.'.$e->getMessage());
        }
    }

    private function getNodeById($map, $id)
    {
        foreach ($map['nodes'] as $node)
        {
            if ($node['id'] == $id)
            {
                return $node;
            }
        }

        return null;
    }

    private function replaceNode($map, $newNode)
    {
        for ($i = 0; $i < sizeof($map['nodes']); $i++)
        {
            if ($map['nodes'][$i]['id'] == $newNode['id'])
            {
                $map['nodes'][$i] =  $newNode;
                return $map;
            }
        }

        return $map;
    }

    private function createNode(array $nodeInfo, $id, $action): array
    {
        $node = [
            'id' => $id,
            'title' => $nodeInfo['name'],
            'posX' => $nodeInfo['posX'],
            'posY' => $nodeInfo['posY'],
            'slots' => []
        ];

        switch ($nodeInfo['name'])
        {
            case 'fetchRequest':
                $node['friendlyName'] = 'Fetch request';
                $node['slots'][] = ['direction' => 'out', 'name' => 'success'];
                $node['slots'][] = ['direction' => 'out', 'name' => 'failure'];
                $node['canConfigure'] = true;
                $typeData = $this->searchType($action->getName());
                $this->clientEditor->service()->addEngineFetch($action->getActionGroup()->getName(), $action->getName(),
                    $typeData['type'], $typeData['fields'], []);
                $this->clientEditor->effect()->addRequest($action->getActionGroup()->getName(), $action->getName());
                return $node;
            case 'updateRequest':
                $node['friendlyName'] = 'Update request';
                $node['slots'][] = ['direction' => 'out', 'name' => 'success'];
                $node['slots'][] = ['direction' => 'out', 'name' => 'failure'];
                $node['canConfigure'] = true;
                return $node;
            case 'deleteRequest':
                $node['friendlyName'] = 'Delete request';
                $node['slots'][] = ['direction' => 'out', 'name' => 'success'];
                $node['slots'][] = ['direction' => 'out', 'name' => 'failure'];
                $node['canConfigure'] = true;
                return $node;
            case 'createRequest':
                $node['slots'][] = ['direction' => 'out', 'name' => 'success'];
                $node['slots'][] = ['direction' => 'out', 'name' => 'failure'];
                $node['friendlyName'] = 'Create request';
                $node['canConfigure'] = true;
                return $node;
            case 'addToList':
                $node['friendlyName'] = 'Add to list';
                $node['slots'][] = ['direction' => 'in', 'name' => 'list'];
                return $node;
        }

        if (substr($nodeInfo['name'], 0, 3))
        {
            $node['noHeaderInSlot'] = true;
            $node['friendlyName'] = substr($nodeInfo['name'], 3);
            $node['slots'][] = ['direction' => 'out', 'name' => 'get'];
            return $node;
        }
    }

    private function findConnectionTarget($from, $toNode)
    {
        $to = ['nodeId' => $toNode['id']];

        switch ($toNode['title'])
        {
            case 'fetchRequest':
            case 'createRequest':
            case 'updateRequest':
            case 'deleteRequest':
            case 'addToList':
                $to['slotId'] = -1;
                return $to;
        }

        // state variable
        if (substr($toNode['title'], 0, 3) == 'get')
        {
            $to['slotId'] = 0;
            return $to;
        }

        throw new \Exception('findConnectionTarget: '.$toNode['name'].' node not implemented.');
    }

    private function searchType(string $str)
    {
        //$schema = require './../../../../Config/Schema/schema.php';

        return [
            'type' => 'todo',
            'fields' => [
                'id', 'name'
            ]
        ];
    }

    private function connectNodes($fromNode, $toNode, $connection, $action)
    {
        if (substr($toNode['title'], 0, 3) == 'get')
        {
            $temp = $toNode;
            $toNode = $fromNode;
            $fromNode = $temp;
        }

        if (substr($fromNode['title'], 0, 3) == 'get' && $toNode['title'] == 'addToList')
        {
            $field = substr($fromNode['title'], 3);
            $this->clientEditor->reducer()->addActionToField($action->getActionGroup()->getName(),
                $action->getName(), $action->getActionGroup()->getName(), $field);
            $this->clientEditor->reducer()->addActionToField($action->getActionGroup()->getName(),
                $action->getName(), $action->getActionGroup()->getName(), $field.'Ids');
        }
    }

    private function setRequestPayload($data, $action)
    {
        $this->clientEditor->service()->replaceEngineData($action->getActionGroup()->getName(), $action->getName(),
            $data, 'fetch');
    }
}
