<?php

namespace App\Domain\Generators;

class ModelGenerator
{
    const MODEL_NAMESPACE = 'App\Domain\Models';

    public function generate(string $entityName, array $fields)
    {
        $str = '<?php'.PHP_EOL.PHP_EOL;
        $str .= 'namespace '.self::MODEL_NAMESPACE.';'.PHP_EOL.PHP_EOL;
        $str .= 'use Library\DataMapper\DataMapperPrimaryKey;'.PHP_EOL;
        $str .= 'use Library\DataMapper\DataMapperTimestamps;'.PHP_EOL.PHP_EOL;
        $str .= '/** @Entity */'.PHP_EOL;
        $str .= 'class '.ucfirst($entityName).PHP_EOL;
        $str .= '{'.PHP_EOL;
        $str .= '    use DataMapperPrimaryKey, DataMapperTimestamps;'.PHP_EOL;
        $str .= '}'.PHP_EOL;

        return $str;
    }
}