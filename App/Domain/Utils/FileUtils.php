<?php

namespace App\Domain\Utils;

class FileUtils
{
    public static function scanFiles(string $root, string $extension = null)
    {
        if (!file_exists($root))
        {
            throw new \Exception('Directory ',$root.' does not exist.');
        }

        $files = [];
        foreach (scandir($root) as $file)
        {
            if ($file == '.' || $file == '..' || (!is_null($extension) && !self::endsWith($file, $extension)))
            {
                continue;
            }

            $files[] = $file;
        }

        return $files;
    }

    public static function scanFilesRecursive(string $root, string $extension = null)
    {
        if (!file_exists($root))
        {
            throw new \Exception('Directory '.$root.' does not exist.');
        }

        return self::scanSubDirectories($root, $extension, '');
    }

    private static function scanSubDirectories($root, $extension, $prefix)
    {
        $files = [];
        foreach (scandir($root) as $file)
        {
            if ($file == '.' || $file == '..')
            {
                continue;
            }

            $fileWithPrefix = $prefix == '' ? $file : $prefix.'/'.$file;
            $fullPath = $root.'/'.$file;

            if (is_dir($fullPath))
            {
                $files = array_merge($files, self::scanSubDirectories($fullPath, $extension, $fileWithPrefix));
            }

            if (!is_null($extension) && !self::endsWith($file, $extension))
            {
                continue;
            }

            $files[] = $fileWithPrefix;
        }

        return $files;
    }

    public static function endsWith(string $haystack, string $needle)
    {
        return substr($haystack, -strlen($needle)) == $needle;
    }

    public static function createDirectoryRecursive(string $path)
    {
        $parts = explode('/', $path);
        $directory = '';
        foreach ($parts as $part)
        {
            $directory .= '/'.$part;
            if (file_exists($directory))
            {
                continue;
            }

            mkdir($directory);
        }
    }

    public static function removeDirectory(string $path)
    {
        if (!file_exists($path))
        {
            return false;
        }

        return rmdir($path);
    }

    public static function removeDirectoryRecursive(string $path)
    {
        if (!file_exists($path))
        {
            return false;
        }

        foreach (array_diff(scandir($path), array('.','..')) as $file)
        {
            is_dir($path.'/'.$file) ? self::removeDirectoryRecursive($path.'/'.$file) : unlink($path.'/'.$file);
        }

        return rmdir($path);
    }

    public static function isDirectoryEmpty(string $path)
    {
        if (!file_exists($path) || !is_dir($path) || sizeof(scandir($path)) > 2)
        {
            return false;
        }

        return true;
    }

    public static function fileExists($file)
    {
        return file_exists($file);
    }

    public static function readFile($file)
    {
        return file_get_contents($file);
    }

    public static function writeFile($file, $content)
    {
        $isNew = false;
        if (!file_exists($file))
        {
            $isNew = true;
        }

        file_put_contents($file, $content);

        if ($isNew)
        {
            chmod($file, 0775);
        }
    }

    public static function insertContent(string $content, int $offset, string $toInsert)
    {
        return substr($content, 0, $offset).$toInsert.substr($content, $offset);
    }

    public static function deleteFile(string $file)
    {
        unlink($file);
    }

    public static function copyRecursive(string $source, string $destination)
    {
        if (is_file($source))
        {
            return copy($source, $destination);
        }

        if (!is_dir($destination))
        {
            mkdir($destination);
        }

        $dir = dir($source);
        while (false !== $entry = $dir->read())
        {
            if ($entry == '.' || $entry == '..')
            {
                continue;
            }

            self::copyRecursive($source.'/'.$entry, $destination.'/'.$entry);
        }

        $dir->close();

        return true;
    }
}