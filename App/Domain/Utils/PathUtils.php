<?php

namespace App\Domain\Utils;

class PathUtils
{
    public const pyramidRoot = '/var/www/html/pyramid';
    public const templateRoot = self::pyramidRoot.'/Storage/pyramid/templates';
    public const moduleTemplateRoot = self::templateRoot.'/modules';
    public const pageTemplateRoot = self::templateRoot.'/pages';
    public const componentTemplateRoot = self::templateRoot.'/components';
    public const actionTemplateRoot = self::templateRoot.'/actions';
    public const reducerTemplateRoot = self::templateRoot.'/reducers';
    public const effectTemplateRoot = self::templateRoot.'/effects';
    public const serviceTemplateRoot = self::templateRoot.'/service';
    public const selectorTemplateRoot = self::templateRoot.'/selectors';

    public const configModelRoot = self::pyramidRoot.'/Storage/pyramid/ConfigModels';
    public const componentConfigModelRoot = self::configModelRoot.'/components';
}