<?php

namespace App\Domain\Processors;

class FileIO
{
    private const scriptDir = '/var/www/html/pyramid/Storage/Scripts';
    private const fixAccessScript = self::scriptDir.'/fix-access';

    public function createDirectoryRecursive(string $path)
    {
        $parts = explode('/', $path);
        $directory = '';
        foreach ($parts as $part)
        {
            $directory .= '/'.$part;
            if (file_exists($directory))
            {
                continue;
            }

            mkdir($directory);
            $this->fixAccess($directory);
        }
    }

    public function createDirectory(string $path)
    {
        if (file_exists($path))
        {
            return false;
        }

        mkdir($path);
        $this->fixAccess($path);

        return true;
    }

    public function writeFile($file, $content)
    {
        file_put_contents($file, $content);
        $this->fixAccess($file);
    }

    private function fixAccess(string $path)
    {
        exec('sudo '.self::fixAccessScript.' '.$path.' guiviko');
    }
}