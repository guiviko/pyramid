<?php

namespace App\Domain\Processors\Client;

use App\Domain\Utils\FileUtils;
use App\Domain\Utils\StringUtils;

class HtmlProcessor
{
    public function addOutputChain($childComponentSelector, $actionName, $content)
    {
        if (strpos($content, $childComponentSelector) === false)
        {
            return $content;
        }

        $str = '('.$actionName.')="'.$actionName.'.emit($event)"';

        return $this->addOutput($childComponentSelector, $str, $content);
    }

    public function addOutputPage($childComponentSelector, $actionName, $content)
    {
        if (strpos($content, $childComponentSelector) === false)
        {
            return $content;
        }

        $str = '('.$actionName.')="'.$actionName.'($event)"';

        return $this->addOutput($childComponentSelector, $str, $content);
    }

    public function addInputPage($childComponentSelector, $inputName, $content)
    {
        if (strpos($content, $childComponentSelector) === false)
        {
            return $content;
        }

        $str = '['.$inputName.']="'.$inputName.' | async"';

        return $this->addOutput($childComponentSelector, $str, $content);
    }

    public function addInputChain($childComponentSelector, $inputName, $content)
    {
        if (strpos($content, $childComponentSelector) === false)
        {
            return $content;
        }

        $str = '['.$inputName.']="'.$inputName.'"';

        return $this->addOutput($childComponentSelector, $str, $content);
    }

    private function addOutput($childComponentSelector, $str, $content)
    {
        preg_match('/<'.$childComponentSelector.'\s*(>)/s', $content, $matches, PREG_OFFSET_CAPTURE);
        if (sizeof($matches) > 0)
        {
            if (strpos($matches[0][0], $str) !== false)
            {
                return $content;
            }
            return $this->addOutputSingleLine($content, $str, $matches[1][1]);
        }

        preg_match('/<'.$childComponentSelector.'[^\/]*(>)/sU', $content, $matches, PREG_OFFSET_CAPTURE);
        if (sizeof($matches) > 0)
        {
            if (strpos($matches[0][0], $str) !== false)
            {
                return $content;
            }

            preg_match('/<'.$childComponentSelector.'\s*(.)/sU', $matches[0][0], $spaceMatches, PREG_OFFSET_CAPTURE);
            $spaceOffset = $spaceMatches[1][1];

            return $this->addOutputMultiLine($content, $str, $matches[1][1], $spaceOffset);
        }

        return $content;
    }

    private function addOutputSingleLine($content, $str, $offset)
    {
        if ($content[$offset - 1] != ' ')
        {
            $str = ' '.$str;
        }

        return FileUtils::insertContent($content, $offset, $str);
    }

    private function addOutputMultiLine($content, $str, $offset, $spaceOffset)
    {
        $str = PHP_EOL.str_repeat(' ', $spaceOffset).$str;

        return FileUtils::insertContent($content, $offset, $str);
    }

    public function setBlockContent($dataKey, $value, $content)
    {
        preg_match('/(<)[^<>]*data-'.$dataKey.'.*>/sU', $content, $matches, PREG_OFFSET_CAPTURE);
        if (sizeof($matches) == 0)
        {
            return $content;
        }

        $value = str_repeat(' ', $this->countSpaces($matches[1][1] - 1, $content)).StringUtils::tab(1).$value;
        return preg_replace('/(<[^<>]*data-'.$dataKey.'.*>\s+).*(\s+<\/)/sU', '$1'.$value.'$2', $content);
    }

    private function countSpaces($offset, $content)
    {
        $count = 0;

        for ($i = $offset; $i >= 0; $i--)
        {
            if ($content[$i] != ' ')
            {
                return $count;
            }

            $count++;
        }

        return $count;
    }

    public function replaceBlockClass($dataKey, $previous, $current, $content)
    {
        return preg_replace('/(<[^<>]*class=".*)'.$previous.'(.*"[^<>]*data-'.$dataKey.'.*>)/su', '$1'.$current.'$2', $content);
    }
}