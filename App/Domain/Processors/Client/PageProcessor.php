<?php

namespace App\Domain\Processors\Client;

use App\Domain\Processors\FileIO;
use App\Domain\Project\CurrentProject;
use App\Domain\Utils\StringUtils;

class PageProcessor
{
    /**
     * @var CurrentProject
     */
    private $currentProject;

    /**
     * @var array
     */
    private $pyrConfig;

    /**
     * @var ComponentProcessor
     */
    private $componentProcessor;

    public function __construct(CurrentProject $currentProject, array $pyrConfig, ComponentProcessor $componentProcessor)
    {
        $this->currentProject = $currentProject;
        $this->pyrConfig = $pyrConfig;
        $this->componentProcessor = $componentProcessor;
    }

    public function createPage($name, $page)
    {
        $name = lcfirst($name);
        $dashName = StringUtils::camelCaseToDash($name);
        $pageRoot = $this->currentProject->pagesRoot().'/'.lcfirst($name);

        $fileIO = new FileIO();
        $fileIO->createDirectoryRecursive($pageRoot);

        $templateDir = $this->pyrConfig['templateRoot'].'/pages/'.$page;

        $htmlContent = file_get_contents($templateDir.'/html.html');
        $htmlContent = str_replace('name0', $dashName, $htmlContent);
        $fileIO->writeFile($pageRoot.'/'.$name.'.page.html', $htmlContent);

        $scssContent = file_get_contents($templateDir.'/scss.scss');
        $scssContent = str_replace('name0', $dashName, $scssContent);
        $fileIO->writeFile($pageRoot.'/'.$name.'.page.scss', $scssContent);

        $tsContent = file_get_contents($templateDir.'/ts.ts');
        $tsContent = str_replace('selectorName0', $dashName, $tsContent);
        $tsContent = str_replace('htmlName0', './'.$name.'.page.html', $tsContent);
        $tsContent = str_replace('styleName0', './'.$name.'.page.css', $tsContent);
        $tsContent = str_replace('className0', ucfirst($name), $tsContent);
        $fileIO->writeFile($pageRoot.'/'.$name.'.page.ts', $tsContent);
    }

    public function addPageToModule()
    {

    }

    public function addPageToRoutes()
    {

    }
}