<?php

namespace App\Domain\Processors\Client;

use App\Domain\Processors\ProjectProcessor;
use App\Domain\Utils\FileUtils;

class SelectorProcessor extends ProjectProcessor
{
    public function addSelector(string $name)
    {
        if (file_exists($this->currentProject->selectorRoot().'/'.$this->getFileName($name)))
        {
            return;
        }

        $content = file_get_contents($this->pyrConfig['templateRoot'].'/selectors/basic.ts');
        $content = str_replace('className0', $this->getSelectorName($name), $content);

        FileUtils::writeFile($this->currentProject->selectorRoot().'/'.$this->getFileName($name), $content);
    }

    public function addToIndex(string $name)
    {
        $indexFile = $this->currentProject->selectorRoot().'/index.ts';
        $content = file_get_contents($indexFile);

        $importFrom = './'.substr($this->getFileName($name), 0, -3);
        $content = $this->tsProcessor->addImport($this->getSelectorName($name), $importFrom, $content);

        preg_match('/export default (\[)/', $content, $matches, PREG_OFFSET_CAPTURE);
        $offset = $matches[1][1] + 2;
        if (strpos(substr($content, $offset), $this->getSelectorName($name)) === false)
        {
            $content = substr($content, 0, $offset).'    '.$this->getSelectorName($name).','.PHP_EOL.substr($content, $offset);
        }

        FileUtils::writeFile($indexFile, $content);
    }

    private function getFileName(string $name)
    {
        return $name.'.selectors.ts';
    }

    private function getSelectorName(string $name)
    {
        return ucfirst($name).'Selectors';
    }
}