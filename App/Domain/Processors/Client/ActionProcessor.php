<?php

namespace App\Domain\Processors\Client;

use App\Domain\Processors\ProjectProcessor;
use App\Domain\Utils\FileUtils;

class ActionProcessor extends ProjectProcessor
{
    public function addActionFile(string $name)
    {
        if (file_exists($this->currentProject->actionRoot().'/'.$this->getFileName($name)))
        {
            return;
        }

        $content = file_get_contents($this->pyrConfig['templateRoot'].'/actions/basic.ts');
        $content = str_replace('className0', $this->getActionName($name), $content);

        FileUtils::writeFile($this->currentProject->actionRoot().'/'.$this->getFileName($name), $content);
    }

    public function addToIndex(string $name)
    {
        $indexFile = $this->currentProject->actionRoot().'/index.ts';
        $content = file_get_contents($indexFile);

        $importFrom = './'.substr($this->getFileName($name), 0, -3);
        $content = $this->tsProcessor->addImport($this->getActionName($name), $importFrom, $content);

        preg_match('/export ({)/', $content, $matches, PREG_OFFSET_CAPTURE);
        $offset = $matches[1][1] + 2;
        if (strpos(substr($content, $offset), $this->getActionName($name)) === false)
        {
            $content = substr($content, 0, $offset).'    '.$this->getActionName($name).','.PHP_EOL.substr($content, $offset);

            preg_match('/export default (\[)/', $content, $matches, PREG_OFFSET_CAPTURE);
            $offset = $matches[1][1] + 2;
            $content = substr($content, 0, $offset).'    '.$this->getActionName($name).','.PHP_EOL.substr($content, $offset);
        }

        FileUtils::writeFile($indexFile, $content);
    }

    public function getFileName(string $name)
    {
        return $name.'.actions.ts';
    }

    public function getActionName(string $name)
    {
        return ucfirst($name).'Actions';
    }
}