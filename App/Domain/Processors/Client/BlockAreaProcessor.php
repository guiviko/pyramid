<?php

namespace App\Domain\Processors\Client;

class BlockAreaProcessor
{
    /**
     * @var array
     */
    private $blockArea;

    /**
     * @var array
     */
    private $blockMap;

    public function __construct(array $blockArea)
    {
        $this->blockArea = $blockArea;
        $this->blockMap = &$this->blockArea['blockMap'];
    }

    public function getBlockArea()
    {
        return $this->blockArea;
    }

    public function splitVertically(int $blockId, int $value)
    {
        $left = $this->createBlock($blockId, $value, 100);
        $this->blockMap[$left['id']] = $left;

        $right = $this->createBlock($blockId, 100 - $value, 100);
        $this->blockMap[$right['id']] = $right;

        $separator = $this->createSeparator($blockId, 'vertical');
        $this->blockMap[$separator['id']] = $separator;

        $this->blockMap[$blockId]['blocks'] = [$left['id'], $separator['id'], $right['id']];
        $this->blockMap[$blockId]['flexDirection'] = 'row';

        if (!is_null($this->blockMap[$blockId]['content']['component']))
        {
            $this->blockMap[$left['id']]['content']['component'] = $this->blockMap[$blockId]['content']['component'];
            $this->blockMap[$blockId]['content']['component'] = null;
        }
    }

    public function splitHorizontally(int $blockId, int $value)
    {
        $top = $this->createBlock($blockId, 100, $value, 'column');
        $this->blockMap[$top['id']] = $top;

        $bottom = $this->createBlock($blockId, 100, 100 - $value, 'column');
        $this->blockMap[$bottom['id']] = $bottom;

        $separator = $this->createSeparator($blockId, 'horizontal');
        $this->blockMap[$separator['id']] = $separator;

        $this->blockMap[$blockId]['blocks'] = [$top['id'], $separator['id'], $bottom['id']];
        $this->blockMap[$blockId]['flexDirection'] = 'column';

        if (!is_null($this->blockMap[$blockId]['content']['component']))
        {
            $this->blockMap[$top['id']]['content']['component'] = $this->blockMap[$blockId]['content']['component'];
            $this->blockMap[$blockId]['content']['component'] = null;
        }
    }

    private function getNextId()
    {
        $this->blockMap['nextId'] += 1;
        return $this->blockMap['nextId'] - 1;
    }

    private function createBlock(int $parentId, int $width, int $height, string $direction = 'row')
    {
        return $this->createDefault([
            'id' => $this->getNextId(),
            'parentId' => $parentId,
            'flexBasis' => $direction == 'row' ? $width : $height,
            'width' => $width,
            'height' => $height,
            'flexDirection' => $direction
        ]);
    }

    private function createSeparator(int $parentId, string $direction)
    {
        return $this->createDefault([
            'id' => $this->getNextId(),
            'parentId' => $parentId,
            'type' => 'separator',
            'separatorDirection' => $direction
        ]);
    }

    private function createDefault(array $overrides = [])
    {
        return [
            'id' => array_key_exists('id', $overrides) ? $overrides['id'] : 0,
            'isRoot' => array_key_exists('isRoot', $overrides) ? $overrides['isRoot'] : false,
            'parentId' => array_key_exists('parentId', $overrides) ? $overrides['parentId'] : 0,
            'blocks' => array_key_exists('blocks', $overrides) ? $overrides['blocks'] : [],
            'flexBasis' => array_key_exists('flexBasis', $overrides) ? $overrides['flexBasis'] : 0,
            'width' => array_key_exists('width', $overrides) ? $overrides['width'] : 0,
            'height' => array_key_exists('height', $overrides) ? $overrides['height'] : 0,
            'flexDirection' => array_key_exists('flexDirection', $overrides) ? $overrides['flexDirection'] : 'row',
            'type' => array_key_exists('type', $overrides) ? $overrides['type'] : 'block',
            'separatorDirection' => array_key_exists('separatorDirection', $overrides) ? $overrides['separatorDirection'] : 'row',
            'isSelected' => array_key_exists('isSelected', $overrides) ? $overrides['isSelected'] : false,
            'content' => array_key_exists('content', $overrides) ? $overrides['content'] : ['component' => null],
            'marginTop' => array_key_exists('marginTop', $overrides) ? $overrides['marginTop'] : 0,
            'marginBottom' => array_key_exists('marginBottom', $overrides) ? $overrides['marginBottom'] : 0,
            'marginLeft' => array_key_exists('marginLeft', $overrides) ? $overrides['marginLeft'] : 0,
            'marginRight' => array_key_exists('marginRight', $overrides) ? $overrides['marginRight'] : 0
        ];
    }
}