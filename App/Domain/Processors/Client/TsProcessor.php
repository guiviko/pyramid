<?php

namespace App\Domain\Processors\Client;

use App\Domain\Utils\FileUtils;
use App\Domain\Utils\StringUtils;

class TsProcessor
{
    public function addImport(string $item, string $from, string $content): string
    {
        $regexFrom = str_replace('.', '\\.', $from);
        $regexFrom = str_replace('/', '\\/', $regexFrom);
        $regexFrom = str_replace('-', '\\-', $regexFrom);

        preg_match('/import.*'.$item.'/', $content, $matches);
        if (sizeof($matches) > 0)
        {
            return $content;
        }

        preg_match('/import.*(})\sfrom \''.$regexFrom.'\'/', $content, $matches, PREG_OFFSET_CAPTURE);
        if (sizeof($matches) > 0)
        {
            $offset = $matches[1][1];
            $str = ($content[$offset - 1] == '{' ? '' : ', ').$item;
            return substr($content, 0, $offset).$str.substr($content, $offset);
        }

        $str = 'import {'.$item.'} from \''.$from.'\';'.PHP_EOL;

        preg_match_all('/import.*from.*;(\n)/', $content, $matches, PREG_OFFSET_CAPTURE | PREG_SET_ORDER);
        if (sizeof($matches) == 0)
        {
            return $str.$content;
        }

        $matches = end($matches);
        $offset = sizeof($matches) == 0 ? 0 : $matches[1][1] + 1;

        return substr($content, 0, $offset).$str.substr($content, $offset);
    }

    public function addComponentRoute(string $filePath, string $uri, string $componentName, $importFrom, bool $isModuleRoot)
    {
        $content = FileUtils::readFile($filePath);
        if (strpos($content, 'component: '.$componentName) !== false)
        {
            return;
        }

        $content = $this->addImport($componentName, $importFrom, $content);

        $offset = strpos($content, '\'**\'');
        if ($offset !== false)
        {
            $offset -= 7;
        }
        else
        {
            preg_match('/Routes\s=\s\[.*(\])}/sU', $content, $matches, PREG_OFFSET_CAPTURE);
            $offset = $matches[1][1];
        }

        $str = $isModuleRoot ? '' : '    ';
        $str .= '{path: \''.$uri.'\', component: '.$componentName.'},'.PHP_EOL;
        $str .= $isModuleRoot ? '    ' : '    ';
        $content = FileUtils::insertContent($content, $offset, $str);
        FileUtils::writeFile($filePath, $content);
    }

    public function removeComponentRoute(string $filePath, string $componentName)
    {
        if (!FileUtils::fileExists($filePath))
        {
            return;
        }

        $content = FileUtils::readFile($filePath);
        $content = StringUtils::removeLinesContainingString($content, 'import {'.$componentName.'}');
        $content = StringUtils::removeLinesContainingString($content, 'component: '.$componentName);
        FileUtils::writeFile($filePath, $content);
    }

    public function addDeclaration(string $filePath, string $componentName, string $importFrom, bool $isModuleRoot)
    {
        $content = FileUtils::readFile($filePath);
        if (strpos($content, $componentName) !== false)
        {
            return;
        }

        $content = $this->addImport($componentName, $importFrom, $content);

        $regex = $isModuleRoot
            ? '/export const APP_DECLARATIONS = (\[).*\];/s'
            : '/declarations: (\[).*\]/s';
        preg_match($regex, $content, $matches, PREG_OFFSET_CAPTURE);
        if (sizeof($matches) == 0)
        {
            return;
        }
        $offset = $matches[1][1] + 1;

        $str = PHP_EOL;
        $str .= $isModuleRoot ? '    ' : '        ';
        $str .= $componentName.',';
        $content = FileUtils::insertContent($content, $offset, $str);

        FileUtils::writeFile($filePath, $content);
    }

    public function removeDeclaration(string $filePath, string $componentName)
    {
        if (!FileUtils::fileExists($filePath))
        {
            return;
        }

        $content = FileUtils::readFile($filePath);
        $content = StringUtils::removeLinesContainingString($content, 'import {'.$componentName.'}');
        $content = StringUtils::removeLinesContainingString($content, $componentName);
        FileUtils::writeFile($filePath, $content);
    }

    public function addOutput(string $name, string $content)
    {
        preg_match('/@Output\(\)\s+'.$name.'/', $content, $matches);
        if (sizeof($matches) > 0)
        {
            return $content;
        }

        $str = '    @Output() '.$name.' = new EventEmitter<any>();'.PHP_EOL;
        $offset = -1;

        preg_match('/@Output\(\).*(;)/', $content, $matches, PREG_OFFSET_CAPTURE);
        if (sizeof($matches) > 0)
        {
            $offset = $matches[1][1] + 2;
            return FileUtils::insertContent($content, $offset, $str);
        }

        preg_match('/@Input\(\).*(;)/', $content, $matches, PREG_OFFSET_CAPTURE);
        if (sizeof($matches) > 0)
        {
            $offset = $matches[1][1] + 2;
            return FileUtils::insertContent($content, $offset, $str);
        }

        preg_match('/export\sclass\s.*\s({)/', $content, $matches, PREG_OFFSET_CAPTURE);
        if (sizeof($matches) > 0)
        {
            $offset = $matches[1][1] + 2;
            return FileUtils::insertContent($content, $offset, $str);
        }
        
        return $content;
    }

    public function addInput(string $name, string $content)
    {
        preg_match('/@Input\(\)\s+'.$name.'/', $content, $matches);
        if (sizeof($matches) > 0)
        {
            return $content;
        }

        $str = '    @Input() '.$name.': any;'.PHP_EOL;
        $offset = -1;

        preg_match('/export\sclass\s.*\s({)/', $content, $matches, PREG_OFFSET_CAPTURE);
        if (sizeof($matches) > 0)
        {
            $offset = $matches[1][1] + 2;
            return FileUtils::insertContent($content, $offset, $str);
        }
        
        return $content;
    }

    public function addMethod(string $methodString, string $content)
    {
        if (strpos($content, $methodString) !== false)
        {
            return $content;
        }

        $offset = strrpos($content, '}');
        if ($offset === false)
        {
            return $content;
        }

        $methodString = PHP_EOL.$methodString.PHP_EOL;

        return FileUtils::insertContent($content, $offset, $methodString);
    }

    public function addActionToReducerField(string $actionGroup, string $actionName, string $reducerField, string $content)
    {
        $fieldPos = strpos($content, 'const '.$reducerField);
        if ($fieldPos === false)
        {
            return $content;
        }

        $offset = strpos($content, 'default:', $fieldPos);
        if ($offset === false)
        {
            return $content;
        }

        $str = 'case '.ucfirst($actionGroup).'Actions.'.StringUtils::camelCaseToConstant($actionName).':'.PHP_EOL;
        $str .= StringUtils::tab(3).'return state;'.PHP_EOL.StringUtils::tab(2);

        return FileUtils::insertContent($content, $offset, $str);
    }
    
    public function addActionCallToMainReducer($actionGroup, $actionName, $reducerField, $content)
    {
        preg_match('/export\sconst\s\w+Reducer:.*{\s+switch\s\(action.type\)\s({).*(d)efault:/sU', $content, $matches, PREG_OFFSET_CAPTURE);
        if (sizeof($matches) < 3)
        {
            return $content;
        }

        $caseLine = 'case '.ucfirst($actionGroup).'Actions.'.StringUtils::camelCaseToConstant($actionName).':';
        if (strpos($content, $caseLine, $matches[1][1]) === false)
        {
            $str = $caseLine.PHP_EOL;
            $str .= StringUtils::tab(3).'return Object.assign({}, state, {'.PHP_EOL;
            $str .= StringUtils::tab(3).'});'.PHP_EOL;
            $str .= StringUtils::tab(2);
            $content = FileUtils::insertContent($content, $matches[2][1], $str);
        }

        preg_match('/export\sconst\s\w+Reducer.*case\s'.ucfirst($actionGroup).'Actions\.'.StringUtils::camelCaseToConstant($actionName).':.*(O)bject\.assign.*(})\);/sU', $content, $matches, PREG_OFFSET_CAPTURE);
        $fieldLine = StringUtils::tab(1).$reducerField.': '.$reducerField.'(state.'.$reducerField.', action),'.PHP_EOL;
        if (strpos(substr($content, $matches[1][1], $matches[2][1] - $matches[1][1]), $fieldLine) !== false)
        {
            return $content;
        }

        return FileUtils::insertContent($content, $matches[2][1], $fieldLine.StringUtils::tab(3));
    }

    public function addReducerField(string $field, string $type, string $default, string $content)
    {
        preg_match('/export\sinterface.*{.*(})/sU', $content, $matches, PREG_OFFSET_CAPTURE);
        if (sizeof($matches) > 0)
        {
            $offset = $matches[1][1];
            $str = '    '.$field.': '.$type.';'.PHP_EOL;
            $content = FileUtils::insertContent($content, $offset, $str);
        }

        preg_match('/export\sconst\sinitialState\s=\s{.*(});/sU', $content, $matches, PREG_OFFSET_CAPTURE);
        if (sizeof($matches) > 0)
        {
            $offset = $matches[1][1];
            $str = '    '.$field.': '.$default.','.PHP_EOL;
            $content = FileUtils::insertContent($content, $offset, $str);
        }

        preg_match('/(e)xport\sconst\s.*Reducer:\sActionReducer/', $content, $matches, PREG_OFFSET_CAPTURE);
        if (sizeof($matches) > 0)
        {
            $offset = $matches[1][1];
            $str = 'const '.$field.' = (state: any, action: Action, args: any = {}) => {'.PHP_EOL;
            $str .= '    switch (action.type) {'.PHP_EOL;
            $str .= '        default:'.PHP_EOL;
            $str .= '            return state;'.PHP_EOL;
            $str .= '    }'.PHP_EOL;
            $str .= '};'.PHP_EOL.PHP_EOL;
            $content = FileUtils::insertContent($content, $offset, $str);
        }

        return $content;
    }

    public function addConstructorParam(string $param, string $content)
    {
        preg_match('/constructor\(.*(\))/sU', $content, $matches, PREG_OFFSET_CAPTURE);
        if (sizeof($matches > 0))
        {
            if (strpos($matches[0][0], $param) !== false)
            {
                return $content;
            }

            $offset = $matches[1][1];
            if ($content[$offset - 1] != '(')
            {
                $param = ','.PHP_EOL.'                '.$param;
            }
            $content = FileUtils::insertContent($content, $offset, $param);
        }

        return $content;
    }

    public function saveReducerContent($fieldName, $actionGroup, $actionName, $actionContent, $content)
    {
        $lines = explode(PHP_EOL, $actionContent);
        foreach ($lines as &$line)
        {
            $line = '            '.$line;
        }
        $actionContent = implode(PHP_EOL, $lines);

        return preg_replace('/(const\s'.$fieldName.'.*=>\s{.*case\s'.ucfirst($actionGroup).'Actions\.'.StringUtils::camelCaseToConstant($actionName).':\s+).*(\s+(?:case|default))/sU', '$1'.$actionContent.'$2', $content);
    }

    public function disconnectOutput($reducerField, $actionGroup, $actionName, $content)
    {
        return preg_replace('/(const\s'.$reducerField.'.*=>\s{.*)\s+case\s'.ucfirst($actionGroup).'Actions\.'.StringUtils::camelCaseToConstant($actionName).':\s+.*(\s+(?:case|default))/sU', '$1$2', $content);
    }

    public function removeActionCallFromMainReducer($reducerField, $actionGroup, $actionName, $content)
    {
        $content = preg_replace('/(export\sconst\s\w+Reducer.*switch\s\(.*case\s'.ucfirst($actionGroup).'Actions\.'.StringUtils::camelCaseToConstant($actionName).':.*return\sObject\.assign.*)\s+'.$reducerField.':\s'.$reducerField.'.*\),(.*}\);)/sU', '$1$2', $content);

        return preg_replace('/(export\sconst\s\w+Reducer.*switch\s\(.*)case\s'.ucfirst($actionGroup).'Actions\.'.StringUtils::camelCaseToConstant($actionName).':.*Object\.assign.*state,\s{\s+}\);\s+((?:case|default))/sU', '$1$2', $content);
    }

    public function addFieldSelector($reducerGroup, $reducerField, $content)
    {
        $str = StringUtils::tab(1).'get'.ucfirst($reducerField).'() {'.PHP_EOL;
        $str .= StringUtils::tab(2).'return this.store.select(s => s.'.$reducerGroup.'.'.$reducerField.');'.PHP_EOL;
        $str .= StringUtils::tab(1).'}';

        return $this->addMethod($str, $content);
    }

    public function addProperty($property, $content)
    {
        preg_match('/'.$property.'/', $content, $matches);
        if (sizeof($matches) > 0)
        {
            return $content;
        }

        $str = '    '.$property.';'.PHP_EOL;
        $offset = -1;

        preg_match('/@Output\(\).*(;)/', $content, $matches, PREG_OFFSET_CAPTURE);
        if (sizeof($matches) > 0)
        {
            $offset = $matches[1][1] + 2;
            return FileUtils::insertContent($content, $offset, $str);
        }

        preg_match('/@Input\(\).*(;)/', $content, $matches, PREG_OFFSET_CAPTURE);
        if (sizeof($matches) > 0)
        {
            $offset = $matches[1][1] + 2;
            return FileUtils::insertContent($content, $offset, $str);
        }

        preg_match('/export\sclass\s.*\s({)/', $content, $matches, PREG_OFFSET_CAPTURE);
        if (sizeof($matches) > 0)
        {
            $offset = $matches[1][1] + 2;
            return FileUtils::insertContent($content, $offset, $str);
        }
        
        return $content;
    }

    public function addConstructorLine($line, $content)
    {
        preg_match('/constructor\(.*\)\s*({)/sU', $content, $matches, PREG_OFFSET_CAPTURE);
        if (sizeof($matches) == 0)
        {
            return $content;
        }

        $offset = $matches[1][1] + 1;
        $str = PHP_EOL.'        '.$line;
        return FileUtils::insertContent($content, $offset, $str);
    }
}