<?php

namespace App\Domain\Processors\Client;

use App\Domain\Processors\ProjectProcessor;
use App\Domain\Utils\FileUtils;

class ReducerProcessor extends ProjectProcessor
{
    public function addReducer(string $name)
    {
        if (file_exists($this->currentProject->reducerRoot().'/'.$this->getFileName($name)))
        {
            return;
        }

        $content = file_get_contents($this->pyrConfig['templateRoot'].'/reducers/basic.ts');
        $content = str_replace('reducerName0', $this->getReducerName($name), $content);
        $content = str_replace('stateName0', $this->getStateName($name), $content);

        FileUtils::writeFile($this->currentProject->reducerRoot().'/'.$this->getFileName($name), $content);
    }

    public function addToIndex(string $name)
    {
        $indexFile = $this->currentProject->reducerRoot().'/index.ts';
        $content = file_get_contents($indexFile);

        $importFrom = './'.substr($this->getFileName($name), 0, -3);
        $content = $this->tsProcessor->addImport($this->getReducerName($name), $importFrom, $content);
        $content = $this->tsProcessor->addImport($this->getStateName($name), $importFrom, $content);

        // add to appstate
        $stateName = ucfirst($name).'State';
        preg_match('/export interface AppState ({)/', $content, $matches, PREG_OFFSET_CAPTURE);
        $offset = $matches[1][1] + 2;
        if (strpos(substr($content, $offset), $stateName) === false)
        {
            $content = substr($content, 0, $offset).'    '.$name.': '.$stateName.','.PHP_EOL.substr($content, $offset);
        }

        // add to reducers
        preg_match('/export const reducers = ({)/', $content, $matches, PREG_OFFSET_CAPTURE);
        $offset = $matches[1][1] + 2;
        if (strpos(substr($content, $offset), lcfirst($name)) === false)
        {
            $content = substr($content, 0, $offset).'    '.lcfirst($name).': '.lcfirst($name).'Reducer,'.PHP_EOL.substr($content, $offset);
        }

        FileUtils::writeFile($indexFile, $content);
    }

    private function getReducerName(string $name)
    {
        return $name.'Reducer';
    }

    private function getStateName(string $name)
    {
        return ucfirst($name).'State';
    }

    private function getFileName(string $name)
    {
        return $name.'.reducer.ts';
    }
}