<?php

namespace App\Domain\Processors\Client;

use App\Domain\Processors\FileIO;
use App\Domain\Project\CurrentProject;
use App\Domain\Utils\StringUtils;

class ComponentProcessor
{
    /**
     * @var CurrentProject
     */
    private $currentProject;

    /**
     * @var array
     */
    private $pyrConfig;

    public function __construct(CurrentProject $currentProject, array $pyrConfig)
    {
        $this->currentProject = $currentProject;
        $this->pyrConfig = $pyrConfig;
    }

    private $excludeTags = [
        'div', 'a', 'p', 'span', 'b', 'i', 'input', 'pre', 'button', 'table', 'tr', 'td', 'th', 'tbody', 'img',
        'ng-content'
    ];

    public function buildComponentTree($file)
    {
        $componentNames = $this->getComponentNames($file);
        $components = [];
        foreach ($componentNames as $name)
        {
            $children = $this->buildComponentTree($this->currentProject->componentRoot().'/'.$name.'/'.$name.'.html');

            $actions = $this->getOutputNames($this->currentProject->componentRoot().'/'.$name.'/'.$name.'.ts');
            foreach ($children as $child)
            {
                foreach ($child['actions'] as $childAction)
                {
                    if(($key = array_search($childAction, $actions)) !== false)
                    {
                        unset($actions[$key]);
                    }
                }
            }

            $inputs = $this->getInputNames($this->currentProject->componentRoot().'/'.$name.'/'.$name.'.ts');
            foreach ($children as $child)
            {
                foreach ($child['inputs'] as $childInput)
                {
                    if(($key = array_search($childInput, $inputs)) !== false)
                    {
                        unset($inputs[$key]);
                    }
                }
            }

            $components[] = [
                'name' => $name,
                'actions' => array_values($actions),
                'inputs' => array_values($inputs),
                'components' => $children
            ];
        }

        return $components;
    }

    private function getComponentNames($file)
    {
        $content = file_get_contents($file);

        preg_match_all('/<([a-zA-Z-]+)/', $content, $matches, PREG_OFFSET_CAPTURE);

        $componentNames = [];
        foreach ($matches[1] as $match)
        {
            if (in_array($match[0], $this->excludeTags))
            {
                continue;
            }

            $name = $this->dashToCamelCase($match[0]);

            $componentNames[] = $name;
        }

        return $componentNames;
    }

    private function getOutputNames($file)
    {
        $content = file_get_contents($file);

        preg_match_all('/@Output\(\)\s+(\w+)/', $content, $matches, PREG_OFFSET_CAPTURE);

        $outputNames = [];
        foreach ($matches[1] as $match)
        {
            $outputNames[] = $match[0];
        }

        return $outputNames;
    }

    private function getInputNames($file)
    {
        $content = file_get_contents($file);

        preg_match_all('/@Input\(\)\s+(\w+)/', $content, $matches, PREG_OFFSET_CAPTURE);

        $inputNames = [];
        foreach ($matches[1] as $match)
        {
            $inputNames[] = $match[0];
        }

        return $inputNames;
    }

    private function dashToCamelCase($str)
    {
        $parts = explode('-', $str);
        for ($i = 1; $i < sizeof($parts); $i++)
        {
            $parts[$i] = ucfirst($parts[$i]);
        }
        return implode('', $parts);
    }

    public function addComponent($name, $component)
    {
        $fileIO = new FileIO();

        $name = lcfirst($name);
        $dashName = StringUtils::camelCaseToDash($name);

        $dirPath = $this->currentProject->componentRoot().'/'.$name;
        if (file_exists($dirPath))
        {
            return false;
        }

        $fileIO->createDirectory($dirPath);

        $localTemplate = $this->pyrConfig['templateRoot'].'/components/'.$component;

        // load html and copy
        $localHtmlFile = $localTemplate.'/html.html';
        $htmlContent = file_get_contents($localHtmlFile);
        $htmlContent = str_replace('name0', $dashName, $htmlContent);
        file_put_contents($dirPath.'/'.$name.'.html', $htmlContent);

        // load scss and copy
        $localScssFile = $localTemplate.'/scss.scss';
        $scssContent = file_get_contents($localScssFile);
        $scssContent = str_replace('name0', $dashName, $scssContent);
        file_put_contents($dirPath.'/'.$name.'.scss', $scssContent);

        // load css and copy
        $localCssFile = $localTemplate.'/css.css';
        $cssContent = file_get_contents($localCssFile);
        $cssContent = str_replace('name0', $dashName, $cssContent);
        file_put_contents($dirPath.'/'.$name.'.css', $cssContent);

        // load ts and copy
        $localTsFile = $localTemplate.'/ts.ts';
        $tsContent = file_get_contents($localTsFile);
        $tsContent = str_replace('selectorName0', $dashName, $tsContent);
        $tsContent = str_replace('htmlName0', $name.'.html', $tsContent);
        $tsContent = str_replace('styleName0', $name.'.css', $tsContent);
        $tsContent = str_replace('className0', ucfirst($name), $tsContent);

        $fileIO->writeFile($dirPath.'/'.$name.'.ts', $tsContent);

        // register with app.declarations
        // check if import exists
        $appContent = file_get_contents($this->currentProject->clientRoot().'/app.declarations.ts');
        preg_match('/import.*'.ucfirst($name).'/', $appContent, $matches);
        if (sizeof($matches) == 0)
        {
            // add import
            preg_match_all('/import.*from.*;(\r?\n)/', $appContent, $matches, PREG_OFFSET_CAPTURE | PREG_SET_ORDER);
            $matches = end($matches);
            $appContent = substr($appContent, 0, $matches[1][1] + 1).'import {'.ucfirst($name).'} from \'./components/'.$name.'/'.$name.'\';'.PHP_EOL
                .substr($appContent, $matches[1][1] + 1);
        }

        // check if declaration exists
        preg_match('/'.ucfirst($name).',?\r?\n/', $appContent, $matches);
        if (sizeof($matches) == 0)
        {
            // add declaration
            preg_match_all('/DECLARATIONS\s?=\s?\[(\n)/si', $appContent, $matches, PREG_OFFSET_CAPTURE);
            $offset = $matches[1][0][1] + 1;
            $appContent = substr($appContent, 0, $offset).'    '.ucfirst($name).','.PHP_EOL.substr($appContent, $offset);
        }

        file_put_contents($this->currentProject->clientRoot().'/app.declarations.ts', $appContent);
    }
}