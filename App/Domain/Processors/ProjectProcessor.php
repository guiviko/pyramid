<?php

namespace App\Domain\Processors;

use App\Domain\Processors\Client\TsProcessor;
use App\Domain\Project\CurrentProject;

class ProjectProcessor
{
    /**
     * @var CurrentProject
     */
    protected $currentProject;

    /**
     * @var array
     */
    protected $pyrConfig;

    /**
     * @var TsProcessor
     */
    protected $tsProcessor;

    public function __construct(CurrentProject $currentProject, array $pyrConfig)
    {
        $this->currentProject = $currentProject;
        $this->pyrConfig = $pyrConfig;
        $this->tsProcessor = new TsProcessor();
    }
}