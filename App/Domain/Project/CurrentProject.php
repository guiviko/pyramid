<?php

namespace App\Domain\Project;

use App\Domain\Models\Projects\Project;

class CurrentProject
{
    private const WEB_ROOT = '/var/www/html';

    /**
     * @var Project
     */
    private $project;

    public function setProject(Project $project)
    {
        $this->project = $project;
    }

    public function project()
    {
        return $this->project;
    }

    public function projectRoot()
    {
        return self::WEB_ROOT.'/'.$this->project->name();
    }

    public function serverRoot()
    {
        return $this->projectRoot().'/App';
    }

    public function serverServicesRoot()
    {
        return $this->serverRoot().'/Domain/Services';
    }

    public function routesFile()
    {
        return $this->serverRoot().'/Http/routes.php';
    }

    public function requestsRoot()
    {
        return $this->serverRoot().'/Http/Requests';
    }

    public function controllersRoot()
    {
        return $this->serverRoot().'/Http/Controllers';
    }

    public function clientRoot()
    {
        return $this->projectRoot().'/src/app';
    }

    public function pagesRoot()
    {
        return $this->clientRoot().'/pages';
    }

    public function componentRoot()
    {
        return $this->clientRoot().'/components';
    }

    public function reducerRoot()
    {
        return $this->clientRoot().'/reducers';
    }

    public function effectRoot()
    {
        return $this->clientRoot().'/effects';
    }

    public function serviceRoot()
    {
        return $this->clientRoot().'/services';
    }

    public function selectorRoot()
    {
        return $this->clientRoot().'/selectors';
    }

    public function actionRoot()
    {
        return $this->clientRoot().'/actions';
    }

    public function moduleRoot()
    {
        return $this->clientRoot().'/modules';
    }
}