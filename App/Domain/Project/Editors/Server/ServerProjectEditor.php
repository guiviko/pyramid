<?php

namespace App\Domain\Project\Editors\Server;

use App\Domain\Project\CurrentProject;
use App\Domain\Project\Editors\ProjectEditor;

class ServerProjectEditor extends ProjectEditor
{
    public function __construct(CurrentProject $currentProject)
    {
        parent::__construct($currentProject);
    }
}