<?php

namespace App\Domain\Project\Editors;

use App\Domain\Processors\Client\TsProcessor;
use App\Domain\Processors\Client\HtmlProcessor;
use App\Domain\Processors\Client\SassProcessor;
use App\Domain\Project\CurrentProject;
use App\Domain\Utils\FileUtils;

abstract class Editor
{
    /**
     * @var CurrentProject
     */
    protected $currentProject;

    /**
     * @var TsProcessor
     */
    protected $tsProcessor;

    /**
     * @var HtmlProcessor
     */
    protected $htmlProcessor;

    /**
     * @var SassProcessor
     */
    protected $sassProcessor;

    public function __construct(CurrentProject $currentProject, TsProcessor $tsProcessor, HtmlProcessor $htmlProcessor,
                                SassProcessor $sassProcessor)
    {
        $this->currentProject = $currentProject;
        $this->tsProcessor = $tsProcessor;
        $this->htmlProcessor = $htmlProcessor;
        $this->sassProcessor = $sassProcessor;
    }

    protected function createDirectory($path)
    {
        FileUtils::createDirectory($path);
    }

    protected function createDirectoryRecursive($path)
    {
        FileUtils::createDirectoryRecursive($path);
    }

    protected function removeDirectoryRecursive($path)
    {
        FileUtils::removeDirectoryRecursive($path);
    }
}