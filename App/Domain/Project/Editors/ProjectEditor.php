<?php

namespace App\Domain\Project\Editors;

use App\Domain\Project\CurrentProject;

abstract class ProjectEditor
{
    /**
     * @var CurrentProject
     */
    protected $currentProject;

    public function __construct(CurrentProject $currentProject)
    {
        $this->currentProject = $currentProject;
    }
}