<?php

namespace App\Domain\Project\Editors\Client;

use App\Domain\Models\Projects\Component;
use App\Domain\Project\Editors\Editor;
use App\Domain\Utils\StringUtils;

class DocumentEditor extends Editor
{
    public function addComponent(Component $component, string $html, $blockAreaId, $blockId)
    {
        preg_match('/data-block-id="'.$blockAreaId.'\.'.$blockId.'".*(>)/', $html, $matches, PREG_OFFSET_CAPTURE);
        if (sizeof($matches) == 0)
        {
            return $html;
        }

        $offset = $matches[1][1] + 1;
        if ($html[$offset + 1] == PHP_EOL)
        {
            $offset++;
        }
        $dashName = StringUtils::camelCaseToDash($component->name());
        $str = '<'.$dashName.'></'.$dashName.'>';

        return substr($html, 0, $offset).$str.substr($html, $offset);
    }
}