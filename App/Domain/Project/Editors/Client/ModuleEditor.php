<?php

namespace App\Domain\Project\Editors\Client;

use App\Domain\Models\Projects\Module;
use App\Domain\Project\Editors\Editor;
use App\Domain\Utils\FileUtils;
use App\Domain\Utils\PathUtils;
use App\Domain\Utils\StringUtils;

class ModuleEditor extends Editor
{
    public function create(Module $module, Module $parentModule)
    {
        $this->createDirectory($module->directoryPath());

        $this->createComponentTs($module);
        $this->createComponentHtml($module);
        $this->createComponentScss($module);
        $this->createComponentCss($module);
        $this->createIndexTs($module);
        $this->createRoutesTs($module);

        $this->addToParentRoutes($module, $parentModule);
    }

    public function destroy(Module $module, Module $parentModule)
    {
        $this->removeDirectoryRecursive($module->directoryPath());

        $this->removeFromParentRoutes($module, $parentModule);
    }

    private function createComponentTs(Module $module)
    {
        $filePath = $module->directoryPath().'/'.$module->name().'.component.ts';
        if (FileUtils::fileExists($filePath))
        {
            return;
        }

        $content = FileUtils::readFile(PathUtils::moduleTemplateRoot.'/component.ts');
        $content = str_replace('className0', ucfirst($module->name().'Component'), $content);
        $content = str_replace('selector0', StringUtils::camelCaseToDash($module->name().'-component'), $content);
        $content = str_replace('htmlName0', $module->name().'.component.html', $content);
        $content = str_replace('styleName0', $module->name().'.component.css', $content);
        FileUtils::writeFile($filePath, $content);
    }

    private function createComponentHtml(Module $module)
    {
        $filePath = $module->directoryPath().'/'.$module->name().'.component.html';
        if (FileUtils::fileExists($filePath))
        {
            return;
        }

        $content = FileUtils::readFile(PathUtils::moduleTemplateRoot.'/component.html');
        $content = str_replace('dashName0', StringUtils::camelCaseToDash($module->name().'-component'), $content);
        FileUtils::writeFile($filePath, $content);
    }

    private function createComponentScss(Module $module)
    {
        $filePath = $module->directoryPath().'/'.$module->name().'.component.scss';
        if (FileUtils::fileExists($filePath))
        {
            return;
        }

        $content = FileUtils::readFile(PathUtils::moduleTemplateRoot.'/component.scss');
        $content = str_replace('dashName0', StringUtils::camelCaseToDash($module->name().'-component'), $content);
        FileUtils::writeFile($filePath, $content);
    }

    private function createComponentCss(Module $module)
    {
        $filePath = $module->directoryPath().'/'.$module->name().'.component.css';
        if (FileUtils::fileExists($filePath))
        {
            return;
        }

        $content = FileUtils::readFile(PathUtils::moduleTemplateRoot.'/component.css');
        $content = str_replace('dashName0', StringUtils::camelCaseToDash($module->name().'-component'), $content);
        FileUtils::writeFile($filePath, $content);
    }

    private function createIndexTs(Module $module)
    {
        $filePath = $module->directoryPath().'/index.ts';
        if (FileUtils::fileExists($filePath))
        {
            return;
        }

        $content = FileUtils::readFile(PathUtils::moduleTemplateRoot.'/index.ts');
        $sharedModuleLevels = '';
        for ($i = 0; $i < sizeof(explode('#', $module->dotChain())); $i++)
        {
            $sharedModuleLevels .= '../';
        }
        $content = str_replace('sharedModuleLevels0', $sharedModuleLevels, $content);
        $content = str_replace('name0', $module->name(), $content);
        $content = str_replace('className0', ucfirst($module->name()).'Module', $content);
        $content = str_replace('componentName0', ucfirst($module->name()).'Component', $content);
        $content = str_replace('componentImport0', $module->name().'.component', $content);
        FileUtils::writeFile($filePath, $content);
    }

    private function createRoutesTs(Module $module)
    {
        $filePath = $module->directoryPath().'/'.$module->name().'.routes.ts';
        if (FileUtils::fileExists($filePath))
        {
            return;
        }

        $content = FileUtils::readFile(PathUtils::moduleTemplateRoot.'/routes.ts');
        $content = str_replace('name0', $module->name(), $content);
        $content = str_replace('componentClassName0', ucfirst($module->name()).'Component', $content);
        FileUtils::writeFile($filePath, $content);
    }

    private function addToParentRoutes(Module $module, Module $parentModule)
    {
        return $parentModule->isRoot()
            ? $this->addToAppRoutes($module, $parentModule)
            : $this->addToGenericRoutes($module, $parentModule);
    }

    private function addToAppRoutes(Module $module, Module $appModule)
    {
        $parentFilePath = $appModule->directoryPath().'/'.$appModule->name().'.routing.ts';
        $content = FileUtils::readFile($parentFilePath);

        if (strpos($content, 'index#'.ucfirst($module->name()).'Module') !== false)
        {
            return;
        }

        $offset = strpos($content, '\'**\'');
        if ($offset !== false)
        {
            for ($i = $offset; $i >= 0; $i--)
            {
                if ($content[$i] == ',' || $content[$i] == '[')
                {
                    $offset = $i + 2;
                    break;
                }
            }
        }
        else
        {
            preg_match('/export const routes: Routes = \[.*\](;)/s', $content, $matches, PREG_OFFSET_CAPTURE);
            if (sizeof($matches) == 0)
            {
                return;
            }
            $offset = $matches[1][1] - 1;
        }

        $uri = $module->uri();
        if ($uri[0] == '/')
        {
            $uri = substr($uri, 1);
        }

        for ($i = $offset; $i >= 0; $i--)
        {
            if ($content[$i] == '[' || $content[$i] == ',')
            {
                break;
            }

            if ($content[$i] == '}')
            {
                $content = FileUtils::insertContent($content, $i + 1, ',');
                $offset++;
                break;
            }
        }

        $str = '    {path: \''.$uri.'\', loadChildren: \'./modules/'.$module->name().'/index#'.ucfirst($module->name()).'Module?sync=true\' },'.PHP_EOL;
        $content = FileUtils::insertContent($content, $offset, $str);
        FileUtils::writeFile($parentFilePath, $content);
    }

    private function addToGenericRoutes(Module $module, Module $parentModule)
    {
        $parentFilePath = $parentModule->directoryPath().'/'.$parentModule->name().'.routes.ts';
        $content = FileUtils::readFile($parentFilePath);

        if (strpos($content, 'index#'.ucfirst($module->name()).'Module') !== false)
        {
            return;
        }

        preg_match('/children: \[.*\](})/s', $content, $matches, PREG_OFFSET_CAPTURE);
        if (sizeof($matches) == 0)
        {
            return;
        }
        $offset = $matches[1][1] - 1;

        $uri = $module->uri();
        if ($uri[0] == '/')
        {
            $uri = substr($uri, 1);
        }

        for ($i = $offset; $i >= 0; $i--)
        {
            if ($content[$i] == '[' || $content[$i] == ',')
            {
                $offset = $i + 2;
                break;
            }

            if ($content[$i] == '}')
            {
                $content = FileUtils::insertContent($content, $i + 1, ',');
                $offset++;
                break;
            }
        }

        $str = '        {path: \''.$uri.'\', loadChildren: \'./'.$module->name().'/index#'.ucfirst($module->name()).'Module?sync=true\' },'.PHP_EOL;
        $content = FileUtils::insertContent($content, $offset, $str);
        FileUtils::writeFile($parentFilePath, $content);
    }

    private function removeFromParentRoutes(Module $module, Module $parentModule)
    {
        $parentFilePath = $parentModule->isRoot()
            ? $parentModule->directoryPath().'/'.$parentModule->name().'.routing.ts'
            : $parentModule->directoryPath().'/'.$parentModule->name().'.routes.ts';
        $content = FileUtils::readFile($parentFilePath);
        $content = StringUtils::removeLinesContainingString($content, 'index#'.ucfirst($module->name()).'Module');
        FileUtils::writeFile($parentFilePath, $content);
    }
}