<?php

namespace App\Domain\Project\Editors\Client;

use App\Domain\Project\Editors\Editor;
use App\Domain\Utils\FileUtils;
use App\Domain\Utils\PathUtils;
use Library\Utils\StringUtils;

class EffectEditor extends Editor
{
    public function addEffectFile(string $name)
    {
        $filePath = $this->currentProject->effectRoot().'/'.$name.'.effects.ts';
        if (FileUtils::fileExists($filePath))
        {
            return;
        }

        $content = FileUtils::readFile(PathUtils::effectTemplateRoot.'/basic.ts');
        $content = str_replace('Name0', ucfirst($name), $content);
        $content = str_replace('name0', $name, $content);
        FileUtils::writeFile($filePath, $content);

        $this->addToIndex($name);
    }

    private function addToIndex($name)
    {
        $effectName = ucfirst($name).'Effects';

        $indexFile = $this->currentProject->effectRoot().'/index.ts';
        $content = file_get_contents($indexFile);

        $importFrom = './'.$name.'.effects';
        $content = $this->tsProcessor->addImport($effectName, $importFrom, $content);

        preg_match('/export\sdefault\s(\[)/', $content, $matches, PREG_OFFSET_CAPTURE);
        $offset = $matches[1][1] + 2;
        if (strpos(substr($content, $offset), $effectName) === false)
        {
            $content = substr($content, 0, $offset).'    '.$effectName.','.PHP_EOL.substr($content, $offset);
        }

        FileUtils::writeFile($indexFile, $content);
    }

    public function addRequest($group, $action)
    {
        $effectFile = $this->currentProject->effectRoot().'/'.$group.'.effects.ts';
        if (!FileUtils::fileExists($effectFile))
        {
            return;
        }

        $content = file_get_contents($effectFile);

        if (strpos($content, '@Effect() '.$action) !== false)
        {
            return;
        }

        $str = '    @Effect() '.$action.' = this.actions$'.PHP_EOL;
        $str .= '        .ofType('.ucfirst($group).'Actions.'.StringUtils::camelCaseToConstant($action).')'.PHP_EOL;
        $str .= '        .switchMap(action => {'.PHP_EOL;
        $str .= '            return this.'.$group.'Service.'.$action.'(action.payload)'.PHP_EOL;
        $str .= '                .map(data => this.'.$group.'Actions.'.$action.'Success(data))'.PHP_EOL;
        $str .= '                .map(err => Observable.of(this.'.$group.'Actions.'.$action.'Failure(err)))'.PHP_EOL;
        $str .= '        });';

        $content = $this->tsProcessor->addMethod($str, $content);
        FileUtils::writeFile($effectFile, $content);
    }
}