<?php

namespace App\Domain\Project\Editors\Client;

use App\Domain\Project\Editors\Editor;

class TemplateEditors extends Editor
{
    public function editor($name)
    {
        $name = str_replace(' ', '', $name);
        $name = ucfirst($name);
        $className = '\\App\\Domain\\Project\\Editors\\Client\\TemplateEditors\\'.$name.'Editor';
        $editor = new $className($this->currentProject, $this->tsProcessor,
                                 $this->htmlProcessor, $this->sassProcessor);
        return $editor;
    }
}