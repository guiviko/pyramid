<?php

namespace App\Domain\Project\Editors\Client;

use App\Domain\Models\Projects\Component;
use App\Domain\Models\Projects\Module;
use App\Domain\Project\Editors\Editor;
use App\Domain\Utils\FileUtils;
use App\Domain\Utils\PathUtils;
use App\Domain\Utils\StringUtils;

class ComponentEditor extends Editor
{
    public function create(Component $component, Module $module)
    {
        $this->createDirectory($component->directoryPath());

        $this->createTs($component);
        $this->createHtml($component);
        $this->createScss($component);

        $this->addToModuleDeclarations($component, $module);
    }

    public function destroy(Component $component, Module $module)
    {
        $this->removeDirectoryRecursive($component->directoryPath());
        $this->removeFromModuleDeclarations($component, $module);
    }

    public function addOutputTs(Component $component, string $name)
    {
        $filePath = $component->directoryPath().'/'.$component->name().'.ts';
        if (!FileUtils::fileExists($filePath)) {
            return;
        }

        $content = FileUtils::readFile($filePath);
        $content = $this->tsProcessor->addImport('Output', '@angular/core', $content);
        $content = $this->tsProcessor->addImport('EventEmitter', '@angular/core', $content);
        $content = $this->tsProcessor->addOutput($name, $content);

        FileUtils::writeFile($filePath, $content); 
    }

    public function addInputTs(Component $component, string $name)
    {
        $filePath = $component->directoryPath().'/'.$component->name().'.ts';
        if (!FileUtils::fileExists($filePath)) {
            return;
        }

        $content = FileUtils::readFile($filePath);
        $content = $this->tsProcessor->addImport('Input', '@angular/core', $content);
        $content = $this->tsProcessor->addInput($name, $content);

        FileUtils::writeFile($filePath, $content); 
    }

    public function addOutputChainHtml(Component $component, string $childComponentSelector, $actionName)
    {
        $filePath = $component->directoryPath().'/'.$component->name().'.html';
        if (!FileUtils::fileExists($filePath)) {
            return;
        }

        $content = FileUtils::readFile($filePath);

        $content = $this->htmlProcessor->addOutputChain($childComponentSelector, $actionName, $content);

        FileUtils::writeFile($filePath, $content);
    }

    public function addOutputPageHtml(Component $component, string $childComponentSelector, $actionName)
    {
        $filePath = $component->directoryPath().'/'.$component->name().'.html';
        if (!FileUtils::fileExists($filePath)) {
            return;
        }

        $content = FileUtils::readFile($filePath);

        $content = $this->htmlProcessor->addOutputPage($childComponentSelector, $actionName, $content);

        FileUtils::writeFile($filePath, $content);
    }

    public function addInputPageHtml(Component $component, string $childComponentSelector, $inputName)
    {
        $filePath = $component->directoryPath().'/'.$component->name().'.html';
        if (!FileUtils::fileExists($filePath)) {
            return;
        }

        $content = FileUtils::readFile($filePath);

        $content = $this->htmlProcessor->addInputPage($childComponentSelector, $inputName, $content);

        FileUtils::writeFile($filePath, $content);
    }

    public function addInputChainHtml(Component $component, string $childComponentSelector, $inputName)
    {
        $filePath = $component->directoryPath().'/'.$component->name().'.html';
        if (!FileUtils::fileExists($filePath)) {
            return;
        }

        $content = FileUtils::readFile($filePath);

        $content = $this->htmlProcessor->addInputChain($childComponentSelector, $inputName, $content);

        FileUtils::writeFile($filePath, $content);
    }

    private function createTs(Component $component)
    {
        $filePath = $component->directoryPath().'/'.$component->name().'.ts';
        if (FileUtils::fileExists($filePath))
        {
            return;
        }

        $content = FileUtils::readFile(PathUtils::componentTemplateRoot.'/'.$component->template()->name().'/ts.ts');
        $content = str_replace('selectorName0', StringUtils::camelCaseToDash($component->name()), $content);
        $content = str_replace('htmlName0', $component->name().'.html', $content);
        $content = str_replace('styleName0', $component->name().'.css', $content);
        $content = str_replace('className0', ucfirst($component->name()), $content);
        FileUtils::writeFile($filePath, $content);
    }

    private function createHtml(Component $component)
    {
        $filePath = $component->directoryPath().'/'.$component->name().'.html';
        if (FileUtils::fileExists($filePath))
        {
            return;
        }

        $content = FileUtils::readFile(PathUtils::componentTemplateRoot.'/'.$component->template()->name().'/html.html');
        $content = str_replace('name0', StringUtils::camelCaseToDash($component->name()), $content);
        FileUtils::writeFile($filePath, $content);
    }

    private function createScss(Component $component)
    {
        $filePath = $component->directoryPath().'/'.$component->name().'.scss';
        if (FileUtils::fileExists($filePath))
        {
            return;
        }

        $content = FileUtils::readFile(PathUtils::componentTemplateRoot.'/'.$component->template()->name().'/scss.scss');
        $content = str_replace('name0', StringUtils::camelCaseToDash($component->name()), $content);
        FileUtils::writeFile($filePath, $content);
    }

    private function addToModuleDeclarations(Component $component, Module $module)
    {
        $filePath = $module->directoryPath().'/';
        $filePath .= $module->isRoot() ? 'app.declarations.ts' : 'index.ts';
        $this->tsProcessor->addDeclaration($filePath, ucfirst($component->name()),
            $this->generateImportFrom($component, $module), $module->isRoot());
    }

    private function removeFromModuleDeclarations(Component $component, Module $module)
    {
        $filePath = $module->directoryPath().'/';
        $filePath .= $module->isRoot() ? 'app.declarations.ts' : 'index.ts';
        $this->tsProcessor->removeDeclaration($filePath, ucfirst($component->name()));
    }

    private function generateImportFrom(Component $component, Module $module)
    {
        $importFrom = './';
        $level = sizeof(explode('#', $module->dotChain()));
        if (!$module->isRoot())
        {
            for ($i = 0; $i < $level; $i++)
            {
                $importFrom .= '../';
            }
        }

        $importFrom .= substr($component->directoryPath(), strpos($component->directoryPath(), 'components/')).'/'.$component->name();

        return $importFrom;
    }
}