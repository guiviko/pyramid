<?php

namespace App\Domain\Project\Editors\Client\TemplateEditors;

use App\Domain\Utils\FileUtils;

class PanelEditor extends TemplateEditor
{
    public function process($component, $groupIndex, $rowIndex, $fieldIndex, $value)
    {
        $indexString = $groupIndex.'.'.$rowIndex.'.'.$fieldIndex;

        $htmlPath = $component->directoryPath().'/'.$component->name().'.html';
        $htmlContent = FileUtils::readFile($htmlPath);

        switch ($indexString)
        {
        case '1.0.0':
            $htmlContent = $this->htmlProcessor->setBlockContent('config-title', $value, $htmlContent);
            break;
        case '0.1.0':
            $htmlContent = $this->htmlProcessor->replaceBlockClass('config-style', 'page-title-style-\d', 'page-title-style-'.$value, $htmlContent);
            break;
        }

        FileUtils::writeFile($htmlPath, $htmlContent);
    }
}