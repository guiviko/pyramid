<?php

namespace App\Domain\Project\Editors\Client\TemplateEditors;

use App\Domain\Processors\Client\HtmlProcessor;
use App\Domain\Processors\Client\TsProcessor;
use App\Domain\Project\CurrentProject;
use App\Domain\Project\Editors\Editor;

abstract class TemplateEditor extends Editor
{
    abstract public function process($component, $groupIndex, $rowIndex, $fieldIndex, $value);
}