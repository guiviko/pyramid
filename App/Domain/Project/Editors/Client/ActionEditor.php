<?php

namespace App\Domain\Project\Editors\Client;

use App\Domain\Project\Editors\Editor;
use App\Domain\Utils\FileUtils;
use App\Domain\Utils\PathUtils;
use App\Domain\Utils\StringUtils;

class ActionEditor extends Editor
{
    public function addActionFile(string $name)
    {
        $this->createFile($name);
        $this->addToIndex($name);
    }

    private function createFile(string $name)
    {
        if (FileUtils::fileExists($this->currentProject->actionRoot().'/'.$this->getFileName($name)))
        {
            return;
        }

        $content = file_get_contents(PathUtils::actionTemplateRoot.'/basic.ts');
        $content = str_replace('className0', $this->getActionName($name), $content);

        FileUtils::writeFile($this->currentProject->actionRoot().'/'.$this->getFileName($name), $content);
    }

    private function addToIndex(string $name)
    {
        $indexFile = $this->currentProject->actionRoot().'/index.ts';
        $content = file_get_contents($indexFile);

        $importFrom = './'.substr($this->getFileName($name), 0, -3);
        $content = $this->tsProcessor->addImport($this->getActionName($name), $importFrom, $content);

        preg_match('/export ({)/', $content, $matches, PREG_OFFSET_CAPTURE);
        $offset = $matches[1][1] + 2;
        if (strpos(substr($content, $offset), $this->getActionName($name)) === false)
        {
            $content = substr($content, 0, $offset).'    '.$this->getActionName($name).','.PHP_EOL.substr($content, $offset);

            preg_match('/export default (\[)/', $content, $matches, PREG_OFFSET_CAPTURE);
            $offset = $matches[1][1] + 2;
            $content = substr($content, 0, $offset).'    '.$this->getActionName($name).','.PHP_EOL.substr($content, $offset);
        }

        FileUtils::writeFile($indexFile, $content);
    }

    public function addAction($name, $actionName)
    {
        if (!FileUtils::fileExists($this->currentProject->actionRoot().'/'.$this->getFileName($name)))
        {
            return;
        }

        $content = FileUtils::readFile($this->currentProject->actionRoot().'/'.$this->getFileName($name));

        if (strpos($content, $actionName) !== false)
        {
            return;
        }

        $constantName = StringUtils::camelCaseToConstant($actionName);
        $str = '    static '.$constantName.' = \''.strtoupper($name).'_'.$constantName.'\';'.PHP_EOL;
        $str .= '    '.$actionName.'(data: any): Action {'.PHP_EOL;
        $str .= '        return {'.PHP_EOL;
        $str .= '            type: '.$this->getActionName($name).'.'.$constantName.','.PHP_EOL;
        $str .= '            payload: data'.PHP_EOL;
        $str .= '        }'.PHP_EOL;
        $str .= '    }'.PHP_EOL;

        $str .= '    static '.$constantName.'_SUCCESS = \''.strtoupper($name).'_'.$constantName.'_SUCCESS\';'.PHP_EOL;
        $str .= '    '.$actionName.'Success(data: any): Action {'.PHP_EOL;
        $str .= '        return {'.PHP_EOL;
        $str .= '            type: '.$this->getActionName($name).'.'.$constantName.'_SUCCESS,'.PHP_EOL;
        $str .= '            payload: data'.PHP_EOL;
        $str .= '        }'.PHP_EOL;
        $str .= '    }'.PHP_EOL;

        $str .= '    static '.$constantName.'_FAILURE = \''.strtoupper($name).'_'.$constantName.'_FAILURE\';'.PHP_EOL;
        $str .= '    '.$actionName.'Failure(data: any): Action {'.PHP_EOL;
        $str .= '        return {'.PHP_EOL;
        $str .= '            type: '.$this->getActionName($name).'.'.$constantName.'_FAILURE,'.PHP_EOL;
        $str .= '            payload: data'.PHP_EOL;
        $str .= '        }'.PHP_EOL;
        $str .= '    }'.PHP_EOL;

        preg_match('/export\sclass\s'.$this->getActionName($name).'\s({)/', $content, $matches, PREG_OFFSET_CAPTURE);
        if (sizeof($matches) == 0)
        {
            return;
        }

        $offset = $matches[1][1] + 2;
        $content = FileUtils::insertContent($content, $offset, $str);

        FileUtils::writeFile($this->currentProject->actionRoot().'/'.$this->getFileName($name), $content);
    }

    private function getFileName(string $name)
    {
        return $name.'.actions.ts';
    }

    private function getActionName(string $name)
    {
        return ucfirst($name).'Actions';
    }
}