<?php

namespace App\Domain\Project\Editors\Client;

use App\Domain\Models\Projects\Page;
use App\Domain\Project\Editors\Editor;
use App\Domain\Utils\FileUtils;
use App\Domain\Utils\PathUtils;
use App\Domain\Utils\StringUtils;
use App\Domain\Models\Projects\Component;

class PageEditor extends Editor
{
    public function create(Page $page)
    {
        $this->createDirectoryRecursive($page->directoryPath());

        $this->createTs($page);
        $this->createHtml($page);
        $this->createScss($page);

        $this->addToModuleRoutes($page);
        $this->addToModuleDeclarations($page);
    }

    public function destroy(Page $page)
    {
        $this->removePageDirectories($page);
        $this->removeFromModuleRoutes($page);
        $this->removeFromModuleDeclarations($page);
    }

    public function addDispatch(Component $component, string $name, string $actionName)
    {
        $filePath = $component->directoryPath().'/'.$component->name().'.ts';
        if (!FileUtils::fileExists($filePath))
        {
            return;
        }

        $content = FileUtils::readFile($filePath);

        $methodString = '    '.$actionName.'(data: any) {'.PHP_EOL;
        $methodString .= '        this.store.dispatch(this.'.$name.'Actions.'.$actionName.'(data));'.PHP_EOL;
        $methodString .= '    }';

        $content = $this->tsProcessor->addMethod($methodString, $content);

        $importFrom = './';
        $level = sizeof(explode('#', $component->dotChain()));
        for ($i = 0; $i < $level + 1; $i++)
        {
            $importFrom .= '../';
        }
        $from = $importFrom.'actions/'.$name.'.actions';
        $content = $this->tsProcessor->addImport(ucfirst($name).'Actions', $from, $content);

        $param = 'private '.$name.'Actions: '.ucfirst($name).'Actions';
        $content = $this->tsProcessor->addConstructorParam($param, $content);

        FileUtils::writeFile($filePath, $content);
    }

    public function addSelection(Component $component, string $group, string $inputName)
    {
        $filePath = $component->directoryPath().'/'.$component->name().'.ts';
        if (!FileUtils::fileExists($filePath))
        {
            return;
        }

        $content = FileUtils::readFile($filePath);

        $importFrom = './';
        $level = sizeof(explode('#', $component->dotChain()));
        for ($i = 0; $i < $level + 1; $i++)
        {
            $importFrom .= '../';
        }
        $from = $importFrom.'selectors/'.$group.'.selectors';
        $content = $this->tsProcessor->addImport(ucfirst($group).'Selectors', $from, $content);

        $param = 'private '.$group.'Selectors: '.ucfirst($group).'Selectors';
        $content = $this->tsProcessor->addConstructorParam($param, $content);

        $property = $inputName.': any';
        $content = $this->tsProcessor->addProperty($property, $content);

        $line = 'this.'.$inputName.' = this.'.$group.'Selectors.get'.ucfirst($inputName).'();';
        $content = $this->tsProcessor->addConstructorLine($line, $content);

        FileUtils::writeFile($filePath, $content);
    }

    private function removePageDirectories(Page $page)
    {
        $directoryPath = $page->directoryPath();
        $this->removeDirectoryRecursive($directoryPath);

        $parts = explode('/', $directoryPath);
        $numberOfParts = sizeof($parts);
        for ($i = 0; $i < $numberOfParts; $i++)
        {
            array_pop($parts);
            $parentDirectory = implode('/', $parts);
            if ($parentDirectory == $this->currentProject->pagesRoot() || !FileUtils::isDirectoryEmpty($parentDirectory))
            {
                return;
            }

            FileUtils::removeDirectory($parentDirectory);
        }
    }

    private function createTs(Page $page)
    {
        $filePath = $page->directoryPath().'/'.$page->name().'.page.ts';
        if (FileUtils::fileExists($filePath))
        {
            return;
        }

        $content = FileUtils::readFile(PathUtils::pageTemplateRoot.'/blankPage/ts.ts');
        $content = str_replace('selectorName0', StringUtils::camelCaseToDash($page->name().'-page'), $content);
        $content = str_replace('htmlName0', $page->name().'.page.html', $content);
        $content = str_replace('styleName0', $page->name().'.page.css', $content);
        $content = str_replace('className0', ucfirst($page->name()).'Page', $content);

        $importFrom = $this->getImportPrefixWithinPage($page).'reducers/index';
        $content = $this->tsProcessor->addImport('AppState', $importFrom, $content);

        FileUtils::writeFile($filePath, $content);
    }

    private function createHtml(Page $page)
    {
        $filePath = $page->directoryPath().'/'.$page->name().'.page.html';
        if (FileUtils::fileExists($filePath))
        {
            return;
        }

        $content = FileUtils::readFile(PathUtils::pageTemplateRoot.'/blankPage/html.html');
        $content = str_replace('dashName0', StringUtils::camelCaseToDash($page->name().'-page'), $content);
        FileUtils::writeFile($filePath, $content);
    }

    private function createScss(Page $page)
    {
        $filePath = $page->directoryPath().'/'.$page->name().'.page.scss';
        if (FileUtils::fileExists($filePath))
        {
            return;
        }

        $content = FileUtils::readFile(PathUtils::pageTemplateRoot.'/blankPage/scss.scss');
        $content = str_replace('dashName0', StringUtils::camelCaseToDash($page->name().'-page'), $content);
        FileUtils::writeFile($filePath, $content);
    }

    private function addToModuleRoutes(Page $page)
    {
        $filePath = $page->module()->directoryPath().'/'.$page->module()->name();
        $filePath .= $page->module()->isRoot() ? '.routing.ts' : '.routes.ts';
        $this->tsProcessor->addComponentRoute($filePath, $page->uri(), ucfirst($page->name()).'Page',
            $this->generateImportFrom($page), $page->module()->isRoot());
    }

    private function removeFromModuleRoutes(Page $page)
    {
        $filePath = $page->module()->directoryPath().'/'.$page->module()->name();
        $filePath .= $page->module()->isRoot() ? '.routing.ts' : '.routes.ts';
        $this->tsProcessor->removeComponentRoute($filePath, ucfirst($page->name()).'Page');
    }

    private function addToModuleDeclarations(Page $page)
    {
        $filePath = $page->module()->directoryPath().'/';
        $filePath .= $page->module()->isRoot() ? 'app.declarations.ts' : 'index.ts';
        $this->tsProcessor->addDeclaration($filePath, ucfirst($page->name()).'Page', $this->generateImportFrom($page),
            $page->module()->isRoot());
    }

    private function removeFromModuleDeclarations(Page $page)
    {
        $filePath = $page->module()->directoryPath().'/';
        $filePath .= $page->module()->isRoot() ? 'app.declarations.ts' : 'index.ts';
        $this->tsProcessor->removeDeclaration($filePath, ucfirst($page->name()).'Page');
    }

    private function generateImportFrom(Page $page)
    {
        return $this->getImportPrefix($page).substr($page->directoryPath(), strpos($page->directoryPath(), 'pages/')).'/'.$page->name().'.page';
    }

    private function getImportPrefix(Page $page)
    {
        $importFrom = './';
        $level = sizeof(explode('#', $page->module()->dotChain()));
        if (!$page->module()->isRoot())
        {
            for ($i = 0; $i < $level + 1; $i++)
            {
                $importFrom .= '../';
            }
        }

        return $importFrom;
    }

    private function getImportPrefixWithinPage(Page $page)
    {
        $importFrom = './';
        if (!$page->module()->isRoot())
        {
            $importFrom .= '../';
        }
        $level = sizeof(explode('#', $page->module()->dotChain()));
        for ($i = 0; $i < $level + 1; $i++)
        {
            $importFrom .= '../';
        }

        return $importFrom;
    }
}