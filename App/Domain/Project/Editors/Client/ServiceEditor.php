<?php

namespace App\Domain\Project\Editors\Client;

use App\Domain\Project\Editors\Editor;
use App\Domain\Utils\FileUtils;
use App\Domain\Utils\PathUtils;
use SebastianBergmann\CodeCoverage\Report\PHP;

class ServiceEditor extends Editor
{
    public function addServiceFile(string $name)
    {
        $filePath = $this->currentProject->serviceRoot().'/'.$name.'.service.ts';
        if (FileUtils::fileExists($filePath))
        {
            return;
        }

        $content = FileUtils::readFile(PathUtils::serviceTemplateRoot.'/basic.ts');
        $content = str_replace('Name0', ucfirst($name), $content);
        FileUtils::writeFile($filePath, $content);

        $this->addToIndex($name);
    }

    private function addToIndex($name)
    {
        $serviceName = ucfirst($name).'Service';

        $indexFile = $this->currentProject->serviceRoot().'/index.ts';
        $content = file_get_contents($indexFile);

        $importFrom = './'.$name.'.service';
        $content = $this->tsProcessor->addImport($serviceName, $importFrom, $content);

        preg_match('/export\sdefault\s(\[)/', $content, $matches, PREG_OFFSET_CAPTURE);
        $offset = $matches[1][1] + 2;
        if (strpos(substr($content, $offset), $serviceName) === false)
        {
            $content = substr($content, 0, $offset).'    '.$serviceName.','.PHP_EOL.substr($content, $offset);
        }

        FileUtils::writeFile($indexFile, $content);
    }

    public function addEngineFetch($group, $funcName, $type, $fields, $conditions)
    {
        $serviceFile = $this->currentProject->serviceRoot().'/'.$group.'.service.ts';
        if (!FileUtils::fileExists($serviceFile))
        {
            return;
        }

        $content = file_get_contents($serviceFile);

        if (strpos($content, $funcName.'(') !== false)
        {
            return;
        }

        $str = '    '.$funcName.'(data) {'.PHP_EOL;
        $str .= '        return this.http.post(API_BASE_URL + ENGINE_URI, {'.PHP_EOL;
        $str .= '        })'.PHP_EOL;
        $str .= '            .map(response => response.json());'.PHP_EOL;
        $str .= '    }';

        $content = $this->tsProcessor->addMethod($str, $content);
        FileUtils::writeFile($serviceFile, $content);
    }

    public function replaceEngineData($group, $funcName, $data, $method)
    {
        $serviceFile = $this->currentProject->serviceRoot().'/'.$group.'.service.ts';
        if (!FileUtils::fileExists($serviceFile))
        {
            return;
        }

        $content = file_get_contents($serviceFile);

        $funcPos = strpos($content, $funcName.'(data)');
        if ($funcPos == false)
        {
            return;
        }

        $pos = strpos($content, 'ENGINE_URI, {'.PHP_EOL, $funcPos);
        if ($pos == false)
        {
            return;
        }
        $pos += 12;

        $str = '{'.PHP_EOL;
        $str .= '            fetch: {'.PHP_EOL;
        foreach ($data as $type)
        {
            $str .= '                '.$type['name'].': {'.PHP_EOL;
            $str .= '                    fields: {'.PHP_EOL;
            foreach ($type['fields'] as $field)
            {
                $str .= '                        '.$field['name'].': {},'.PHP_EOL;
            }
            $str .= '                    },'.PHP_EOL;
            $str .= '                },'.PHP_EOL;
        }
        $str .= '            }'.PHP_EOL;
        $str .= '        })';

        $part1 = substr($content, 0, $pos);
        $part2 = substr($content, $pos);
        $part2 = preg_replace('/{[\r\n]?(.*)[\r\n]?\s*}\)/sU', $str, $part2, 1);
        FileUtils::writeFile($serviceFile, $part1.$part2);
    }
}