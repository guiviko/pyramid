<?php

namespace App\Domain\Project\Editors\Client;

use App\Domain\Project\CurrentProject;
use App\Domain\Project\Editors\ProjectEditor;

class ClientProjectEditor extends ProjectEditor
{
    /**
     * @var ModuleEditor
     */
    private $module;

    /**
     * @var PageEditor
     */
    private $page;

    /**
     * @var ComponentEditor
     */
    private $component;

    /**
     * @var DocumentEditor
     */
    private $document;

    /**
     * @var ActionEditor
     */
    private $action;

    /**
     * @var ReducerEditor
     */
    private $reducer;

    /**
     * @var EffectEditor
     */
    private $effect;

    /**
     * @var ServiceEditor
     */
    private $service;

    /**
     * @var TemplateEditors
     */
    private $template;

    public function __construct(CurrentProject $currentProject, ModuleEditor $module, PageEditor $page,
                                ComponentEditor $component, DocumentEditor $document, ActionEditor $action,
                                ReducerEditor $reducer, EffectEditor $effect, ServiceEditor $service,
                                TemplateEditors $template)
    {
        parent::__construct($currentProject);

        $this->module = $module;
        $this->page = $page;
        $this->component = $component;
        $this->document = $document;
        $this->action = $action;
        $this->reducer = $reducer;
        $this->effect = $effect;
        $this->service = $service;
        $this->template = $template;
    }

    public function module()
    {
        return $this->module;
    }

    public function page()
    {
        return $this->page;
    }

    public function component()
    {
        return $this->component;
    }

    public function document()
    {
        return $this->document;
    }

    public function action()
    {
        return $this->action;
    }

    public function reducer()
    {
        return $this->reducer;
    }

    public function effect()
    {
        return $this->effect;
    }

    public function service()
    {
        return $this->service;
    }

    public function template()
    {
        return $this->template;
    }
}