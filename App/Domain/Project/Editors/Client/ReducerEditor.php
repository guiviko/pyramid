<?php

namespace App\Domain\Project\Editors\Client;

use App\Domain\Project\Editors\Editor;
use App\Domain\Utils\FileUtils;
use App\Domain\Utils\PathUtils;

class ReducerEditor extends Editor
{
    public function addActionToField(string $actionGroup, string $actionName, string $reducerGroup,
                                     string $reducerField)
    {
        $filePath = $this->currentProject->reducerRoot().'/'.$reducerGroup.'.reducer.ts';
        $content = FileUtils::readFile($filePath);

        $content = $this->tsProcessor->addImport(ucfirst($actionGroup).'Actions', '../actions/'.$actionGroup.'.actions', $content);
        $content = $this->tsProcessor->addActionToReducerField($actionGroup, $actionName, $reducerField, $content);
        $content = $this->tsProcessor->addActionCallToMainReducer($actionGroup, $actionName, $reducerField, $content);

        FileUtils::writeFile($filePath, $content);
    }

    public function addField(string $reducerGroup, string $field, string $type, string $default)
    {
        $filePath = $this->currentProject->reducerRoot().'/'.$reducerGroup.'.reducer.ts';
        if (!FileUtils::fileExists($filePath))
        {
            $this->addReducerFile($reducerGroup);
        }

        $content = FileUtils::readFile($filePath);

        $content = $this->tsProcessor->addReducerField($field, $type, $default, $content);

        FileUtils::writeFile($filePath, $content);

        $this->addFieldSelector($reducerGroup, $field);
    }

    public function addReducerFile(string $name)
    {
        $filePath = $this->currentProject->reducerRoot().'/'.$name.'.reducer.ts';
        if (FileUtils::fileExists($filePath))
        {
            return;
        }

        $content = FileUtils::readFile(PathUtils::reducerTemplateRoot.'/basic.ts');
        $content = str_replace('stateName0', ucfirst($name).'State', $content);
        $content = str_replace('reducerName0', $name.'Reducer', $content);
        FileUtils::writeFile($filePath, $content);

        $this->addToIndex($name);

        $this->addSelectorFile($name);
    }

    private function addToIndex(string $name)
    {
        $stateName = ucfirst($name).'State';
        $reducerName = $name.'Reducer';

        $indexFile = $this->currentProject->reducerRoot().'/index.ts';
        $content = file_get_contents($indexFile);

        $importFrom = './'.$name.'.reducer';
        $content = $this->tsProcessor->addImport($stateName, $importFrom, $content);
        $content = $this->tsProcessor->addImport($reducerName, $importFrom, $content);

        preg_match('/export interface AppState ({)/', $content, $matches, PREG_OFFSET_CAPTURE);
        $offset = $matches[1][1] + 2;
        if (strpos(substr($content, $offset), $stateName) === false)
        {
            $content = substr($content, 0, $offset).'    '.$name.': '.$stateName.','.PHP_EOL.substr($content, $offset);

            preg_match('/export const reducers = ({)/', $content, $matches, PREG_OFFSET_CAPTURE);
            $offset = $matches[1][1] + 2;
            $content = substr($content, 0, $offset).'    '.$name.': '.$reducerName.','.PHP_EOL.substr($content, $offset);
        }

        FileUtils::writeFile($indexFile, $content);
    }

    private function addSelectorFile($reducerGroup)
    {
        $filePath = $this->currentProject->selectorRoot().'/'.$reducerGroup.'.selectors.ts';
        if (FileUtils::fileExists($filePath))
        {
            return;

        }
        $content = FileUtils::readFile(PathUtils::selectorTemplateRoot.'/basic.ts');
        $content = str_replace('className0', ucfirst($reducerGroup).'Selectors', $content);
        FileUtils::writeFile($filePath, $content);

        $this->addSelectorToIndex($reducerGroup);
    }

    private function addSelectorToIndex($reducerGroup)
    {
        $name = ucfirst($reducerGroup).'Selectors';

        $indexFile = $this->currentProject->selectorRoot().'/index.ts';
        $content = file_get_contents($indexFile);

        $importFrom = './'.$reducerGroup.'.selectors';
        $content = $this->tsProcessor->addImport($name, $importFrom, $content);

        preg_match('/export\sdefault\s(\[)/', $content, $matches, PREG_OFFSET_CAPTURE);
        $offset = $matches[1][1] + 2;
        if (strpos(substr($content, $offset), $name) === false)
        {
            $content = substr($content, 0, $offset).'    '.$name.','.PHP_EOL.substr($content, $offset);
        }

        FileUtils::writeFile($indexFile, $content);
    }

    private function addFieldSelector($reducerGroup, $reducerField)
    {
        $filePath = $this->currentProject->selectorRoot().'/'.$reducerGroup.'.selectors.ts';
        if (!FileUtils::fileExists($filePath))
        {
            return;
        }

        $content = FileUtils::readFile($filePath);
        $content = $this->tsProcessor->addFieldSelector($reducerGroup, $reducerField, $content);
        FileUtils::writeFile($filePath, $content);
    }

    public function saveContent($reducerName, $fieldName, $actionGroup, $actionName, $actionContent)
    {
        $filePath = $this->currentProject->reducerRoot().'/'.$reducerName.'.reducer.ts';
        $content = FileUtils::readFile($filePath);

        $content = $this->tsProcessor->saveReducerContent($fieldName, $actionGroup, $actionName, $actionContent, $content);

        FileUtils::writeFile($filePath, $content);
    }

    public function disconnectOutput($reducerGroup, $reducerField, $actionGroup, $actionName)
    {
        $filePath = $this->currentProject->reducerRoot().'/'.$reducerGroup.'.reducer.ts';
        $content = FileUtils::readFile($filePath);

        $content = $this->tsProcessor->disconnectOutput($reducerField, $actionGroup, $actionName, $content);
        $content = $this->tsProcessor->removeActionCallFromMainReducer($reducerField, $actionGroup, $actionName, $content);

        FileUtils::writeFile($filePath, $content);
    }
}