<?php

namespace App\Domain\Models;

use Library\DataMapper\DataMapperPrimaryKey;

/** @Entity */
class SchemaType
{
    use DataMapperPrimaryKey;

    /** @Column(type="string") */
    private $name;

    /** @HasMany(target="App\Domain\Models\SchemaField", mappedBy="type") */
    private $fields;

    /** @BelongsTo(target="App\Domain\Models\Projects\Project") */
    private $project;

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getSchemaField()
    {
        return $this->fields;
    }

    public function getProjec()
    {
        return $this->project;
    }

    public function setProject($project)
    {
        $this->project = $project;
    }
}