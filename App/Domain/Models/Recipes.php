<?php

namespace App\Domain\Models;

use Library\DataMapper\DataMapperPrimaryKey;

/** @Entity */
class Recipes
{
    use DataMapperPrimaryKey;

    /** @Column(type="string") */
    private $name;

    /** @Column(type="string") */
    private $cat;

    /** @Column(type="integer") */
    private $prepTime;

    /** @HasMany(target="App\Domain\Models\Ingredients", mappedBy="recipes") */
    private $ingredients;

    public function __construct($name, $cat, $prepTime, $ingredients)
    {
        $this->name = $name;
        $this->cat = $cat;
        $this->prepTime = $prepTime;
        $this->ingredients = $ingredients;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getCat()
    {
        return $this->cat;
    }

    public function getPrepTime()
    {
        return $this->prepTime;
    }

    public function getIngredients()
    {
        return $this->ingredients;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function setCat(string $cat)
    {
        $this->cat = $cat;
    }

    public function setPrepTime(int $prepTime)
    {
        $this->prepTime = $prepTime;
    }

    public function setIngredients($ingredients)
    {
        $this->ingredients = $ingredients;
    }
}