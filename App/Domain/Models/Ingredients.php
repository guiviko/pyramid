<?php

namespace App\Domain\Models;

use Library\DataMapper\DataMapperPrimaryKey;

/** @Entity */
class Ingredients
{
    use DataMapperPrimaryKey;

    /** @Column(type="string") */
    private $name;

    /** @Column(type="integer") */
    private $amount;

    /** @Column(type="string") */
    private $unit;

    /** @BelongsTo(target="App\Domain\Models\Recipes") */
    private $recipes;

    public function __construct($name, $amount, $unit, $recipe)
    {
        $this->name = $name;
        $this->amount = $amount;
        $this->unit = $unit;
        $this->recipes = $recipe;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getUnit()
    {
        return $this->unit;
    }

    public function getRecipes()
    {
        return $this->recipes;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    public function setUnit($unit)
    {
        $this->unit = $unit;
    }

    public function setRecipes($recipes)
    {
        $this->recipes = $recipes;
    }
}