<?php

namespace App\Domain\Models;

use Library\DataMapper\DataMapperPrimaryKey;

/** @Entity */
class SchemaField
{
    use DataMapperPrimaryKey;

    /** @Column(type="string") */
    private $name;

    /** @Column(type="string") */
    private $type;

    /** @BelongsTo(target="App\Domain\Models\SchemaType") */
    private $schemaType;

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getSchemaType()
    {
        return $this->schemaType;
    }

    public function setSchemaType($schemaType)
    {
        $this->schemaType = $schemaType;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }
}