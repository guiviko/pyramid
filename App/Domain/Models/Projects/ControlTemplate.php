<?php

namespace App\Domain\Models\Projects;

use Library\DataMapper\DataMapperPrimaryKey;

/** @Entity */
class ControlTemplate
{
    use DataMapperPrimaryKey;

    /** @Column(type="string") */
    private $name;

    /** @Column(type="string") */
    private $icon;

    /** @Column(type="json") */
    private $config;

    public function __construct(string $name, string $icon, array $config)
    {
        $this->name = $name;
        $this->icon = $icon;
        $this->config = $config;
    }

    public function name()
    {
        return $this->name;
    }

    public function icon()
    {
        return $this->icon;
    }

    public function config()
    {
        return $this->config;
    }
}