<?php

namespace App\Domain\Models\Projects;

use Library\DataMapper\DataMapperPrimaryKey;
use Library\DataMapper\DataMapperTimestamps;

/** @Entity */
class ComponentTemplate
{
    use DataMapperPrimaryKey, DataMapperTimestamps;

    /** @Column(type="string") */
    private $name;

    /** @Column(type="string") */
    private $icon;

    /** @Column(type="string") */
    private $directoryPath;

    /** @Column(type="json") */
    private $emptyConfig;

    /** @Column(type="json") */
    private $blockAreas;

    /** @Column(type="json") */
    private $metadata;

    public function __construct(string $name, string $icon, string $directoryPath, array $emptyConfig,
                                array $blockAreas, array $metadata)
    {
        $this->name = $name;
        $this->icon = $icon;
        $this->directoryPath = $directoryPath;
        $this->emptyConfig = $emptyConfig;
        $this->blockAreas = $blockAreas;
        $this->metadata = $metadata;
    }

    public function name()
    {
        return $this->name;
    }

    public function icon()
    {
        return $this->icon;
    }

    public function directoryPath()
    {
        return $this->directoryPath;
    }

    public function emptyConfig()
    {
        return $this->emptyConfig;
    }

    public function blockAreas()
    {
        return $this->blockAreas;
    }

    public function metadata()
    {
        return $this->metadata;
    }
}