<?php

namespace App\Domain\Models\Projects;

use Library\DataMapper\DataMapperPrimaryKey;
use Library\DataMapper\DataMapperTimestamps;

/** @Entity */
class Page
{
    use DataMapperPrimaryKey, DataMapperTimestamps;

    /** @Column(type="string") */
    private $name;

    /** @Column(type="string") */
    private $uri;

    /** @Column(type="string") */
    private $dotChain;

    /** @Column(type="string) */
    private $directoryPath;

    /** @BelongsTo(target="App\Domain\Models\Projects\Module") */
    private $module;

    /** @BelongsTo(target="App\Domain\Models\Projects\Component", cascade="delete") */
    private $component;

    public function __construct(Module $module, Component $component, string $name, string $uri, string $dotChain, string $directoryPath)
    {
        $this->module = $module;
        $this->component = $component;
        $this->name = $name;
        $this->uri = $uri;
        $this->dotChain = $dotChain;
        $this->directoryPath = $directoryPath;
    }

    public function name()
    {
        return $this->name;
    }

    public function uri()
    {
        return $this->uri;
    }

    public function dotChain()
    {
        return $this->dotChain;
    }

    public function directoryPath()
    {
        return $this->directoryPath;
    }

    public function module()
    {
        return $this->module;
    }

    public function component()
    {
        return $this->component;
    }
}