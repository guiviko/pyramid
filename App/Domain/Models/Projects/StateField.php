<?php

namespace App\Domain\Models\Projects;

use Library\DataMapper\DataMapperPrimaryKey;
use Library\DataMapper\DataMapperTimestamps;

/** @Entity */
class StateField
{
    use DataMapperPrimaryKey, DataMapperTimestamps;

    /** @Column(type="string") */
    private $name;

    /** @BelongsTo(target="App\Domain\Models\Projects\ActionGroup") */
    private $actionGroup;

    /** @Column(type="string") */
    private $type;

    public function __construct($name, $type, $actionGroup)
    {
        $this->name = $name;
        $this->type = $type;
        $this->actionGroup = $actionGroup;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getActionGroup()
    {
        return $this->actionGroup;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }
}