<?php

namespace App\Domain\Models\Projects;

use Library\DataMapper\Collection\EntityCollection;
use Library\DataMapper\DataMapperPrimaryKey;
use Library\DataMapper\DataMapperTimestamps;

/** @Entity */
class Component
{
    use DataMapperPrimaryKey, DataMapperTimestamps;

    /** @Column(type="string") */
    private $name;

    /** @Column(type="string", nullable="true") */
    private $dotChain;

    /** @Column(type="string") */
    private $directoryPath;

    /** @Column(type="json") */
    private $config;

    /** @BelongsTo(target="App\Domain\Models\Projects\ComponentTemplate") */
    private $template;

    /** @Column(type="json") */
    private $blockAreas;

    /** @Column(type="json") */
    private $metadata;

    public function __construct(ComponentTemplate $template, string $name, string $directoryPath, string $dotChain,
                                array $config, array $blockAreas, array $metadata)
    {
        $this->template = $template;
        $this->name = $name;
        $this->directoryPath = $directoryPath;
        $this->dotChain = $dotChain;
        $this->config = $config;
        $this->blockAreas = $blockAreas;
        $this->metadata = $metadata;
        $this->components = new EntityCollection();
    }

    public function template()
    {
        return $this->template;
    }

    public function directoryPath()
    {
        return $this->directoryPath;
    }

    public function name()
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function dotChain()
    {
        return $this->dotChain;
    }

    public function config()
    {
        return $this->config;
    }

    public function setConfig(array $config)
    {
        $this->config = $config;
    }

    public function blockAreas()
    {
        return $this->blockAreas;
    }

    public function setBlockAreas(array $blockAreas)
    {
        $this->blockAreas = $blockAreas;
    }

    public function metadata()
    {
        return $this->metadata;
    }

    public function setMetadata(array $metadata)
    {
        $this->metadata = $metadata;
    }
}