<?php

namespace App\Domain\Models\Projects;

use Library\DataMapper\Collection\EntityCollection;
use Library\DataMapper\DataMapperPrimaryKey;
use Library\DataMapper\DataMapperTimestamps;

/** @Entity */
class Module
{
    use DataMapperPrimaryKey, DataMapperTimestamps;

    /** @Column(type="string") */
    private $name;

    /** @Column(type="string") */
    private $uri;

    /** @Column(type="string") */
    private $dotChain;

    /** @Column(type="string) */
    private $directoryPath;

    /** @HasMany(target="App\Domain\Models\Projects\Page", mappedBy="module", cascade="delete") */
    private $pages;

    /** @BelongsTo(target="App\Domain\Models\Projects\Project") */
    private $project;

    public function __construct(Project $project, string $name, string $uri, string $dotChain, string $directoryPath)
    {
        $this->project = $project;
        $this->name = $name;
        $this->uri = $uri;
        $this->dotChain = $dotChain;
        $this->directoryPath = $directoryPath;

        $this->pages = new EntityCollection();
    }

    public function name()
    {
        return $this->name;
    }

    public function uri()
    {
        return $this->uri;
    }

    public function project()
    {
        return $this->project;
    }

    public function dotChain()
    {
        return $this->dotChain;
    }

    public function directoryPath()
    {
        return $this->directoryPath;
    }

    public function pages()
    {
        return $this->pages;
    }

    public function isRoot()
    {
        return $this->dotChain == '';
    }
}