<?php

namespace App\Domain\Models\Projects;

use Library\DataMapper\Collection\EntityCollection;
use Library\DataMapper\DataMapperPrimaryKey;
use Library\DataMapper\DataMapperTimestamps;

/** @Entity */
class ActionGroup
{
    use DataMapperPrimaryKey, DataMapperTimestamps;

    /** @Column(type="string") */
    private $name;

    /** @HasMany(target="App\Domain\Models\Projects\Action", mappedBy="actionGroup") */
    private $actions;

    /** @HasMany(target="App\Domain\Models\Projects\StateField", mappedBy="actionGroup") */
    private $stateFields;

    /** @BelongsTo(target="App\Domain\Models\Projects\Project") */
    private $project;

    public function __construct($name, $project)
    {
        $this->name = $name;
        $this->project = $project;
        $this->actions = new EntityCollection();
        $this->stateFields = new EntityCollection();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getActions()
    {
        return $this->actions;
    }

    public function getStateFields()
    {
        return $this->stateFields;
    }

    public function project()
    {
        return $this->project;
    }
}