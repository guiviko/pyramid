<?php

namespace App\Domain\Models\Projects;

use Library\DataMapper\Collection\EntityCollection;
use Library\DataMapper\DataMapperPrimaryKey;
use Library\DataMapper\DataMapperTimestamps;

/** @Entity */
class Project
{
    use DataMapperPrimaryKey, DataMapperTimestamps;

    /** @Column(type="string") */
    private $name;

    /** @Column(type="json") */
    private $clientMap;

    /** @HasMany(target="App\Domain\Models\Projects\Module", mappedBy="project", cascade="delete") */
    private $modules;

    /** @HasMany(target="App\Domain\Models\Projects\ActionGroup", mappedBy="project") */
    private $actionGroups;

    public function __construct(string $name)
    {
        $this->name = $name;

        $this->modules = new EntityCollection();
        $this->clientMap = [];

        $this->actionGroups = new EntityCollection();
    }

    public function name()
    {
        return $this->name;
    }

    public function clientMap()
    {
        return $this->clientMap;
    }

    public function setClientMap(array $map)
    {
        $this->clientMap = $map;
    }

    public function modules()
    {
        return $this->modules;
    }

    public function appModule()
    {
        foreach ($this->modules as $module)
        {
            if ($module->name() == 'app')
            {
                return $module;
            }
        }

        return null;
    }

    public function getActionGroups()
    {
        return $this->actionGroups;
    }
}