<?php

namespace App\Domain\Models\Projects;

use Library\DataMapper\DataMapperPrimaryKey;
use Library\DataMapper\DataMapperTimestamps;

/** @Entity */
class Action
{
    use DataMapperPrimaryKey, DataMapperTimestamps;

    /** @Column(type="string") */
    private $name;

    /** @BelongsTo(target="App\Domain\Models\Projects\ActionGroup") */
    private $actionGroup;

    /** @Column(type="json") */
    private $map;

    public function __construct($name, $actionGroup)
    {
        $this->name = $name;
        $this->actionGroup = $actionGroup;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getActionGroup()
    {
        return $this->actionGroup;
    }

    public function getMap()
    {
        return $this->map;
    }

    public function setMap($map)
    {
        $this->map = $map;
    }
}