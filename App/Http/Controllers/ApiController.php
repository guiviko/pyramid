<?php

namespace App\Http\Controllers;

use Library\Transformer\Transformer;
use App\Http\Requests\Api\FetchRequest;
use App\Domain\Services\ProjectService;
use App\Http\Requests\Api\CreateRequest;
use App\Http\Requests\Api\DestroyRequest;

abstract class ApiController extends Controller
{
    /**
     * @var Transformer
     */
    protected $transformer;

    public function __construct(Transformer $transformer)
    {
        $this->transformer = $transformer;
    }
}