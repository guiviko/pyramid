<?php

namespace App\Http\Controllers;

use Library\Engine\EngineController;
use App\Domain\Utils\FileUtils;
use App\Domain\Utils\StringUtils;
use App\Domain\Generators\ModelGenerator;

class SchemaTypeController extends EngineController
{
    const CONTROLLER_RELATIVE_ROOT = '../App/Http/Controllers';
    const CONTROLLER_NAMESPACE = 'App\Http\Controllers';
    const MODEL_RELATIVE_ROOT = '../App/Domain/Models';
    const MODEL_NAMESPACE = 'App\Domain\Models';
    const DATAMAPPER_CONFIG_FILE = '../Config/Datamapper/datamapper.php';

    public function onCreate($event, ModelGenerator $modelGenerator)
    {
        $str = '<?php'.PHP_EOL.PHP_EOL;
        $str .= 'namespace '.self::CONTROLLER_NAMESPACE.';'.PHP_EOL.PHP_EOL;
        $str .= 'use Library\Engine\EngineController;'.PHP_EOL.PHP_EOL;
        $str .= 'class '.ucfirst($event->entity()->getName()).'Controller extends EngineController'.PHP_EOL;
        $str .= '{'.PHP_EOL.PHP_EOL;
        $str .= '}'.PHP_EOL;
        FileUtils::writeFile(self::CONTROLLER_RELATIVE_ROOT.'/'.ucfirst($event->entity()->getName()).'Controller.php', $str);

        $modelStr = $modelGenerator->generate($event->entity()->getName(), []);
        FileUtils::writeFile(self::MODEL_RELATIVE_ROOT.'/'.ucfirst($event->entity()->getName()).'.php', $modelStr);

        $configContent = FileUtils::readFile(self::DATAMAPPER_CONFIG_FILE);
        $classesPos = strpos($configContent, '\'classes\' => [') + 14;
        $offset = strpos($configContent, ']', $classesPos);
        $str = '    '.self::MODEL_NAMESPACE.'\\'.ucfirst($event->entity()->getName()).'::class,'.PHP_EOL.'    ';
        $configContent = FileUtils::insertContent($configContent, $offset, $str);
        FileUtils::writeFile(self::DATAMAPPER_CONFIG_FILE, $configContent);
    }

    public function onDelete($event)
    {
        $controllerFile = self::CONTROLLER_RELATIVE_ROOT.'/'.ucfirst($event->entity()->getName()).'Controller.php';
        if (FileUtils::fileExists($controllerFile))
        {
            FileUtils::deleteFile($controllerFile);
        }

        $modelFile = self::MODEL_RELATIVE_ROOT.'/'.ucfirst($event->entity()->getName()).'.php';
        if (FileUtils::fileExists($modelFile))
        {
            FileUtils::deleteFile($modelFile);
        }

        $configContent = FileUtils::readfile(self::DATAMAPPER_CONFIG_FILE);
        $str = self::MODEL_NAMESPACE.'\\'.ucfirst($event->entity()->getName()).'::class';
        $configContent = StringUtils::removeLinesContainingString($configContent, $str);
        FileUtils::writeFile(self::DATAMAPPER_CONFIG_FILE, $configContent);
    }
}