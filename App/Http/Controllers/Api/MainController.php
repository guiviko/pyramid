<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CreateProjectRequest;
use App\Http\Requests\Api\ParseRequest;
use Library\Http\Response;
use Library\Utils\FileUtils;
use Library\Utils\StringUtils;

class MainController extends Controller
{
    // ADD
    // ============
    //+ imports in all components
    //+ imports in page
    //+ output in all components
    //+ html in all components except leaf
    //+ html in page
    // action call in page ts
    // action in actions file
    // import of actions in reducer
    // default action in main reducer function

    // REMOVE
    // ===========
    //+ output in all components
    //+ html in all components except leaf
    //+ html in page
    // * if page included:
    // ... action call in page ts
    // * if reducer included:
    // ... action in main reducer function
    // ... action in all other functions

    private const scriptsDir = '/var/www/html/pyramid/Storage/Scripts';
    private const projectsDir = '/var/www/html';
    private const root = '/var/www/html/reciper/src/app';
    private const pagesDir = self::root.'/pages';
    private const componentDir = self::root.'/components';
    private const reducerDir = self::root.'/reducers';

    private $excludeTags = [
        'div', 'a', 'p', 'span', 'b', 'i', 'input', 'pre', 'button', 'table', 'tr', 'td', 'th', 'tbody', 'img',
        'ng-content'
    ];

    public function projects(ParseRequest $request)
    {
        $dirs = [];
        $files = scandir(self::projectsDir);
        foreach ($files as $file)
        {
            if ($file == '.' || $file == '..' || !is_dir(self::projectsDir.'/'.$file))
            {
                continue;
            }

            $dirs[] = $file;
        }

        return new Response(Response::STATUS_OK, $dirs);
    }

    public function createProject(CreateProjectRequest $request)
    {
        $name = $request->data('name');
        $initScript = self::scriptsDir.'/pyramid-init';

        exec('sudo '.$initScript.' '.$name.' 2>&1', $output);

        return new Response(Response::STATUS_OK, $output);
    }

    public function deleteProject(CreateProjectRequest $request)
    {
        $name = $request->data('name');
        $destroyScript = self::scriptsDir.'/pyramid-destroy';

        exec('sudo '.$destroyScript.' '.$name.' 2>&1', $output);

        return new Response(Response::STATUS_OK, $output);
    }

    public function pages(ParseRequest $request)
    {
        $files = scandir(self::pagesDir);
        $pages = [];
        foreach ($files as $file)
        {
            if ($file == '.' || $file == '..')
            {
                continue;
            }

            $pages[] = $file;
        }

        return new Response(Response::STATUS_OK, $pages);
    }

    public function reducers(ParseRequest $request)
    {
        $files = scandir(self::reducerDir);
        $reducers = [];
        foreach ($files as $file)
        {
            if ($file == '.' || $file == '..' || $file == 'index.ts')
            {
                continue;
            }

            $parts = explode('.', $file);
            if (sizeof($parts) != 3)
            {
                continue;
            }

            $reducers[] = $this->parseReducer($file);
        }

        return new Response(Response::STATUS_OK, $reducers);
    }

    private function parseReducer($file)
    {
        $content = FileUtils::readFile($file);

        return [
            'name' => explode('.', $file)[0],
            'fields' => $this->parseReducerFields($content)
        ];
    }

    private function parseReducerFields($content)
    {
        preg_match('/interface\s\w+State\s{[\r\n]+(.*)}/', $content, $matches);
        if (sizeof($matches) == 0)
        {
            return [];
        }
        
        $fieldStrings = explode(PHP_EOL, $matches[1][0]);
        $fields = [];
        foreach ($fieldStrings as $fieldString)
        {
            $fields[] = $this->parseReducerField($content, $fieldString);
        }

        return $fields;
    }

    private function parseReducerField($content, $fieldString)
    {
        $parts = explode(':', $fieldString);
        $name = $parts[0];
        $type = trim(str_replace(';', '', $parts[1]));

        preg_match('/const\s'.$name.'\s=\s.*=>\s{\s+switch\s\(action.type\)\s{\s(.*)\s+default:/sU', $content, $matches);

        return [
            'name' => $name,
            'type' => $type,
            'actions' => $this->parseReducerFieldActions($matches[1][0])
        ];
    }

    private function parseReducerFieldActions($caseContent)
    {
        preg_match('/case\s(\w+)Actions.(\w+):/sU', $caseContent, $matches);
        if (sizeof($matches) == 0)
        {
            return [];
        }

        $actions = [];

        for ($i = 1; $i < sizeof($maches); $i++)
        {
            $actions[] = $this->parseReducerFieldAction($caseContent, $matches[$i][0], $matches[$i][1]);
        }

        return $actions;
    }

    private function parseReducerFieldAction($caseContent, $actionGroup, $actionName)
    {
        preg_match('/case\s'.$actionGroup.'\.'.$actionName.':\s+(.*)\s+case/sU', $caseContent, $matches);

        return [
            'actionGroup' => lcfirst($actionGroup),
            'actionName' => StringUtils::constantToCamelCase($actionName),
            'content' => $matches[1][0]
        ];
    }

    public function page(ParseRequest $request)
    {
        return new Response(Response::STATUS_OK, [
            'page' => $request->data('pageName'),
            'components' => $this->buildPageComponentTree($request->data('pageName'))
        ]);
    }

    public function addAction(ParseRequest $request)
    {
        $pageName = $request->data('pageName');
        $componentName = $request->data('componentName');
        $reducerName = $request->data('reducerName');
        $actionName = $request->data('actionName');

        $path = $this->buildComponentPath($pageName, $componentName);

        $this->addOutputToComponents($path, $actionName);
        $this->addHtmlActionToMidComponents($path, $actionName);

        $this->addActionToPage($pageName, $actionName, $reducerName, $path);
        // add action to actions file
        // add action to reducer

        return new Response(Response::STATUS_OK);
    }

    public function deleteAction(ParseRequest $request)
    {
        $pageName = $request->data('pageName');
        $componentName = $request->data('componentName');
        $actionName = $request->data('actionName');
        $includePage = $request->data('includePage');
        $includeReducer = $request->data('includeReducer');

        $path = $this->buildComponentPath($pageName, $componentName);

        $this->removeOutputFromComponents($path, $actionName);
        $this->removeHtmlActionFromMidComponents($path, $actionName);
        $m = $this->removeHtmlActionFromPage($pageName, $path, $actionName);

        return new Response(Response::STATUS_OK, $m);
    }

    private function removeHtmlActionFromMidComponents($path, $action)
    {
        if (sizeof($path) <= 1)
        {
            return;
        }

        for ($i = sizeof($path) - 2; $i >= 0; $i--)
        {
            $compDash = $this->camelCaseToDash($path[$i + 1]);
            $file = self::componentDir.'/'.$path[$i].'/'.$path[$i].'.html';
            $content = file_get_contents($file);

            // check if exists
            preg_match('/(<'.$compDash.'.*\('.$action.'\)=.*><\/'.$compDash.'>)/s', $content, $matches);
            if (sizeof($matches) == 0)
            {
                continue;
            }

            preg_match('/(<'.$compDash.'.*(\()'.$action.'\)=.*(")><\/'.$compDash.'>)/s', $content, $matches, PREG_OFFSET_CAPTURE);
            $startOffset = $matches[2][1];
            $endOffset = $matches[3][1];
            $modifiedContent = substr($content, 0, $startOffset - 1).substr($content, $endOffset + 1);
            file_put_contents($file, $modifiedContent);
        }
    }

    private function removeHtmlActionFromPage($page, $path, $action)
    {
        if (sizeof($path) == 0)
        {
            return;
        }

        $compDash = $this->camelCaseToDash($path[0]);
        $file = self::pagesDir.'/'.$page.'/'.$page.'.page.html';
        $content = file_get_contents($file);

        // check if exists
        preg_match('/(<'.$compDash.'.*\('.$action.'\)=.*><\/'.$compDash.'>)/s', $content, $matches);
        if (sizeof($matches) == 0)
        {
            return;
        }

        preg_match('/(<'.$compDash.'.*(\()'.$action.'\)=.*(")><\/'.$compDash.'>)/s', $content, $matches, PREG_OFFSET_CAPTURE);
        $startOffset = $matches[2][1];
        $endOffset = $matches[3][1];
        $modifiedContent = substr($content, 0, $startOffset - 1).substr($content, $endOffset + 1);
        file_put_contents($file, $modifiedContent);
    }

    private function removeOutputFromComponents($path, $action)
    {
        foreach ($path as $component)
        {
            $file = self::componentDir.'/'.$component.'/'.$component.'.ts';
            $content = file_get_contents($file);

            // check import
            preg_match('/import.*Output/', $content, $matches);
            if (sizeof($matches) > 0)
            {
                // if there are no other outputs in the component, remove the import of output and eventemitter
                //preg_match('/import.*(Output)/', $content, $matches, PREG_OFFSET_CAPTURE);
            }

            // check if exists
            preg_match('/(@)Output\(\)\s'.$action.'.*(;)/', $content, $matches, PREG_OFFSET_CAPTURE);
            if (sizeof($matches) == 0)
            {
                continue;
            }

            // remove output
            $startOffset = $matches[1][1];
            $endOffset = $matches[2][1];

            $modifiedTs = substr($content, 0, $startOffset - 5).substr($content, $endOffset + 1);
            file_put_contents($file, $modifiedTs);
        }
    }

    private function addHtmlActionToMidComponents($path, $action)
    {
        if (sizeof($path) <= 1)
        {
            return;
        }

        for ($i = sizeof($path) - 2; $i >= 0; $i--)
        {
            $compDash = $this->camelCaseToDash($path[$i + 1]);
            $file = self::componentDir.'/'.$path[$i].'/'.$path[$i].'.html';
            $content = file_get_contents($file);

            // check if exists
            preg_match('/(<'.$compDash.'.*\('.$action.'\)=.*><\/'.$compDash.'>)/s', $content, $matches);
            if (sizeof($matches) > 0)
            {
                continue;
            }

            preg_match('/(<'.$compDash.'.*(>)<\/'.$compDash.'>)/s', $content, $matches, PREG_OFFSET_CAPTURE);
            $caretOffset = $matches[2][1];
            $modifiedContent = substr($content, 0, $caretOffset).' ('.$action.')="'.$action.'.emit($event)"'.substr($content, $caretOffset);
            file_put_contents($file, $modifiedContent);
        }
    }

    private function addOutputToComponents($path, $action)
    {
        foreach ($path as $component)
        {
            $file = self::componentDir.'/'.$component.'/'.$component.'.ts';
            $content = file_get_contents($file);

            // check import
            preg_match('/import.*Output/', $content, $matches);
            if (sizeof($matches) == 0)
            {
                preg_match('/import.*(})\sfrom \'@angular\/core\';/', $content, $matches, PREG_OFFSET_CAPTURE);
                $content = substr($content, 0, $matches[1][1]).', Output'.substr($content, $matches[1][1]);
            }

            preg_match('/import.*EventEmitter/', $content, $matches);
            if (sizeof($matches) == 0)
            {
                preg_match('/import.*(})\sfrom \'@angular\/core\';/', $content, $matches, PREG_OFFSET_CAPTURE);
                $content = substr($content, 0, $matches[1][1]).', EventEmitter'.substr($content, $matches[1][1]);
            }

            // check if exists
            preg_match('/@Output\(\)\s'.$action.'/', $content, $matches);
            if (sizeof($matches) > 0)
            {
                continue;
            }

            // do output
            preg_match_all('/@Output.*(\r?\n)/', $content, $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
            $prevLine = end($matches);
            if (!$prevLine)
            {
                preg_match_all('/@Input.*(\r?\n)/', $content, $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
                $prevLine = end($matches);
                if (!$prevLine)
                {
                    preg_match_all('/export\sclass.*(\r?\n)/', $content, $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
                    $prevLine = end($matches);
                }
            }

            $prevLineOffset = $prevLine[1][1];

            $modifiedTs = substr($content, 0, $prevLineOffset + 1).'    @Output() '.$action.' = new EventEmitter<any>();'.PHP_EOL.substr($content, $prevLineOffset + 1);
            file_put_contents($file, $modifiedTs);
        }
    }

    private function addActionToPage($page, $action, $reducer, $path)
    {
        if (sizeof($path) == 0)
        {
            return 'not implemented yet';
            // case when action is added only on the page
        }

        $component = $this->camelCaseToDash($path[0]);

        // check if reducer is imported

        // add action in html
        $html = self::pagesDir.'/'.$page.'/'.$page.'.page.html';
        $htmlContent = file_get_contents($html);

        // check if exists
        preg_match('/<'.$component.'.*\('.$action.'\)=.*><\/'.$component.'>/s', $htmlContent, $matches);
        if (sizeof($matches) == 0)
        {
            preg_match('/(<'.$component.'.*(>)<\/'.$component.'>)/s', $htmlContent, $matches, PREG_OFFSET_CAPTURE);
            $caretOffset = $matches[2][1];
            $modifiedHtml = substr($htmlContent, 0, $caretOffset).' ('.$action.')="'.$action.'($event)"'.substr($htmlContent, $caretOffset);
            file_put_contents($html, $modifiedHtml);
        }

        // add action in ts
        $ts = self::pagesDir.'/'.$page.'/'.$page.'.page.ts';
        $tsContent = file_get_contents($ts);
        // ...
    }

    private function buildComponentPath($page, $component)
    {
        $html = self::pagesDir.'/'.$page.'/'.$page.'.page.html';
        $path = $this->buildComponentPathRec($html, $component);
        return explode('.', $path);
    }

    private function buildComponentPathRec($html, $component)
    {
        $componentNames = $this->getComponentNames($html);
        foreach ($componentNames as $name)
        {
            if ($name == $component)
            {
                return $component;
            }

            $path = $this->buildComponentPathRec(self::componentDir.'/'.$name.'/'.$name.'.html', $component);
            if ($path != '')
            {
                return $name.'.'.$path;
            }
        }

        return '';
    }

    public function buildPageComponentTree($pageName)
    {
        $html = self::pagesDir.'/'.$pageName.'/'.$pageName.'.page.html';

        return $this->buildComponentTree($html);
    }

    public function buildComponentTree($file)
    {
        $componentNames = $this->getComponentNames($file);
        $components = [];
        foreach ($componentNames as $name)
        {
            $children = $this->buildComponentTree(self::componentDir.'/'.$name.'/'.$name.'.html');

            $actions = $this->getOutputNames(self::componentDir.'/'.$name.'/'.$name.'.ts');
            foreach ($children as $child)
            {
                foreach ($child['actions'] as $childAction)
                {
                    if(($key = array_search($childAction, $actions)) !== false)
                    {
                        unset($actions[$key]);
                    }
                }
            }

            $inputs = $this->getInputNames(self::componentDir.'/'.$name.'/'.$name.'.ts');
            foreach ($children as $child)
            {
                foreach ($child['inputs'] as $childInput)
                {
                    if(($key = array_search($childInput, $inputs)) !== false)
                    {
                        unset($inputs[$key]);
                    }
                }
            }

            $components[] = [
                'name' => $name,
                'actions' => array_values($actions),
                'inputs' => array_values($inputs),
                'components' => $children
            ];
        }

        return $components;
    }

    private function getComponentNames($file)
    {
        $content = file_get_contents($file);

        preg_match_all('/<([a-zA-Z-]+)/', $content, $matches, PREG_OFFSET_CAPTURE);

        $componentNames = [];
        foreach ($matches[1] as $match)
        {
            if (in_array($match[0], $this->excludeTags))
            {
                continue;
            }

            $name = $this->dashToCamelCase($match[0]);

            $componentNames[] = $name;
        }

        return $componentNames;
    }

    private function getOutputNames($file)
    {
        $content = file_get_contents($file);

        preg_match_all('/@Output\(\)\s+(\w+)/', $content, $matches, PREG_OFFSET_CAPTURE);

        $outputNames = [];
        foreach ($matches[1] as $match)
        {
            $outputNames[] = $match[0];
        }

        return $outputNames;
    }

    private function getInputNames($file)
    {
        $content = file_get_contents($file);

        preg_match_all('/@Input\(\)\s+(\w+)/', $content, $matches, PREG_OFFSET_CAPTURE);

        $inputNames = [];
        foreach ($matches[1] as $match)
        {
            $inputNames[] = $match[0];
        }

        return $inputNames;
    }

    private function dashToCamelCase($str)
    {
        $parts = explode('-', $str);
        for ($i = 1; $i < sizeof($parts); $i++)
        {
            $parts[$i] = ucfirst($parts[$i]);
        }
        return implode('', $parts);
    }

    private function camelCaseToDash($str)
    {
        $data = preg_split('/(?=[A-Z])/', $str);

        foreach ($data as &$word)
        {
            $word = lcfirst($word);
        }

        return implode('-', $data);
    }
}