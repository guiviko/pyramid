<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Projects\Client\Platform\FetchRequest;
use Library\Http\Response;
use Library\Engine\Engine;
use Library\DataMapper\DataMapper;

class TestController extends Controller
{
    public function test(FetchRequest $request, Engine $engine)
    {
        $result = $engine->run($request->data('data'));

        return new Response($result['status'], $result['content']);
    }
}