<?php

namespace App\Http\Controllers\Api\Projects;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Projects\FetchRequest;
use App\Domain\Services\ProjectService;
use App\Http\Requests\Api\Projects\CreateRequest;
use App\Http\Requests\Api\Projects\DestroyRequest;

class ProjectsController extends Controller
{
    public function fetch(FetchRequest $request, ProjectService $service)
    {
        return $service->fetch();
    }

    public function create(CreateRequest $request, ProjectService $service)
    {
        return $service->create($request->data('name'));
    }

    public function destroy(DestroyRequest $request, ProjectService $service)
    {
        return $service->destroy();
    }
}