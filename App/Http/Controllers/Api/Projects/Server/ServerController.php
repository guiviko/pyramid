<?php

namespace App\Http\Controllers\Api\Projects\Server;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\ParseRequest;
use Library\Http\Response;

class ServerController extends Controller
{
    public function test(ParseRequest $request)
    {
        return new Response(Response::STATUS_OK, $request->data('project'));
    }
}