<?php

namespace App\Http\Controllers\Api\Projects\Server\Database;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Projects\Server\Routes\FetchRequest;
use App\Domain\Services\Server\DatabaseService;

class DatabaseController extends Controller
{
    public function fetch(FetchRequest $request, DatabaseService $service)
    {
        return $service->fetch();
    }

    public function addType(FetchRequest $request, DatabaseService $service)
    {
        return $service->addType($request->data('name'));
    }

    public function removeType(FetchRequest $request, DatabaseService $service)
    {
        return $service->removeType($request->data('name'));
    }

    public function addField(FetchRequest $request, DatabaseService $service)
    {
        return $service->addField($request->data('typeName'), $request->data('field'));
    }

    public function removeField(FetchRequest $request, DatabaseService $service)
    {
        return $service->removeField($request->data('typeName'), $request->data('fieldName'));
    }
}
