<?php

namespace App\Http\Controllers\Api\Api\Projects\Server;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Projects\Server\Models\FetchRequest;
use App\Domain\Services\Server\ModelService;

class ModelsController extends Controller
{
    public function fetch(FetchRequest $request, ModelService $service)
    {
        $service->fetch();
    }
}