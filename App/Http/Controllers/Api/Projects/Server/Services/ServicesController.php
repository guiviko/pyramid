<?php

namespace App\Http\Controllers\Api\Projects\Server\Services;

use App\Domain\Services\Server\ServicesService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Projects\Server\Services\CreateRequest;
use App\Http\Requests\Api\Projects\Server\Services\FetchRequest;

class ServicesController extends Controller
{
    public function fetch(FetchRequest $request, ServicesService $service)
    {
        return $service->fetch();
    }

    public function create(CreateRequest $request, ServicesService $service)
    {
        return $service->create($request->data('name'), $request->data('parent'));
    }
}