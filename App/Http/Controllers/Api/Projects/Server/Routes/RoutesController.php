<?php

namespace App\Http\Controllers\Api\Projects\Server\Routes;

use App\Domain\Services\Server\RouteService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Projects\Server\Routes\CreateRequest;
use App\Http\Requests\Api\Projects\Server\Routes\FetchRequest;

class RoutesController extends Controller
{
    public function fetch(FetchRequest $request, RouteService $service)
    {
        return $service->fetch();
    }

    public function create(CreateRequest $request, RouteService $service)
    {
        return $service->create($request->data('method'), $request->data('uri'), $request->data('controller'),
            $request->data('action'), $request->data('service'));
    }
}