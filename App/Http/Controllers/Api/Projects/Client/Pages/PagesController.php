<?php

namespace App\Http\Controllers\Api\Projects\Client\Pages;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Projects\Client\Pages\CreateRequest;
use App\Domain\Services\Client\PageService;
use App\Http\Requests\Api\Projects\Client\Pages\DestroyRequest;
use App\Http\Requests\Api\Projects\Client\Pages\FetchRequest;

class PagesController extends Controller
{
    public function fetch(FetchRequest $request, PageService $service)
    {
        return $service->fetch($request->data('pageId'));
    }

    public function create(CreateRequest $request, PageService $service)
    {
        return $service->create($request->data('parentId'), $request->data('name'), $request->data('uri'));
    }

    public function destroy(DestroyRequest $request, PageService $service)
    {
        return $service->destroy($request->data('pageId'));
    }
}