<?php

namespace App\Http\Controllers\Api\Projects\Client\Modules;

use App\Domain\Services\Client\ModuleService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Projects\Client\Modules\CreateRequest;
use App\Http\Requests\Api\Projects\Client\Modules\DestroyRequest;

class ModulesController extends Controller
{
    public function fetch(ModuleService $service)
    {
        return $service->fetch();
    }

    public function create(CreateRequest $request, ModuleService $service)
    {
        return $service->create($request->data('name'), $request->data('parentId'), $request->data('uri'));
    }

    public function destroy(DestroyRequest $request, ModuleService $service)
    {
        return $service->destroy($request->data('moduleId'));
    }
}