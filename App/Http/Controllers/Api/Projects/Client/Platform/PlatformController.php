<?php

namespace App\Http\Controllers\Api\Projects\Client\Platform;

use App\Domain\Services\Client\PlatformService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Projects\Client\Platform\AddComponentRequest;
use App\Http\Requests\Api\Projects\Client\Platform\FetchRequest;
use App\Http\Requests\Api\Projects\Client\Platform\SplitHorizontallyRequest;
use App\Http\Requests\Api\Projects\Client\Platform\SplitVerticallyRequest;

class PlatformController extends Controller
{
    public function fetch(FetchRequest $request, PlatformService $service)
    {
        return $service->fetch($request->data('componentId'));
    }

    public function fetchTemplates(PlatformService $service)
    {
        return $service->fetchTemplates();
    }

    public function fetchFormTemplates(PlatformService $service)
    {
        return $service->fetchFormTemplates();
    }

    public function reducers(PlatformService $service)
    {
        return $service->fetchReducers();
    }

    public function dropComponentInBlock(AddComponentRequest $request, PlatformService $service)
    {
        return $service->addComponent($request->data('componentId'), $request->data('blockAreaId'),
                                      $request->data('blockId'), $request->data('templateId'), $request->data('name'));
    }

    public function splitVertically(SplitVerticallyRequest $request, PlatformService $service)
    {
        return $service->splitVertically($request->data('componentId'), $request->data('blockAreaId'),
            $request->data('blockId'), $request->data('value'));
    }

    public function splitHorizontally(SplitHorizontallyRequest $request, PlatformService $service)
    {
        return $service->splitHorizontally($request->data('componentId'), $request->data('blockAreaId'),
            $request->data('blockId'), $request->data('value'));
    }

    public function dropControlInForm(FetchRequest $request, PlatformService $service)
    {
        return $service->dropControlInForm($request->data('componentId'), $request->data('controlId'));
    }

    public function dropControlInRow(FetchRequest $request, PlatformService $service)
    {
        return $service->dropControlInRow($request->data('componentId'), $request->data('controlId'),
            $request->data('rowIndex'));
    }

    public function dropExistingControlInForm(FetchRequest $request, PlatformService $service)
    {
        return $service->dropExistingControlInForm($request->data('componentId'), $request->data('rowIndex'),
            $request->data('controlIndex'));
    }

    public function deleteControl(FetchRequest $request, PlatformService $service)
    {
        return $service->deleteControl($request->data('componentId'), $request->data('rowIndex'),
            $request->data('controlIndex'));
    }

    public function reorderFormRows(FetchRequest $request, PlatformService $service)
    {
        return $service->reorderFormRows($request->data('componentId'), $request->data('firstRowIndex'),
            $request->data('secondRowIndex'));
    }

    public function updateControlConfig(FetchRequest $request, PlatformService $service)
    {
        return $service->updateControlConfig($request->data('componentId'), $request->data('controlRowIndex'),
            $request->data('controlIndex'), $request->data('config'));
    }

    public function updateComponentConfig(FetchRequest $request, PlatformService $service)
    {
        return $service->updateComponentConfig($request->data('componentId'), $request->data('groupIndex'),
            $request->data('rowIndex'), $request->data('fieldIndex'), $request->data('value'));
    }

    public function addOutput(FetchRequest $request, PlatformService $service) {
        return $service->addOutput($request->data('componentId'), $request->data('name'),
                                   $request->data('actionName'));
    }

    public function connectActionToReducer(FetchRequest $request, PlatformService $service) {
        return $service->connectActionToReducer($request->data('componentId'), $request->data('actionGroup'),
                                   $request->data('actionName'), $request->data('reducerGroup'),
                                   $request->data('reducerField'));
    }

    public function addReducerField(FetchRequest $request, PlatformService $service) {
        return $service->addReducerField($request->data('componentId'), $request->data('reducerGroup'),
                                                $request->data('reducerField'), $request->data('type'),
                                                $request->data('default'));
    }

    public function saveReducerContent(FetchRequest $request, PlatformService $service) {
        return $service->saveReducerContent($request->data('reducerName'), $request->data('fieldName'),
                                         $request->data('actionGroup'), $request->data('actionName'),
                                         $request->data('content'));
    }

    public function disconnectOutputFromReducer(FetchRequest $request, PlatformService $service) {
        return $service->disconnectOutputFromReducer($request->data('reducerGroup'), $request->data('reducerField'),
                                            $request->data('actionGroup'), $request->data('actionName'));
    }

    public function addInput(FetchRequest $request, PlatformService $service) {
        return $service->addInput($request->data('componentId'), $request->data('group'), $request->data('inputName'));
    }

    public function map(FetchRequest $request, PlatformService $service) {
        return $service->map($request->get('actionId'));
    }

    public function update(FetchRequest $request, PlatformService $service) {
        return $service->update($request->data('data'), $request->data('actionId'));
    }
}