<?php

namespace App\Http\Controllers\Api\Projects\Client\Action;

use App\Domain\Services\Client\ActionService;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Api\Projects\Server\Models\FetchRequest;

class ActionController extends ApiController
{
    public function fetch(FetchRequest $request, ActionService $service)
    {
        return $service->fetch();
    }

    public function createGroup(FetchRequest $request, ActionService $service)
    {
        return $service->createGroup($request->all());
    }

    public function createAction(FetchRequest $request, ActionService $service)
    {
        return $service->createAction($request->all());
    }

    public function createStateField(FetchRequest $request, ActionService $service)
    {
        return $service->createStateField($request->all());
    }
}