<?php

namespace App\Http\Controllers\Api\Projects\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Projects\Client\Reducers\FetchRequest;
use App\Domain\Services\Client\ReducerService;
use App\Http\Requests\Api\Projects\Client\Reducers\CreateRequest;

class ReducersController extends Controller
{
    public function fetch(FetchRequest $request, ReducerService $service)
    {
        return $service->fetch();
    }

    public function create(CreateRequest $request, ReducerService $service)
    {
        return $service->create($request->data('name'));
    }
}