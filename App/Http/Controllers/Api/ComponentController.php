<?php

namespace App\Http\Controllers\Api;

use App\Domain\Utils\StringUtils;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\AddRequest;
use Library\Http\Response;

class ComponentController extends Controller
{
    private const scriptsDir = '/var/www/html/pyramid/Storage/Scripts';
    private const projectsDir = '/var/www/html';
    private const root = '/var/www/html/reciper/src/app';
    private const pagesDir = self::root.'/pages';
    private const componentDir = self::root.'/components';
    private const reducerDir = self::root.'/reducers';

    private const localRoot = '/var/www/html/pyramid';
    private const localTemplateDir = self::localRoot.'/Storage/pyramid/templates';

    public function add(AddRequest $request)
    {
        $name = lcfirst($request->data('name'));
        $dashName = StringUtils::camelCaseToDash($name);
        $component = $request->data('component');

        $dirPath = self::componentDir.'/'.$name;
        if (file_exists($dirPath))
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Directory '.$dirPath.' already exists.');
        }

        mkdir($dirPath);

        $localTemplate = self::localTemplateDir.'/components/'.$component;

        // load html and copy
        $localHtmlFile = $localTemplate.'/html.html';
        $htmlContent = file_get_contents($localHtmlFile);
        $htmlContent = str_replace('name0', $dashName, $htmlContent);
        file_put_contents($dirPath.'/'.$name.'.html', $htmlContent);

        // load scss and copy
        $localScssFile = $localTemplate.'/scss.scss';
        $scssContent = file_get_contents($localScssFile);
        $scssContent = str_replace('name0', $dashName, $scssContent);
        file_put_contents($dirPath.'/'.$name.'.scss', $scssContent);

        // load css and copy
        $localCssFile = $localTemplate.'/css.css';
        $cssContent = file_get_contents($localCssFile);
        $cssContent = str_replace('name0', $dashName, $cssContent);
        file_put_contents($dirPath.'/'.$name.'.css', $cssContent);

        // load ts and copy
        $localTsFile = $localTemplate.'/ts.ts';
        $tsContent = file_get_contents($localTsFile);
        $tsContent = str_replace('selectorName0', $dashName, $tsContent);
        $tsContent = str_replace('htmlName0', $name.'.html', $tsContent);
        $tsContent = str_replace('styleName0', $name.'.css', $tsContent);
        $tsContent = str_replace('className0', ucfirst($name), $tsContent);
        file_put_contents($dirPath.'/'.$name.'.ts', $tsContent);

        $fixAccessScript = self::scriptsDir.'/fix-access';
        exec('sudo '.$fixAccessScript.' '.$dirPath.' guiviko');

        // register with app.declarations
        // check if import exists
        $appContent = file_get_contents(self::root.'/app.declarations.ts');
        preg_match('/import.*'.ucfirst($name).'/', $appContent, $matches);
        if (sizeof($matches) == 0)
        {
            // add import
            preg_match_all('/import.*from.*;(\r?\n)/', $appContent, $matches, PREG_OFFSET_CAPTURE | PREG_SET_ORDER);
            $matches = end($matches);
            $appContent = substr($appContent, 0, $matches[1][1] + 1).'import {'.ucfirst($name).'} from \'./components/'.$name.'/'.$name.'\';'.PHP_EOL
                .substr($appContent, $matches[1][1] + 1);
        }

        // check if declaration exists
        preg_match('/'.ucfirst($name).',?\r?\n/', $appContent, $matches);
        if (sizeof($matches) == 0)
        {
            // add declaration
            preg_match_all('/DECLARATIONS\s?=\s?\[(\n)/si', $appContent, $matches, PREG_OFFSET_CAPTURE);
            $offset = $matches[1][0][1] + 1;
            $appContent = substr($appContent, 0, $offset).'    '.ucfirst($name).','.PHP_EOL.substr($appContent, $offset);
        }

        file_put_contents(self::root.'/app.declarations.ts', $appContent);

        return new Response(Response::STATUS_OK);
    }
}