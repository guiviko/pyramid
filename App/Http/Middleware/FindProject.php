<?php

namespace App\Http\Middleware;

use App\Domain\Models\Projects\Project;
use App\Domain\Project\CurrentProject;
use Closure;
use Library\DataMapper\DataMapper;
use Library\Http\Request;
use Library\Http\Response;

class FindProject
{
    /**
     * @var CurrentProject
     */
    private $currentProject;

    /**
     * @var DataMapper
     */
    private $dm;

    public function __construct(CurrentProject $currentProject, DataMapper $dm)
    {
        $this->currentProject = $currentProject;
        $this->dm = $dm;
    }

    public function handle(Request $request, Closure $next)
    {
        if (!$request->dataExists('projectId'))
        {
            return $next($request);
        }

        $project = $this->dm->find(Project::class, $request->data('projectId'));
        if (is_null($project))
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not find project.');
        }

        $this->currentProject->setProject($project);

        return $next($request);
    }
}