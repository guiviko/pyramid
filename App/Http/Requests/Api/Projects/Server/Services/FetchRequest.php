<?php

namespace App\Http\Requests\Api\Projects\Server\Services;

use App\Http\Requests\Request;

class FetchRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }
}