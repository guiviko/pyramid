<?php

namespace App\Http\Requests\Api\Projects;

use App\Http\Requests\Request;

class DestroyRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }
}