<?php

namespace App\Http\Requests\Api\Projects\Client\Modules;

use App\Http\Requests\Request;

class CreateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }
}