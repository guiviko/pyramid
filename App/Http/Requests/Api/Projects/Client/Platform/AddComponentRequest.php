<?php

namespace App\Http\Requests\Api\Projects\Client\Platform;

use App\Http\Requests\Request;

class AddComponentRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }
}