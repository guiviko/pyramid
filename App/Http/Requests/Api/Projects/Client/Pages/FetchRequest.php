<?php

namespace App\Http\Requests\Api\Projects\Client\Pages;

use App\Http\Requests\Request;

class FetchRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'pageId' => 'required'
        ];
    }
}