<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Request;

class AddRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'component' => 'required'
        ];
    }
}