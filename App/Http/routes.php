<?php

$this->group(['namespace' => 'Api', 'prefix' => '/api', 'as' => 'api'], function()
{
    $this->post('/projects', 'Projects\\ProjectsController@create');
    $this->get('/projects', 'Projects\\ProjectsController@fetch');

    $this->get('/pages', 'MainController@pages');
    $this->post('/pages/{page}', 'MainController@page');
    $this->get('/reducers', 'MainController@reducers');
    $this->post('/components/add-action', 'MainController@addAction');
    $this->post('/components/delete-action', 'MainController@deleteAction');
    $this->post('/components/add', 'ComponentController@add');

    $this->group(['namespace' => 'Projects', 'prefix' => '/projects/{projectId}', 'as' => 'projects', 'middleware' => 'FindProject'], function()
    {
        $this->delete('', 'ProjectsController@destroy');

        $this->group(['namespace' => 'Client', 'prefix' => '/client', 'as' => 'client'], function()
        {
            $this->group(['namespace' => 'Action', 'prefix' => '/actions', 'as' => 'actions'], function()
            {
                $this->get('', 'ActionController@fetch');
                $this->post('/group', 'ActionController@createGroup');
                $this->post('/action', 'ActionController@createAction');
                $this->post('/stateField', 'ActionController@createStateField');
            });

            $this->group(['namespace' => 'Pages', 'prefix' => '/pages', 'as' => 'pages'], function()
            {
                $this->get('/{pageId}', 'PagesController@fetch');
                $this->post('', 'PagesController@create');
                $this->delete('/{pageId}', 'PagesController@destroy');
            });

            $this->group(['namespace' => 'Modules', 'prefix' => '/modules', 'as' => 'modules'], function()
            {
                $this->get('', 'ModulesController@fetch');
                $this->post('', 'ModulesController@create');
                $this->delete('/{moduleId}', 'ModulesController@destroy');
            });

            $this->group(['namespace' => 'Platform', 'prefix' => '/platform', 'as' => 'platform'], function()
            {
                $this->get('/map/{actionId}', 'PlatformController@map');
                $this->put('/update', 'PlatformController@update');

                $this->get('', 'PlatformController@fetchTemplates');
                $this->get('/form-templates', 'PlatformController@fetchFormTemplates');
                $this->get('/reducers', 'PlatformController@reducers');
                $this->get('/{componentId}', 'PlatformController@fetch');
                $this->patch('/{componentId}/drop-component-in-block', 'PlatformController@dropComponentInBlock');
                $this->patch('/{componentId}/split-vertically', 'PlatformController@splitVertically');
                $this->patch('/{componentId}/split-horizontally', 'PlatformController@splitHorizontally');
                $this->patch('/{componentId}/drop-control-in-form', 'PlatformController@dropControlInForm');
                $this->patch('/{componentId}/drop-control-in-row', 'PlatformController@dropControlInRow');
                $this->patch('/{componentId}/drop-existing-control-in-form', 'PlatformController@dropExistingControlInForm');
                $this->patch('/{componentId}/delete-control', 'PlatformController@deleteControl');
                $this->patch('/{componentId}/reorder-form-rows', 'PlatformController@reorderFormRows');
                $this->patch('/{componentId}/update-control-config', 'PlatformController@updateControlConfig');
                $this->patch('/{componentId}/update-component-config', 'PlatformController@updateComponentConfig');
                $this->patch('/{componentId}/add-output', 'PlatformController@addOutput');
                $this->patch('/{componentId}/connect-output-to-reducer', 'PlatformController@connectActionToReducer');
                $this->patch('/{componentId}/add-reducer-field', 'PlatformController@addReducerField');
                $this->patch('/{componentId}/save-reducer-content', 'PlatformController@saveReducerContent');
                $this->patch('/{componentId}/add-input', 'PlatformController@addInput');
                $this->patch('/disconnect-output-from-reducer', 'PlatformController@disconnectOutputFromReducer');
            });
        });

        $this->group(['namespace' => 'Server', 'prefix' => '/server', 'as' => 'server'], function()
        {
            $this->group(['namespace' => 'Models', 'prefix' => '/models', 'as' => 'models'], function()
            {
                $this->get('', 'ModelsController@fetch');
            });

            $this->group(['namespace' => 'Database', 'prefix' => '/database', 'as' => 'database'], function()
            {
                $this->get('/schema', 'DatabaseController@fetch');
                $this->post('/schema', 'DatabaseController@addType');
                $this->delete('/schema/{name}', 'DatabaseController@removeType');
                $this->post('/schema/fields', 'DatabaseController@addField');
                $this->delete('/schema/types/{typeName}/fields/{fieldName}', 'DatabaseController@removeField');
            });

            $this->group(['namespace' => 'Routes', 'prefix' => '/routes', 'as' => 'routes'], function()
            {
                $this->get('', 'RoutesController@fetch');
                $this->post('', 'RoutesController@create');
            });

            $this->group(['namespace' => 'Services', 'prefix' => '/services', 'as' => 'services'], function()
            {
                $this->get('', 'ServicesController@fetch');
                $this->post('', 'ServicesController@create');
            });
        });
    });
});