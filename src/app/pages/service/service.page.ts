import {Component} from '@angular/core';
import {Http} from "@angular/http";

@Component({
    selector: 'service-page',
    templateUrl: './service.page.html',
    styleUrls: ['./service.page.css']
})
export class ServicePage {
    services: any;

    constructor(private http: Http) {
        this.http.get('/api/projects/lala/server/services')
            .map(resp => resp.json())
            .subscribe(
                resp => this.services = resp,
                error => console.log(error)
            );
    }
}