import {Component, ViewChild} from '@angular/core';
import {Http} from "@angular/http";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'page-list-page',
    templateUrl: './pageList.page.html',
    styleUrls: ['./pageList.page.css']
})
export class PageListPage {
    @ViewChild('confirmModalModule') confirmModalModule;
    @ViewChild('confirmModalPage') confirmModalPage;
    data;
    projectId;
    modules: any[];
    newName: string = '';
    newUri: string = '';
    showNewModule = false;
    parentModuleId;
    showNewPage = false;

    constructor(private http: Http, private route: ActivatedRoute) {
        this.route.parent.parent.parent.params.subscribe(params => {
            this.projectId = params['projectId'];
            this.refresh();
        });
    }

    refresh() {
        this.http.get('/api/projects/' + this.projectId + '/client/modules')
            .map(response => response.json())
            .subscribe(
                data => {
                    this.modules = data;
                },
                err => {
                    console.log('error');
                }
            );
    }

    generateUriString(str) {
        this.newUri = '/' + str;
    }

    selectModule(module) {
        this.modules.map(module => {
            if (module.isSelected) {
                module.isSelected = false;
            }
        });

        module.isSelected = true;
        module.pages.map(page => page.isSelected = false);
        this.showNewPage = false;
    }

    selectedModule() {
        if (!this.modules) {
            return undefined;
        }

        let selected = this.modules.filter(module => module.isSelected);
        return selected.length == 1 ? selected[0] : undefined;
    }

    selectPage(page) {
        let module = this.selectedModule();
        if (!module) {
            return;
        }

        module.pages.map(page => {
            if (page.isSelected) {
                page.isSelected = false;
            }
        });

        page.isSelected = true;
    }

    selectedPage() {
        let module = this.selectedModule();
        if (!module) {
            return undefined;
        }

        let selected = module.pages.filter(page => page.isSelected);
        return selected.length == 1 ? selected[0] : undefined;
    }

    addModule(parent) {
        this.http.post('/api/projects/' + this.projectId + '/client/modules', {
            parentId: parent.id,
            name: this.newName,
            uri: this.newUri
        })
            .map(response => response.json())
            .subscribe(
                data => {
                    this.refresh();
                    this.showNewModule = false;
                },
                err => console.log(err)
            );
    }

    addPage() {
        let module = this.selectedModule();
        if (!module) {
            return;
        }

        this.http.post('/api/projects/' + this.projectId + '/client/pages', {
            parentId: module.id,
            name: this.newName,
            uri: this.newUri
        })
            .map(response => response.json())
            .subscribe(
                data => {
                    module.pages.push(data);
                    this.showNewPage = false;
                },
                err => console.log(err)
            );
    }

    removeModule(module) {
        this.confirmModalModule.open(() => this.removeModuleForReal(module));
    }

    removeModuleForReal(module) {
        this.http.delete('/api/projects/' + this.projectId + '/client/modules/' + module.id)
            .map(response => response.json())
            .subscribe(
                data => {
                    this.refresh();
                },
                err => console.log(err)
            );
    }

    removePage(page) {
        this.confirmModalPage.open(() => this.removePageForReal(page));
    }

    removePageForReal(page) {
        let module = this.selectedModule();
        if (!module) {
            return;
        }

        this.http.delete('/api/projects/' + this.projectId + '/client/pages/' + page.id)
            .map(response => response.json())
            .subscribe(
                data => {
                    module.pages = module.pages.filter(p => p.id != page.id);
                },
                err => console.log(err)
            );
    }

    newModule(module) {
        this.newName = '';
        this.newUri = '';
        this.parentModuleId = module.id;
        this.showNewModule = true;
        this.showNewPage = false;
    }

    newPage() {
        let module = this.selectedModule();
        if (!module) {
            return;
        }

        this.newName = '';
        this.newUri = '';
        this.parentModuleId = module.id;
        this.showNewPage = true;
        this.showNewModule = false;
    }

    cancelAdd() {
        this.showNewModule = false;
        this.showNewPage = false;
    }
}