import {Component} from '@angular/core';
import {Http} from "@angular/http";
import {ActivatedRoute} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../../../Storage/Scripts/.pyramid-init/frontend_template/src/app/reducers/index";
import {PlatformSelectors} from "../../../selectors/platform.selectors";
import {PlatformActions} from "../../../actions/platform.actions";
import {PlatformService} from "../../../services/platform.service";

@Component({
    selector: 'platform-page',
    templateUrl: './platform.page.html',
    styleUrls: ['./platform.page.css']
})
export class PlatformPage {
    projectId;
    component;
    componentId;
    lineData;
    blockMap;
    mode;
    selectedItem;
    configTab;
    dragData;
    templates;
    formTemplates;
    crumbs;
    editMode;
    isLogicPanelOpen;
    selectedLogicCategory;
    selectedLogicItem;
    reducers;

    constructor(private http: Http, private route: ActivatedRoute,
                private store: Store<AppState>,
                private platformSelectors: PlatformSelectors,
                private platformActions: PlatformActions,
                private platformService: PlatformService) {
        this.route.parent.parent.parent.params.subscribe(params => {
            this.projectId = params['projectId'];
            this.platformService.setProjectId(this.projectId);

            this.store.dispatch(this.platformActions.fetchReducers({}));
            
            this.route.params.subscribe(params => {
                this.platformService.setComponentId(params['componentId']);
                this.componentId = params['componentId'];

                this.store.dispatch(this.platformActions.fetchComponent({}));
            });
        });

        this.http.get('/api/projects/' + this.projectId + '/client/platform')
            .map(response => response.json())
            .subscribe(
                templates => this.templates = templates
            );

        this.http.get('/api/projects/' + this.projectId + '/client/platform/form-templates')
            .map(response => response.json())
            .subscribe(
                templates => this.formTemplates = templates
            );

        this.component = this.platformSelectors.getComponent();
        this.lineData = this.platformSelectors.getLineData();
        this.crumbs = this.platformSelectors.getParsedCrumbs();
        this.reducers = this.platformSelectors.getReducers();
        this.mode = this.platformSelectors.getMode();
        this.selectedItem = this.platformSelectors.getSelectedItem();
        this.configTab = this.platformSelectors.getConfigTab();
        this.dragData = this.platformSelectors.getDragData();
        this.editMode = this.platformSelectors.getEditMode();
        this.isLogicPanelOpen = this.platformSelectors.getIsLogicPanelOpen();
        this.selectedLogicCategory = this.platformSelectors.getSelectedLogicCategory();
        this.selectedLogicItem = this.platformSelectors.getSelectedLogicItem();
    }

    setMode(data) {
        this.store.dispatch(this.platformActions.setMode(data));
    }

    selectTab(data) {
        this.store.dispatch(this.platformActions.selectTab(data));
    }

    draggableMouseDown(data) {
        this.store.dispatch(this.platformActions.draggableMouseDown(data));
    }

    draggableMouseUp(data) {
        this.store.dispatch(this.platformActions.draggableMouseUp(data));
    }

    windowUpdates(data) {
        this.store.dispatch(this.platformActions.windowUpdates(data));
    }

    formActions(data) {
        this.store.dispatch(this.platformActions.formActions(data));
    }

    updateConfig(data) {
        this.store.dispatch(this.platformActions.updateControlConfig(data));
    }

    updateMainConfig(data) {
        this.store.dispatch(this.platformActions.updateComponentConfig(data));
    }

    setEditMode(data) {
        this.store.dispatch(this.platformActions.setEditMode(data));
    }

    addOutput(data) {
        this.store.dispatch(this.platformActions.addOutput(data));
    }

    connectOutputToReducer(data) {
        this.store.dispatch(this.platformActions.connectOutputToReducer(data));
    }

    addReducerField(data) {
        this.store.dispatch(this.platformActions.addReducerField(data));
    }

    toggleLogicPanel(data) {
        this.store.dispatch(this.platformActions.toggleLogicPanel(data));
    }

    selectLogicCategory(data) {
        this.store.dispatch(this.platformActions.selectLogicCategory(data));
    }

    selectLogicItem(data) {
        this.store.dispatch(this.platformActions.selectLogicItem(data));
    }

    saveReducerContent(data) {
        this.store.dispatch(this.platformActions.saveReducerContent(data));
    }

    disconnectOutputFromReducer(data) {
        this.store.dispatch(this.platformActions.disconnectOutputFromReducer(data));
    }

    addInput(data) {
        this.store.dispatch(this.platformActions.addInput(data));
    }
}
