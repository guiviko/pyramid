import {Component} from '@angular/core';
import {Http} from "@angular/http";
import {ActivatedRoute} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../../../Storage/Scripts/.pyramid-init/frontend_template/src/app/reducers/index";
import {DatabaseService} from "../../services/database.service";
import {EngineSelectors} from "../../selectors/engine.selectors";
import {EngineActions} from "../../actions/engine.actions";

@Component({
    selector: 'schema-page',
    templateUrl: 'schema.page.html',
    styleUrls: ['./schema.page.css']
})
export class SchemaPage {
    projectId;
    isAddingSchemaType;
    schema;

    constructor(private http: Http, private route: ActivatedRoute,
                private store: Store<AppState>,
                private databaseService: DatabaseService,
                private engineSelectors: EngineSelectors,
                private engineActions: EngineActions) {
        this.route.parent.parent.parent.params.subscribe(params => {
            this.projectId = params['projectId'];
            this.databaseService.setProjectId(this.projectId);
        });

        this.store.dispatch(this.engineActions.getSchema({}));

        this.isAddingSchemaType = this.engineSelectors.getIsAddingSchemaType();
        this.schema = this.engineSelectors.getSchema();
    }

    setIsAddingSchemaType(data) {
        this.store.dispatch(this.engineActions.setIsAddingSchemaType(data));
    }

    addType(data) {
        this.store.dispatch(this.engineActions.addType(data));
    }

    removeType(data) {
        this.store.dispatch(this.engineActions.removeType(data));
    }

    addField(data) {
        this.store.dispatch(this.engineActions.addField(data));
    }

    removeField(data) {
        this.store.dispatch(this.engineActions.removeField(data));
    }
}
