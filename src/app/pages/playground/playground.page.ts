import {Component, Output, EventEmitter, Input} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../reducers/index";
import {Http} from "@angular/http";
import {DatabaseService} from "../../services/database.service";
import {EngineSelectors} from "../../selectors/engine.selectors";
import {EngineActions} from "../../actions/engine.actions";

@Component({
    selector: 'playground-page',
    templateUrl: 'playground.page.html',
    styleUrls: ['./playground.page.css']
})
export class PlaygroundPage {
    projectId;
    projectName;
    schema;
    queryResult;
    selectedQuery;
    query;

    constructor(private route: ActivatedRoute,
                private store: Store<AppState>,
                private databaseService: DatabaseService,
                private engineSelectors: EngineSelectors,
                private engineActions: EngineActions,
                private http: Http) {
        this.route.parent.parent.parent.params.subscribe(params => {
            this.projectId = params['projectId'];
            this.databaseService.setProjectId(this.projectId);
            this.refresh();
        });

        this.store.dispatch(this.engineActions.getSchema({}));

        this.schema = this.engineSelectors.getSchema();
        this.queryResult = this.engineSelectors.getQueryResult();
        this.selectedQuery = this.engineSelectors.getSelectedQuery();
        this.query = this.engineSelectors.getQuery();
    }
        
    refresh() {
        this.http.get('/api/projects')
            .map(response => response.json())
            .subscribe(
                data => {
                    this.projectName = data.filter(project => project.id == this.projectId)[0].name;
                },
                err => {
                    console.log(err)
                }
            );
    }

    runQuery(data) {
        this.store.dispatch(this.engineActions.runQuery({
            projectName: this.projectName
        }));
    }

    selectQuery(data) {
        this.store.dispatch(this.engineActions.selectQuery(data));
    }

    addQueryType(data) {
        this.store.dispatch(this.engineActions.addQueryType(data));
    }

    addQueryField(data) {
        this.store.dispatch(this.engineActions.addQueryField(data));
    }
}
