import {Component} from '@angular/core';
import {Http} from "@angular/http";
import {ActivatedRoute} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../reducers/index";

@Component({
    selector: 'plugin-page',
    templateUrl: 'plugin.page.html',
    styleUrls: ['./plugin.page.css']
})
export class PluginPage {
    projectId;
    pluginName;

    constructor(private http: Http, private route: ActivatedRoute,
                private store: Store<AppState>) {
        this.route.parent.parent.parent.params.subscribe(params => {
            this.projectId = params['projectId'];
            this.pluginName = params['pluginName'];
        });
    }
}
