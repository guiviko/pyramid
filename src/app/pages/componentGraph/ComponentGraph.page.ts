import {Component, ViewChild} from '@angular/core';
import {Http} from "@angular/http";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'component-graph-page',
    templateUrl: 'componentGraph.page.html',
    styleUrls: ['componentGraph.page.css']
})
export class ComponentGraphPage {
    @ViewChild('addActionModal') addActionModal;
    @ViewChild('deleteActionModal') deleteActionModal;
    @ViewChild('addComponentModal') addComponentModal;
    data;
    pageName;
    reducers;

    constructor(private http: Http, private route: ActivatedRoute) {}

    ngOnInit() {
        this.route.params.subscribe(
            params => {
                this.pageName = params['page'];
                this.refresh();
            }
        );
    }

    refresh() {
        this.http.post('/api/pages/' + this.pageName, {
            pageName: this.pageName
        })
            .map(response => response.json())
            .subscribe(
                data => {
                    this.data = data;
                },
                err => {
                    console.log('error');
                }
            );

        this.http.get('/api/reducers')
            .map(response => response.json())
            .subscribe(
                data => {
                    this.reducers = data;
                },
                err => {
                    console.log('error');
                }
            );
    }

    addAction(compName) {
        this.addActionModal.open(compName);
    }

    deleteAction(data) {
        this.deleteActionModal.open(data.compName, data.action);
    }

    openAddComponentModal() {
        this.addComponentModal.open();
    }
}