import {Component} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Http} from "@angular/http";
import {DatabaseService} from "../../services/database.service";
import {AppState} from "../../reducers/index";
import {Store} from "@ngrx/store";
import {PlatformActions} from "../../actions/platform.actions";
import {PlatformSelectors} from "../../selectors/platform.selectors";
import {PlatformService} from "../../services/platform.service";
import {EngineActions} from "../../actions/engine.actions";
import {EngineSelectors} from "../../selectors/engine.selectors";

@Component({
    selector: 'actions-page',
    templateUrl: 'actions.page.html',
    styleUrls: ['./actions.page.css']
})
export class ActionsPage {
    projectId;
    projectName;
    actions;
    selectedAction;
    selectedGroup;
    requestConfigure;
    schema;

    constructor(private route: ActivatedRoute,
                private databaseService: DatabaseService,
                private http: Http,
                private store: Store<AppState>,
                private platformActions: PlatformActions,
                private platformSelectors: PlatformSelectors,
                private platformService: PlatformService,
                private engineActions: EngineActions,
                private engineSelectors: EngineSelectors) {
        this.route.parent.parent.parent.params.subscribe(params => {
            this.projectId = params['projectId'];
            this.databaseService.setProjectId(this.projectId);
            this.platformService.setProjectId(this.projectId);
            this.store.dispatch(this.platformActions.getActions({}));
            this.store.dispatch(this.engineActions.getSchema({}));
            this.refresh();
        });

        this.actions = this.platformSelectors.getActions();
        this.selectedAction = this.platformSelectors.getSelectedAction();
        this.selectedGroup = this.platformSelectors.getSelectedGroup();
        this.requestConfigure = this.platformSelectors.getRequestConfigure();
        this.schema = this.engineSelectors.getSchema();
    }

    refresh() {
        this.http.get('/api/projects')
            .map(response => response.json())
            .subscribe(
                data => {
                    this.projectName = data.filter(project => project.id == this.projectId)[0].name;
                },
                err => {
                    console.log(err)
                }
            );
    }

    updateMap(data) {
        this.store.dispatch(this.platformActions.update(data));
    }

    createActionGroup(data) {
        this.store.dispatch(this.platformActions.createActionGroup(data));
    }

    createAction(data) {
        this.store.dispatch(this.platformActions.createAction(data));
    }

    createStateField(data) {
        this.store.dispatch(this.platformActions.createStateField(data));
    }

    selectAction(data) {
        this.store.dispatch(this.platformActions.selectAction(data));
    }

    selectGroup(data) {
        this.store.dispatch(this.platformActions.selectGroup(data));
    }

    toggleRequestConfigure(data) {
        this.store.dispatch(this.platformActions.toggleRequestConfigure(data));
    }
}
