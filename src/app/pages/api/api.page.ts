import {Component} from '@angular/core';
import {Http} from "@angular/http";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'api-page',
    templateUrl: 'api.page.html',
    styleUrls: ['./api.page.css']
})
export class ApiPage {
    routes;
    services;
    projectId;

    constructor(private http: Http, private route: ActivatedRoute) {
        this.route.params.subscribe(params => {
            this.projectId = params['projectId'];
            this.refresh();
        });
    }

    refresh() {
        this.http.get('/api/projects/' + this.projectId + '/server/routes')
            .map(resp => resp.json())
            .subscribe(
                resp => this.routes = resp,
                error => console.log(error)
            );

        this.http.get('/api/projects/' + this.projectId + '/server/services')
            .map(resp => resp.json())
            .subscribe(
                resp => this.services = resp,
                error => console.log(error)
            );
    }
}