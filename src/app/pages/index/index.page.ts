import {Component} from '@angular/core';
import {Http} from "@angular/http";

@Component({
    selector: 'index-page',
    templateUrl: 'index.page.html',
    styleUrls: ['index.page.css']
})
export class IndexPage {
    projects: any;

    constructor(private http: Http) {
        this.refresh();
    }

    refresh() {
        this.http.get('/api/projects')
            .map(response => response.json())
            .subscribe(
                data => {
                    this.projects = data;
                },
                err => {
                    console.log(err)
                }
            );
    }
}
