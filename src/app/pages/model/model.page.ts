import {Component} from '@angular/core';
import {Http} from "@angular/http";

@Component({
    selector: 'model-page',
    templateUrl: 'model.page.html',
    styleUrls: ['./model.page.css']
})
export class ModelPage {
    models;

    constructor(private http: Http) {
        this.http.get('/api/projects/lala/server/models')
            .map(resp => resp.json())
            .subscribe(
                resp => this.models = resp,
                error => console.log(error)
            );
    }
}