import {Component} from '@angular/core';
import {Http} from "@angular/http";

@Component({
    selector: 'reducer-page',
    templateUrl: 'reducer.page.html',
    styleUrls: ['./reducer.page.css']
})
export class ReducerPage {
    reducers;

    constructor(private http: Http) {
        this.http.get('/api/projects/lala/client/reducers')
            .map(resp => resp.json())
            .subscribe(
                resp => this.reducers = resp,
                error => console.log(error)
            );
    }
}