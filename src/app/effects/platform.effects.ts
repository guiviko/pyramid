import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import {Observable} from "rxjs";
import {PlatformActions} from "../actions/platform.actions";
import {PlatformService} from "../services/platform.service";
import {AppState} from "../reducers/index";
import {Store} from "@ngrx/store";

@Injectable()
export class PlatformEffects {
    constructor(private actions$: Actions,
                private platformService: PlatformService,
                private store$: Store<AppState>,
                private platformActions: PlatformActions
    ) { }

    @Effect() fetchComponent = this.actions$
        .ofType(PlatformActions.FETCH_COMPONENT)
        .switchMap(action => {
            return this.platformService.fetchComponent()
                .map((data) => this.platformActions.fetchComponentSuccess(data))
                .catch((err) => Observable.of(this.platformActions.fetchComponentFailure(err)))
        });

    @Effect() fetchReducers = this.actions$
        .ofType(PlatformActions.FETCH_REDUCERS)
        .switchMap(action => {
            return this.platformService.fetchReducers()
                .map((data) => this.platformActions.fetchReducersSuccess(data))
                .catch((err) => Observable.of(this.platformActions.fetchReducersFailure(err)))
        });

    @Effect() dropComponentInBlock = this.actions$
        .ofType(PlatformActions.WINDOW_UPDATES)
        .map(action => action.payload)
        .filter(action => action.type == PlatformActions.DROP_COMPONENT_IN_BLOCK)
        .switchMap(action => {
            return this.platformService.dropComponentInBlock(action.payload)
                .map((data) => this.platformActions.windowUpdates(this.platformActions.dropComponentInBlockSuccess(data)))
                .catch((err) => Observable.of(this.platformActions.windowUpdates(this.platformActions.dropComponentInBlockFailure())))
        });

    @Effect() splitVertically = this.actions$
        .ofType(PlatformActions.WINDOW_UPDATES)
        .map(action => action.payload)
        .filter(action => action.type == PlatformActions.SPLIT_VERTICALLY)
        .switchMap(action => {
            return this.platformService.splitVertically(action.payload)
                .map((data) => this.platformActions.windowUpdates(this.platformActions.splitVerticallySuccess(data)))
                .catch((err) => Observable.of(this.platformActions.windowUpdates(this.platformActions.splitVerticallyFailure())))
        });

    @Effect() splitHorizontally = this.actions$
        .ofType(PlatformActions.WINDOW_UPDATES)
        .map(action => action.payload)
        .filter(action => action.type == PlatformActions.SPLIT_HORIZONTALLY)
        .switchMap(action => {
            return this.platformService.splitHorizontally(action.payload)
                .map((data) => this.platformActions.windowUpdates(this.platformActions.splitHorizontallySuccess(data)))
                .catch((err) => Observable.of(this.platformActions.windowUpdates(this.platformActions.splitHorizontallyFailure())))
        });

    @Effect() dropControlInForm = this.actions$
        .ofType(PlatformActions.FORM_ACTIONS)
        .map(action => action.payload)
        .filter(action => action.type == PlatformActions.DROP_CONTROL_IN_FORM)
        .switchMap(action => {
            return this.platformService.dropControlInForm(action.payload)
                .map((data) => this.platformActions.formActions(this.platformActions.dropControlInFormSuccess(data)))
                .catch((err) => Observable.of(this.platformActions.formActions(this.platformActions.dropControlInFormFailure())))
        });

    @Effect() dropControlInRow = this.actions$
        .ofType(PlatformActions.FORM_ACTIONS)
        .map(action => action.payload)
        .filter(action => action.type == PlatformActions.DROP_CONTROL_IN_ROW)
        .switchMap(action => {
            return this.platformService.dropControlInRow(action.payload)
                .map((data) => this.platformActions.formActions(this.platformActions.dropControlInRowSuccess(data)))
                .catch((err) => Observable.of(this.platformActions.formActions(this.platformActions.dropControlInRowFailure(err))))
        });

    @Effect() dropExistingControlInForm = this.actions$
        .ofType(PlatformActions.FORM_ACTIONS)
        .map(action => action.payload)
        .filter(action => action.type == PlatformActions.DROP_EXISTING_CONTROL_IN_FORM)
        .switchMap(action => {
            return this.platformService.dropExistingControlInForm(action.payload)
                .map((data) => this.platformActions.formActions(this.platformActions.dropExistingControlInFormSuccess(data)))
                .catch((err) => Observable.of(this.platformActions.formActions(this.platformActions.dropExistingControlInFormFailure(err))))
        });

    @Effect() deleteControl = this.actions$
        .ofType(PlatformActions.FORM_ACTIONS)
        .map(action => action.payload)
        .filter(action => action.type == PlatformActions.DELETE_CONTROL)
        .switchMap(action => {
            return this.platformService.deleteControl(action.payload)
                .map((data) => this.platformActions.formActions(this.platformActions.deleteControlSuccess(data)))
                .catch((err) => Observable.of(this.platformActions.formActions(this.platformActions.deleteControlFailure(err))))
        });

    @Effect() reorderFormRow = this.actions$
        .ofType(PlatformActions.FORM_ACTIONS)
        .map(action => action.payload)
        .filter(action => action.type == PlatformActions.REORDER_FORM_ROWS)
        .switchMap(action => {
            return this.platformService.reorderFormRows(action.payload)
                .map((data) => this.platformActions.formActions(this.platformActions.reorderFormRowsSuccess(data)))
                .catch((err) => Observable.of(this.platformActions.formActions(this.platformActions.reorderFormRowsFailure())))
        });

    @Effect() updateControlConfig = this.actions$
        .ofType(PlatformActions.UPDATE_CONTROL_CONFIG)
        .switchMap(action => {
            return this.platformService.updateControlConfig(action.payload)
                .map((data) => this.platformActions.formActions(this.platformActions.updateControlConfigSuccess(data)))
                .catch((err) => Observable.of(this.platformActions.formActions(this.platformActions.updateControlConfigFailure())))
        });

    @Effect() updateComponentConfig = this.actions$
        .ofType(PlatformActions.UPDATE_COMPONENT_CONFIG)
        .switchMap(action => {
            return this.platformService.updateComponnetConfig(action.payload)
                .map((data) => this.platformActions.formActions(this.platformActions.updateComponentConfigSuccess(data)))
                .catch((err) => Observable.of(this.platformActions.formActions(this.platformActions.updateComponentConfigFailure(err))))
        });

    @Effect() addOutput = this.actions$
        .ofType(PlatformActions.ADD_OUTPUT)
        .switchMap(action => {
            return this.platformService.addOutput(action.payload)
                .map((data) => this.platformActions.addOutputSuccess(data))
                .catch((err) => Observable.of(this.platformActions.addOutputFailure(err)))
        });

    @Effect() addOutputSuccess = this.actions$
        .ofType(PlatformActions.ADD_OUTPUT_SUCCESS)
        .mergeMap(action => [
            this.platformActions.fetchComponent({}),
            this.platformActions.fetchReducers({})
        ]);

    @Effect() connectOutputToReducer = this.actions$
        .ofType(PlatformActions.CONNECT_OUTPUT_TO_REDUCER)
        .switchMap(action => {
            return this.platformService.connectOutputToReducer(action.payload)
                .map((data) => this.platformActions.connectOutputToReducerSuccess(data))
                .catch((err) => Observable.of(this.platformActions.connectOutputToReducerFailure(err)))
        });

    @Effect() connectOutputToReducerSuccess = this.actions$
        .ofType(PlatformActions.CONNECT_OUTPUT_TO_REDUCER_SUCCESS)
        .mergeMap(action => [
            this.platformActions.fetchComponent({}),
            this.platformActions.fetchReducers({})
        ]);

    @Effect() addReducerField = this.actions$
        .ofType(PlatformActions.ADD_REDUCER_FIELD)
        .switchMap(action => {
            return this.platformService.addReducerField(action.payload)
                .map((data) => this.platformActions.addReducerFieldSuccess(data))
                .catch((err) => Observable.of(this.platformActions.addReducerFieldFailure(err)))
        });

    @Effect() addReducerFieldSuccess = this.actions$
        .ofType(PlatformActions.ADD_REDUCER_FIELD_SUCCESS)
        .mergeMap(action => [
            this.platformActions.fetchComponent({}),
            this.platformActions.fetchReducers({})
        ]);

    @Effect() saveReducerContent = this.actions$
        .ofType(PlatformActions.SAVE_REDUCER_CONTENT)
        .switchMap(action => {
            return this.platformService.saveReducerContent(action.payload)
                .map((data) => this.platformActions.saveReducerContentSuccess(action.payload))
                .catch((err) => Observable.of(this.platformActions.saveReducerContentFailure(err)))
        });

    @Effect() disconnectFromReducer = this.actions$
        .ofType(PlatformActions.DISCONNECT_OUTPUT_FROM_REDUCER)
        .mergeMap(action => {
            return this.platformService.disconnectOutputFromReducer(action.payload)
                .map((data) => this.platformActions.disconnectOutputFromReducerSuccess(action.payload))
                .catch((err) => Observable.of(this.platformActions.disconnectOutputFromReducerFailure(err)))
        });

    @Effect() disconnectOutputFromReducerSuccess = this.actions$
        .ofType(PlatformActions.DISCONNECT_OUTPUT_FROM_REDUCER_SUCCESS)
        .mergeMap(action => [
            this.platformActions.selectLogicItem(''),
            this.platformActions.fetchComponent({}),
            this.platformActions.fetchReducers({})
        ]);

    @Effect() addInput = this.actions$
        .ofType(PlatformActions.ADD_INPUT)
        .mergeMap(action => {
            return this.platformService.addInput(action.payload)
                .map((data) => this.platformActions.addInputSuccess(action.payload))
                .catch((err) => Observable.of(this.platformActions.addInputFailure(err)))
        });

    @Effect() addInputSuccess = this.actions$
        .ofType(PlatformActions.ADD_INPUT_SUCCESS)
        .mergeMap(action => [
            this.platformActions.fetchComponent({}),
            this.platformActions.fetchReducers({})
        ]);

    @Effect() update = this.actions$
        .ofType(PlatformActions.UPDATE)
        .withLatestFrom(this.store$)
        .mergeMap(([action, state]) => {
            return this.platformService.update({data: action.payload, actionId: state.platform.selectedAction.id})
                .map((data) => this.platformActions.updateSuccess(action.payload))
                .catch((err) => Observable.of(this.platformActions.updateFailure(err)))
        });

    @Effect() updateSuccess = this.actions$
        .ofType(PlatformActions.UPDATE_SUCCESS)
        .withLatestFrom(this.store$)
        .mergeMap(([action, state]) => [
            this.platformActions.getMap(state.platform.selectedAction.id),
        ]);

    @Effect() getMap = this.actions$
        .ofType(PlatformActions.GET_MAP)
        .mergeMap(action => {
            return this.platformService.getMap(action.payload)
                .map((data) => this.platformActions.getMapSuccess(data))
                .catch((err) => Observable.of(this.platformActions.getMapFailure(err)))
        });

    @Effect() getActions = this.actions$
        .ofType(PlatformActions.GET_ACTIONS)
        .mergeMap(action => {
            return this.platformService.getActions({})
                .map((data) => this.platformActions.getActionsSuccess(data))
                .catch((err) => Observable.of(this.platformActions.getActionsFailure(err)))
        });

    @Effect() createActionGroup = this.actions$
        .ofType(PlatformActions.CREATE_ACTION_GROUP)
        .mergeMap(action => {
            return this.platformService.createActionGroup(action.payload)
                .map((data) => this.platformActions.createActionGroupSuccess(data))
                .catch((err) => Observable.of(this.platformActions.createActionGroupFailure(err)))
        });

    @Effect() createAction = this.actions$
        .ofType(PlatformActions.CREATE_ACTION)
        .mergeMap(action => {
            return this.platformService.createAction(action.payload)
                .map((data) => this.platformActions.createActionSuccess(data))
                .catch((err) => Observable.of(this.platformActions.createActionFailure(err)))
        });

    @Effect() createStateField = this.actions$
        .ofType(PlatformActions.CREATE_STATE_FIELD)
        .mergeMap(action => {
            return this.platformService.createStateField(action.payload)
                .map((data) => this.platformActions.createStateFieldSuccess(data))
                .catch((err) => Observable.of(this.platformActions.createStateFieldFailure(err)))
        });

    @Effect() createActionGroupSuccess = this.actions$
        .ofType(PlatformActions.CREATE_ACTION_GROUP_SUCCESS)
        .mergeMap(action => [
            this.platformActions.getActions({}),
        ]);

    @Effect() createActionSuccess = this.actions$
        .ofType(PlatformActions.CREATE_ACTION_SUCCESS)
        .mergeMap(action => [
            this.platformActions.getActions({}),
        ]);

    @Effect() createStateFieldSuccess = this.actions$
        .ofType(PlatformActions.CREATE_STATE_FIELD_SUCCESS)
        .mergeMap(action => [
            this.platformActions.getActions({}),
        ]);
}
