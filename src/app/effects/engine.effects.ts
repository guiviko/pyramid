import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import {Observable} from "rxjs";
import {EngineActions} from "../actions/engine.actions";
import {DatabaseService} from "../services/database.service";
import {AppState} from "../reducers/index";
import {Store} from "@ngrx/store";

@Injectable()
export class EngineEffects {
    constructor(private actions$: Actions,
                private store$: Store<AppState>,
                private databaseService: DatabaseService,
                private engineActions: EngineActions
    ) { }

    @Effect() getSchema = this.actions$
        .ofType(EngineActions.GET_SCHEMA)
        .switchMap(action => {
            return this.databaseService.getSchema(action.payload)
                .map((data) => this.engineActions.getSchemaSuccess(data))
                .catch((err) => Observable.of(this.engineActions.getSchemaFailure(err)))
        });

    @Effect() addType = this.actions$
        .ofType(EngineActions.ADD_TYPE)
        .switchMap(action => {
            return this.databaseService.addType(action.payload)
                .map((data) => this.engineActions.addTypeSuccess(action.payload.name))
                .catch((err) => Observable.of(this.engineActions.addTypeFailure(err)))
        });

    @Effect() removeType = this.actions$
        .ofType(EngineActions.REMOVE_TYPE)
        .switchMap(action => {
            return this.databaseService.removeType(action.payload)
                .map((data) => this.engineActions.removeTypeSuccess(action.payload))
                .catch((err) => Observable.of(this.engineActions.removeTypeFailure(err)))
        });

    @Effect() addField = this.actions$
        .ofType(EngineActions.ADD_FIELD)
        .switchMap(action => {
            return this.databaseService.addField(action.payload)
                .map((data) => this.engineActions.addFieldSuccess(action.payload))
                .catch((err) => Observable.of(this.engineActions.addFieldFailure(err)))
        });

    @Effect() removeField = this.actions$
        .ofType(EngineActions.REMOVE_FIELD)
        .switchMap(action => {
            return this.databaseService.removeField(action.payload)
                .map((data) => this.engineActions.removeFieldSuccess(action.payload))
                .catch((err) => Observable.of(this.engineActions.removeFieldFailure(err)))
        });

    @Effect() runQuery = this.actions$
        .ofType(EngineActions.RUN_QUERY)
        .withLatestFrom(this.store$)
        .mergeMap(([action, state]) => {
            return this.databaseService.runQuery(Object.assign(state.engine.query, action.payload))
                .map((data) => this.engineActions.runQuerySuccess(data))
                .catch((err) => Observable.of(this.engineActions.runQueryFailure(err)))
        });
}
