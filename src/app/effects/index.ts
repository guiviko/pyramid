import {PlatformEffects} from "./platform.effects";
import {EngineEffects} from "./engine.effects";

export default [
    PlatformEffects,
    EngineEffects,
];
