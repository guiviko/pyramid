import { IndexPage } from './pages/index/index.page';
import { NotFound404Component } from './not-found404.component';
import {Header} from './components/header/Header';
import {ProjectList} from './components/projectList/ProjectList';

export const APP_DECLARATIONS = [
    IndexPage,
    NotFound404Component,
    Header,
    ProjectList
];
