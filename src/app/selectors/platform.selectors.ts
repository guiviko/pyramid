import {Injectable} from '@angular/core';
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";

import {AppState} from "./../reducers/index";

@Injectable()
export class PlatformSelectors {
    constructor(private store: Store<AppState>) { }

    getComponent(): Observable<any> {
        return this.store.select(s => this.buildComponent(s));
    }

    private buildComponent(s) {
        if (!s.platform.component) {
            return undefined;
        }

        let component = Object.assign({}, s.platform.component);

        let blockAreas = component.blockAreas.map(id => {
            return s.platform.blockAreas[id];
        });
        component.blockAreas = blockAreas;

        return component;
    }

    getLineData() {
        return this.store.select(s => s.platform.component.outputs.reduce((acc, data) => acc.concat(data.connections), []));
    }

    getParsedCrumbs() {
        return this.store.select(s => this.parseCrumbs(s.platform.component));
    }

    parseCrumbs(component) {
        if (!component) {
            return [];
        }

        let dotParts = component.dotChain.split('#');

        let parents = dotParts.map(dotPart => {
            let parts = dotPart.split(':');
            let type = 'module';
            switch (parts[0]) {
                case 'p':
                    type = 'page';
                    break;
                case 'c':
                    type = 'component';
                    break;
            }
            return {
                id: parts[2],
                type: type,
                name: parts[1]
            };
        });

        let type = 'component';
        let name = component.name;
        if (component.name.substr(-5) == '.page') {
            type = 'page';
            name = name.substr(0, name.length - 5);
        }

        parents.push({
            id: component.id,
            type: type,
            name: name
        });

        return parents;
    }

    getReducers() {
        return this.store.select(s => s.platform.reducers);
    }

    getMode(): Observable<any> {
        return this.store.select(s => s.platform.mode);
    }

    getSelectedItem(): Observable<any> {
        return this.store.select(s => {
            if (s.platform.selected.type == '') {
                return undefined;
            }

            switch (s.platform.selected.type) {
                case 'block':
                    return {
                        blockAreaId: s.platform.selected.blockAreaId,
                        item: s.platform.blockAreas[s.platform.selected.blockAreaId].blockMap[s.platform.selected.id],
                        type: 'block'
                    };
                default:
                    return s.platform.selected;
            }
        });
    }

    getConfigTab(): Observable<any> {
        return this.store.select(s => s.platform.configTab);
    }

    getDragData(): Observable<any> {
        return this.store.select(s => s.platform.dragData);
    }

    getEditMode(): Observable<any> {
        return this.store.select(s => s.platform.editMode);
    }

    getIsLogicPanelOpen(): Observable<any> {
        return this.store.select(s => s.platform.isLogicPanelOpen);
    }

    getSelectedLogicCategory(): Observable<any> {
        return this.store.select(s => s.platform.selectedLogicCategory);
    }

    getSelectedLogicItem(): Observable<any> {
        return this.store.select(s => s.platform.selectedLogicItem);
    }

    getActions(): Observable<any> {
        return this.store.select(s => s.platform.actionIds
            .map(groupId => s.platform.actions[groupId]));
    }

    getSelectedAction(): Observable<any> {
        return this.store.select(s => s.platform.selectedAction);
    }

    getSelectedGroup(): Observable<any> {
        return this.store.select(s => s.platform.selectedGroup);
    }

    getRequestConfigure(): Observable<any> {
        return this.store.select(s => s.platform.requestConfigure);
    }
}
