import {PlatformSelectors} from './platform.selectors';
import {EngineSelectors} from './engine.selectors';

export default [
    PlatformSelectors,
    EngineSelectors,
];
