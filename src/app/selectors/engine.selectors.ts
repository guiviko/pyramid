import {Injectable} from '@angular/core';
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";

import {AppState} from "./../reducers/index";

@Injectable()
export class EngineSelectors {
    constructor(private store: Store<AppState>) { }

    getIsAddingSchemaType(): Observable<any> {
        return this.store.select(s => s.engine.isAddingSchemaType);
    }

    getSchema(): Observable<any> {
        return this.store.select(s => {
            let types = [];
            for (let key in s.engine.schema) {
                if (s.engine.schema.hasOwnProperty(key)) {
                    let fields = [];
                    for (let j in s.engine.schema[key]) {
                        if (s.engine.schema[key].hasOwnProperty(j)) {
                            fields.push({
                                name: j,
                                attributes: s.engine.schema[key][j]
                            });
                        }
                    }

                    types.push({
                        name: key,
                        fields: fields
                    });
                }
            }
            return types;
        });
    }

    getQueryResult() {
        return this.store.select(s => s.engine.queryResult);
    }

    getSelectedQuery(): Observable<any> {
        return this.store.select(s => s.engine.selectedQuery);
    }

    getQuery(): Observable<any> {
        return this.store.select(s => {
            let fetch = Object.assign({}, s.engine.query.data.fetch);
            let flattened = {
                data: {
                    fetch: []
                }
            };
            for (let key in fetch) {
                if (fetch.hasOwnProperty(key)) {
                    let typeValue = fetch[key];
                    let flatType = {
                        key: key,
                        value: Object.assign({}, typeValue)
                    };

                    let fields = [];
                    for (let fieldKey in typeValue.fields) {
                        if (typeValue.fields.hasOwnProperty(fieldKey)) {
                            fields.push({
                                key: fieldKey,
                                value: typeValue.fields[fieldKey]
                            });
                        }
                    }

                    flatType.value.fields = fields;
                    flattened.data.fetch.push(flatType);
                }
            }
            return flattened;
        });
    }
}
