export class Block {
    id: number;
    parentId: number;
    blocks: any[];
    flexBasis: number;
    flexDirection: string;
    width: number;
    height: number;
    type: string;
    separatorDirection: string;
    isSelected: boolean;
    marginTop: number;
    marginBottom: number;
    marginRight: number;
    marginLeft: number;
    content: any;

    constructor(data: any = {}) {
        this.id = data.id || 0;
        this.parentId = data.parentId || 0;
        this.blocks = data.blocks || [];
        this.flexBasis = data.flexBasis || 0;
        this.flexDirection = data.flexDirection || 'row';
        this.width = data.width || 0;
        this.height = data.height || 0;
        this.type = data.type || 'block';
        this.separatorDirection = data.separatorDirection || '';
        this.isSelected = data.isSelected || false;
        this.content = data.content || undefined;
        this.marginTop = data.marginTop || 0;
        this.marginBottom = data.marginBottom || 0;
        this.marginLeft = data.marginLeft || 0;
        this.marginRight = data.marginRight || 0;

        if (this.content == undefined) {
            this.content = {
                component: undefined
            };
        }
    }
}
