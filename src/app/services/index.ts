import {PlatformService} from "./platform.service";
import {PyrDraggableService} from "./generic/pyrDraggable.service";
import {DatabaseService} from "./database.service";
import {ConnectionService} from "./connection.service";

export default [
    PlatformService,
    PyrDraggableService,
    DatabaseService,
    ConnectionService
];
