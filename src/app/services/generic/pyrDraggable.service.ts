import {Injectable} from '@angular/core';

@Injectable()
export class PyrDraggableService {
    isDragging: boolean;
    dragType: string;
    dragPayload: any;
    id: number;

    constructor() {
        this.stopDrag();
    }

    startDrag(type: string, payload: any, id: number) {
        this.isDragging = true;
        this.dragType = type;
        this.dragPayload = payload;
        this.id = id;
    }

    stopDrag() {
        this.isDragging = false;
        this.dragType = '';
        this.dragPayload = {};
        this.id = 0;
    }
}