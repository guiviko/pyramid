import {Http} from "@angular/http";

export class Engine {
    private http: Http;
    private type: string;

    constructor(http: Http) {
        this.http = http;
    }

    prepareFetch(type: string) {
        this.type = type;
        return this;
    }

    execute() {
        this.clear();
    }

    clear() {
        this.type = '';
    }
}