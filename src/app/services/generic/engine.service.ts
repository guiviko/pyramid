import {Http} from "@angular/http";
import {Engine} from "./engine";

export class EngineService {
    protected engine;

    constructor(private http: Http) {
        this.engine = new Engine(http);
    }
}