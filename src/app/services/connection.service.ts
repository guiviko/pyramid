import {Injectable} from '@angular/core';

@Injectable()
export class ConnectionService {
    isDragging: boolean;
    isPaused: boolean;
    startDragX;
    startDragY;
    lastMouseX;
    lastMouseY;
    r;
    node;
    index;

    constructor() {
        window.addEventListener('mousemove', (e) => this.handleMouseMove(e));
        window.addEventListener('keydown', (e) => this.handleKeyDown(e));
    }

    pause() {
        this.isPaused = true;
    }

    mouseDown(e, node, index) {
        if (this.isDragging) {
            return;
        }

        this.node = node;
        this.index = index;

        if (!this.r) {
            let container = document.getElementById('logic-container');
            this.r = container.getBoundingClientRect();
        }

        this.startDragX = e.pageX - this.r.left;
        this.startDragY = e.pageY - this.r.top - 4;
        this.lastMouseX = this.startDragX;
        this.lastMouseY = this.startDragY;
        this.isDragging = true;

        e.preventDefault();
    }

    mouseUp(e) {
        if (!this.isDragging) {
            return;
        }

        e.preventDefault();
        e.stopPropagation();

        this.isDragging = false;
    }

    handleMouseMove(e) {
        if (!this.isDragging || this.isPaused) {
            return;
        }

        this.lastMouseX = e.pageX - this.r.left;
        this.lastMouseY = e.pageY - this.r.top;
    }

    dragLeft() {
        return this.lastMouseX - this.startDragX < 0 ? this.lastMouseX : this.startDragX;
    }

    dragTop() {
        return this.lastMouseY - this.startDragY < 0 ? this.lastMouseY : this.startDragY;
    }

    dragWidth() {
        return Math.abs(this.lastMouseX - this.startDragX);
    }

    dragHeight() {
        return Math.abs(this.lastMouseY - this.startDragY);
    }

    dragd() {
        let height = Math.abs(this.startDragX - this.lastMouseX);
        if (height < 5) {
            return 'M 0,0 L 100,0';
        }

        if ((this.lastMouseY - this.startDragY < 0 && this.lastMouseX - this.startDragX >= 0) ||
            (this.lastMouseY - this.startDragY >= 0 && this.lastMouseX - this.startDragX < 0)) {
            return 'M 0,100 C 35,100 65,0 100,0';
        }

        return 'M 0,0 C 35,0 65,100 100,100';
    }

    handleKeyDown(e) {
        if (!this.isDragging) {
            return;
        }

        if (e.keyCode == 27) {
            this.isDragging = false;
            this.isPaused = false;
        }
    }
}