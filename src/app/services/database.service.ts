import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';

@Injectable()
export class DatabaseService {
    projectId;

    constructor(private http: Http) { }

    setProjectId(id) {
        this.projectId = id;
    }

    getSchema(data): any {
        return this.http.get('/api/projects/' + this.projectId + '/server/database/schema')
            .map(response => response.json());
    }

    addType(data): any {
        return this.http.post('/api/projects/' + this.projectId + '/server/database/schema', data)
            .map(response => response.json());
    }

    removeType(data): any {
        return this.http.delete('/api/projects/' + this.projectId + '/server/database/schema/' + data)
            .map(response => response.json());
    }

    addField(data): any {
        return this.http.post('/api/projects/' + this.projectId + '/server/database/schema/fields', data)
            .map(response => response.json());
    }

    removeField(data): any {
        return this.http.delete('/api/projects/' + this.projectId + '/server/database/schema/types/' + data.typeName + '/fields/' + data.field.name)
            .map(response => response.json());
    }

    runQuery(data): any {
        return this.http.post('http://' + data.projectName + '.dev:3000/api/engine', data, {
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        })
            .map(response => response.json());
    }
}
