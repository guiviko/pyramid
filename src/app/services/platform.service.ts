import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

@Injectable()
export class PlatformService {
    projectId;
    componentId;

    constructor(private http: Http) { }

    setProjectId(id) {
        this.projectId = id;
    }

    setComponentId(id) {
        this.componentId = id;
    }

    dropComponentInBlock(data): any {
        return this.http.patch('/api/projects/' + this.projectId + '/client/platform/' + this.componentId + '/drop-component-in-block', {
            blockAreaId: data.blockAreaId,
            blockId: data.block.id,
            templateId: data.templateId,
            name: data.name
        })
            .map(response => response.json());
    }

    splitVertically(data): any {
        return this.http.patch('/api/projects/' + this.projectId + '/client/platform/' + this.componentId + '/split-vertically', {
            blockAreaId: data.blockAreaId,
            blockId: data.block.id,
            value: data.value
        })
            .map(response => response.json());
    }

    splitHorizontally(data): any {
        return this.http.patch('/api/projects/' + this.projectId + '/client/platform/' + this.componentId + '/split-horizontally', {
            blockAreaId: data.blockAreaId,
            blockId: data.block.id,
            value: data.value
        })
            .map(response => response.json());
    }

    dropControlInForm(data): any {
        return this.http.patch('/api/projects/' + this.projectId + '/client/platform/' + this.componentId + '/drop-control-in-form', {
            controlId: data.controlId
        })
            .map(response => response.json());
    }

    dropControlInRow(data): any {
        return this.http.patch('/api/projects/' + this.projectId + '/client/platform/' + this.componentId + '/drop-control-in-row', {
            controlId: data.controlId,
            rowIndex: data.rowIndex
        })
            .map(response => response.json());
    }

    dropExistingControlInForm(data): any {
        return this.http.patch('/api/projects/' + this.projectId + '/client/platform/' + this.componentId + '/drop-existing-control-in-Form', {
            rowIndex: data.rowIndex,
            controlIndex: data.controlIndex
        })
            .map(response => response.json());
    }

    deleteControl(data): any {
        return this.http.patch('/api/projects/' + this.projectId + '/client/platform/' + this.componentId + '/delete-control', {
            rowIndex: data.rowIndex,
            controlIndex: data.controlIndex
        })
            .map(response => response.json());
    }

    reorderFormRows(data): any {
        return this.http.patch('/api/projects/' + this.projectId + '/client/platform/' + this.componentId + '/reorder-form-rows', data)
            .map(response => response.json());
    }

    updateControlConfig(data): any {
        return this.http.patch('/api/projects/' + this.projectId + '/client/platform/' + this.componentId + '/update-control-config', data)
            .map(response => response.json());
    }

    updateComponnetConfig(data): any {
        return this.http.patch('/api/projects/' + this.projectId + '/client/platform/' + this.componentId + '/update-component-config', data)
            .map(response => response.json());
    }

    addOutput(data): any {
        return this.http.patch('/api/projects/' + this.projectId + '/client/platform/' + this.componentId + '/add-output', data)
            .map(response => response.json());
    }

    connectOutputToReducer(data): any {
        return this.http.patch('/api/projects/' + this.projectId + '/client/platform/' + this.componentId + '/connect-output-to-reducer', data)
            .map(response => response.json());
    }

    addReducerField(data): any {
        return this.http.patch('/api/projects/' + this.projectId + '/client/platform/' + this.componentId + '/add-reducer-field', data)
            .map(response => response.json());
    }

    saveReducerContent(data): any {
        return this.http.patch('/api/projects/' + this.projectId + '/client/platform/' + this.componentId + '/save-reducer-content', data)
            .map(response => response.json());
    }

    fetchComponent() {
        return this.http.get('/api/projects/' + this.projectId + '/client/platform/' + this.componentId)
            .map(response => response.json());
    }

    fetchReducers() {
        return this.http.get('/api/projects/' + this.projectId + '/client/platform/reducers')
            .map(response => response.json());
    }

    disconnectOutputFromReducer(data) {
        return this.http.patch('/api/projects/' + this.projectId + '/client/platform/disconnect-output-from-reducer', data)
            .map(response => response.json());
    }

    addInput(data) {
        return this.http.patch('/api/projects/' + this.projectId + '/client/platform/' + this.componentId + '/add-input', data)
            .map(response => response.json());
    }

    getMap(data) {
        return this.http.get('/api/projects/' + this.projectId + '/client/platform/map/' + data)
            .map(response => response.json());
    }

    update(data) {
        return this.http.put('/api/projects/' + this.projectId + '/client/platform/update', data)
            .map(response => response.json());
    }

    getActions(data) {
        return this.http.get('/api/projects/' + this.projectId + '/client/actions')
            .map(response => response.json());
    }

    createActionGroup(data) {
        return this.http.post('/api/projects/' + this.projectId + '/client/actions/group', data)
            .map(response => response.json());
    }

    createAction(data) {
        return this.http.post('/api/projects/' + this.projectId + '/client/actions/action', data)
            .map(response => response.json());
    }

    createStateField(data) {
        return this.http.post('/api/projects/' + this.projectId + '/client/actions/stateField', data)
            .map(response => response.json());
    }
}
