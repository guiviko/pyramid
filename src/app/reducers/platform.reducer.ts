import {ActionReducer, Action} from '@ngrx/store';
import {PlatformActions} from "../actions/platform.actions";
import {normalize} from "../utils/normalizer.utils";

export interface PlatformState {
    component: any;
    blockAreas: any;
    reducers: any;
    mode: string;
    selected: any;
    configTab: string;
    dragData: any;
    editMode: string;
    isLogicPanelOpen: boolean;
    selectedLogicCategory: string;
    selectedLogicItem: string;
    actions: any;
    actionIds: any;
    selectedAction: any;
    selectedGroup: any;
    requestConfigure: any;
}

export const initialState = {
    component: undefined,
    blockAreas: {},
    reducers: [],
    mode: 'select',
    selected: {
        blockAreaId: 0,
        id: 0,
        type: ''
    },
    configTab: 'main',
    dragData: {
        id: 0,
        type: ''
    },
    editMode: 'design',
    isLogicPanelOpen: false,
    selectedLogicCategory: 'reducers',
    selectedLogicItem: '',
    actions: {},
    actionIds: [],
    selectedAction: undefined,
    selectedGroup: undefined,
    requestConfigure: {
        isOpen: true,
        node: undefined
    }
};

const component = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case PlatformActions.FETCH_COMPONENT_SUCCESS:
            let component = Object.assign({}, action.payload);
            component.blockAreas = component.blockAreas.map(area => area.id);
            return component;
        case PlatformActions.DROP_CONTROL_IN_FORM_SUCCESS:
            state.metadata = action.payload;
            return state;
        case PlatformActions.DROP_CONTROL_IN_ROW_SUCCESS:
            state.metadata = action.payload;
            return state;
        case PlatformActions.DROP_EXISTING_CONTROL_IN_FORM_SUCCESS:
            state.metadata = action.payload;
            return state;
        case PlatformActions.DELETE_CONTROL_SUCCESS:
            state.metadata = action.payload;
            return state;
        case PlatformActions.REORDER_FORM_ROWS_SUCCESS:
            state.metadata = action.payload;
            return state;
        case PlatformActions.REORDER_FORM_ROWS_TEMP:
            let temp = state.metadata.columns[0].rows[action.payload.fromIndex];
            state.metadata.columns[0].rows[action.payload.fromIndex] = state.metadata.columns[0].rows[action.payload.toIndex];
            state.metadata.columns[0].rows[action.payload.toIndex] = temp;
            return state;
        case PlatformActions.SELECT_FORM_ROW:
            state.metadata.columns[0].rows.map(row => row.controls.map(control => control.isSelected = false));
            state.metadata.columns[0].rows[action.payload.rowIndex].controls[action.payload.colIndex].isSelected = true;
            return state;
        case PlatformActions.UPDATE_CONTROL_CONFIG_SUCCESS:
            state.metadata.columns[0].rows[action.payload.controlRowIndex].controls[action.payload.controlIndex].config = action.payload.config;
            return state;
        case PlatformActions.UPDATE_COMPONENT_CONFIG_SUCCESS:
            state.config.groups[action.payload.groupIndex].rows[action.payload.rowIndex].fields[action.payload.fieldIndex].value = action.payload.value;
            return state;
        default:
            return state;
    }
};

const blockAreas = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case PlatformActions.FETCH_COMPONENT_SUCCESS:
            return normalize(action.payload.blockAreas);
        case PlatformActions.DROP_COMPONENT_IN_BLOCK_SUCCESS:
            state[action.payload.id] = action.payload;
            return state;
        case PlatformActions.SPLIT_VERTICALLY_SUCCESS:
            state[action.payload.id] = action.payload;
            return state;
        case PlatformActions.SPLIT_HORIZONTALLY_SUCCESS:
            state[action.payload.id] = action.payload;
            return state;
        case PlatformActions.SELECT:
            switch (args.selected.type) {
                case 'block':
                    state[args.selected.blockAreaId].blockMap[args.selected.id].isSelected = false;
                    break;
            }
            switch (action.payload.type) {
                case 'block':
                    state[action.payload.blockAreaId].blockMap[action.payload.id].isSelected = true;
                    break;
            }
            return state;
        default:
            return state;
    }
};

const reducers = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case PlatformActions.FETCH_REDUCERS_SUCCESS:
            return action.payload;
        default:
            return state;
    }
};

const mode = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case PlatformActions.SET_MODE:
            return action.payload;
        default:
            return state;
    }
};

const selected = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case PlatformActions.SELECT:
            return action.payload;
        case PlatformActions.SELECT_FORM_ROW:
            return {
                id: 0,
                type: 'formControl',
                blockAreaId: 0,
                controlRowIndex: action.payload.rowIndex,
                controlIndex: action.payload.colIndex,
                config: action.payload.control.config
            };
        // case PlatformActions.UPDATE_CONTROL_CONFIG_SUCCESS:
        //     return Object.assign(state, {
        //         config: action.payload.config
        //     });
        default:
            return state;
    }
};

const configTab = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case PlatformActions.SELECT_TAB:
            return action.payload;
        case PlatformActions.SELECT_FORM_ROW:
        case PlatformActions.SELECT:
            if (state == 'second') {
                return state;
            }
            return 'second';
        default:
            return state;
    }
};

const dragData = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case PlatformActions.DRAGGABLE_MOUSE_DOWN:
            return action.payload;
        case PlatformActions.DRAGGABLE_MOUSE_UP:
            return {
                id: 0,
                type: ''
            };
        case PlatformActions.REORDER_FORM_ROWS_TEMP:
            return {
                id: action.payload.toIndex,
                type: 'formRow'
            };
        default:
            return state;
    }
};

const editMode = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
    case PlatformActions.SET_EDIT_MODE:
        return action.payload;
        default:
            return state;
    }
};

const isLogicPanelOpen = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
    case PlatformActions.TOGGLE_LOGIC_PANEL:
        return !state;
        default:
            return state;
    }
};

const selectedLogicCategory = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
    case PlatformActions.SELECT_LOGIC_CATEGORY:
        return action.payload;
        default:
            return state;
    }
};

const selectedLogicItem = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
    case PlatformActions.SELECT_LOGIC_ITEM:
        return action.payload;
        default:
            return state;
    }
};

const actions = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case PlatformActions.GET_ACTIONS_SUCCESS:
            return normalize(action.payload);
        case PlatformActions.GET_MAP_SUCCESS:
            for (let i = 0; i < state[action.payload.groupId].actions.length; i++) {
                if (state[action.payload.groupId].actions[i].id == action.payload.actionId) {
                    state[action.payload.groupId].actions[i] = Object.assign(state[action.payload.groupId].actions[i], {
                        map: action.payload.map
                    });
                    break;
                }
            }
            return state;
        default:
            return state;
    }
};

const actionIds = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case PlatformActions.GET_ACTIONS_SUCCESS:
            return action.payload.map(group => group.id);
        default:
            return state;
    }
};

const selectedAction = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case PlatformActions.SELECT_ACTION:
            return action.payload;
        case PlatformActions.GET_ACTIONS_SUCCESS:
            if (action.payload.length == 0 || action.payload[0].actions.length == 0) {
                return state;
            }
            return action.payload[0].actions[0];
        default:
            return state;
    }
};

const selectedGroup = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case PlatformActions.SELECT_GROUP:
            return action.payload;
        case PlatformActions.GET_ACTIONS_SUCCESS:
            return action.payload.length > 0 ? action.payload[0] : undefined;
        default:
            return state;
    }
};

const requestConfigure = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case PlatformActions.TOGGLE_REQUEST_CONFIGURE:
            return {
                isOpen: !state.isOpen,
                node: action.payload
            };
        default:
            return state;
    }
};

export const platformReducer: ActionReducer<PlatformState> = (state: PlatformState = initialState, action: Action) => {
    switch (action.type) {
        case PlatformActions.WINDOW_UPDATES:
            return Object.assign({}, state, {
                blockAreas: blockAreas(state.blockAreas, action.payload, state),
                selected: selected(state.selected, action.payload),
                configTab: configTab(state.configTab, action.payload)
            });
        case PlatformActions.FORM_ACTIONS:
            return Object.assign({}, state, {
                component: component(state.component, action.payload, state),
                dragData: dragData(state.dragData, action.payload, state),
                selected: selected(state.selected, action.payload),
                configTab: configTab(state.configTab, action.payload)
            });
        case PlatformActions.FETCH_COMPONENT_SUCCESS:
            return Object.assign({}, state, {
                component: component(state.component, action),
                blockAreas: blockAreas(state.blockAreas, action)
            });
        case PlatformActions.FETCH_REDUCERS_SUCCESS:
            return Object.assign({}, state, {
                reducers: reducers(state.reducers, action)
            });
        case PlatformActions.SET_MODE:
            return Object.assign({}, state, {mode: mode(state.mode, action)});
        case PlatformActions.SELECT_TAB:
            return Object.assign({}, state, {configTab: configTab(state.configTab, action)});
        case PlatformActions.DRAGGABLE_MOUSE_DOWN:
            return Object.assign({}, state, {dragData: dragData(state.dragData, action)});
        case PlatformActions.DRAGGABLE_MOUSE_UP:
            return Object.assign({}, state, {dragData: dragData(state.dragData, action)});
        case PlatformActions.UPDATE_CONTROL_CONFIG_SUCCESS:
            return Object.assign({}, state, {
                component: component(state.component, action)
            });
        case PlatformActions.UPDATE_COMPONENT_CONFIG_SUCCESS:
            return Object.assign({}, state, {
                component: component(state.component, action)
            });
    case PlatformActions.SET_EDIT_MODE:
        return Object.assign({}, state, {
            editMode: editMode(state.editMode, action)
        });
    case PlatformActions.ADD_OUTPUT_SUCCESS:
        return state;
    case PlatformActions.TOGGLE_LOGIC_PANEL:
        return Object.assign({}, state, {
            isLogicPanelOpen: isLogicPanelOpen(state.isLogicPanelOpen, action)
        });
    case PlatformActions.SELECT_LOGIC_CATEGORY:
        return Object.assign({}, state, {
            selectedLogicCategory: selectedLogicCategory(state.selectedLogicCategory, action)
        });
    case PlatformActions.SELECT_LOGIC_ITEM:
        return Object.assign({}, state, {
            selectedLogicItem: selectedLogicItem(state.selectedLogicItem, action)
        });
    case PlatformActions.GET_MAP_SUCCESS:
        return Object.assign({}, state, {
            actions: actions(state.actions, action)
        });
    case PlatformActions.GET_ACTIONS_SUCCESS:
        return Object.assign({}, state, {
            actions: actions(state.actions, action),
            actionIds: actionIds(state.actionIds, action),
            selectedGroup: selectedGroup(state.selectedGroup, action),
            selectedAction: selectedAction(state.selectedAction, action)
        });
    case PlatformActions.SELECT_ACTION:
        return Object.assign({}, state, {
            selectedAction: selectedAction(state.selectedAction, action)
        });
    case PlatformActions.SELECT_GROUP:
        return Object.assign({}, state, {
            selectedGroup: selectedGroup(state.selectedGroup, action)
        });
    case PlatformActions.TOGGLE_REQUEST_CONFIGURE:
        return Object.assign({}, state, {
            requestConfigure: requestConfigure(state.requestConfigure, action)
        });
    default:
        return state;
    }
};
