import {ActionReducer, Action} from '@ngrx/store';
import {EngineActions} from '../actions/engine.actions';

export interface EngineState {
    isAddingSchemaType: boolean;
    schema: any;
    queryResult: any;
    selectedQuery: string;
    query: any;
}

export const initialState = {
    isAddingSchemaType: false,
    schema: {},
    queryResult: '',
    selectedQuery: 'fetch',
    query: {
        data: {
            fetch: {}
        }
    }
};

const isAddingSchemaType = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
    case EngineActions.SET_IS_ADDING_SCHEMA_TYPE:
        return action.payload;
    default:
        return state;
    }
};

const schema = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
    case EngineActions.GET_SCHEMA_SUCCESS:
        return action.payload;
    case EngineActions.REMOVE_TYPE_SUCCESS:
        let copy = Object.assign({}, state);
        delete copy[action.payload];
        return copy;
    case EngineActions.ADD_TYPE_SUCCESS:
        return Object.assign({}, state, {
            [action.payload]: {}
        });
    case EngineActions.ADD_FIELD_SUCCESS:
        return Object.assign({}, state, {
            [action.payload.typeName]: Object.assign(state[action.payload.typeName], {
                [action.payload.field.name]: action.payload.field
            })
        });
    case EngineActions.REMOVE_FIELD_SUCCESS:
        let copyField = Object.assign({}, state);
        delete copyField[action.payload.typeName][action.payload.field.name];
        return copyField;
    default:
        return state;
    }
};

const queryResult = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
    case EngineActions.RUN_QUERY_SUCCESS:
        return action.payload;
    default:
        return state;
    }
};

const selectedQuery = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
    case EngineActions.SELECT_QUERY:
        return action.payload;
    default:
        return state;
    }
};

const query = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case EngineActions.ADD_QUERY_TYPE:
            if (action.payload == '') {
                return state;
            }
            let copy = Object.assign({}, state);
            copy.data.fetch[action.payload] = {fields: {}};
            return copy;
        case EngineActions.ADD_QUERY_FIELD:
            if (action.payload.field == '') {
                return state;
            }
            let copy2 = Object.assign({}, state);
            copy2.data.fetch[action.payload.type].fields[action.payload.field] = {as: action.payload.field};
            return copy2;
    default:
        return state;
    }
};

export const engineReducer: ActionReducer<EngineState> = (state: EngineState = initialState, action: Action) => {
    switch (action.type) {
    case EngineActions.SET_IS_ADDING_SCHEMA_TYPE:
        return Object.assign({}, state, {
            isAddingSchemaType: isAddingSchemaType(state.isAddingSchemaType, action)
        });
    case EngineActions.GET_SCHEMA_SUCCESS:
        return Object.assign({}, state, {
            schema: schema(state.schema, action)
        });
    case EngineActions.REMOVE_TYPE_SUCCESS:
        return Object.assign({}, state, {
            schema: schema(state.schema, action)
        });
    case EngineActions.ADD_TYPE_SUCCESS:
        return Object.assign({}, state, {
            schema: schema(state.schema, action)
        });
    case EngineActions.ADD_FIELD_SUCCESS:
        return Object.assign({}, state, {
            schema: schema(state.schema, action)
        });
    case EngineActions.REMOVE_FIELD_SUCCESS:
        return Object.assign({}, state, {
            schema: schema(state.schema, action)
        });
    case EngineActions.RUN_QUERY_SUCCESS:
        return Object.assign({}, state, {
            queryResult: queryResult(state.queryResult, action)
        });
    case EngineActions.SELECT_QUERY:
        return Object.assign({}, state, {
            selectedQuery: selectedQuery(state.selectedQuery, action)
        });
    case EngineActions.ADD_QUERY_TYPE:
        return Object.assign({}, state, {
            query: query(state.query, action)
        });
    case EngineActions.ADD_QUERY_FIELD:
        return Object.assign({}, state, {
            query: query(state.query, action)
        });
    default:
        return state;
    }
};
