import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';

@Injectable()
export class EngineActions {
    static SET_IS_ADDING_SCHEMA_TYPE = 'PLATFORM_IS_ADDING_SCHEMA_TYPE';
    setIsAddingSchemaType(data): Action {
        return {
            type: EngineActions.SET_IS_ADDING_SCHEMA_TYPE,
            payload: data
        };
    }

    static GET_SCHEMA = 'ENGINE_GET_SCHEMA';
    getSchema(data): Action {
        return {
            type: EngineActions.GET_SCHEMA,
            payload: data
        };
    }

    static GET_SCHEMA_SUCCESS = 'ENGINE_GET_SCHEMA_SUCCESS';
    getSchemaSuccess(data): Action {
        return {
            type: EngineActions.GET_SCHEMA_SUCCESS,
            payload: data
        };
    }

    static GET_SCHEMA_FAILURE = 'ENGINE_GET_SCHEMA_FAILURE';
    getSchemaFailure(data): Action {
        return {
            type: EngineActions.GET_SCHEMA_FAILURE,
            payload: data
        };
    }

    static ADD_TYPE = 'ENGINE_ADD_TYPE';
    addType(data): Action {
        return {
            type: EngineActions.ADD_TYPE,
            payload: data
        };
    }

    static ADD_TYPE_SUCCESS = 'ENGINE_ADD_TYPE_SUCCESS';
    addTypeSuccess(data): Action {
        return {
            type: EngineActions.ADD_TYPE_SUCCESS,
            payload: data
        };
    }

    static ADD_TYPE_FAILURE = 'ENGINE_ADD_TYPE_FAILURE';
    addTypeFailure(data): Action {
        return {
            type: EngineActions.ADD_TYPE_FAILURE,
            payload: data
        };
    }

    static REMOVE_TYPE = 'ENGINE_REMOVE_TYPE';
    removeType(data): Action {
        return {
            type: EngineActions.REMOVE_TYPE,
            payload: data
        };
    }

    static REMOVE_TYPE_SUCCESS = 'ENGINE_REMOVE_TYPE_SUCCESS';
    removeTypeSuccess(data): Action {
        return {
            type: EngineActions.REMOVE_TYPE_SUCCESS,
            payload: data
        };
    }

    static REMOVE_TYPE_FAILURE = 'ENGINE_REMOVE_TYPE_FAILURE';
    removeTypeFailure(data): Action {
        return {
            type: EngineActions.REMOVE_TYPE_FAILURE,
            payload: data
        };
    }

    static ADD_FIELD = 'ENGINE_ADD_FIELD';
    addField(data): Action {
        return {
            type: EngineActions.ADD_FIELD,
            payload: data
        };
    }
    
    static ADD_FIELD_SUCCESS = 'ENGINE_ADD_FIELD_SUCCESS';
    addFieldSuccess(data): Action {
        return {
            type: EngineActions.ADD_FIELD_SUCCESS,
            payload: data
        };
    }

    static ADD_FIELD_FAILURE = 'ENGINE_ADD_FIELD_FAILURE';
    addFieldFailure(data): Action {
        return {
            type: EngineActions.ADD_FIELD_FAILURE,
            payload: data
        };
    }

    static REMOVE_FIELD = 'ENGINE_REMOVE_FIELD';
    removeField(data): Action {
        return {
            type: EngineActions.REMOVE_FIELD,
            payload: data
        };
    }

    static REMOVE_FIELD_SUCCESS = 'ENGINE_REMOVE_FIELD_SUCCESS';
    removeFieldSuccess(data): Action {
        return {
            type: EngineActions.REMOVE_FIELD_SUCCESS,
            payload: data
        };
    }

    static REMOVE_FIELD_FAILURE = 'ENGINE_REMOVE_FIELD_FAILURE';
    removeFieldFailure(data): Action {
        return {
            type: EngineActions.REMOVE_FIELD_FAILURE,
            payload: data
        };
    }

    static RUN_QUERY = 'ENGINE_RUN_QUERY';
    runQuery(data): Action {
        return {
            type: EngineActions.RUN_QUERY,
            payload: data
        };
    }
    
    static RUN_QUERY_SUCCESS = 'ENGINE_RUN_QUERY_SUCCESS';
    runQuerySuccess(data): Action {
        return {
            type: EngineActions.RUN_QUERY_SUCCESS,
            payload: data
        };
    }

    static RUN_QUERY_FAILURE = 'ENGINE_RUN_QUERY_FAILURE';
    runQueryFailure(data): Action {
        return {
            type: EngineActions.RUN_QUERY_FAILURE,
            payload: data
        };
    }

    static SELECT_QUERY = 'ENGINE_SELECT_QUERY';
    selectQuery(data): Action {
        return {
            type: EngineActions.SELECT_QUERY,
            payload: data
        };
    }

    static ADD_QUERY_TYPE = 'ENGINE_ADD_QUERY_TYPE';
    addQueryType(data): Action {
        return {
            type: EngineActions.ADD_QUERY_TYPE,
            payload: data
        };
    }

    static ADD_QUERY_FIELD = 'ENGINE_ADD_QUERY_FIELD';
    addQueryField(data): Action {
        return {
            type: EngineActions.ADD_QUERY_FIELD,
            payload: data
        };
    }
}
