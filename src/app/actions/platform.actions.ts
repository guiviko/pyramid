import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';
import {Block} from "../models/block";

@Injectable()
export class PlatformActions {
    static WINDOW_UPDATES = 'PLATFORM_WINDOW_UPDATES';
    windowUpdates(data: any): Action {
        return {
            type: PlatformActions.WINDOW_UPDATES,
            payload: data
        };
    }

    static FETCH_COMPONENT = 'PLATFORM_FETCH_COMPONENT';
    fetchComponent(data): Action {
        return {
            type: PlatformActions.FETCH_COMPONENT,
            payload: data
        };
    }

    static FETCH_COMPONENT_SUCCESS = 'PLATFORM_FETCH_COMPONENT_SUCCESS';
    fetchComponentSuccess(data): Action {
        return {
            type: PlatformActions.FETCH_COMPONENT_SUCCESS,
            payload: data
        };
    }

    static FETCH_COMPONENT_FAILURE = 'PLATFORM_FETCH_COMPONENT_FAILURE';
    fetchComponentFailure(data): Action {
        return {
            type: PlatformActions.FETCH_COMPONENT_FAILURE,
            payload: data
        };
    }

    static FETCH_REDUCERS = 'PLATFORM_FETCH_REDUCERS';
    fetchReducers(data): Action {
        return {
            type: PlatformActions.FETCH_REDUCERS,
            payload: data
        };
    }
    
    static FETCH_REDUCERS_SUCCESS = 'PLATFORM_FETCH_REDUCERS_SUCCESS';
    fetchReducersSuccess(data): Action {
        return {
            type: PlatformActions.FETCH_REDUCERS_SUCCESS,
            payload: data
        };
    }

    static FETCH_REDUCERS_FAILURE = 'PLATFORM_FETCH_REDUCERS_FAILURE';
    fetchReducersFailure(data): Action {
        return {
            type: PlatformActions.FETCH_REDUCERS_FAILURE,
            payload: data
        };
    }

    static SET_MODE = 'PLATFORM_SET_MODE';
    setMode(mode: string): Action {
        return {
            type: PlatformActions.SET_MODE,
            payload: mode
        };
    }

    static SPLIT_VERTICALLY = 'PLATFORM_SPLIT_VERTICALLY';
    splitVertically(data): Action {
        return {
            type: PlatformActions.SPLIT_VERTICALLY,
            payload: {block: data.block, value: data.value, blockAreaId: data.blockAreaId}
        };
    }

    static SPLIT_VERTICALLY_SUCCESS = 'PLATFORM_SPLIT_VERTICALLY_SUCCESS';
    splitVerticallySuccess(data): Action {
        return {
            type: PlatformActions.SPLIT_VERTICALLY_SUCCESS,
            payload: data
        };
    }

    static SPLIT_VERTICALLY_FAILURE = 'PLATFORM_SPLIT_VERTICALLY_FAILURE';
    splitVerticallyFailure(): Action {
        return {
            type: PlatformActions.SPLIT_VERTICALLY_FAILURE
        };
    }

    static SPLIT_HORIZONTALLY = 'PLATFORM_SPLIT_HORIZONTALLY';
    splitHorizontally(data): Action {
        return {
            type: PlatformActions.SPLIT_HORIZONTALLY,
            payload: {block: data.block, value: data.value, blockAreaId: data.blockAreaId}
        };
    }

    static SPLIT_HORIZONTALLY_SUCCESS = 'PLATFORM_SPLIT_HORIZONTALLY_SUCCESS';
    splitHorizontallySuccess(data): Action {
        return {
            type: PlatformActions.SPLIT_HORIZONTALLY_SUCCESS,
            payload: data
        };
    }

    static SPLIT_HORIZONTALLY_FAILURE = 'PLATFORM_SPLIT_HORIZONTALLY_FAILURE';
    splitHorizontallyFailure(): Action {
        return {
            type: PlatformActions.SPLIT_HORIZONTALLY_FAILURE
        };
    }

    static SELECT = 'SELECT';
    select(data: any): Action {
        return {
            type: PlatformActions.SELECT,
            payload: {
                blockAreaId: data.blockAreaId,
                id: data.id,
                type: data.type
            }
        };
    }

    static SELECT_TAB = 'SELECT_TAB';
    selectTab(data: any): Action {
        return {
            type: PlatformActions.SELECT_TAB,
            payload: data
        };
    }

    static DRAGGABLE_MOUSE_DOWN = 'DRAGGABLE_MOUSE_DOWN';
    draggableMouseDown(data: any): Action {
        return {
            type: PlatformActions.DRAGGABLE_MOUSE_DOWN,
            payload: {
                type: data.type,
                id: data.id
            }
        };
    }

    static DRAGGABLE_MOUSE_UP = 'DRAGGABLE_MOUSE_UP';
    draggableMouseUp(data: any): Action {
        return {
            type: PlatformActions.DRAGGABLE_MOUSE_UP,
            payload: data
        };
    }
    
    static OPEN_DROP_COMPONENT_MODAL = 'OPEN_DROP_COMPONENT_MODAL';
    openDropComponentModal(data: any): Action {
        return {
            type: PlatformActions.OPEN_DROP_COMPONENT_MODAL,
            payload: data
        };
    }

    static DROP_COMPONENT_IN_BLOCK = 'DROP_COMPONENT_IN_BLOCK';
    dropComponentInBlock(data: any): Action {
        return {
            type: PlatformActions.DROP_COMPONENT_IN_BLOCK,
            payload: data
        };
    }

    static DROP_COMPONENT_IN_BLOCK_SUCCESS = 'DROP_COMPONENT_IN_BLOCK_SUCCESS';
    dropComponentInBlockSuccess(data): Action {
        return {
            type: PlatformActions.DROP_COMPONENT_IN_BLOCK_SUCCESS,
            payload: data
        };
    }

    static DROP_COMPONENT_IN_BLOCK_FAILURE = 'DROP_COMPONENT_IN_BLOCK_FAILURE';
    dropComponentInBlockFailure(): Action {
        return {
            type: PlatformActions.DROP_COMPONENT_IN_BLOCK_FAILURE
        };
    }

    static FORM_ACTIONS = 'FORM_ACTIONS';
    formActions(action): Action {
        return {
            type: PlatformActions.FORM_ACTIONS,
            payload: action
        };
    }

    static DROP_CONTROL_IN_FORM = 'PLATFORM_DROP_CONTROL_IN_FORM';
    dropControlInForm(data): Action {
        return {
            type: PlatformActions.DROP_CONTROL_IN_FORM,
            payload: data
        };
    }

    static DROP_CONTROL_IN_FORM_SUCCESS = 'PLATFORM_DROP_CONTROL_IN_FORM_SUCCESS';
    dropControlInFormSuccess(data): Action {
        return {
            type: PlatformActions.DROP_CONTROL_IN_FORM_SUCCESS,
            payload: data
        };
    }

    static DROP_CONTROL_IN_FORM_FAILURE = 'PLATFORM_DROP_CONTROL_IN_FORM_FAILURE';
    dropControlInFormFailure(): Action {
        return {
            type: PlatformActions.DROP_CONTROL_IN_FORM_FAILURE
        };
    }

    static DROP_EXISTING_CONTROL_IN_FORM = 'PLATFORM_DROP_EXISTING_CONTROL_IN_FORM';
    dropExistingControlInForm(data): Action {
        return {
            type: PlatformActions.DROP_EXISTING_CONTROL_IN_FORM,
            payload: data
        };
    }

    static DROP_EXISTING_CONTROL_IN_FORM_SUCCESS = 'PLATFORM_DROP_EXISTING_CONTROL_IN_FORM_SUCCESS';
    dropExistingControlInFormSuccess(data): Action {
        return {
            type: PlatformActions.DROP_EXISTING_CONTROL_IN_FORM_SUCCESS,
            payload: data
        };
    }

    static DROP_EXISTING_CONTROL_IN_FORM_FAILURE = 'PLATFORM_DROP_EXISTING_CONTROL_IN_FORM_FAILURE';
    dropExistingControlInFormFailure(data): Action {
        return {
            type: PlatformActions.DROP_EXISTING_CONTROL_IN_FORM_FAILURE,
            payload: data
        };
    }

    static DROP_CONTROL_IN_ROW = 'PLATFORM_DROP_CONTROL_IN_ROW';
    dropControlInRow(data): Action {
        return {
            type: PlatformActions.DROP_CONTROL_IN_ROW,
            payload: data
        };
    }

    static DROP_CONTROL_IN_ROW_SUCCESS = 'PLATFORM_DROP_CONTROL_IN_ROW_SUCCESS';
    dropControlInRowSuccess(data): Action {
        return {
            type: PlatformActions.DROP_CONTROL_IN_ROW_SUCCESS,
            payload: data
        };
    }

    static DROP_CONTROL_IN_ROW_FAILURE = 'PLATFORM_DROP_CONTROL_IN_ROW_FAILURE';
    dropControlInRowFailure(data): Action {
        return {
            type: PlatformActions.DROP_CONTROL_IN_ROW_FAILURE,
            payload: data
        };
    }

    static DELETE_CONTROL = 'PLATFORM_DELETE_CONTROL';
    deleteControl(data): Action {
        return {
            type: PlatformActions.DELETE_CONTROL,
            payload: data
        };
    }

    static DELETE_CONTROL_SUCCESS = 'PLATFORM_DELETE_CONTROL_SUCCESS';
    deleteControlSuccess(data): Action {
        return {
            type: PlatformActions.DELETE_CONTROL_SUCCESS,
            payload: data
        };
    }

    static DELETE_CONTROL_FAILURE = 'PLATFORM_DELETE_CONTROL_FAILURE';
    deleteControlFailure(data): Action {
        return {
            type: PlatformActions.DELETE_CONTROL_FAILURE,
            payload: data
        };
    }

    static REORDER_FORM_ROWS = 'PLATFORM_REORDER_FORM_ROWS';
    reorderFormRows(data): Action {
        return {
            type: PlatformActions.REORDER_FORM_ROWS,
            payload: data
        };
    }

    static REORDER_FORM_ROWS_SUCCESS = 'PLATFORM_REORDER_FORM_ROWS_SUCCESS';
    reorderFormRowsSuccess(data): Action {
        return {
            type: PlatformActions.REORDER_FORM_ROWS_SUCCESS,
            payload: data
        };
    }

    static REORDER_FORM_ROWS_FAILURE = 'PLATFORM_REORDER_FORM_ROWS_FAILURE';
    reorderFormRowsFailure(): Action {
        return {
            type: PlatformActions.REORDER_FORM_ROWS_FAILURE
        };
    }

    static REORDER_FORM_ROWS_TEMP = 'PLATFORM_REORDER_FORM_ROWS_TEMP';
    reorderFormRowsTemp(data): Action {
        return {
            type: PlatformActions.REORDER_FORM_ROWS_TEMP,
            payload: data
        };
    }

    static SELECT_FORM_ROW = 'PLATFORM_SELECT_FORM_ROW';
    selectFormRow(data): Action {
        return {
            type: PlatformActions.SELECT_FORM_ROW,
            payload: data
        };
    }

    static UPDATE_CONTROL_CONFIG = 'PLATFORM_UPDATE_CONTROL_CONFIG';
    updateControlConfig(data): Action {
        return {
            type: PlatformActions.UPDATE_CONTROL_CONFIG,
            payload: data
        };
    }

    static UPDATE_CONTROL_CONFIG_SUCCESS = 'PLATFORM_UPDATE_CONTROL_CONFIG_SUCCESS';
    updateControlConfigSuccess(data): Action {
        return {
            type: PlatformActions.UPDATE_CONTROL_CONFIG_SUCCESS,
            payload: data
        };
    }

    static UPDATE_CONTROL_CONFIG_FAILURE = 'PLATFORM_UPDATE_CONTROL_CONFIG_FAILURE';
    updateControlConfigFailure(): Action {
        return {
            type: PlatformActions.UPDATE_CONTROL_CONFIG_FAILURE
        };
    }

    static UPDATE_COMPONENT_CONFIG = 'PLATFORM_UPDATE_COMPONENT_CONFIG';
    updateComponentConfig(data): Action {
        return {
            type: PlatformActions.UPDATE_COMPONENT_CONFIG,
            payload: data
        };
    }

    static UPDATE_COMPONENT_CONFIG_SUCCESS = 'PLATFORM_UPDATE_COMPONENT_CONFIG_SUCCESS';
    updateComponentConfigSuccess(data): Action {
        return {
            type: PlatformActions.UPDATE_COMPONENT_CONFIG_SUCCESS,
            payload: data
        };
    }

    static UPDATE_COMPONENT_CONFIG_FAILURE = 'PLATFORM_UPDATE_COMPONENT_CONFIG_FAILURE';
    updateComponentConfigFailure(data): Action {
        return {
            type: PlatformActions.UPDATE_COMPONENT_CONFIG_FAILURE,
            payload: data
        };
    }

    static SET_EDIT_MODE = 'PLATFORM_SET_EDIT_MODE';
    setEditMode(data): Action {
        return {
            type: PlatformActions.SET_EDIT_MODE,
            payload: data
        };
    }

    static ADD_OUTPUT = 'PLATFORM_ADD_OUTPUT';
    addOutput(data): Action {
        return {
            type: PlatformActions.ADD_OUTPUT,
            payload: data
        };
    }

    static ADD_OUTPUT_SUCCESS = 'PLATFORM_ADD_OUTPUT_SUCCESS';
    addOutputSuccess(data): Action {
        return {
            type: PlatformActions.ADD_OUTPUT_SUCCESS,
            payload: data
        };
    }

    static ADD_OUTPUT_FAILURE = 'PLATFORM_ADD_OUTPUT_FAILURE';
    addOutputFailure(data): Action {
        return {
            type: PlatformActions.ADD_OUTPUT_FAILURE,
            payload: data
        };
    }

    static CONNECT_OUTPUT_TO_REDUCER = 'PLATFORM_CONNECT_OUTPUT_TO_REDUCER';
    connectOutputToReducer(data): Action {
        return {
            type: PlatformActions.CONNECT_OUTPUT_TO_REDUCER,
            payload: data
        };
    }

    static CONNECT_OUTPUT_TO_REDUCER_SUCCESS = 'PLATFORM_CONNECT_OUTPUT_TO_REDUCER_SUCCESS';
    connectOutputToReducerSuccess(data): Action {
        return {
            type: PlatformActions.CONNECT_OUTPUT_TO_REDUCER_SUCCESS,
            payload: data
        };
    }

    static CONNECT_OUTPUT_TO_REDUCER_FAILURE = 'PLATFORM_CONNECT_OUTPUT_TO_REDUCER_FAILURE';
    connectOutputToReducerFailure(data): Action {
        return {
            type: PlatformActions.CONNECT_OUTPUT_TO_REDUCER_FAILURE,
            payload: data
        };
    }

    static DISCONNECT_OUTPUT_FROM_REDUCER = 'PLATFORM_DISCONNECT_OUTPUT_FROM_REDUCER';
    disconnectOutputFromReducer(data): Action {
        return {
            type: PlatformActions.DISCONNECT_OUTPUT_FROM_REDUCER,
            payload: data
        };
    }

    static DISCONNECT_OUTPUT_FROM_REDUCER_SUCCESS = 'PLATFORM_DISCONNECT_OUTPUT_FROM_REDUCER_SUCCESS';
    disconnectOutputFromReducerSuccess(data): Action {
        return {
            type: PlatformActions.DISCONNECT_OUTPUT_FROM_REDUCER_SUCCESS,
            payload: data
        };
    }

    static DISCONNECT_OUTPUT_FROM_REDUCER_FAILURE = 'PLATFORM_DISCONNECT_OUTPUT_FROM_REDUCER_FAILURE';
    disconnectOutputFromReducerFailure(data): Action {
        return {
            type: PlatformActions.DISCONNECT_OUTPUT_FROM_REDUCER_FAILURE,
            payload: data
        };
    }

    static ADD_REDUCER_FIELD = 'PLATFORM_ADD_REDUCER_FIELD';
    addReducerField(data): Action {
        return {
            type: PlatformActions.ADD_REDUCER_FIELD,
            payload: data
        };
    }

    static ADD_REDUCER_FIELD_SUCCESS = 'PLATFORM_ADD_REDUCER_FIELD_SUCCESS';
    addReducerFieldSuccess(data): Action {
        return {
            type: PlatformActions.ADD_REDUCER_FIELD_SUCCESS,
            payload: data
        };
    }

    static ADD_REDUCER_FIELD_FAILURE = 'PLATFORM_ADD_REDUCER_FIELD_FAILURE';
    addReducerFieldFailure(data): Action {
        return {
            type: PlatformActions.ADD_REDUCER_FIELD_FAILURE,
            payload: data
        };
    }

    static TOGGLE_LOGIC_PANEL = 'PLATFORM_TOGGLE_LOGIC_PANEL';
    toggleLogicPanel(data): Action {
        return {
            type: PlatformActions.TOGGLE_LOGIC_PANEL,
            payload: data
        };
    }

    static SELECT_LOGIC_CATEGORY = 'PLATFORM_SELECT_LOGIC_CATEGORY';
    selectLogicCategory(data): Action {
        return {
            type: PlatformActions.SELECT_LOGIC_CATEGORY,
            payload: data
        };
    }

    static SELECT_LOGIC_ITEM = 'PLATFORM_SELECT_LOGIC_ITEM';
    selectLogicItem(data): Action {
        return {
            type: PlatformActions.SELECT_LOGIC_ITEM,
            payload: data
        };
    }

    static SAVE_REDUCER_CONTENT = 'PLATFORM_SAVE_REDUCER_CONTENT';
    saveReducerContent(data): Action {
        return {
            type: PlatformActions.SAVE_REDUCER_CONTENT,
            payload: data
        };
    }

    static SAVE_REDUCER_CONTENT_SUCCESS = 'PLATFORM_SAVE_REDUCER_CONTENT_SUCCESS';
    saveReducerContentSuccess(data): Action {
        return {
            type: PlatformActions.SAVE_REDUCER_CONTENT_SUCCESS,
            payload: data
        };
    }

    static SAVE_REDUCER_CONTENT_FAILURE = 'PLATFORM_SAVE_REDUCER_CONTENT_FAILURE';
    saveReducerContentFailure(data): Action {
        return {
            type: PlatformActions.SAVE_REDUCER_CONTENT_FAILURE,
            payload: data
        };
    }

    static ADD_INPUT = 'PLATFORM_ADD_INPUT';
    addInput(data): Action {
        return {
            type: PlatformActions.ADD_INPUT,
            payload: data
        };
    }

    static ADD_INPUT_SUCCESS = 'PLATFORM_ADD_INPUT_SUCCESS';
    addInputSuccess(data): Action {
        return {
            type: PlatformActions.ADD_INPUT_SUCCESS,
            payload: data
        };
    }

    static ADD_INPUT_FAILURE = 'PLATFORM_ADD_INPUT_FAILURE';
    addInputFailure(data): Action {
        return {
            type: PlatformActions.ADD_INPUT_FAILURE,
            payload: data
        };
    }

    static GET_MAP = 'GET_MAP';
    getMap(data): Action {
        return {
            type: PlatformActions.GET_MAP,
            payload: data
        };
    }

    static GET_MAP_SUCCESS = 'GET_MAP_SUCCESS';
    getMapSuccess(data): Action {
        return {
            type: PlatformActions.GET_MAP_SUCCESS,
            payload: data
        };
    }

    static GET_MAP_FAILURE = 'GET_MAP_FAILURE';
    getMapFailure(data): Action {
        return {
            type: PlatformActions.GET_MAP_FAILURE,
            payload: data
        };
    }

    static UPDATE = 'UPDATE';
    update(data): Action {
        return {
            type: PlatformActions.UPDATE,
            payload: data
        };
    }

    static UPDATE_SUCCESS = 'UPDATE_SUCCESS';
    updateSuccess(data): Action {
        return {
            type: PlatformActions.UPDATE_SUCCESS,
            payload: data
        };
    }

    static UPDATE_FAILURE = 'UPDATE_FAILURE';
    updateFailure(data): Action {
        return {
            type: PlatformActions.UPDATE_FAILURE,
            payload: data
        };
    }

    static GET_ACTIONS = 'GET_ACTIONS';
    getActions(data): Action {
        return {
            type: PlatformActions.GET_ACTIONS,
            payload: data
        };
    }

    static GET_ACTIONS_SUCCESS = 'GET_ACTIONS_SUCCESS';
    getActionsSuccess(data): Action {
        return {
            type: PlatformActions.GET_ACTIONS_SUCCESS,
            payload: data
        };
    }

    static GET_ACTIONS_FAILURE = 'GET_ACTIONS_FAILURE';
    getActionsFailure(data): Action {
        return {
            type: PlatformActions.GET_ACTIONS_FAILURE,
            payload: data
        };
    }

    static CREATE_ACTION_GROUP = 'CREATE_ACTION_GROUP';
    createActionGroup(data): Action {
        return {
            type: PlatformActions.CREATE_ACTION_GROUP,
            payload: data
        };
    }

    static CREATE_ACTION_GROUP_SUCCESS = 'CREATE_ACTION_GROUP_SUCCESS';
    createActionGroupSuccess(data): Action {
        return {
            type: PlatformActions.CREATE_ACTION_GROUP_SUCCESS,
            payload: data
        };
    }

    static CREATE_ACTION_GROUP_FAILURE = 'CREATE_ACTION_GROUP_FAILURE';
    createActionGroupFailure(data): Action {
        return {
            type: PlatformActions.CREATE_ACTION_GROUP_FAILURE,
            payload: data
        };
    }

    static CREATE_ACTION = 'CREATE_ACTION';
    createAction(data): Action {
        return {
            type: PlatformActions.CREATE_ACTION,
            payload: data
        };
    }

    static CREATE_ACTION_SUCCESS = 'CREATE_ACTION_SUCCESS';
    createActionSuccess(data): Action {
        return {
            type: PlatformActions.CREATE_ACTION_SUCCESS,
            payload: data
        };
    }

    static CREATE_ACTION_FAILURE = 'CREATE_ACTION_FAILURE';
    createActionFailure(data): Action {
        return {
            type: PlatformActions.CREATE_ACTION_FAILURE,
            payload: data
        };
    }

    static SELECT_ACTION = 'SELECT_ACTION';
    selectAction(data): Action {
        return {
            type: PlatformActions.SELECT_ACTION,
            payload: data
        };
    }

    static SELECT_GROUP = 'SELECT_GROUP';
    selectGroup(data): Action {
        return {
            type: PlatformActions.SELECT_GROUP,
            payload: data
        };
    }

    static TOGGLE_REQUEST_CONFIGURE = 'TOGGLE_REQUEST_CONFIGURE';
    toggleRequestConfigure(data): Action {
        return {
            type: PlatformActions.TOGGLE_REQUEST_CONFIGURE,
            payload: data
        };
    }

    static CREATE_STATE_FIELD = 'CREATE_STATE_FIELD';
    createStateField(data): Action {
        return {
            type: PlatformActions.CREATE_STATE_FIELD,
            payload: data
        };
    }

    static CREATE_STATE_FIELD_SUCCESS = 'CREATE_STATE_FIELD_SUCCESS';
    createStateFieldSuccess(data): Action {
        return {
            type: PlatformActions.CREATE_STATE_FIELD_SUCCESS,
            payload: data
        };
    }

    static CREATE_STATE_FIELD_FAILURE = 'CREATE_STATE_FIELD_FAILURE';
    createStateFieldFailure(data): Action {
        return {
            type: PlatformActions.CREATE_STATE_FIELD_FAILURE,
            payload: data
        };
    }
}
