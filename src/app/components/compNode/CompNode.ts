import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'comp-node',
    templateUrl: './compNode.html',
    styleUrls: ['./compNode.css']
})
export class CompNode {
    @Input() component;
    @Output() addAction = new EventEmitter<any>();
    @Output() deleteAction = new EventEmitter<any>();
}
