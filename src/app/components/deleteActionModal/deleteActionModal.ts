import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Http} from "@angular/http";

@Component({
    selector: 'delete-action-modal',
    templateUrl: 'deleteActionModal.html',
    styleUrls: ['deleteActionModal.css']
})
export class DeleteActionModal {
    @Input() reducerNames;
    @Input() pageName;
    @Output() shouldRefresh = new EventEmitter<any>();
    show: boolean;
    compName: any;
    actionName;
    includePage: boolean = false;
    includeReducer: boolean = false;

    constructor(private http: Http) {
        this.show = false;
    }

    ngOnInit() {
        window.addEventListener('keypress', e => this.handleKeyPress(e));
    }

    open(compName: any, action: any): void {
        this.compName = compName;
        this.actionName = action;
        this.show = true;
    }

    continue(): void {
        this.show = false;

        if (!this.compName) {
            return;
        }

        this.http.post('/api/components/delete-action', {
            componentName: this.compName,
            actionName: this.actionName,
            pageName: this.pageName,
            includePage: this.includePage,
            includeReducer: this.includeReducer
        })
            .map(response => response.json())
            .subscribe(
                data => {
                    console.log('worked');
                    this.shouldRefresh.emit();
                },
                err => {
                    console.log('error');
                }
            );
    }

    cancel(): void {
        if (!this.show) {
            return;
        }

        this.show = false;
    }

    cancelWithClick(e): void {
        if ((e.target.className).indexOf('veil') == -1) {
            return;
        }

        this.cancel();
    }

    handleKeyPress(e): void {
        if (e.keyCode == 27) {
            this.cancel();
        }
    }
}