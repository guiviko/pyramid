import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'window',
    templateUrl: './window.html',
    styleUrls: ['./window.css']
})
export class Window {
    @Input() blockArea: any;
    @Input() mode: string;
    @Input() projectId: any;
    @Output() windowUpdates = new EventEmitter<any>();
}
