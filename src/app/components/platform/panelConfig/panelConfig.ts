import {Component, Input, EventEmitter, Output} from '@angular/core';

@Component({
    selector: 'panel-config',
    templateUrl: './panelConfig.html',
    styleUrls: ['./panelConfig.css']
})
export class PanelConfig {
    @Input() component;
    @Input() mode: string;
    @Input() projectId;
    @Output() windowUpdates = new EventEmitter<any>();
}
