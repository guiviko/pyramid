import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'empty-config',
    templateUrl: './emptyConfig.html',
    styleUrls: ['./emptyConfig.css']
})
export class EmptyConfig {
    @Input() component;
    @Input() mode: string;
    @Input() projectId;
    @Output() windowUpdates = new EventEmitter<any>();
}
