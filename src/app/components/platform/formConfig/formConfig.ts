import {Component, Input, EventEmitter, Output} from '@angular/core';
import {PlatformActions} from "../../../actions/platform.actions";

@Component({
    selector: 'form-config',
    templateUrl: './formConfig.html',
    styleUrls: ['./formConfig.css']
})
export class FormConfig {
    @Input() component;
    @Input() mode: string;
    @Input() dragData: any;
    @Input() projectId;
    @Output() formActions = new EventEmitter<any>();
    @Output() draggableMouseDown = new EventEmitter<any>();
    @Output() draggableMouseUp = new EventEmitter<any>();
    originalRowIndex = -1;

    constructor(private platformActions: PlatformActions) {

    }

    parseId(id) {
        return parseInt(id);
    }

    handleBodyDrop(data) {
        switch (data.type) {
            case 'formControl':
                this.formActions.emit(this.platformActions.dropControlInForm({
                    controlId: data.payload.id
                }));
                break;
            case 'formExistingControl':
                this.formActions.emit(this.platformActions.dropExistingControlInForm({
                    rowIndex: data.payload.rowIndex,
                    controlIndex: data.payload.controlIndex
                }));
                break;
        }
    }

    handleRowDrop(data, rowIndex) {
        switch (data.type) {
            case 'formControl':
                this.formActions.emit(this.platformActions.dropControlInRow({
                    controlId: data.payload.id,
                    rowIndex: rowIndex
                }));
                break;
            case 'formExistingControl':
                console.log('drop existing in row');
                break;
        }
    }

    deleteControl(e, rowIndex, controlIndex) {
        e.stopPropagation();

        this.formActions.emit(this.platformActions.deleteControl({
            rowIndex, controlIndex
        }));
    }

    handleBodyMouseUp(e) {
        if (this.dragData.type != 'formRow') {
            return;
        }

        this.formActions.emit(this.platformActions.reorderFormRows({
            firstRowIndex: this.originalRowIndex,
            secondRowIndex: this.dragData.id
        }));

        this.originalRowIndex = -1;
    }

    handleMouseUp(e) {
        this.draggableMouseUp.emit();
    }

    handleRowMouseEnter(e, index) {
        if (this.dragData.type != 'formRow' || this.dragData.id == index) {
            return;
        }

        // if (this.dragData.id < index) {
        //     this.initialMouseY += (e.target.getBoundingClientRect().height + 8);
        // } else {
        //     this.initialMouseY -= (e.target.getBoundingClientRect().height + 8);
        // }

        this.formActions.emit(this.platformActions.reorderFormRowsTemp({
            fromIndex: this.dragData.id,
            toIndex: index
        }));
    }

    selectFormControl(rowIndex, colIndex, control) {
        this.formActions.emit(this.platformActions.selectFormRow({
            rowIndex, colIndex, control
        }));
    }
}