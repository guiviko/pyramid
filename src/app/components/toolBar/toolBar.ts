import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'tool-bar',
    templateUrl: './toolBar.html',
    styleUrls: ['./toolBar.css']
})
export class ToolBar {
    @Input() mode: string;
    @Output() setMode = new EventEmitter<any>();

    ngOnInit() {
        window.addEventListener('keypress', e => this.handleKeyPress(e));
    }

    handleKeyPress(e) {
        switch (e.keyCode) {
            case 118:
                this.setMode.emit('verticalSplit');
                break;
            case 104:
                this.setMode.emit('horizontalSplit');
                break;
            case 115:
                this.setMode.emit('select');
                break;
        }
    }
}
