import {Component, EventEmitter, HostBinding, Input, Output} from '@angular/core';
import {ConnectionService} from "../../services/connection.service";

@Component({
    selector: 'node',
    templateUrl: './node.html',
    styleUrls: ['./node.css']
})
export class Node {
    @Input() node;
    @Input() map;
    @Output() updateMap = new EventEmitter<any>();
    @Output() toggleRequestConfigure = new EventEmitter<any>();
    originalNodeX;
    originalNodeY;
    moveMouseStartX;
    moveMouseStartY;
    moveMouseCurrentX;
    moveMouseCurrentY;
    isMoving: boolean;

    @HostBinding('style.left.px') left = 0;
    @HostBinding('style.top.px') top = 0;

    constructor(private connectionService: ConnectionService) {
        window.addEventListener('mousemove', (e) => this.mouseMove(e));
        window.addEventListener('keydown', (e) => this.cancelMove(e));
    }

    ngOnInit() {
        this.left = this.node.posX;
        this.top = this.node.posY;
    }

    handleConnectionMouseDown(e, i) {
        this.connectionService.mouseDown(e, this.node, i);
    }

    handleConnectionMouseUp(e, i) {
        this.connectionService.mouseUp(e);

        if (this.connectionService.node.id == this.node.id || (this.connectionService.index == -1 && i == -1)) {
            return;
        }

        if ((this.connectionService.index == -1 && this.node.slots[i].direction == 'in') ||
            (i == -1 && this.connectionService.node.slots[this.connectionService.index].direction == 'in')) {
            return;
        }

        if (this.connectionService.index > -1 && i > -1) {
            if (this.connectionService.node.slots[this.connectionService.index].direction == this.node.slots[i].direction) {
                return;
            }
        }

        let toNodeId;
        let toIndex;
        let fromNodeId;
        let fromIndex;

        if (this.connectionService.index == -1 || this.connectionService.node.slots[this.connectionService.index].direction == 'in') {
            toNodeId = this.connectionService.node.id;
            toIndex = this.connectionService.index;
            fromNodeId = this.node.id;
            fromIndex = i;
        } else {
            toNodeId = this.node.id;
            toIndex = i;
            fromNodeId = this.connectionService.node.id;
            fromIndex = this.connectionService.index;
        }

        this.updateMap.emit({
            updates: [{
                action: 'connectNodes',
                data: {
                    from: {
                        nodeId: fromNodeId,
                        slotId: fromIndex
                    },
                    to: {
                        nodeId: toNodeId,
                        slotId: toIndex
                    }
                }
            }]
        });
    }

    startMove(e) {
        this.moveMouseStartX = e.pageX;
        this.moveMouseStartY = e.pageY;
        this.originalNodeX = this.node.posX;
        this.originalNodeY = this.node.posY;
        this.isMoving = true;
    }

    stopMove(e) {
        if (!this.isMoving) {
            return;
        }

        this.isMoving = false;
        this.left = this.originalNodeX + (e.pageX - this.moveMouseStartX);
        this.top = this.originalNodeY + (e.pageY - this.moveMouseStartY);
        this.node.posX = this.left;
        this.node.posY = this.top;

        this.updateMap.emit({
            updates: [{
                action: 'updateNode',
                data: {id: this.node.id, posX: this.node.posX, posY: this.node.posY}
            }]
        });
    }

    cancelMove(e) {
        if (!this.isMoving) {
            return;
        }

        if (e.keyCode != 27) {
            return;
        }

        this.isMoving = false;
        this.left = this.node.posX;
        this.top = this.node.posY;
    }

    mouseMove(e) {
        if (!this.isMoving) {
            return;
        }

        e.preventDefault();

        this.moveMouseCurrentX = e.pageX;
        this.moveMouseCurrentY = e.pageY;

        this.left = this.originalNodeX + (this.moveMouseCurrentX - this.moveMouseStartX);
        this.top = this.originalNodeY + (this.moveMouseCurrentY - this.moveMouseStartY);
        this.node.posX = this.left;
        this.node.posY = this.top;
    }
}