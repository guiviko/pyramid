import {Component, Input, Output, EventEmitter, ViewChild} from '@angular/core';

@Component({
    selector: 'query-editor',
    templateUrl: './queryEditor.html',
    styleUrls: ['./queryEditor.css']
})
export class QueryEditor {
    @Input() selectedQuery;
    @Input() query;
    @Output() runQuery = new EventEmitter<any>();
    @Output() selectQuery = new EventEmitter<any>();
    @Output() addQueryType = new EventEmitter<any>();
    @Output() addQueryField = new EventEmitter<any>();
}
