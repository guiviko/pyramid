import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Http} from "@angular/http";

@Component({
    selector: 'confirmation-modal',
    templateUrl: './confirmationModal.html',
    styleUrls: ['./confirmationModal.css']
})
export class ConfirmationModal {
    @Input() reducerNames;
    @Input() pageName;
    @Output() shouldRefresh = new EventEmitter<any>();
    show: boolean;
    compName: any;
    reducerName;
    actionName;

    constructor(private http: Http) {
        this.show = false;
    }

    ngOnInit() {
        window.addEventListener('keypress', e => this.handleKeyPress(e));
    }

    open(compName: any): void {
        this.compName = compName;
        this.show = true;
    }

    continue(): void {
        this.show = false;

        if (!this.compName) {
            return;
        }

        this.http.post('/api/components/add-action', {
            componentName: this.compName,
            reducerName: this.reducerName,
            actionName: this.actionName,
            pageName: this.pageName
        })
            .map(response => response.json())
            .subscribe(
                data => {
                    console.log('worked');
                    this.shouldRefresh.emit();
                },
                err => {
                    console.log('error');
                }
            );
    }

    cancel(): void {
        if (!this.show) {
            return;
        }

        this.show = false;
    }

    cancelWithClick(e): void {
        if ((e.target.className).indexOf('veil') == -1) {
            return;
        }

        this.cancel();
    }

    handleKeyPress(e): void {
        if (e.keyCode == 27) {
            this.cancel();
        }
    }
}