import {Component, Input} from '@angular/core';
import {Http} from "@angular/http";

@Component({
    selector: 'add-model-modal',
    templateUrl: './addModelModal.html',
    styleUrls: ['./addModelModal.css']
})
export class AddModelModal {
    @Input() models;
    show: boolean;
    name: string;

    constructor(private http: Http) {
        this.show = true;
    }

    ngOnInit() {
        window.addEventListener('keypress', e => this.handleKeyPress(e));
    }

    open(): void {
        this.show = true;
    }

    continue(): void {
        if (!this.show) {
            return;
        }

        this.show = false;

        // this.http.post('/api/projects/lala/server/models',{
        //     name: this.name
        // })
        //     .map(resp => resp.json())
        //     .subscribe(
        //         resp => console.log(resp),
        //         error => console.log(error)
        //     );
    }

    cancel(): void {
        if (!this.show) {
            return;
        }

        this.show = false;
    }

    cancelWithClick(e): void {
        if ((e.target.className).indexOf('veil') == -1) {
            return;
        }

        this.cancel();
    }

    handleKeyPress(e): void {
        if (e.keyCode == 27) {
            this.cancel();
        }
    }
}