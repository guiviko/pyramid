import {Component, Input, Output, EventEmitter} from '@angular/core';
import {PlatformActions} from "../../actions/platform.actions";

@Component({
    selector: 'block',
    templateUrl: './block.html',
    styleUrls: ['./block.css']
})
export class Block {
    @Input() projectId: any;
    @Input() block: any;
    @Input() blockMap: any;
    @Input() blockAreaId: any;
    @Input() mode: string;
    @Input() flexDirection: string;
    @Output() windowUpdates = new EventEmitter<any>();
    isMouseInBlock: boolean;
    relMouseX: number;
    relMouseY: number;
    lastBlockWidth: number;
    lastBlockHeight: number;
    shouldHover: boolean;

    constructor(private platformActions: PlatformActions) {
        this.isMouseInBlock = false;
        this.relMouseX = 0;
        this.relMouseY = 0;
        this.lastBlockWidth = 0;
        this.lastBlockHeight = 0;
        this.shouldHover = false;
    }

    handleDropInBlock(data) {
        if (data.event['blockProcessed']) {
            return;
        }
        data.event['blockProcessed'] = true;

        switch (data.type) {
            case 'component':
            this.windowUpdates.emit(this.platformActions.openDropComponentModal({
                blockAreaId: this.blockAreaId,
                block: this.block,
                templateId: data.payload.id
            }));
            break;
        }
    }

    handleMouseMove(e) {
        if (e['blockProcessed']) {
            return;
        }
        e['blockProcessed'] = true;

        if (!this.isMouseInBlock) {
            this.isMouseInBlock = true;
        }

        if (this.mode == 'select') {
            this.handleMouseMoveForSelect();
            return;
        }

        if (this.shouldHover) {
            this.shouldHover = false;
        }

        if (this.mode != 'verticalSplit' && this.mode != 'horizontalSplit') {
            return;
        }

        let rect = e.target.getBoundingClientRect();

        let tempRelX = e.clientX - rect.left;
        if (tempRelX > 0) {
            this.relMouseX = tempRelX;
            this.lastBlockWidth = rect.width;
        }

        let tempRelY = e.clientY - rect.top;
        if (tempRelY > 0) {
            this.relMouseY = tempRelY;
            this.lastBlockHeight = rect.height;
        }
    }

    handleMouseMoveForSelect() {
        if (this.block.type != 'block') {
            return;
        }

        if (!this.shouldHover) {
            this.shouldHover = true;
        }
    }

    handleMouseLeave(e) {
        if (e['blockProcessed']) {
            return;
        }

        e['blockProcessed'] = true;

        this.isMouseInBlock = false;
        this.shouldHover = false;
    }

    handleMouseClick(e) {
        if (e['blockProcessed']) {
            return;
        }

        e['blockProcessed'] = true;

        if (this.mode == 'select' && !this.block.isSelected) {
            this.windowUpdates.emit(this.platformActions.select({
                blockAreaId: this.blockAreaId,
                id: this.block.id,
                type: 'block'
            }));
            return;
        }

        if (!this.isMouseInBlock || (this.mode != 'verticalSplit' && this.mode != 'horizontalSplit')) {
            return;
        }

        if (this.mode == 'verticalSplit') {
            this.windowUpdates.emit(this.platformActions.splitVertically({
                value: this.getPercentageRight(),
                block: this.block,
                blockAreaId: this.blockAreaId
            }));
            return;
        }

        this.windowUpdates.emit(this.platformActions.splitHorizontally({
            value: this.getPercentageTop(),
            block: this.block,
            blockAreaId: this.blockAreaId
        }));
    }

    getPercentageRight() {
        return Math.round(this.relMouseX * 100 / this.lastBlockWidth);
    }

    getPercentageLeft() {
        return 100 - Math.round(this.relMouseX * 100 / this.lastBlockWidth);
    }

    getPercentageTop() {
        return Math.round(this.relMouseY * 100 / this.lastBlockHeight);
    }

    getPercentageBottom() {
        return 100 - Math.round(this.relMouseY * 100 / this.lastBlockHeight);
    }

    stopPropagating(e) {
        e.stopPropagation();
    }
}
