import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'disconnect-from-reducer-modal',
    templateUrl: './disconnectFromReducerModal.html',
    styleUrls: ['./disconnectFromReducerModal.css']
})
export class DisconnectFromReducerModal {
    @Input() component;
    @Output() disconnectOutputFromReducer = new EventEmitter<any>();
    show: boolean;
    rid: any;
    outputs: any[];
    reducer: any;

    constructor() {
        this.show = false;
        this.outputs = [];
    }

    ngOnInit() {
        window.addEventListener('keypress', e => this.handleKeyPress(e));
    }

    open(rid): void {
        this.rid = rid;
        this.setOutputs();
        this.setReducer();
        this.show = true;
    }

    setOutputs() {
        this.outputs = [];

        this.component.outputs.map(output => output.connections
                                   .map(connection => {
                                       if (connection.to == this.rid) {
                                           this.outputs.push(Object.assign(output, {isSelected: false}));
                                       }
                                   }));
    }

    setReducer() {
        this.reducer = this.component.allReducers.find(reducer => reducer.id == this.rid);
    }

    getSelectedCount() {
        return this.outputs.filter(output => output.isSelected).length;
    }

    continue(): void {
        if (!this.show) {
            return;
        }

        this.show = false;

        let selectedOutputs = [];
        if (this.outputs.length == 1) {
            selectedOutputs = this.outputs;
        } else {
            selectedOutputs = this.outputs.filter(output => output.isSelected);
        }

        if (selectedOutputs.length == 0) {
            return;
        }

        if (!this.reducer) {
            return;
        }

        selectedOutputs.map(output => {
            this.disconnectOutputFromReducer.emit({
                reducerGroup: this.reducer.prefix,
                reducerField: this.reducer.suffix,
                actionGroup: output.prefix,
                actionName: output.suffix
            });
        });
    }

    cancel(): void {
        if (!this.show) {
            return;
        }

        this.show = false;
    }

    cancelWithClick(e): void {
        if ((e.target.className).indexOf('veil') == -1) {
            return;
        }

        this.cancel();
    }

    handleKeyPress(e): void {
        if (e.keyCode == 27) {
            this.cancel();
        }
    }
}
