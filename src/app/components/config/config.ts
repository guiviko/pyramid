import {Component, Input, EventEmitter, Output} from '@angular/core';

@Component({
    selector: 'config',
    templateUrl: './config.html',
    styleUrls: ['./config.css']
})
export class Config {
    @Input() configTab: string;
    @Input() component: any;
    @Input() selectedItem: any;
    @Output() selectTab = new EventEmitter<any>();
    @Output() updateConfig = new EventEmitter<any>();
    @Output() updateMainConfig = new EventEmitter<any>();

    openGroup(group) {
        group['isClosed'] = false;
    }

    closeGroup(group) {
        group['isClosed'] = true;
    }

    handleChange(e, groupIndex, rowIndex, fieldIndex) {
        let newConfig = Object.assign({}, this.selectedItem.config);
        newConfig.groups[groupIndex].rows[rowIndex].fields[fieldIndex].value = e.target.value;
        this.updateConfig.emit({
            config: newConfig,
            controlRowIndex: this.selectedItem.controlRowIndex,
            controlIndex: this.selectedItem.controlIndex
        });
    }

    handleChangeMain(e, groupIndex, rowIndex, fieldIndex) {
        this.updateMainConfig.emit({
            groupIndex: groupIndex,
            rowIndex: rowIndex,
            fieldIndex: fieldIndex,
            value: e.target.value
        });
    }
}
