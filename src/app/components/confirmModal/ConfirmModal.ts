import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Http} from "@angular/http";

@Component({
    selector: 'confirm-modal',
    templateUrl: './confirmationModal.html',
    styleUrls: ['./confirmationModal.css']
})
export class ConfirmModal {
    show: boolean;
    callback;

    constructor(private http: Http) {
        this.show = false;
    }

    ngOnInit() {
        window.addEventListener('keypress', e => this.handleKeyPress(e));
    }

    open(callback): void {
        this.show = true;
        this.callback = callback;
    }

    continue(): void {
        this.show = false;
        this.callback();
    }

    cancel(): void {
        if (!this.show) {
            return;
        }

        this.show = false;
    }

    cancelWithClick(e): void {
        if ((e.target.className).indexOf('veil') == -1) {
            return;
        }

        this.cancel();
    }

    handleKeyPress(e): void {
        if (e.keyCode == 27) {
            this.cancel();
        }
    }
}