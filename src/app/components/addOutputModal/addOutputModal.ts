import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Http} from "@angular/http";

@Component({
    selector: 'add-output-modal',
    templateUrl: './addOutputModal.html',
    styleUrls: ['./addOutputModal.css']
})
export class AddOutputModal {
    show: boolean;
    selected;
    name: string;
    @Input() component: any;
    @Output() addOutput = new EventEmitter<any>();
    selectedGroup;
    selectedAction;
    newGroupActive: boolean;
    newActionActive: boolean;
    newGroupText: string;
    newActionText: string;

    constructor(private http: Http) {
        this.show = false;
        this.selectedGroup = '';
        this.selectedAction = '';
        this.newGroupActive = false;
        this.newActionActive = false;
    }

    ngOnInit() {
        window.addEventListener('keypress', e => this.handleKeyPress(e));
    }

    getGroupList() {
        let list = [];

        for (let key in this.component.allActions) {
            if (this.component.allActions.hasOwnProperty(key)) {
                if (key == 'form') {
                    continue;
                }

                list.push(key);
            }
        }

        return list;
    }

    getActionList() {
        if (!this.selectedGroup || this.selectedGroup == '' || this.selectedGroup == '#new#') {
            return [];
        }

        return this.component.allActions[this.selectedGroup]
            .filter(action => !this.component.outputs.find(output => output.prefix == this.selectedGroup && output.suffix == action));
    }

    activateNewGroup() {
        this.newGroupActive = true;
        this.selectedGroup = '#new#';
        this.newGroupText = '';
    }

    activateNewAction() {
        this.newActionActive = true;
        this.selectedAction = '#new#';
        this.newActionText = '';
    }

    selectGroup(group) {
        this.selectedGroup = group;
        this.selectedAction = '';
        this.newGroupActive = false;
        this.newActionActive = false;
    }

    selectAction(action) {
        this.selectedAction = action;
        this.newActionActive = false;
    }

    open(): void {
        this.show = true;
        this.selectedGroup = '';
        this.selectedAction = '';
        this.newGroupActive = false;
        this.newActionActive = false;
        this.newGroupText = '';
        this.newActionText = '';
    }

    continue(): void {
        if (!this.show || this.selectedAction == '') {
            return;
        }

        this.show = false;

        this.addOutput.emit({
            componentId: this.component.id,
            name: this.selectedGroup == '#new#' ? this.newGroupText : this.selectedGroup,
            actionName: this.selectedAction == '#new#' ? this.newActionText : this.selectedAction
        });
    }

    cancel(): void {
        if (!this.show) {
            return;
        }

        this.show = false;
    }

    cancelWithClick(e): void {
        if ((e.target.className).indexOf('veil') == -1) {
            return;
        }

        this.cancel();
    }

    handleKeyPress(e): void {
        if (e.keyCode == 27) {
            this.cancel();
        }
    }

    select(str) {
        if (this.selected == str) {
            this.selected = undefined;
            return;
        }

        this.selected = str;
    }
}
