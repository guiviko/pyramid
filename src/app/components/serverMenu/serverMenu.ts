import {Component, Input} from '@angular/core';

@Component({
    selector: 'server-menu',
    templateUrl: './serverMenu.html',
    styleUrls: ['./serverMenu.css']
})
export class ServerMenu {
    @Input() projectId;
}
