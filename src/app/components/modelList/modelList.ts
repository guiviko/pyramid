import {Component, Input, ViewChild} from '@angular/core';

@Component({
    selector: 'model-list',
    templateUrl: './modelList.html',
    styleUrls: ['./modelList.css']
})
export class ModelList {
    @Input() models;
    @ViewChild('addModelModal') addModelModal;

    openAddModelModal() {
        this.addModelModal.open();
    }
}