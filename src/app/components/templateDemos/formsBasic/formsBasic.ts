import {Component, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'forms-basic',
    templateUrl: './formsBasic.html',
    styleUrls: ['./formsBasic.css']
})
export class FormsBasic {
    @Output() formUpdates = new EventEmitter<any>();
}