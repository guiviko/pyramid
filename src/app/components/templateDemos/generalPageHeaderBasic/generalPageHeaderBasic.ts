import {Component, Input} from '@angular/core';

@Component({
    selector: 'general-page-header-basic',
    templateUrl: './generalPageHeaderBasic.html',
    styleUrls: ['./generalPageHeaderBasic.css']
})
export class GeneralPageHeaderBasic {
    @Input() title;
    @Input() section;
}