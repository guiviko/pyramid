import {Component, Output, EventEmitter, Input} from '@angular/core';

@Component({
    selector: 'component-selector',
    templateUrl: './componentSelector.html',
    styleUrls: ['./componentSelector.css']
})
export class ComponentSelector {
    @Input() component: any;
    @Input() dragData: any;
    @Input() templates: any;
    @Input() formTemplates: any;
}
