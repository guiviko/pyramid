import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Http} from "@angular/http";

@Component({
    selector: 'add-field-modal',
    templateUrl: 'addFieldModal.html',
    styleUrls: ['addFieldModal.css']
})
export class AddFieldModal {
    @Output() addField = new EventEmitter<any>();
    show: boolean;
    name: string;
    fieldType: string;
    type: any;

    constructor(private http: Http) {
        this.show = false;
        this.reset();
    }

    reset() {
        this.name = '';
        this.fieldType = 'string';
    }

    selectType(type) {
        this.fieldType = type;
    }

    ngOnInit() {
        window.addEventListener('keypress', e => this.handleKeyPress(e));
    }

    open(type: any): void {
        this.show = true;
        this.type = type;
        this.name = '';
        this.fieldType = 'string';
    }

    continue(): void {
        this.show = false;

        this.addField.emit({
            typeName: this.type.name,
            field: {
                name: this.name,
                type: this.fieldType
            }
        });
    }

    cancel(): void {
        if (!this.show) {
            return;
        }

        this.show = false;
    }

    cancelWithClick(e): void {
        if ((e.target.className).indexOf('veil') == -1) {
            return;
        }

        this.cancel();
    }

    handleKeyPress(e): void {
        if (e.keyCode == 27) {
            this.cancel();
        }
    }
}
