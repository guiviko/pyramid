import {Component, Input, ViewChild} from '@angular/core';

@Component({
    selector: 'api-list',
    templateUrl: './apiList.html',
    styleUrls: ['./apiList.css']
})
export class ApiList {
    @Input() routes: any[];
    @Input() services: any[];
    @ViewChild('addApiModal') addApiModal;
    apiFilter: string;

    filteredRoutes() {
        if (!this.apiFilter || this.apiFilter == '') {
            return this.routes;
        }

        return this.routes.filter(route => route.uri.toLowerCase().includes(this.apiFilter.toLowerCase()) || route.method.toLocaleLowerCase().includes(this.apiFilter.toLowerCase()));
    }

    openAddApiModal() {
        this.addApiModal.open();
    }

    toggleRoute(route) {
        if (!route.isOpen) {
            this.routes.map(route => route.isOpen = false);
        }

        route.isOpen = !route.isOpen;
    }
}