import {Component, Input} from '@angular/core';
import {Http} from "@angular/http";

@Component({
    selector: 'project-list',
    templateUrl: './projectList.html',
    styleUrls: ['./projectList.css']
})
export class ProjectList {
    @Input() projects;
    newProjectName: string;

    constructor(private http: Http) {

    }

    createProject() {
        this.http.post('/api/projects', {name: this.newProjectName})
            .map(response => response.json())
            .subscribe(
                data => {
                    console.log('created');
                },
                err => {
                    console.log('error');
                }
            );
    }

    deleteProject(project) {
        this.http.delete('/api/projects/' + project.id)
            .map(response => response.json())
            .subscribe(
                data => {
                    console.log('deleted');
                },
                err => {
                    console.log('error');
                }
            );
    }
}
