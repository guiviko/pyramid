import {Component, ViewChild, Input, EventEmitter, Output} from '@angular/core';
import {PlatformActions} from '../../actions/platform.actions';

@Component({
    selector: 'template-config',
    templateUrl: './templateConfig.html',
    styleUrls: ['./templateConfig.css']
})
export class TemplateConfig {
    @Input() component;
    @Input() templates;
    @Input() dragData;
    @Input() mode;
    @Input() projectId;
    @Output() windowUpdates = new EventEmitter<any>();
    @Output() formActions = new EventEmitter<any>();
    @Output() draggableMouseDown = new EventEmitter<any>();
    @Output() draggableMouseUp = new EventEmitter<any>();
    @ViewChild('dropComponentModal') dropComponentModal;

    handleWindowUpdates(event) {
        switch (event.type) {
        case PlatformActions.OPEN_DROP_COMPONENT_MODAL:
            this.dropComponentModal.open(event.payload);
            return;
        }

        this.windowUpdates.emit(event);
    }
}
