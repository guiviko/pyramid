import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'add-input-modal',
    templateUrl: './addInputModal.html',
    styleUrls: ['./addInputModal.css']
})
export class AddInputModal {
    @Input() component;
    @Output() addInput= new EventEmitter<any>();
    show: boolean;
    group: string;
    name: string;

    constructor() {
        this.show = false;
        this.group = '';
        this.name = '';
    }

    ngOnInit() {
        window.addEventListener('keypress', e => this.handleKeyPress(e));
    }

    open(): void {
        this.show = true;
        this.name = '';
        this.group = '';
    }

    continue(): void {
        if (!this.show) {
            return;
        }

        this.show = false;

        this.addInput.emit({
            componentId: this.component.id,
            inputName: this.name,
            group: this.group
        });
    }

    cancel(): void {
        if (!this.show) {
            return;
        }

        this.show = false;
    }

    cancelWithClick(e): void {
        if ((e.target.className).indexOf('veil') == -1) {
            return;
        }

        this.cancel();
    }

    handleKeyPress(e): void {
        if (e.keyCode == 27) {
            this.cancel();
        }
    }
}
