import {Component, Input, Output, EventEmitter} from '@angular/core';
import {PlatformActions} from '../../actions/platform.actions';

@Component({
    selector: 'drop-component-modal',
    templateUrl: './dropComponentModal.html',
    styleUrls: ['./dropComponentModal.css']
})
export class DropComponentModal {
    @Output() windowUpdates = new EventEmitter<any>();
    show: boolean;
    data: any;
    name: string;

    constructor(private platformActions: PlatformActions) {
        this.show = false;
        this.name = '';
    }

    ngOnInit() {
        window.addEventListener('keypress', e => this.handleKeyPress(e));
    }

    open(data): void {
        this.data = data;
        this.show = true;
        this.name = '';
    }

    continue(): void {
        if (!this.show) {
            return;
        }

        this.show = false;

        this.windowUpdates.emit(this.platformActions.dropComponentInBlock(Object.assign(this.data, {
            name: this.name
        })));
    }

    cancel(): void {
        if (!this.show) {
            return;
        }

        this.show = false;
    }

    cancelWithClick(e): void {
        if ((e.target.className).indexOf('veil') == -1) {
            return;
        }

        this.cancel();
    }

    handleKeyPress(e): void {
        if (e.keyCode == 27) {
            this.cancel();
        }
    }
}
