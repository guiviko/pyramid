import {Component, ChangeDetectorRef, Input, Output, EventEmitter, ViewChild} from '@angular/core';

@Component({
    selector: 'io-editor',
    templateUrl: './ioEditor.html',
    styleUrls: ['./ioEditor.css']
})
export class IOEditor {
    @Input() component: any;
    @Input() isLogicPanelOpen: boolean;
    @Input() lineData: any;
    @Output() addOutput = new EventEmitter<any>();
    @Output() connectOutputToReducer = new EventEmitter<any>();
    @Output() addReducerField = new EventEmitter<any>();
    @Output() selectLogicCategory = new EventEmitter<any>();
    @Output() selectLogicItem = new EventEmitter<any>();
    @Output() toggleLogicPanel = new EventEmitter<any>();
    @Output() disconnectOutputFromReducer = new EventEmitter<any>();
    @Output() addInput = new EventEmitter<any>();
    @ViewChild('addOutputModal') addOutputModal;
    @ViewChild('addReducerFieldModal') addReducerFieldModal;
    @ViewChild('disconnectFromReducerModal') disconnectFromReducerModal;
    @ViewChild('addInputModal') addInputModal;
    data: any;
    startDragX;
    startDragY;
    isDraggingLine: boolean;
    draggedOutput;
    lastMouseX;
    lastMouseY;

    constructor(private cdRef:ChangeDetectorRef) {
        this.startDragX = 0;
        this.startDragY = 0;
        this.lastMouseX = 0;
        this.lastMouseY = 0;
        this.isDraggingLine = false;
        this.draggedOutput = '';

        window.addEventListener('mousemove', e => this.handleMouseMove(e));
        window.addEventListener('keydown', e => this.handleKeyDown(e));
    };

    ngOnInit() {
        this.cdRef.detectChanges();
    }

    ngOnChanges() {
        this.cdRef.detectChanges();
    }

    handleMouseMove(e) {
        let container = document.getElementById('io-container');
        if (!container) {
            return;
        }
        let r = container.getBoundingClientRect();

        this.lastMouseX = e.pageX - r.left;
        this.lastMouseY = e.pageY - r.top;

        if (this.isDraggingLine) {
            this.cdRef.detectChanges();
        }
    }

    handleKeyDown(e) {
        if (!this.isDraggingLine) {
            return;
        }

        if (e.keyCode == 27) {
            this.isDraggingLine = false;
        }
    }

    startDraggingLine(e, output) {
        if (this.isDraggingLine) {
            return;
        } 

        let from = document.getElementById(output.id);

        this.isDraggingLine = true;
        this.draggedOutput = output;
        this.startDragX = from.offsetLeft + from.clientWidth;
        this.startDragY = from.offsetTop + from.clientHeight / 2;

        this.cdRef.detectChanges();
    }

    handleReducerReceiverClick(reducer) {
        if (!this.isDraggingLine) {
            return;
        }

        this.isDraggingLine = false;

        this.connectOutputToReducer.emit({
            actionGroup: this.draggedOutput.prefix,
            actionName: this.draggedOutput.suffix,
            reducerGroup: reducer.prefix,
            reducerField: reducer.suffix
        });
    }

    isStraight(line) {
        let from = document.getElementById(line.from);
        let to = document.getElementById(line.to);
        if (!from || !to) {
            return false;
        }

        return Math.abs(from.offsetTop - to.offsetTop) < 5;
    }

    dotEndLeft(line) {
        let from = document.getElementById(line.from);
        let to = document.getElementById(line.to);
        if (!from || !to) {
            return 0;
        }

        return to.offsetLeft;
    }

    dotPlusLeft(id) {
        let from = document.getElementById(id);
        if (!from) {
            return 0;
        }
        
        return from.offsetLeft + from.clientWidth;
    }

    dotReceiverLeft(id) {
        if (!this.isDraggingLine) {
            return 0;
        }
        
        let from = document.getElementById(id);
        if (!from) {
            return 0;
        }

        return from.offsetLeft;
    }

    left(line) {
        let from = document.getElementById(line.from);
        if (!from) {
            return 0;
        }

        return from.offsetLeft + from.clientWidth;
    }

    dotEndTop(line) {
        let to = document.getElementById(line.to);
        if (!to) {
            return 0;
        }

        return to.offsetTop + to.clientHeight / 2;
    }

    dotReceiverTop(id) {
        if (!this.isDraggingLine) {
            return 0;
        }

        let el = document.getElementById(id);
        if (!el) {
            return 0;
        }
        
        return el.offsetTop + el.clientHeight / 2;
    }

    dotPlusTop(id) {
        let from = document.getElementById(id);
        if (!from) {
            return 0;
        }
        
        return from.offsetTop + from.clientHeight / 2;
    }

    top(line) {
        let from = document.getElementById(line.from);
        let to = document.getElementById(line.to);
        if (!from || !to) {
            return 0;
        }
        let highest = to.offsetTop - from.offsetTop < -5 ? to : from;

        return highest.offsetTop + highest.clientHeight / 2 - 2;
    }

    dragTop() {
        let highest = this.lastMouseY - this.startDragY < -5 ? this.lastMouseY : this.startDragY;
        return highest;
    }

    width(line) {
        let from = document.getElementById(line.from);
        let to = document.getElementById(line.to);
        if (!from || !to) {
            return 0;
        }

        return to.offsetLeft - (from.offsetLeft + from.clientWidth);
    }

    height(line) {
        let from = document.getElementById(line.from);
        let to = document.getElementById(line.to);
        if (!from || !to) {
            return 0;
        }
        
        let height = Math.abs(from.offsetTop - to.offsetTop);
        return (height < 5 ? 0 : height) + 4;
    }

    dragWidth() {
        return Math.abs(this.lastMouseX - this.startDragX);
    }

    dragHeight() {
        return Math.abs(this.lastMouseY - this.startDragY);
    }

    d(line) {
        let from = document.getElementById(line.from);
        let to = document.getElementById(line.to);
        if (!from || !to) {
            return 'M 0,0';
        }

        let height = Math.abs(from.offsetTop - to.offsetTop);
        if (height < 5) {
            return 'M 0,0 L 100,0';
        }

        if (to.offsetTop - from.offsetTop < -5) {
            return 'M 0,100 C 35,100 65,0 100,0';
        }

        return 'M 0,0 C 35,0 65,100 100,100';
    }

    dragd() {
        let height = Math.abs(this.startDragX - this.lastMouseX);
        if (height < 5) {
            return 'M 0,0 L 100,0';
        }

        if (this.lastMouseY - this.startDragY < - 5) {
            return 'M 0,100 C 35,100 65,0 100,0';
        }

        return 'M 0,0 C 35,0 65,100 100,100';
    }

    openAddOutputModal() {
        this.addOutputModal.open();
    }

    openAddReducerFieldModal() {
        this.addReducerFieldModal.open();
    }

    openDisconnectFromReducerModal(to) {
        this.disconnectFromReducerModal.open(to);
    }

    openAddInputModal() {
        this.addInputModal.open();
    }

    handleReducerBlockClicked(reducer) {
        this.selectLogicCategory.emit('reducers');
        if (reducer.actions.length > 0) {
            this.selectLogicItem.emit(reducer.prefix + '@' + reducer.suffix + '@' + reducer.actions[0].prefix + '::' + reducer.actions[0].suffix);
        }
        if (!this.isLogicPanelOpen) {
            this.toggleLogicPanel.emit({});
        }
    }
}
