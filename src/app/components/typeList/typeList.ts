import {Component, Input, Output, EventEmitter, ViewChild} from '@angular/core';
import {Http} from '@angular/http';

@Component({
    selector: 'type-list',
    templateUrl: './typeList.html',
    styleUrls: ['./typeList.css']
})
export class TypeList {
    @Input() isAddingSchemaType;
    @Input() schema: any;
    @Output() setIsAddingSchemaType = new EventEmitter<any>();
    @Output() addType = new EventEmitter<any>();
    @Output() removeType = new EventEmitter<any>();
    @Output() addField = new EventEmitter<any>();
    @Output() removeField = new EventEmitter<any>();
    @ViewChild('addFieldModal') addFieldModal;
    @ViewChild('confirmModal') confirmModal;
    newTypeName: string;

    add() {
        this.addType.emit({
            name: this.newTypeName
        });

        this.setIsAddingSchemaType.emit(false);
    }

    openAddFieldModal(type) {
        this.addFieldModal.open(type);
    }

    deleteType(type) {
        this.confirmModal.open(() => this.removeType.emit(type.name));
    }
}
