import {Directive, ElementRef, HostListener, Input, EventEmitter, Output} from '@angular/core';
import {PyrDraggableService} from "../../../services/generic/pyrDraggable.service";

@Directive({
    selector: '[pyrDroppable]'
})
export class PyrDroppable {
    /**
     * Types allowed to drop
     */
    @Input() allow: string[];

    /**
     * on drop event
     */
    @Output() pyrDrop = new EventEmitter<any>();

    /**
     * on dragover event
     */
    @Output() pyrDragEnter = new EventEmitter<any>();

    /**
     * on dragleave event
     */
    @Output() pyrDragLeave = new EventEmitter<any>();

    /**
     * The native element reference
     */
    el;

    constructor(private elRef: ElementRef,
                private pyrDraggableService: PyrDraggableService) {
        this.el = elRef.nativeElement;
    }

    @HostListener('mouseup', ['$event']) handleMouseUp(e) {
        if (!this.pyrDraggableService.isDragging) {
            return;
        }

        if (this.allow && this.allow.includes(this.pyrDraggableService.dragType)) {
            this.el.classList.remove('pyr-dragover');

            this.pyrDrop.emit({
                type: this.pyrDraggableService.dragType,
                payload: this.pyrDraggableService.dragPayload,
                event: e
            });

            return;
        }

        this.el.classList.remove('pyr-dragover-forbidden');
    }

    @HostListener('mouseenter', ['$event']) handleMouseEnter(e) {
        if (!this.pyrDraggableService.isDragging) {
            return;
        }

        this.pyrDragEnter.emit({});

        if (this.allow && this.allow.includes(this.pyrDraggableService.dragType)) {
            this.el.classList.add('pyr-dragover');
        } else {
            this.el.classList.add('pyr-dragover-forbidden');
        }
    }

    @HostListener('mouseleave', ['$event']) handleMouseLeave(e) {
        if (!this.pyrDraggableService.isDragging) {
            return;
        }

        this.pyrDragLeave.emit({});

        if (this.allow && this.allow.includes(this.pyrDraggableService.dragType)) {
            this.el.classList.remove('pyr-dragover');
        } else {
            this.el.classList.remove('pyr-dragover-forbidden');
        }
    }
}