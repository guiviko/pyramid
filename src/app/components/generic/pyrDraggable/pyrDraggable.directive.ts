import {Directive, ElementRef, HostListener, Input, EventEmitter, Output} from '@angular/core';
import {PyrDraggableService} from "../../../services/generic/pyrDraggable.service";

@Directive({
    selector: '[pyrDraggable]'
})
export class PyrDraggable {
    /**
     * The id of the drag event
     */
    @Input() dragType: string;

    /**
     * The drag event's payload
     */
    @Input() dragPayload: any;

    /**
     * The mode to operate in
     * -> normal: drag and drop without manipulating original elements
     */
    @Input() mode: string;

    /**
     * on drag start event
     */
    @Output() dragStart = new EventEmitter<any>();

    /**
     * on drag end event
     */
    @Output() dragEnd = new EventEmitter<any>();

    /**
     * The native element reference
     */
    el;

    /**
     * Initial styles on drag start
     */
    originalStyles: any;

    /**
     * Initial mouse x position on drag start
     */
    initialMouseX;

    /**
     * Initial mouse y position on drag start
     */
    initialMouseY;

    /**
     * Initial element left position on drag start
     */
    initialLeft;

    /**
     * Initial element top position on drag start
     */
    initialTop;

    /**
     * Used to differentiate between a click and a drag
     */
    isPreDragging;

    /**
     * The element clone
     */
    clone;

    /**
     * Used to give an unique id to each drag event
     */
    id: number;

    constructor(private elRef: ElementRef,
                private pyrDraggableService: PyrDraggableService) {
        this.el = elRef.nativeElement;
        this.id = new Date().getUTCMilliseconds() + Math.floor((1 + Math.random()) * 0x10000);

        this.originalStyles = {
            position: this.el.position,
            pointerEvents: this.el.pointerEvents,
            boxShadow: this.el.boxShadow,
            zIndex: this.el.zIndex
        };
    }

    @HostListener('window:mouseup', ['$event']) handleMouseUp(e) {
        if (this.isPreDragging) {
            this.isPreDragging = false;
            return;
        }

        if (!this.pyrDraggableService.isDragging || this.id != this.pyrDraggableService.id) {
            return;
        }

        this.dragEnd.emit({});

        this.pyrDraggableService.stopDrag();

        for (let key in this.originalStyles) {
            if (this.originalStyles.hasOwnProperty(key)) {
                this.el.style[key] = this.originalStyles[key];
            }
        }
        this.el.style.left = this.initialLeft + 'px';
        this.el.style.top = this.initialTop + 'px';

        if ((!this.mode || this.mode == 'normal') && this.el.parentElement && this.clone) {
            this.el.parentElement.removeChild(this.clone);
            this.clone = undefined;
        }
    }

    @HostListener('window:mousemove', ['$event']) handleMouseMove(e) {
        if (this.isPreDragging) {
            if (Math.abs(e.clientX - this.initialMouseX) > 2 || Math.abs(e.clientY - this.initialMouseY) > 2) {
                this.isPreDragging = false;
                this.startDragging();
            }
            return;
        }

        if (!this.pyrDraggableService.isDragging || this.id != this.pyrDraggableService.id) {
            return;
        }

        // this is to disable text selection when dragging
        e.preventDefault();

        this.el.style.left = this.initialLeft + (e.clientX - this.initialMouseX) + 'px';
        this.el.style.top = this.initialTop + (e.clientY - this.initialMouseY) + 'px';
    }

    startDragging() {
        this.dragStart.emit({});

        if (!this.mode || this.mode == 'normal') {
            this.clone = this.el.cloneNode(true);
            this.clone.classList.add('pyr-is-dragged');
        }

        let boundingRect = this.el.getBoundingClientRect();

        this.initialLeft = boundingRect.left;
        this.initialTop = boundingRect.top;
        for (let key in this.originalStyles) {
            if (this.originalStyles.hasOwnProperty(key)) {
                this.originalStyles[key] = this.el.style[key];
            }
        }

        this.el.style.position = 'absolute';
        this.el.style.width = boundingRect.width + 'px';
        this.el.style.height = boundingRect.height + 'px';
        this.el.style.left = this.initialLeft + 'px';
        this.el.style.top = this.initialTop + 'px';
        this.el.style.pointerEvents = 'none';
        this.el.style.boxShadow = '0px 0px 5px 0px rgba(0,0,0,0.75)';
        this.el.style.zIndex = 1000;

        if (!this.mode || this.mode == 'normal') {
            this.el.parentElement.insertBefore(this.clone, this.el);
        }

        this.pyrDraggableService.startDrag(this.dragType, this.dragPayload, this.id);
    }

    @HostListener('mousedown', ['$event']) handleMouseDown(e) {
        this.initialMouseX = e.clientX;
        this.initialMouseY = e.clientY;

        this.isPreDragging = true;
    }
}