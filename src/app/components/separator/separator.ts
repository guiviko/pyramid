import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'separator',
    templateUrl: './separator.html',
    styleUrls: ['./separator.css']
})
export class Separator {
    @Input() block: number;
    @Input() mode: string;

    constructor() {

    }
}
