import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'add-reducer-field-modal',
    templateUrl: './addReducerFieldModal.html',
    styleUrls: ['./addReducerFieldModal.css']
})
export class AddReducerFieldModal {
    @Input() component;
    @Output() addReducerField = new EventEmitter<any>();
    show: boolean;
    reducerGroup: string;
    reducerField: string;
    type: string;
    defaultValue: string;

    constructor() {
        this.show = false;
    }

    ngOnInit() {
        window.addEventListener('keypress', e => this.handleKeyPress(e));
    }

    open(): void {
        this.show = true;
        this.reducerGroup = '';
        this.reducerField = '';
        this.type = '';
        this.defaultValue = '';
    }

    continue(): void {
        if (!this.show) {
            return;
        }

        this.show = false;

        this.addReducerField.emit({
            componentId: this.component.id,
            reducerGroup: this.reducerGroup,
            reducerField: this.reducerField,
            type: this.type,
            default: this.defaultValue 
        });
    }

    cancel(): void {
        if (!this.show) {
            return;
        }

        this.show = false;
    }

    cancelWithClick(e): void {
        if ((e.target.className).indexOf('veil') == -1) {
            return;
        }

        this.cancel();
    }

    handleKeyPress(e): void {
        if (e.keyCode == 27) {
            this.cancel();
        }
    }
}
