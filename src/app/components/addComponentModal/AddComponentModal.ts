import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Http} from "@angular/http";

@Component({
    selector: 'add-component-modal',
    templateUrl: './addComponentModal.html',
    styleUrls: ['./addComponentModal.css']
})
export class AddComponentModal {
    show: boolean;
    selected: string;
    selectedCategory: string;
    categories: string[];
    name: string;

    constructor(private http: Http) {
        this.show = true;
        this.categories = ['general', 'headers', 'menus', 'navigation', 'panels', 'modals', 'tables', 'forms'];
        this.selectedCategory = 'forms';
    }

    ngOnInit() {
        window.addEventListener('keypress', e => this.handleKeyPress(e));
    }

    open(): void {
        this.show = true;
    }

    continue(): void {
        if (!this.show || !this.selected) {
            return;
        }

        this.show = false;

        this.http.post('/api/components/add', {
            name: this.name,
            component: this.selected
        })
            .subscribe(
                response => console.log(response),
                error => console.log(error)
            );
    }

    cancel(): void {
        if (!this.show) {
            return;
        }

        this.show = false;
    }

    cancelWithClick(e): void {
        if ((e.target.className).indexOf('veil') == -1) {
            return;
        }

        this.cancel();
    }

    handleKeyPress(e): void {
        if (e.keyCode == 27) {
            this.cancel();
        }
    }

    select(component: string) {
        if (this.selected == component) {
            this.selected = '';
            return;
        }

        this.selected = component;
    }

    setCategory(category: string) {
        this.selectedCategory = category;
    }
}