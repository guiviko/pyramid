import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
    selector: 'action-list',
    templateUrl: './actionList.html',
    styleUrls: ['./actionList.css']
})
export class ActionList {
    @Input() actions;
    @Input() selectedAction;
    @Input() selectedGroup;
    @Output() createActionGroup = new EventEmitter<any>();
    @Output() createAction = new EventEmitter<any>();
    @Output() createStateField = new EventEmitter<any>();
    @Output() selectAction = new EventEmitter<any>();
    @Output() selectGroup = new EventEmitter<any>();
    showGroupInput;
    showActionInput;
    showFieldInput;
    groupName;
    actionName;
    fieldName;
    fieldType;
    showActionInputGroupId;

    showAddGroup() {
        this.groupName = '';
        this.showGroupInput = true;
    }

    showAddAction(groupId) {
        this.actionName = '';
        this.showActionInput = true;
        this.showActionInputGroupId = groupId;
    }

    showAddField(groupId) {
        this.fieldName = '';
        this.fieldType = 'list';
        this.showFieldInput = true;
        this.showActionInputGroupId = groupId;
    }

    cancelAddGroup(e) {
        e.preventDefault();
        e.stopPropagation();

        this.showGroupInput = false;
    }

    handleGroupKeyDown(e) {
        if (e.keyCode == 27) {
            this.showGroupInput = false;
        } else if (e.keyCode == 13) {
            this.saveGroup(e);
        }
    }

    handleActionKeyDown(e, groupId) {
        if (e.keyCode == 27) {
            this.showActionInput = false;
        } else if (e.keyCode == 13) {
            this.saveAction(e, groupId);
        }
    }

    handleFieldKeyDown(e, groupId) {
        if (e.keyCode == 27) {
            this.showFieldInput = false;
        } else if (e.keyCode == 13) {
            this.saveField(e, groupId);
        }
    }

    cancelAddAction(e, groupId) {
        e.preventDefault();
        e.stopPropagation();

        this.showActionInput = false;
    }

    cancelAddField(e, groupId) {
        e.preventDefault();
        e.stopPropagation();

        this.showFieldInput = false;
    }

    saveGroup(e) {
        e.preventDefault();
        e.stopPropagation();

        this.showGroupInput = false;

        this.createActionGroup.emit({
            name: this.groupName
        });
    }

    saveAction(e, groupId) {
        e.preventDefault();
        e.stopPropagation();

        this.showActionInput = false;

        this.createAction.emit({
            groupId: groupId,
            name: this.actionName
        });
    }

    saveField(e, groupId) {
        e.preventDefault();
        e.stopPropagation();

        this.showFieldInput = false;

        this.createStateField.emit({
            groupId: groupId,
            name: this.fieldName,
            type: this.fieldType
        });
    }
}