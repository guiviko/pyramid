import {Component} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'dashboard-menu',
    templateUrl: './dashboardMenu.html',
    styleUrls: ['./dashboardMenu.css']
})
export class DashboardMenu {
    isCollapsed: boolean;
    projectId;

    constructor(private route: ActivatedRoute) {
        this.isCollapsed = true;
        this.route.params.subscribe(params => this.projectId = params['projectId']);
    }

    toggleCollapse() {
        this.isCollapsed = !this.isCollapsed;
    }
}
