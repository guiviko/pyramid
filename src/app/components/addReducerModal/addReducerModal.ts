import {Component, Input} from '@angular/core';
import {Http} from "@angular/http";

@Component({
    selector: 'add-reducer-modal',
    templateUrl: './addReducerModal.html',
    styleUrls: ['./addReducerModal.css']
})
export class AddReducerModal {
    show: boolean;
    name: string;

    constructor(private http: Http) {
        this.show = false;
    }

    ngOnInit() {
        window.addEventListener('keypress', e => this.handleKeyPress(e));
    }

    open(): void {
        this.show = true;
    }

    continue(): void {
        if (!this.show) {
            return;
        }

        this.show = false;

        this.http.post('/api/projects/lala/client/reducers',{
            name: this.name
        })
            .map(resp => resp.json())
            .subscribe(
                resp => console.log(resp),
                error => console.log(error)
            );
    }

    cancel(): void {
        if (!this.show) {
            return;
        }

        this.show = false;
    }

    cancelWithClick(e): void {
        if ((e.target.className).indexOf('veil') == -1) {
            return;
        }

        this.cancel();
    }

    handleKeyPress(e): void {
        if (e.keyCode == 27) {
            this.cancel();
        }
    }
}