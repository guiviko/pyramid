import {Component, Input, ViewChild} from '@angular/core';

@Component({
    selector: 'reducer-list',
    templateUrl: './reducerList.html',
    styleUrls: ['./reducerList.css']
})
export class ReducerList {
    @Input() reducers;
    @ViewChild('addReducerModal') addReducerModal;

    openAddReducerModal() {
        this.addReducerModal.open();
    }

    toggleReducer(reducer) {
        if (!reducer.isOpen) {
            this.reducers.map(reducer => reducer.isOpen = false);
        }

        reducer.isOpen = !reducer.isOpen;
    }
}