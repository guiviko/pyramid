import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
    selector: 'request-configure',
    templateUrl: './requestConfigure.html',
    styleUrls: ['./requestConfigure.css']
})
export class RequestConfigure {
    @Input() requestConfigure;
    @Input() schema;
    @Output() toggleRequestConfigure = new EventEmitter<any>();
    @Output() updateMap = new EventEmitter<any>();
    request = [];

    handleKeyPress(e, isNew) {
        switch (e.keyCode) {
            case 13: // enter
                this.addType(e.target.value);
                if (isNew) {
                    e.target.value = '';
                }
                break;
        }
    }

    handleCheckboxChanged(e, i, j) {
        this.request[i].fields[j].isChecked = !this.request[i].fields[j].isChecked;

        this.sendUpdate();
    }

    addType(name) {
        for (let i = 0; i < this.request.length; i++) {
            if (this.request[i].name == name) {
                this.request.splice(i, 1);
            }
        }

        let typeIndex = -1;
        for (let i = 0; i < this.schema.length; i++) {
            if (this.schema[i].name == name) {
                typeIndex = i;
                break;
            }
        }

        if (typeIndex == -1) {
            return;
        }

        this.request.push({
            name: name,
            fields: this.schema[typeIndex].fields
                .map(field => Object.assign(field, {isChecked: true}))
        });

        this.sendUpdate();
    }

    sendUpdate() {
        this.updateMap.emit({
            updates: [
                {
                    action: 'setRequestPayload',
                    data: this.buildRequest()
                }
            ]
        });
    }

    buildRequest() {
        return Object.assign({}, this.request.map(type => Object.assign({}, type, {
            fields: type.fields.filter(field => field.isChecked)
        })));
    }
}