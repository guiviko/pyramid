import {Component, Input} from '@angular/core';
import {Http} from "@angular/http";

@Component({
    selector: 'add-api-modal',
    templateUrl: './addApiModal.html',
    styleUrls: ['./addApiModal.css']
})
export class AddApiModal {
    show: boolean;
    @Input() services;
    methodList;
    selectedMethod;
    uri: string;
    controller: string;
    action: string;
    service: string;

    constructor(private http: Http) {
        this.show = false;
        this.service = 'none';

        this.methodList = ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'];
        this.selectedMethod = 'GET';
        this.generateControllerName('');
        this.generateActionName('GET');
    }

    ngOnInit() {
        window.addEventListener('keypress', e => this.handleKeyPress(e));
    }

    open(): void {
        this.show = true;
    }

    continue(): void {
        if (!this.show) {
            return;
        }

        this.show = false;

        this.http.post('/api/projects/lala/server/routes',{
            method: this.selectedMethod,
            uri: this.uri,
            controller: this.controller,
            action: this.action,
            service: this.service
        })
            .map(resp => resp.json())
            .subscribe(
                resp => console.log(resp),
                error => console.log(error)
            );
    }

    cancel(): void {
        if (!this.show) {
            return;
        }

        this.show = false;
    }

    cancelWithClick(e): void {
        if ((e.target.className).indexOf('veil') == -1) {
            return;
        }

        this.cancel();
    }

    handleKeyPress(e): void {
        if (e.keyCode == 27) {
            this.cancel();
        }
    }

    generateControllerName(uri) {
        let uriValue = 'api/' + uri;
        let parts = uriValue.split('/');

        let namespaces = [];
        let max = parts[parts.length - 1] == '' ? parts.length - 2 : parts.length - 2;
        for (let i = 0; i < max; i++) {
            if (parts[i].charAt(0) == '{') {
                continue;
            }

            namespaces.push(parts[i].charAt(0).toUpperCase() + parts[i].slice(1));
        }

        let controllerPartIndex = parts.length - 2;
        if (parts[controllerPartIndex].charAt(0) == '{') {
            namespaces.pop();
            controllerPartIndex--;
        }
        let controllerName = parts[controllerPartIndex].charAt(0).toUpperCase() + parts[controllerPartIndex].slice(1) + 'Controller';
        this.controller = namespaces.join('\\') + (parts.length > 2 ? '\\' : '') + controllerName;
    }

    generateActionName(method) {
        if (!method || method == '') {
            this.action = '';
            return;
        }

        switch (method) {
            case 'GET':
                this.action = 'fetch';
                break;
            case 'POST':
                this.action = 'create';
                break;
            case 'PUT':
                this.action = 'update';
                break;
            case 'PATCH':
                this.action = 'update';
                break;
            case 'DELETE':
                this.action = 'destroy';
                break;
            default:
                this.action = '';
                break;
        }
    }
}