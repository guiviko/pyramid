import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'breadcrumb',
    templateUrl: './breadcrumb.html',
    styleUrls: ['./breadcrumb.css']
})
export class Breadcrumb {
    @Input() projectId;
    @Input() crumbs;
    @Output() setEditMode = new EventEmitter<any>();
}
