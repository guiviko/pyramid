import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ConnectionService} from "../../services/connection.service";

@Component({
    selector: 'client-map',
    templateUrl: './map.html',
    styleUrls: ['./map.css']
})
export class Map {
    @Input() selectedGroup;
    @Input() selectedAction;
    @Output() updateMap = new EventEmitter<any>();
    @Output() toggleRequestConfigure = new EventEmitter<any>();
    c;
    showSearch = true;
    searchX = 10;
    searchY = 10;
    slotOffsetOutX = 167;
    slotOffsetOutY = 49;
    slotOffsetHeaderInY = 18;
    slotOffsetInX = 12;
    slotOffsetMargin = 30;

    constructor(private connectionService: ConnectionService,) {
        this.c = this.connectionService;
        window.addEventListener('keydown', (e) => this.handleKeyDown(e));
    }

    handleKeyDown(e) {
        if (!this.showSearch) {
            return;
        }

        if (e.keyCode == 27) {
            this.showSearch = false;
        }
    }

    handleEditorMouseUp(e) {
        if (!this.connectionService.isDragging || this.showSearch) {
            return;
        }

        this.connectionService.pause();

        this.searchX = e.pageX - this.connectionService.r.left;
        this.searchY = e.pageY - this.connectionService.r.top;
        this.showSearch = true;
    }

    createNode(nodeName) {
        this.update([
            {
                action: 'createNode',
                nodeInfo: {
                    name: nodeName,
                    posX: this.searchX,
                    posY: this.searchY
                }
            },
            {
                action: 'connectNodes',
                data: {
                    from: {
                        nodeId: this.connectionService.node.id,
                        slotId: this.connectionService.index
                    }
                }
            }
        ]);

        this.showSearch = false;
        this.connectionService.isPaused = false;
        this.connectionService.isDragging = false;
    }

    update(data) {
        this.updateMap.emit({
            updates: data
        });
    }

    connTop(conn) {
        let fromNode;
        let toNode;

        for (let i = 0; i < this.selectedAction.map.nodes.length; i++) {
            if (this.selectedAction.map.nodes[i].id == conn.from.nodeId) {
                fromNode = this.selectedAction.map.nodes[i];
            } else if (this.selectedAction.map.nodes[i].id == conn.to.nodeId) {
                toNode = this.selectedAction.map.nodes[i];
            }
        }

        let top;
        let topSlotId;
        if (fromNode.posY < toNode.posY) {
            top = fromNode.posY;
            topSlotId = conn.from.slotId;
        } else {
            top = toNode.posY;
            topSlotId = conn.to.slotId;
        }

        if (topSlotId == -1) {
            top += this.slotOffsetHeaderInY;
        } else {
            top += this.slotOffsetOutY;
            top += conn.from.slotId * this.slotOffsetMargin;
        }

        return top;
    }

    connLeft(conn) {
        let fromNode;
        let toNode;

        for (let i = 0; i < this.selectedAction.map.nodes.length; i++) {
            if (this.selectedAction.map.nodes[i].id == conn.from.nodeId) {
                fromNode = this.selectedAction.map.nodes[i];
            } else if (this.selectedAction.map.nodes[i].id == conn.to.nodeId) {
                toNode = this.selectedAction.map.nodes[i];
            }
        }

        let leftNode;
        let leftSlotId;
        let left;
        if (fromNode.posX < toNode.posX) {
            leftNode = fromNode;
            leftSlotId = conn.from.slotId;
        } else {
            leftNode = toNode;
            leftSlotId = conn.to.slotId;
        }
        left = leftNode.posX;

        if (leftSlotId == -1) {
            left += this.slotOffsetInX;
        } else {
            if (leftNode.slots[leftSlotId].direction == 'in') {
                left += this.slotOffsetInX;
            } else {
                left += this.slotOffsetOutX;
            }
        }

        return left;
    }

    connWidth(conn) {
        let fromNode;
        let toNode;

        for (let i = 0; i < this.selectedAction.map.nodes.length; i++) {
            if (this.selectedAction.map.nodes[i].id == conn.from.nodeId) {
                fromNode = this.selectedAction.map.nodes[i];
            } else if (this.selectedAction.map.nodes[i].id == conn.to.nodeId) {
                toNode = this.selectedAction.map.nodes[i];
            }
        }

        let width = Math.abs(fromNode.posX - toNode.posX);

        let rightNode;
        let rightSlotId;
        let leftNode;
        let leftSlotId;
        if (fromNode.posX < toNode.posX) {
            rightNode = toNode;
            rightSlotId = conn.to.slotId;
            leftNode = fromNode;
            leftSlotId = conn.from.slotId;
        } else {
            rightNode = fromNode;
            rightSlotId = conn.from.slotId;
            leftNode = toNode;
            leftSlotId = conn.to.slotId;
        }

        if (leftSlotId == -1) {
            width -= this.slotOffsetInX;
        } else {
            if (leftNode.slots[leftSlotId].direction == 'in') {
                width -= this.slotOffsetInX;
            } else {
                width -= this.slotOffsetOutX;
            }
        }

        if (rightSlotId == -1) {
            width += this.slotOffsetInX;
        } else {
            if (rightNode.slots[rightSlotId].direction == 'in') {
                width += this.slotOffsetInX;
            } else {
                width += this.slotOffsetOutX;
            }
        }

        return width;
    }

    connHeight(conn) {
        let fromNode;
        let toNode;

        for (let i = 0; i < this.selectedAction.map.nodes.length; i++) {
            if (this.selectedAction.map.nodes[i].id == conn.from.nodeId) {
                fromNode = this.selectedAction.map.nodes[i];
            } else if (this.selectedAction.map.nodes[i].id == conn.to.nodeId) {
                toNode = this.selectedAction.map.nodes[i];
            }
        }

        let height = Math.abs(fromNode.posY - toNode.posY);

        let topSlotId;
        let bottomSlotId;
        if (fromNode.posY < toNode.posY) {
            topSlotId = conn.from.slotId;
            bottomSlotId = conn.to.slotId;
        } else {
            topSlotId = conn.to.slotId;
            bottomSlotId = conn.from.slotId;
        }

        if (topSlotId == -1) {
            height -= this.slotOffsetHeaderInY;
        } else {
            height -= this.slotOffsetOutY;
            height -= topSlotId * this.slotOffsetMargin;
        }

        if (bottomSlotId == -1) {
            height += this.slotOffsetHeaderInY;
        } else {
            height += this.slotOffsetOutY;
            height += bottomSlotId * this.slotOffsetMargin;
        }

        return height;
    }

    connD(conn) {
        let fromNode;
        let toNode;

        for (let i = 0; i < this.selectedAction.map.nodes.length; i++) {
            if (this.selectedAction.map.nodes[i].id == conn.from.nodeId) {
                fromNode = this.selectedAction.map.nodes[i];
            } else if (this.selectedAction.map.nodes[i].id == conn.to.nodeId) {
                toNode = this.selectedAction.map.nodes[i];
            }
        }

        let height = Math.abs(fromNode.posY - toNode.posY);
        if (height < 5) {
            return 'M 0,0 L 100,0';
        }

        if ((fromNode.posY - toNode.posY < 0 && fromNode.posX - toNode.posX >= 0) ||
            (fromNode.posY - toNode.posY >= 0 && fromNode.posX - toNode.posX < 0)) {
            return 'M 0,100 C 35,100 65,0 100,0';
        }

        return 'M 0,0 C 35,0 65,100 100,100';
    }
}