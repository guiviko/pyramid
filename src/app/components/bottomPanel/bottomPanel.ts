import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'bottom-panel',
    templateUrl: './bottomPanel.html',
    styleUrls: ['./bottomPanel.css']
})
export class BottomPanel {
    @Input() isLogicPanelOpen: boolean;
    @Input() selectedLogicCategory: string;
    @Input() selectedLogicItem: string;
    @Input() projectId;
    @Input() reducers: any;
    @Output() toggleLogicPanel = new EventEmitter<any>();
    @Output() selectLogicCategory = new EventEmitter<any>();
    @Output() selectLogicItem = new EventEmitter<any>();
    @Output() saveReducerContent = new EventEmitter<any>();
    aceOptions;
    text = '';
    canSave: boolean;

    constructor() {
        this.aceOptions = {
            fontSize: '18px'
        };
    }

    getSelectedContent() {
        if (!this.selectedLogicItem || this.selectedLogicItem == '') {
            return '';
        }

        let parts = this.selectedLogicItem.split('@');
        if (this.selectedLogicCategory == 'reducers') {
            return this.reducers.find(reducer => reducer.name == parts[0]).fields
                .find(field => field.name == parts[1]).actions
                .find(action => action.actionGroup + '::' + action.actionName == parts[2]).content;
        }
    }

    getSelectedItemParts() {
        if (!this.selectedLogicItem || this.selectedLogicItem == '') {
            return [];
        }

        return this.selectedLogicItem.split('@');
    }

    handleTextChanged(e) {
        this.text = e;

        if (!this.canSave) {
            this.canSave = true;
        }
    }

    saveContent() {
        this.canSave = false;
        let parts = this.getSelectedItemParts();
        let actionParts = parts[2].split('::');
        this.saveReducerContent.emit({
            reducerName: parts[0],
            fieldName: parts[1],
            actionGroup: actionParts[0],
            actionName: actionParts[1],
            content: this.text
        });

        if (this.selectedLogicCategory == 'reducers') {
            let action = this.reducers.find(reducer => reducer.name == parts[0]).fields
                .find(field => field.name == parts[1]).actions
                .find(action => action.actionGroup + '::' + action.actionName == parts[2]);
            action.content = this.text;
        }
    }
}
