import {Component, Input} from '@angular/core';

@Component({
    selector: 'page-title',
    templateUrl: './pageTitle.html',
    styleUrls: ['./pageTitle.css']
})
export class PageTitle {
    @Input() title: string;
    @Input() back: string;
}
