import {Component, Input, ViewChild} from '@angular/core';

@Component({
    selector: 'service-list',
    templateUrl: './serviceList.html',
    styleUrls: ['./serviceList.css']
})
export class ServiceList {
    @Input() services;
    @ViewChild('addServiceModal') addServiceModal;

    openAddServiceModal() {
        this.addServiceModal.open();
    }
}