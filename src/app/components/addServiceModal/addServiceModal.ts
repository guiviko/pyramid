import {Component, Input} from '@angular/core';
import {Http} from "@angular/http";

@Component({
    selector: 'add-service-modal',
    templateUrl: './addServiceModal.html',
    styleUrls: ['./addServiceModal.css']
})
export class AddServiceModal {
    @Input() abstractServices;
    show: boolean;
    name: string;
    parent: any;

    constructor(private http: Http) {
        this.show = false;
    }

    ngOnInit() {
        window.addEventListener('keypress', e => this.handleKeyPress(e));
    }

    open(): void {
        this.show = true;
    }

    continue(): void {
        if (!this.show) {
            return;
        }

        this.show = false;

        this.http.post('/api/projects/lala/server/services',{
            name: this.name,
            parent: this.parent
        })
            .map(resp => resp.json())
            .subscribe(
                resp => console.log(resp),
                error => console.log(error)
            );
    }

    cancel(): void {
        if (!this.show) {
            return;
        }

        this.show = false;
    }

    cancelWithClick(e): void {
        if ((e.target.className).indexOf('veil') == -1) {
            return;
        }

        this.cancel();
    }

    handleKeyPress(e): void {
        if (e.keyCode == 27) {
            this.cancel();
        }
    }
}