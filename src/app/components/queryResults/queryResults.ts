import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Http} from '@angular/http';

@Component({
    selector: 'query-results',
    templateUrl: './queryResults.html',
    styleUrls: ['./queryResults.css']
})
export class QueryResults {
    @Input() queryResult;
}
