import {NgModule} from '@angular/core';
import {SharedModule} from "../../sharedModule/shared.module";
import {RouterModule} from "@angular/router";

import {routes} from './plugins.routes';
import {PluginsComponent} from "./plugins.component";
import {PluginManagementPage} from "../../../pages/pluginManagement/pluginManagement.page";
import {PluginPage} from "../../../pages/plugin/plugin.page";

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
    ],
    declarations: [
        PluginsComponent,
        PluginManagementPage,
        PluginPage
    ]
})
export class PluginsModule {

}
