import {Routes} from '@angular/router';
import {PluginsComponent} from "./plugins.component";
import {PluginManagementPage} from "../../../pages/pluginManagement/pluginManagement.page";
import {PluginPage} from "../../../pages/plugin/plugin.page";

export const routes: Routes = [
    { path: '', component: PluginsComponent, children: [
        { path: '', component: PluginManagementPage},
        { path: ':pluginName', component: PluginPage}
    ]}
];
