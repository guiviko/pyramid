import {Component} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'plugins-component',
    templateUrl: 'plugins.component.html',
    styleUrls: ['./plugins.component.css']
})
export class PluginsComponent {
    projectId;

    constructor(private route: ActivatedRoute) {
        this.route.parent.parent.params.subscribe(params => {
            this.projectId = params['projectId'];
        });
    }
}
