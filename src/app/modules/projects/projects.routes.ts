import {Routes} from '@angular/router';
import {ProjectsComponent} from "./projects.component";

export const routes: Routes = [
    { path: ':projectId', component: ProjectsComponent, children: [
        {path: 'client', loadChildren: './client/index#ClientModule?sync=true' },
        {path: 'server', loadChildren: './server/index#ServerModule?sync=true' },
        {path: 'plugins', loadChildren: './plugins/index#PluginsModule?sync=true' }
    ]}
];
