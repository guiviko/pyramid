import {NgModule} from '@angular/core';
import {SharedModule} from "../../sharedModule/shared.module";
import {RouterModule} from "@angular/router";

import {routes} from './client.routes';
import {ComponentGraphPage} from "../../../pages/componentGraph/ComponentGraph.page";
import {ClientComponent} from "./client.component";
import {PageListPage} from "../../../pages/pageList/PageList.page";
import {CompNode} from "../../../components/compNode/CompNode";
import {DeleteActionModal} from "../../../components/deleteActionModal/deleteActionModal";
import {ConfirmationModal} from "../../../components/confirmationModal/ConfirmationModal";
import {AddComponentModal} from "../../../components/addComponentModal/AddComponentModal";
import {BlankDemo} from "../../../components/templateDemos/blank/blankDemo";
import {PanelDemo} from "../../../components/templateDemos/panel/panelDemo";
import {HeadersBasic} from "../../../components/templateDemos/headersBasic/headersBasic";
import {MenusSideBasic} from "../../../components/templateDemos/menusSideBasic/menusSideBasic";
import {GeneralPageHeaderBasic} from "../../../components/templateDemos/generalPageHeaderBasic/generalPageHeaderBasic";
import {AddPageModal} from "../../../components/addPageModal/addPageModal";
import {TitlePanelPage} from "../../../components/templateDemos/pages/titlePanelPage/titlePanelPage";
import {BlankPage} from "../../../components/templateDemos/pages/blankPage/blankPage";
import {FormsBasic} from "../../../components/templateDemos/formsBasic/formsBasic";
import {ReducerPage} from "../../../pages/reducer/reducer.page";
import {AddReducerModal} from "../../../components/addReducerModal/addReducerModal";
import {ReducerList} from "../../../components/reducerList/reducerList";
import {PlatformPage} from '../../../pages/client/platform/platform.page';
import {Config} from "../../../components/config/config";
import {Window} from "../../../components/window/window";
import {ToolBar} from "../../../components/toolBar/toolBar";
import {ComponentSelector} from "../../../components/componentSelector/componentSelector";
import {Breadcrumb} from "../../../components/breadcrumb/breadcrumb";
import {Block} from "../../../components/block/block";
import {Separator} from "../../../components/separator/separator";
import {PanelConfig} from "../../../components/platform/panelConfig/panelConfig";
import {EmptyConfig} from "../../../components/platform/emptyConfig/emptyConfig";
import {TemplateConfig} from "../../../components/templateConfig/templateConfig";
import {FormConfig} from "../../../components/platform/formConfig/formConfig";
import {IOEditor} from "../../../components/ioEditor/ioEditor";
import {AddOutputModal} from "../../../components/addOutputModal/addOutputModal";
import {AddReducerFieldModal} from "../../../components/addReducerFieldModal/addReducerFieldModal";
import {DisconnectFromReducerModal} from "../../../components/disconnectFromReducerModal/disconnectFromReducerModal";
import {BottomPanel} from "../../../components/bottomPanel/bottomPanel";
import {DropComponentModal} from "../../../components/dropComponentModal/dropComponentModal";
import {AddInputModal} from "../../../components/addInputModal/addInputModal";
import {Node} from "../../../components/node/node";
import {Map} from "../../../components/map/map";
import {ActionsPage} from "../../../pages/actions/actions.page";
import {AceEditorModule} from 'ng2-ace-editor';
import {ActionList} from "../../../components/actionList/actionList";
import {RequestConfigure} from "../../../components/requestConfigure/requestConfigure";

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
        AceEditorModule
    ],
    declarations: [
        ComponentGraphPage,
        PageListPage,
        ClientComponent,
        CompNode,
        DeleteActionModal,
        ConfirmationModal,
        AddComponentModal,
        BlankDemo,
        PanelDemo,
        HeadersBasic,
        MenusSideBasic,
        GeneralPageHeaderBasic,
        AddPageModal,
        BlankPage,
        TitlePanelPage,
        FormsBasic,
        ReducerPage,
        ReducerList,
        AddReducerModal,
        PlatformPage,
        Config,
        Breadcrumb,
        ComponentSelector,
        ToolBar,
        Window,
        Block,
        Separator,
        TemplateConfig,
        IOEditor,
        EmptyConfig,
        PanelConfig,
        FormConfig,
        AddOutputModal,
        AddReducerFieldModal,
        BottomPanel,
        DisconnectFromReducerModal,
        DropComponentModal,
        AddInputModal,
        ActionsPage,
        Node,
        Map,
        ActionList,
        RequestConfigure
    ]
})
export class ClientModule {

}
