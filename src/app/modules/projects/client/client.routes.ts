import {Routes} from '@angular/router';
import {ClientComponent} from "./client.component";
import {PageListPage} from "../../../pages/pageList/PageList.page";
import {PlatformPage} from '../../../pages/client/platform/platform.page';
import {ActionsPage} from "../../../pages/actions/actions.page";

export const routes: Routes = [
    { path: '', component: ClientComponent, children: [
        { path: 'pages', component: PageListPage},
        {path: 'platform/:componentId', component: PlatformPage},
        {path: 'actions', component: ActionsPage},
    ]}
];