import {Component} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'projects-component',
    templateUrl: 'projects.component.html',
    styleUrls: ['./projects.component.css']
})
export class ProjectsComponent {
    projectId;

    constructor(private route: ActivatedRoute) {
        this.route.params.subscribe(
            params => this.projectId = params['projectId']
        )
    }
}