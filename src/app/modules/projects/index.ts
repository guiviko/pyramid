import {NgModule} from '@angular/core';
import {SharedModule} from "../sharedModule/shared.module";
import {RouterModule} from "@angular/router";

import {routes} from './projects.routes';
import {ProjectsComponent} from "./projects.component";
import {DashboardMenu} from "../../components/dashboardMenu/DashboardMenu";

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        ProjectsComponent,
        DashboardMenu
    ]
})
export class ProjectsModule {

}