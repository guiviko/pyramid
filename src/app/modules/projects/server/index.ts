import {NgModule} from '@angular/core';
import {SharedModule} from "../../sharedModule/shared.module";
import {RouterModule} from "@angular/router";

import {routes} from './server.routes';
import {ServerComponent} from "./Server.component";
import {ApiPage} from "../../../pages/api/api.page";
import {ApiList} from "../../../components/apiList/apiList";
import {AddApiModal} from "../../../components/addApiModal/addApiModal";
import {ServerMenu} from "../../../components/serverMenu/serverMenu";
import {ServicePage} from "../../../pages/service/service.page";
import {SchemaPage} from "../../../pages/schema/schema.page";
import {ServiceList} from "../../../components/serviceList/serviceList";
import {AddServiceModal} from "../../../components/addServiceModal/addServiceModal";
import {ModelPage} from "../../../pages/model/model.page";
import {ModelList} from "../../../components/modelList/modelList";
import {AddModelModal} from "../../../components/addModelModal/addModelModal";
import {TypeList} from "../../../components/typeList/typeList";
import {AddFieldModal} from "../../../components/addFieldModal/addFieldModal";
import {QueryEditor} from "../../../components/queryEditor/queryEditor";
import {QueryResults} from "../../../components/queryResults/queryResults";
import {PlaygroundPage} from "../../../pages/playground/playground.page";

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
    ],
    declarations: [
        ServerComponent,
        ApiPage,
        ApiList,
        AddApiModal,
        ServerMenu,
        ServicePage,
        PlaygroundPage,
        ServiceList,
        AddServiceModal,
        ModelPage,
        ModelList,
        AddModelModal,
        SchemaPage,
        TypeList,
        AddFieldModal,
        QueryEditor,
        QueryResults,
    ]
})
export class ServerModule {

}
