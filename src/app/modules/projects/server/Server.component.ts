import {Component} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'server-component',
    templateUrl: 'server.component.html',
    styleUrls: ['./server.component.css']
})
export class ServerComponent {
    projectId;

    constructor(private route: ActivatedRoute) {
        this.route.parent.parent.params.subscribe(params => {
            this.projectId = params['projectId'];
        });
    }
}
