import {Routes} from '@angular/router';
import {SchemaPage} from "../../../pages/schema/schema.page";
import {PlaygroundPage} from "../../../pages/playground/playground.page";
import {ServerComponent} from "./Server.component";

export const routes: Routes = [
    { path: '', component: ServerComponent, children: [
        { path: 'schema', component: SchemaPage},
        { path: 'playground', component: PlaygroundPage},
    ]}
];
