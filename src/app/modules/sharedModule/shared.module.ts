import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import {FormField} from '../../components/formField/formField';
import {UnderTitleMenu} from "../../components/underTitleMenu/UnderTitleMenu";
import {PageTitle} from "../../components/pageTitle/PageTitle";
import {RouterModule} from "@angular/router";
import {ConfirmModal} from "../../components/confirmModal/ConfirmModal";
import {PyrDroppable} from "../../components/generic/pyrDraggable/pyrDroppable.directive";
import {PyrDraggable} from "../../components/generic/pyrDraggable/pyrDraggable.directive";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule
    ],
    declarations: [
        FormField,
        PageTitle,
        UnderTitleMenu,
        ConfirmModal,
        PyrDraggable,
        PyrDroppable
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FormField,
        PageTitle,
        UnderTitleMenu,
        ConfirmModal,
        PyrDraggable,
        PyrDroppable
    ]
})
export class SharedModule {

}