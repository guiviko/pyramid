<?php

namespace Tests\Services;

use App\Domain\Services\Client\PlatformService;
use Tests\ProjectBaseTest;
use Tests\Factories\ComponentFactory;
use App\Domain\Models\Projects\ComponentTemplate;
use App\Domain\Models\Projects\Component;
use Library\Http\Response;
use App\Domain\Utils\PathUtils;
use App\Domain\Utils\StringUtils;

class PlatformServiceTest extends ProjectBaseTest
{
    /**
     * @var PlatformService
     */
    private $service;

    public function setUp()
    {
        parent::setUp();

        $this->setUpApplication();
        $this->setUpDatamapper([
            \App\Domain\Models\Projects\Project::class,
            \App\Domain\Models\Projects\Page::class,
            \App\Domain\Models\Projects\Module::class,
            \App\Domain\Models\Projects\Component::class,
            \App\Domain\Models\Projects\ComponentTemplate::class,
            \App\Domain\Models\Projects\ControlTemplate::class
        ]);

        $this->app->container()->registerInstance('currentProject', $this->currentProject);
        $this->service = $this->app->container()->resolve(\App\Domain\Services\Client\PlatformService::class);
    }

    public function setUpPageComponent()
    {
        $this->dm->persist($this->currentProject->project());
        $this->dm->persist(ComponentFactory::createAppModule($this->currentProject));
        $this->dm->persist(ComponentFactory::createTemplate());
        $this->dm->flush();
        $template = $this->dm->find(ComponentTemplate::class, 1);
        $component = ComponentFactory::createComponent($template, $this->currentProject);
        $this->dm->persist($component);
        $this->dm->flush();
        $this->dm->detachAll();

        // creating component directory
        $directory = $this->currentProject->componentRoot().'/'.$component->name();
        mkdir($directory);
        $this->assertTrue(file_exists($directory));

        // creating component files
        $ts = file_get_contents(PathUtils::componentTemplateRoot.'/empty/ts.ts');
        $ts = str_replace('selectorName0', 'some-name', $ts);
        $ts = str_replace('htmlName0', 'someName.html', $ts);
        $ts = str_replace('styleName0', 'someName.css', $ts);
        $ts = str_replace('className0', 'SomeName', $ts);
        file_put_contents($this->currentProject->componentRoot().'/'.$component->name().'/'.$component->name().'.ts', $ts);
        $this->assertTrue(file_exists($this->currentProject->componentRoot().'/'.$component->name().'/'.$component->name().'.ts'));

        $html = file_get_contents(PathUtils::componentTemplateRoot.'/empty/html.html');
        $html = str_replace('name0', 'some-name', $html);
        file_put_contents($this->currentProject->componentRoot().'/'.$component->name().'/'.$component->name().'.html', $html);
        $this->assertTrue(file_exists($this->currentProject->componentRoot().'/'.$component->name().'/'.$component->name().'.html'));

        $scss = file_get_contents(PathUtils::componentTemplateRoot.'/empty/scss.scss');
        $scss = str_replace('name0', 'some-name', $scss);
        file_put_contents($this->currentProject->componentRoot().'/'.$component->name().'/'.$component->name().'.scss', $scss);
        $this->assertTrue(file_exists($this->currentProject->componentRoot().'/'.$component->name().'/'.$component->name().'.scss'));

        $css = file_get_contents(PathUtils::componentTemplateRoot.'/empty/css.css');
        $css = str_replace('name0', 'some-name', $css);
        file_put_contents($this->currentProject->componentRoot().'/'.$component->name().'/'.$component->name().'.scss', $css);
        $this->assertTrue(file_exists($this->currentProject->componentRoot().'/'.$component->name().'/'.$component->name().'.scss'));
    }

    public function test_addComponent_addsANewComponentToTheDatabase()
    {
        // Arrange
        $this->setUpPageComponent();

        // Act
        $response = $this->service->addComponent(1, 1, 2, 1);

        // Assert
        $this->assertEquals(Response::STATUS_OK, $response->statusCode());
        $components = $this->dm->findAll(Component::class);
        $this->assertEquals(2, $components->count());
    }


    public function test_addComponent_addsItToTheParentBlock()
    {
        // Arrange
        $this->setUpPageComponent();

        // Act
        $response = $this->service->addComponent(1, 1, 2, 1);

        // Assert
        $this->assertEquals(Response::STATUS_OK, $response->statusCode());
        $pageComponent = $this->dm->find(Component::class, 1);
        $componentData = $pageComponent->blockAreas()[0]['blockMap'][2]['content']['component'];
        $template = $this->dm->find(ComponentTemplate::class, 1);
        $this->assertNotNull($componentData);
        $this->assertEquals([
            'templateId' => '1',
            'id' => '2',
            'name' => $template->name(),
            'icon' => $template->icon()
        ], $componentData);
    }

    public function test_addComponent_addsItToTheParentHtml()
    {
        // Arrange
        $this->setUpPageComponent();

        // Act
        $response = $this->service->addComponent(1, 1, 2, 1);

        // Assert
        $this->assertEquals(Response::STATUS_OK, $response->statusCode());
        $pageComponent = $this->dm->find(Component::class, 1);
        $addedComponent = $this->dm->find(Component::class, 2);
        $this->assertNotNull($pageComponent);
        $this->assertNotNull($addedComponent);
        $addedComponentDashName = StringUtils::camelCaseToDash($addedComponent->name());
        $parentHtml = file_get_contents($pageComponent->directoryPath().'/'.$pageComponent->name().'.html');
        $this->assertNotFalse(strpos($parentHtml, '<'.$addedComponentDashName.'></'.$addedComponentDashName.'>'));
    }

    public function test_addOutput_addsTheEventEmitterToTheComponent()
    {
        // Arrange
        $this->setUpPageComponent();
        $response = $this->service->addComponent(1, 1, 2, 1);
        $this->assertEquals(Response::STATUS_OK, $response->statusCode());

        // Act
        $response = $this->service->addOutput(2, 'some', 'anAction');

        // Assert
        $this->assertEquals(Response::STATUS_OK, $response->statusCode());
        $component = $this->dm->find(Component::class, 2);
        $this->assertNotNull($component);
        $tsContent = file_get_contents($component->directoryPath().'/'.$component->name().'.ts');
        $this->assertNotFalse($tsContent, '@Output() anAction = new EventEmitter<any>();'.PHP_EOL);
    }

    public function test_addOutput_addsTheNecessaryImportsToTheComponent()
    {
        // Arrange
        $this->setUpPageComponent();
        $response = $this->service->addComponent(1, 1, 2, 1);
        $this->assertEquals(Response::STATUS_OK, $response->statusCode());

        // Act
        $response = $this->service->addOutput(2, 'some', 'anAction');

        // Assert
        $this->assertEquals(Response::STATUS_OK, $response->statusCode());
        $component = $this->dm->find(Component::class, 2);
        $this->assertNotNull($component);
        $tsContent = file_get_contents($component->directoryPath().'/'.$component->name().'.ts');
        preg_match('/import.*{.*Output.*}.*from/', $tsContent, $matches);
        $this->assertTrue(sizeof($matches) > 0);
        preg_match('/import.*{.*EventEmitter.*}.*from/', $tsContent, $matches);
        $this->assertTrue(sizeof($matches) > 0);
    }

    public function test_addOutput_addsTheDispatchFunctionInThePage()
    {
        // Arrange
        $this->setUpPageComponent();
        $response = $this->service->addComponent(1, 1, 2, 1);
        $this->assertEquals(Response::STATUS_OK, $response->statusCode());
        $pageComponent = $this->dm->find(Component::class, 1);
        $this->assertNotNull($pageComponent);

        // Act
        $response = $this->service->addOutput(2, 'some', 'anAction');

        // Assert
        $this->assertEquals(Response::STATUS_OK, $response->statusCode());
        $tsContent = file_get_contents($pageComponent->directoryPath().'/'.$pageComponent->name().'.ts');
        $str = '    anAction(data: any) {'.PHP_EOL;
        $str .= '        this.store.dispatch(this.someActions.anAction(data));'.PHP_EOL;
        $str .= '    }';
        $this->assertNotFalse(strpos($tsContent, $str));
    }

    public function test_addOutput_addsTheCorrectImportForTheActionFile()
    {
        // Arrange
        $this->setUpPageComponent();
        $response = $this->service->addComponent(1, 1, 2, 1);
        $this->assertEquals(Response::STATUS_OK, $response->statusCode());
        $pageComponent = $this->dm->find(Component::class, 1);
        $this->assertNotNull($pageComponent);

        // Act
        $response = $this->service->addOutput(2, 'some', 'anAction');

        // Assert
        $this->assertEquals(Response::STATUS_OK, $response->statusCode());
        $tsContent = file_get_contents($pageComponent->directoryPath().'/'.$pageComponent->name().'.ts');
        $this->assertNotFalse(strpos($tsContent, 'import {SomeActions} from \'./../../actions/some.actions\''));
    }
}