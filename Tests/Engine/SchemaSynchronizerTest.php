<?php

namespace Tests\Engine;

use Tests\BaseTest;
use Library\Engine\Schema\SchemaSynchronizer;

class SchemaSynchronizerTest extends BaseTest
{
    /**
     * @var SchemaSynchronizer
     */
    private $synchronizer;

    public function test_synchronize_addsANewModel()
    {
        // Arrange
        $this->synchronizer = new SchemaSynchronizer([
            'test' => [
                'name' => ['type' => 'string']
            ]
        ], []);

        // Act
        $result = $this->synchronizer->synchronize();

        // Assert
        $this->assertTrue($result['success']);
    }
}