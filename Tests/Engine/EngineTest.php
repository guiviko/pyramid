<?php

namespace Tests\Engine;

use Tests\BaseTest;
use Library\Engine\Engine;
use App\Domain\Models\Recipes;
use App\Domain\Models\Ingredients;
use Library\DataMapper\Collection\EntityCollection;

class EngineTest extends BaseTest
{
    public function setUp()
    {
        parent::setUp();

        $this->setUpDatamapper([
            \App\Domain\Models\Recipes::class,
            \App\Domain\Models\Ingredients::class
        ]);

        $this->setUpApplication();

        $this->setUpEngine([
            'recipes' => [
                'name' => 'string',
                'cat' => 'string',
                'prepTime' => 'int',
                'ingredients' => '@hasMany:ingredients'
            ],

            'ingredients' => [
                'name' => 'string',
                'amount' => 'int',
                'unit' => 'string'
            ]
        ]);
    }

    public function setUpBasicEntities()
    {
        $recipe1 = new Recipes('pasta', 'italian', 10, new EntityCollection());
        $recipe2 = new Recipes('tomato soup', 'soups', 10, new EntityCollection());
        $this->dm->persist($recipe1);
        $this->dm->persist($recipe2);
        $this->dm->persist(new Ingredients('sauce', 5, 'kg', $recipe1));
        $this->dm->persist(new Ingredients('pasta', 5, 'L', $recipe1));
        $this->dm->persist(new Ingredients('broth', 5, 'L', $recipe2));
        $this->dm->persist(new Ingredients('water', 5, 'L', $recipe2));
        $this->dm->flush();
        $this->dm->detachAll();
    }

    public function test_create_createsTheEntity()
    {
        // Act
        $result = $this->engine->run([
            'create' => [
                'recipes' => [
                    'values' => [
                        [
                            'name' => 'tomato soup',
                            'prepTime' => 15,
                            'cat' => 'soups',
                            'ingredients' => []
                        ],
                        [
                            'name' => 'pasta',
                            'prepTime' => 35,
                            'cat' => 'italian',
                            'ingredients' => []
                        ]
                    ],
                    'fields' => [
                        'id' => ['as' => 'id'],
                        'name' => ['as' => 'name']
                    ]
                ]
            ],
        ]);

        // Assert
        $this->assertEquals(200, $result['status']);
        $recipes = $this->dm->findAll(Recipes::class)->toArray();
        $this->assertEquals(2, sizeof($recipes));
        $this->assertEquals('tomato soup', $recipes[0]->getName());
    }

    public function test_create_returnsTheFieldsAfterCreating()
    {
        // Act
        $result = $this->engine->run([
            'create' => [
                'recipes' => [
                    'values' => [
                        [
                            'name' => 'tomato soup',
                            'prepTime' => 15,
                            'cat' => 'soups',
                            'ingredients' => []
                        ],
                        [
                            'name' => 'pasta',
                            'prepTime' => 35,
                            'cat' => 'italian',
                            'ingredients' => []
                        ]
                    ],
                    'fields' => [
                        'id' => ['as' => 'id'],
                        'name' => ['as' => 'name']
                    ]
                ]
            ],
        ]);

        // Assert
        $this->assertEquals(200, $result['status']);
        $this->assertEquals(2, sizeof($result['content']['recipes']));
        $this->assertEquals(1, $result['content']['recipes'][0]['id']);
        $this->assertEquals('tomato soup', $result['content']['recipes'][0]['name']);
    }

    public function test_create_whenThereIsABelongsToRelationship()
    {
        // Arrange
        $this->setUpBasicEntities();

        // Act
        $result = $this->engine->run([
            'create' => [
                'ingredients' => [
                    'values' => [
                        [
                            'recipesId' => 1,
                            'name' => 'carrot',
                            'amount' => '2',
                            'unit' => 'mg'
                        ]
                    ],
                    'fields' => [
                        'id' => ['as' => 'id'],
                        'name' => ['as' => 'name']
                    ]
                ]
            ],
        ]);

        // Assert
        $this->assertEquals(200, $result['status']);
        $this->dm->detachAll();
        $recipe = $this->dm->find(Recipes::class, 1);
        $this->assertEquals(3, $recipe->getIngredients()->count());
        $this->assertEquals('carrot', $result['content']['ingredients'][0]['name']);
    }

    public function test_fetch_getsAllEntities()
    {
        // Arrange
        $this->setUpBasicEntities();

        // Act
        $result = $this->engine->run([
            'fetch' => [
                'recipes' => [
                    'fields' => [
                        'name' => ['as' => 'name']
                    ]
                ]
            ]
        ]);

        // Assert
        $this->assertEquals(200, $result['status']);
        $this->assertEquals(2, sizeof($result['content']['recipes']));
    }

    public function test_fetch_getsOnlyTheRequestedFields()
    {
        // Arrange
        $this->setUpBasicEntities();

        // Act
        $result = $this->engine->run([
            'fetch' => [
                'recipes' => [
                    'fields' => [
                        'id' => ['as' => 'id'],
                        'name' => ['as' => 'name']
                    ]
                ]
            ]
        ]);

        // Assert
        $this->assertEquals(200, $result['status']);
        $this->assertTrue(isset($result['content']['recipes'][0]['id']));
        $this->assertTrue(isset($result['content']['recipes'][0]['name']));
        $this->assertFalse(isset($result['content']['recipes'][0]['cat']));
        $this->assertFalse(isset($result['content']['recipes'][0]['prepTime']));
    }

    public function test_fetch_correctlyReturnsAliases()
    {
        // Arrange
        $this->setUpBasicEntities();

        // Act
        $result = $this->engine->run([
            'fetch' => [
                'recipes' => [
                    'fields' => [
                        'id' => ['as' => 'theId'],
                        'name' => ['as' => 'theName']
                    ]
                ]
            ]
        ]);

        // Assert
        $this->assertEquals(200, $result['status']);
        $this->assertTrue(isset($result['content']['recipes'][0]['theId']));
        $this->assertTrue(isset($result['content']['recipes'][0]['theName']));
        $this->assertFalse(isset($result['content']['recipes'][0]['id']));
        $this->assertFalse(isset($result['content']['recipes'][0]['name']));
    }

    public function test_fetch_withWhere()
    {
        // Arrange
        $this->setUpBasicEntities();

        // Act
        $result = $this->engine->run([
            'fetch' => [
                'recipes' => [
                    'conditions' => [
                        ['id', '=', 2]
                    ],
                    'fields' => [
                        'id' => ['as' => 'id'],
                        'name' => ['as' => 'name']
                    ]
                ]
            ]
        ]);

        // Assert
        $this->assertEquals(200, $result['status']);
        $this->assertEquals(1, sizeof($result['content']['recipes']));
        $this->assertEquals('tomato soup', $result['content']['recipes'][0]['name']);
    }

    public function test_fetch_whenThereIsAOneToManyRelationship()
    {
        // Arrange
        $this->setUpBasicEntities();

        // Act
        $result = $this->engine->run([
            'fetch' => [
                'recipes' => [
                    'fields' => [
                        'id' => ['as' => 'id'],
                        'name' => ['as' => 'name'],
                        'ingredients' => [
                            'as' => 'ingredients',
                            'fields' => [
                                'id' => ['as' => 'id'],
                                'name' => ['as' => 'name']
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        // Assert
        $this->assertEquals(200, $result['status']);
        $firstRecipe = $result['content']['recipes'][0];
        $this->assertEquals(2, sizeof($firstRecipe['ingredients']));
        $this->assertEquals('sauce', $firstRecipe['ingredients'][0]['name']);
    }

    public function test_fetch_correctlyReturnsAliasesForRelation()
    {
        // Arrange
        $this->setUpBasicEntities();

        // Act
        $result = $this->engine->run([
            'fetch' => [
                'recipes' => [
                    'fields' => [
                        'id' => ['as' => 'id'],
                        'name' => ['as' => 'name'],
                        'ingredients' => [
                            'as' => 'aliasName',
                            'fields' => [
                                'id' => ['as' => 'id'],
                                'name' => ['as' => 'name']
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        // Assert
        $this->assertEquals(200, $result['status']);
        $firstRecipe = $result['content']['recipes'][0];
        $this->assertEquals(2, sizeof($firstRecipe['aliasName']));
        $this->assertEquals('sauce', $firstRecipe['aliasName'][0]['name']);
    }

    public function test_delete_onlyDeletesTheEntityWeAskFor()
    {
        // Arrange
        $this->setUpBasicEntities();

        // Act
        $result = $this->engine->run([
            'delete' => [
                'ingredients' => [
                    'conditions' => [
                        ['id', '=', 1]
                    ]
                ]
            ]
        ]);

        // Assert
        $this->assertEquals(200, $result['status']);
        $ingredients = $this->dm->findAll(Ingredients::class)->toArray();
        $this->assertEquals(2, $ingredients[0]->getId());
    }
}