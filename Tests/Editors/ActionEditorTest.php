<?php

namespace Tests\Editors;

use App\Domain\Project\Editors\Client\ActionEditor;
use App\Domain\Processors\Client\TsProcessor;
use App\Domain\Processors\Client\HtmlProcessor;
use App\Domain\Utils\FileUtils;
use Tests\ProjectBaseTest;

class ActionEditorTest extends ProjectBaseTest
{
    /**
     * @var ActionEditor
     */
    private $editor;

    public function setUp()
    {
        parent::setUp();

        $this->editor = new ActionEditor($this->currentProject, new TsProcessor(), new HtmlProcessor());
    }

    public function test_addActionFile_createsTheActionFileCorrectly()
    {
        // Act
        $this->editor->addActionFile('some');

        // Assert
        $this->assertTrue(FileUtils::fileExists($this->currentProject->actionRoot().'/some.actions.ts'));
    }

    public function test_addActionFile_correctlyNamesTheActionClass()
    {
        // Act
        $this->editor->addActionFile('some');

        // Assert
        $content = FileUtils::readFile($this->currentProject->actionRoot().'/some.actions.ts');
        $this->assertNotFalse(strpos($content, ' SomeActions '));
    }

    public function test_addActionFile_addsTheImportInIndex()
    {
        // Act
        $this->editor->addActionFile('some');

        // Assert
        $content = FileUtils::readFile($this->currentProject->actionRoot().'/index.ts');
        $this->assertNotFalse(strpos($content, 'import {SomeActions} from \'./some.actions'));
    }

    public function test_addActionFile_registersInIndex()
    {
        // Act
        $this->editor->addActionFile('some');

        // Assert
        $content = FileUtils::readFile($this->currentProject->actionRoot().'/index.ts');
        $this->assertNotFalse(strpos($content, 'SomeActions,'.PHP_EOL));
    }

    public function test_addAction_addsItToTheFileCorrectly()
    {
        // Arrange
        $this->editor->addActionFile('some');

        // Act
        $this->editor->addAction('some', 'theAction');

        // Assert
        $expected = '    static THE_ACTION = \'SOME_THE_ACTION\';'.PHP_EOL;
        $expected .= '    theAction(data: any): Action {'.PHP_EOL;
        $expected .= '        return {'.PHP_EOL;
        $expected .= '            type: SomeActions.THE_ACTION,'.PHP_EOL;
        $expected .= '            payload: data'.PHP_EOL;
        $expected .= '        }'.PHP_EOL;
        $expected .= '    }'.PHP_EOL;
        $content = FileUtils::readFile($this->currentProject->actionRoot().'/some.actions.ts');
        $this->assertNotFalse(strpos($content, $expected));
    }
}