<?php

namespace Tests\Editors;

use App\Domain\Models\Projects\Component;
use App\Domain\Models\Projects\ComponentTemplate;
use App\Domain\Models\Projects\Module;
use App\Domain\Processors\Client\TsProcessor;
use App\Domain\Processors\Client\HtmlProcessor;
use App\Domain\Project\Editors\Client\ComponentEditor;
use App\Domain\Utils\FileUtils;
use App\Domain\Utils\PathUtils;
use App\Domain\Utils\StringUtils;
use Tests\ProjectBaseTest;

class ComponentEditorTest extends ProjectBaseTest
{
    /**
     * @var ComponentEditor
     */
    private $editor;

    public function setup()
    {
        parent::setUp();

        $this->editor = new ComponentEditor($this->currentProject, new TsProcessor(), new HtmlProcessor());
    }

    private function getComponent()
    {
        $templateData = require(PathUtils::componentTemplateRoot.'/empty/data.php');
        $templateComponent = new ComponentTemplate('name', $templateData['icon'],
            '/var/www/html/pyramid_test/src/app/components', $templateData['config'], $templateData['blockAreas'],
            $templateData['metadata']);

        return new Component($templateComponent, 'someName', $this->currentProject->componentRoot().'/someName', '',
            $templateComponent->emptyConfig(), $templateComponent->blockAreas(), $templateComponent->metadata());
    }

    private function getAppModule()
    {
        return new Module($this->currentProject->project(), 'app', '/', '', $this->currentProject->clientRoot());
    }

    public function test_create_createsTheComponentDirectory()
    {
        // Arrange
        $component = $this->getComponent();
        $module = $this->getAppModule();

        // Act
        $this->editor->create($component, $module);

        // Assert
        $this->assertTrue(file_exists($this->currentProject->componentRoot().'/'.$component->name()));
    }

    public function test_create_createsTsFile()
    {
        // Arrange
        $component = $this->getComponent();
        $module = $this->getAppModule();

        // Act
        $this->editor->create($component, $module);

        // Assert
        $this->assertTrue(file_exists($this->currentProject->componentRoot().'/'.$component->name().'/'.$component->name().'.ts'));
    }

    /**
     * @depends test_create_createsTsFile
     */
    public function test_create_generatesTsContentCorrectly()
    {
        // Arrange
        $component = $this->getComponent();
        $module = $this->getAppModule();

        // Act
        $this->editor->create($component, $module);
        $content = FileUtils::readFile($this->currentProject->componentRoot().'/'.$component->name().'/'.$component->name().'.ts');

        // Assert
        $this->assertNotFalse(strpos($content, 'selector: \''.StringUtils::camelCaseToDash($component->name()).'\','));
        $this->assertNotFalse(strpos($content, 'templateUrl: \'./'.$component->name().'.html').'\',');
        $this->assertNotFalse(strpos($content, 'styleUrls: [\'./'.$component->name().'.html').'\'],');
        $this->assertNotFalse(strpos($content, 'export class '.ucfirst($component->name()).' {'));
    }

    public function test_create_createsHtmlFile()
    {
        // Arrange
        $component = $this->getComponent();
        $module = $this->getAppModule();

        // Act
        $this->editor->create($component, $module);

        // Assert
        $this->assertTrue(file_exists($this->currentProject->componentRoot().'/'.$component->name().'/'.$component->name().'.html'));
    }

    /**
     * @depends test_create_createsHtmlFile
     */
    public function test_create_generatesHtmlContentCorrectly()
    {
        // Arrange
        $component = $this->getComponent();
        $module = $this->getAppModule();

        // Act
        $this->editor->create($component, $module);
        $content = FileUtils::readFile($this->currentProject->componentRoot().'/'.$component->name().'/'.$component->name().'.html');

        // Assert
        $this->assertNotFalse(strpos($content, 'class="'.StringUtils::camelCaseToDash($component->name()).'"'));
    }

    public function test_create_createsScssFile()
    {
        // Arrange
        $component = $this->getComponent();
        $module = $this->getAppModule();

        // Act
        $this->editor->create($component, $module);

        // Assert
        $this->assertTrue(file_exists($this->currentProject->componentRoot().'/'.$component->name().'/'.$component->name().'.scss'));
    }

    /**
     * @depends test_create_createsScssFile
     */
    public function test_create_generatesScssContentCorrectly()
    {
        // Arrange
        $component = $this->getComponent();
        $module = $this->getAppModule();

        // Act
        $this->editor->create($component, $module);
        $content = FileUtils::readFile($this->currentProject->componentRoot().'/'.$component->name().'/'.$component->name().'.scss');

        // Assert
        $this->assertNotFalse(strpos($content, '.'.StringUtils::camelCaseToDash($component->name()).' {'));
    }

    public function test_create_createsCssFile()
    {
        // Arrange
        $component = $this->getComponent();
        $module = $this->getAppModule();

        // Act
        $this->editor->create($component, $module);

        // Assert
        $this->assertTrue(file_exists($this->currentProject->componentRoot().'/'.$component->name().'/'.$component->name().'.css'));
    }

    public function test_create_addsComponentToAppModuleDeclarationsImports()
    {
        // Arrange
        $component = $this->getComponent();
        $module = $this->getAppModule();

        // Act
        $this->editor->create($component, $module);

        // Assert
        $appDeclarationsContent = FileUtils::readFile($this->currentProject->clientRoot().'/app.declarations.ts');
        $importString = 'import {'.ucfirst($component->name()).'} from \'./components/'.$component->name().'/'.$component->name().'\';';
        $this->assertNotFalse(strpos($appDeclarationsContent, $importString));
    }

    public function test_create_addsComponentToAppModuleDeclarationsArray()
    {
        // Arrange
        $component = $this->getComponent();
        $module = $this->getAppModule();

        // Act
        $this->editor->create($component, $module);

        // Assert
        $appDeclarationsContent = FileUtils::readFile($this->currentProject->clientRoot().'/app.declarations.ts');
        $declarationsArrayString = 'APP_DECLARATIONS = ['.PHP_EOL.'    '.ucfirst($component->name());
        $this->assertNotFalse(strpos($appDeclarationsContent, $declarationsArrayString));
    }

    public function test_addOutputChainHtml_addsTheCorrectContent()
    {
        // Arrange
        $component = $this->getComponent();
        // need to add the child component manually
        
        // Act
        //$this->editor->addOutputChainHtml($component, )
    }
}