<?php

namespace Tests\Factories;

use App\Domain\Utils\PathUtils;
use App\Domain\Models\Projects\ComponentTemplate;
use App\Domain\Models\Projects\Component;
use App\Domain\Models\Projects\Module;

class ComponentFactory
{
    public static function createTemplate()
    {
        $templateData = require(PathUtils::componentConfigModelRoot.'/base.php');
        return new ComponentTemplate('name', 'home',
                                     '/var/www/html/pyramid_test/src/app/components', $templateData['config'], $templateData['blockAreas'],
                                     $templateData['metadata']);
    }

    public static function createComponent(ComponentTemplate $template, $currentProject)
    {
        return new Component($template, 'someName', $currentProject->componentRoot().'/someName', 'm:app:1',
                             $template->emptyConfig(), $template->blockAreas(), $template->metadata());
    }

    public static function createAppModule($currentProject)
    {
        return new Module($currentProject->project(), 'app', '/', '', $currentProject->clientRoot());
    }
}