<?php

namespace Tests;

use App\Domain\Models\Projects\Project;
use App\Domain\Project\CurrentProject;

abstract class ProjectBaseTest extends BaseTest
{
    private const scriptDir = '/var/www/html/pyramid/Storage/Scripts';

    /**
     * @var CurrentProject
     */
    protected $currentProject;

    public function setUp()
    {
        parent::setUp();

        echo exec('sudo '.self::scriptDir.'/pyramid-test-init pyramid_test 2>&1');

        $this->currentProject = new CurrentProject();
        $this->currentProject->setProject(new Project('pyramid_test'));
    }

    public function tearDown()
    {
        parent::tearDown();

        exec('sudo '.self::scriptDir.'/pyramid-test-destroy pyramid_test');
    }
}