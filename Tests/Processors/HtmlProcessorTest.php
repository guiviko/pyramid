<?php

namespace Tests\Processors;

use App\Domain\Processors\Client\HtmlProcessor;
use Tests\BaseTest;

class HtmlProcessorTest extends BaseTest
{
    /**
     * @var HtmlProcessor
     */
    private $processor;
    
    public function setUp()
    {
        $this->processor = new HtmlProcessor();
    }

    public function test_addOutputChain_adds_it_correctly_when_inline()
    {
        // Arrange
        $part1 = '<div class="stuff"></div>'.PHP_EOL;
        $part1 .= '<some-component';
        $part2 = '></some-component>';
        $content = $part1.$part2;

        // Act
        $result = $this->processor->addOutputChain('some-component', 'anAction', $content);

        // Assert
        $this->assertEquals($part1.' (anAction)="anAction.emit($event)"'.$part2, $result);
    }

    public function test_addOutputChain_adds_it_correctly_when_it_spans_multiple_lines()
    {
        // Arrange
        $part1 = '<div class="stuff"></div>'.PHP_EOL;
        $part1 .= '<some-component *ngIf="something"'.PHP_EOL;
        $part1 .= '                (otherAction)="do()"';
        $part2 = '></some-component>';
        $content = $part1.$part2;

        // Act
        $result = $this->processor->addOutputChain('some-component', 'anAction', $content);
        echo $result;

        // Assert
        $this->assertEquals($part1.PHP_EOL.'                (anAction)="anAction.emit($event)"'.$part2, $result);
    }

    public function test_addOutputChain_doesNotAddItTwice()
    {
        // Arrange
        $content = '<some-component (anAction)="anAction.emit($event)"></some-component>';

        // Act
        $result = $this->processor->addOutputChain('some-component', 'anAction', $content);

        // Assert
        $this->assertEquals($content, $result);
    }

    public function test_addOutputChain_addsItIfTheSameActionIsDefinedInADifferentComponentLater()
    {
        // Arrange
        $part1 = '<some-component';
        $part2 = '></some-component>'.PHP_EOL;
        $part2 .= '<another-component (anAction)="anAction.emit($event)"></another-component>';
        $content = $part1.$part2;

        // Act
        $result = $this->processor->addOutputChain('some-component', 'anAction', $content);

        // Assert
        $this->assertEquals($part1.' (anAction)="anAction.emit($event)"'.$part2, $result);
    }
}