<?php

namespace Tests\Processors;

use App\Domain\Processors\Client\TsProcessor;
use Tests\BaseTest;

class TsProcessorTest extends BaseTest
{
    /**
     * @var TsProcessor
     */
    private $tsProcessor;

    public function setUp()
    {
        parent::setUp();

        $this->tsProcessor = new TsProcessor();
    }

    public function test_addImport_onEmptyString()
    {
        // Act
        $result = $this->tsProcessor->addImport('some', 'theFile', '');

        // Assert
        $this->assertEquals('import {some} from \'theFile\';'.PHP_EOL, $result);
    }

    public function test_addImport_onNonEmptyStringWithoutImports()
    {
        // Arrange
        $content = 'text'.PHP_EOL.'more text';

        // Act
        $result = $this->tsProcessor->addImport('some', 'theFile', $content);

        // Assert
        $this->assertEquals('import {some} from \'theFile\';'.PHP_EOL.$content, $result);
    }

    public function test_addImport_whenThereExistUnrelatedImports()
    {
        // Arrange
        $part1 = 'import {this} from \'that\';'.PHP_EOL;
        $part1 .= 'import {this} from \'that\';'.PHP_EOL;
        $part2 = PHP_EOL.'more text here';
        $content = $part1.$part2;

        // Act
        $result = $this->tsProcessor->addImport('some', 'theFile', $content);

        // Assert
        $this->assertEquals($part1.'import {some} from \'theFile\';'.PHP_EOL.$part2, $result);
    }

    public function test_addImport_whenTheImportAlreadyExists()
    {
        // Arrange
        $content = 'import {this} from \'that\';'.PHP_EOL;
        $content .= 'import {some} from \'theFile\';'.PHP_EOL;
        $content .= 'import {that} from \'this\';'.PHP_EOL;

        // Act
        $result = $this->tsProcessor->addImport('some', 'theFile', $content);

        // Assert
        $this->assertEquals($content, $result);
    }

    public function test_addImport_whenTheImportAlreadyExistsWithinMultipleImportsOfTheSameFile()
    {
        // Arrange
        $content = 'import {this} from \'that\';'.PHP_EOL;
        $content .= 'import {one, some, two, three} from \'theFile\';'.PHP_EOL;
        $content .= 'import {that} from \'this\';'.PHP_EOL;

        // Act
        $result = $this->tsProcessor->addImport('some', 'theFile', $content);

        // Assert
        $this->assertEquals($content, $result);
    }

    public function test_addImport_whenThereAreOnlyOtherImportsFromTheSameFile()
    {
        // Arrange
        $part1 = 'import {this} from \'that\';'.PHP_EOL;
        $part1 .= 'import {one, two';
        $part2 = '} from \'theFile\';'.PHP_EOL;
        $part2 .= 'import {that} from \'this\';'.PHP_EOL;
        $content = $part1.$part2;

        // Act
        $result = $this->tsProcessor->addImport('some', 'theFile', $content);

        // Assert
        $this->assertEquals($part1.', some'.$part2, $result);
    }

    public function test_addImport_whenThereIsAnEmptyImportFromTheSameFile()
    {
        // Arrange
        $part1 = 'import {this} from \'that\';'.PHP_EOL;
        $part1 .= 'import {';
        $part2 = '} from \'theFile\';'.PHP_EOL;
        $part2 .= 'import {that} from \'this\';'.PHP_EOL;
        $content = $part1.$part2;

        // Act
        $result = $this->tsProcessor->addImport('some', 'theFile', $content);

        // Assert
        $this->assertEquals($part1.'some'.$part2, $result);
    }

    public function test_addOutput_whenThereIsAnEmptyString() {
        // Arrange
        $content = '';

        // Act
        $result = $this->tsProcessor->addOutput('some', $content);

        // Assert
        $this->assertEquals('', $content);
    }

    public function test_addOutput_whenThereAreNoOutputsOrInputsPresentAlready() {
        // Arrange
        $part1 = PHP_EOL.'export class SomeClass {'.PHP_EOL;
        $part2 = PHP_EOL.'}';
        $content = $part1.$part2;

        // Act
        $result = $this->tsProcessor->addOutput('some', $content);

        // Assert
        $expected = $part1.'    @Output() some = new EventEmitter<any>();'.PHP_EOL.$part2;
        $this->assertEquals($expected, $result);
    }

    public function test_addOutput_whenThereAreOnlyInputsPresent() {
        // Arrange
        $part1 = PHP_EOL.'export class SomeClass {'.PHP_EOL;
        $part1 = '    @Input() one: any;'.PHP_EOL;
        $part1 = '    @Input() two: any;'.PHP_EOL;
        $part2 = '    aProperty: any;'.PHP_EOL;
        $part2 = PHP_EOL.'}';
        $content = $part1.$part2;

        // Act
        $result = $this->tsProcessor->addOutput('some', $content);

        // Assert
        $expected = $part1.'    @Output() some = new EventEmitter<any>();'.PHP_EOL.$part2;
        $this->assertEquals($expected, $result);
    }

    public function test_addOutput_whenThereAreOutputsPresent() {
        // Arrange
        $part1 = PHP_EOL.'export class SomeClass {'.PHP_EOL;
        $part1 = '    @Input() one: any;'.PHP_EOL;
        $part1 = '    @Output() two = new EventEmitter<any>();'.PHP_EOL;
        $part1 = '    @Output() three = new EventEmitter<any>();'.PHP_EOL;
        $part1 = '    @Output() four = new EventEmitter<any>();'.PHP_EOL;
        $part2 = '    aProperty: any;'.PHP_EOL;
        $part2 = PHP_EOL.'}';
        $content = $part1.$part2;

        // Act
        $result = $this->tsProcessor->addOutput('some', $content);

        // Assert
        $expected = $part1.'    @Output() some = new EventEmitter<any>();'.PHP_EOL.$part2;
        $this->assertEquals($expected, $result);
    }

    public function test_addMethod_worksCorrectly()
    {
        // Arrange
        $part1 = 'export class Some {'.PHP_EOL;
        $part1 .= '    aMethod() {'.PHP_EOL;
        $part1 .= '    }'.PHP_EOL;
        $part2 = '}';
        $content = $part1.$part2;

        // Act
        $result = $this->tsProcessor->addMethod('some', $content);

        // Assert
        $this->assertEquals($part1.'some'.PHP_EOL.$part2, $result);
    }

    public function test_addMethod_doesNotAddItTwice()
    {
        // Arrange
        $content = 'export class Some {'.PHP_EOL;
        $content .= '    some'.PHP_EOL;
        $content .= '    aMethod() {'.PHP_EOL;
        $content .= '    }'.PHP_EOL;
        $content .= '}';

        // Act
        $result = $this->tsProcessor->addMethod('some', $content);

        // Assert
        $this->assertEquals($content, $result);
    }

    public function test_addActionToReducerField_addsTheActionCorrectly()
    {
        // Arrange
        $part1 = 'class someReducer {'.PHP_EOL;
        $part1 .= '    const one = () => {'.PHP_EOL;
        $part1 .= '        switch (action.type) {'.PHP_EOL;
        $part1 .= '        default:'.PHP_EOL;
        $part1 .= '                return state;'.PHP_EOL;
        $part1 .= '        }'.PHP_EOL;
        $part1 .= '    };'.PHP_EOL.PHP_EOL;
        $part1 .= '    const some = () => {'.PHP_EOL;
        $part1 .= '        switch (action.type) {'.PHP_EOL;
        $part2 = '        default:'.PHP_EOL;
        $part2 .= '                return state;'.PHP_EOL;
        $part2 .= '        }'.PHP_EOL;
        $part2 .= '    };'.PHP_EOL.PHP_EOL;
        $part2 .= '}';
        $content = $part1.$part2;

        // Act
        $result = $this->tsProcessor->addActionToReducerField('some', 'anAction', 'some', $content);

        // Assert
        $str = '        case SomeActions.AN_ACTION:'.PHP_EOL;
        $str .= '            return state;'.PHP_EOL;
        $this->assertEquals($part1.$str.$part2, $result);
    }

    public function test_addReducerField_addsTheFieldToTheState()
    {
        // Arrange
        $part1 = 'export interface SomeState {'.PHP_EOL;
        $part1 .= '    one: any;'.PHP_EOL;
        $part1 .= '    two: any;'.PHP_EOL;
        $part2 = '}'.PHP_EOL;
        $content = $part1.$part2;

        // Act
        $result = $this->tsProcessor->addReducerField('three', 'any', '{}', $content);

        // Assert
        $str = '    three: any;'.PHP_EOL;
        $this->assertEquals($part1.$str.$part2, $result);
    }

    public function test_addReducerField_addsTheFieldToTheInitialState()
    {
        // Arrange
        $part1 = 'export const initialState = {'.PHP_EOL;
        $part1 .= '    one: {},'.PHP_EOL;
        $part1 .= '    two: {},'.PHP_EOL;
        $part2 = '};'.PHP_EOL;
        $content = $part1.$part2;

        // Act
        $result = $this->tsProcessor->addReducerField('three', 'any', '{}', $content);

        // Assert
        $str = '    three: {},'.PHP_EOL;
        $this->assertEquals($part1.$str.$part2, $result);
    }

    public function test_addReducerField_addsTheFieldFunction()
    {
        // Arrange
        $part1 = 'export const initialState {'.PHP_EOL;
        $part1 .= '    one: {},'.PHP_EOL;
        $part1 .= '    two: {},'.PHP_EOL;
        $part1 .= '}'.PHP_EOL.PHP_EOL;
        $part2 = 'export const SomeReducer: ActionReducer<SomeState> = (state: SomeState = initialState, action: Action) => {'.PHP_EOL;
        $part2 .= '}';
        $content = $part1.$part2;

        // Act
        $result = $this->tsProcessor->addReducerField('three', 'any', '{}', $content);

        // Assert
        $str = 'const three = (state: any, action: Action, args: any = {}) => {'.PHP_EOL;
        $str .= '    switch (action.type) {'.PHP_EOL;
        $str .= '        default:'.PHP_EOL;
        $str .= '            return state;'.PHP_EOL;
        $str .= '    }'.PHP_EOL;
        $str .= '};'.PHP_EOL.PHP_EOL;
        $this->assertEquals($part1.$str.$part2, $result);
    }

    public function test_addConstructorParam_addsItWhenThereAreNoOtherParams()
    {
        // Arrange
        $part1 = 'class Some {'.PHP_EOL;
        $part1 .= '    constructor(';
        $part2 = ') {'.PHP_EOL.PHP_EOL;
        $part2 .= '    }'.PHP_EOL;
        $part2 .= '}';
        $content = $part1.$part2;
        $param = 'aParam';

        // Act
        $result = $this->tsProcessor->addConstructorParam($param, $content);

        // Assert
        $this->assertEquals($part1.$param.$part2, $result);
    }

    public function test_addConstructorParam_addsItWhenThereIsOneOtherParam()
    {
        // Arrange
        $part1 = 'class Some {'.PHP_EOL;
        $part1 .= '    constructor(some';
        $part2 = ') {'.PHP_EOL.PHP_EOL;
        $part2 .= '    }'.PHP_EOL;
        $part2 .= '}';
        $content = $part1.$part2;
        $param = 'aParam';

        // Act
        $result = $this->tsProcessor->addConstructorParam($param, $content);

        // Assert
        $this->assertEquals($part1.','.PHP_EOL.'                '.$param.$part2, $result);
    }

    public function test_addConstructorParam_addsItWhenThereAreManyOtherParams()
    {
        // Arrange
        $part1 = 'class Some {'.PHP_EOL;
        $part1 .= '    constructor(some,'.PHP_EOL;
        $part1 .= '                some2';
        $part2 = ') {'.PHP_EOL.PHP_EOL;
        $part2 .= '    }'.PHP_EOL;
        $part2 .= '}';
        $content = $part1.$part2;
        $param = 'aParam';

        // Act
        $result = $this->tsProcessor->addConstructorParam($param, $content);

        // Assert
        $this->assertEquals($part1.','.PHP_EOL.'                '.$param.$part2, $result);
    }
}