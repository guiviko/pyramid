<?php

namespace Library\Engine\Console\Modules;

use Library\Engine\Schema\SchemaSynchronizer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SynchronizeSchema extends Command
{
    private $schema;
    private $previousSchema = [];
    private $previousSchemaFile;
    private $basePath;
    private $datamapperConfigFile;

    public function __construct($basePath)
    {
        parent::__construct();

        $this->basePath = $basePath;
        $this->schema = require $basePath.'/Config/Schema/schema.php';
        $this->previousSchemaFile = $basePath.'/Storage/Framework/previousSchema.json';
        if (file_exists($this->previousSchemaFile))
        {
            $this->previousSchema = json_decode(file_get_contents($this->previousSchemaFile), true);
        }
        $this->datamapperConfigFile = $basePath.'/Config/FeaturesConfig/datamapper.php';
    }

    protected function configure()
    {
        $this
            ->setName('schema:sync')
            ->setDescription('Synchronize schema.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $synchronizer = new SchemaSynchronizer($this->schema, $this->previousSchema);
        $result = $synchronizer->synchronize();

        if ($result['success'])
        {
            file_put_contents($this->previousSchemaFile, json_encode($this->schema, JSON_PRETTY_PRINT));

            exec('php '.$this->datamapperConfigFile.' schema:update --force 2>&1', $dataMapperOutput);
            foreach ($dataMapperOutput as $dataMapperLine)
            {
                $output->writeln('<info>'.$dataMapperLine.'</info>');
            }

            echo PHP_EOL;
            $output->writeln('<info>Sync successfull.</info>');
            return;
        }

        $output->writeln('<error>Error synchronizing schema: '.$result['message'].'</error>');
    }
}