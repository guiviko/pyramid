<?php

return [

    App\Domain\Models\Users\User::class => [
        'id' => function($o) { return $o->getId(); },
        'firstName' => function($o) { return $o->firstName(); },
        'lastName' => function($o) { return $o->lastName(); },
        'email' => function($o) { return $o->email(); },
        'type' => function($o) { return $o->type(); }
    ],

    App\Domain\Models\Projects\Project::class => [
        'id' => function($o) { return $o->getId(); },
        'name' => function($o) { return $o->name(); }
    ],

    App\Domain\Models\Projects\ComponentTemplate::class => [
        'id' => function($o) { return $o->getId(); },
        'name' => function($o) { return $o->name(); },
        'icon' => function($o) { return $o->icon(); }
    ],

    App\Domain\Models\Projects\ControlTemplate::class => [
        'id' => function($o) { return $o->getId(); },
        'name' => function($o) { return $o->name(); },
        'icon' => function($o) { return $o->icon(); }
    ],

];