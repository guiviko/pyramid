<?php

return [

    'mappingDriver' => 'annotation',

    'databaseDriver' => 'mysql',

    'mysql' => [
        'host' => env('DATABASE_HOST'),
        'database' => env('DATABASE_NAME'),
        'username' => env('DATABASE_USERNAME'),
        'password' => env('DATABASE_PASSWORD')
    ],

    'classes' => [
        App\Domain\Models\Users\User::class,
        App\Domain\Models\Projects\Project::class,
        App\Domain\Models\Projects::class,
        App\Domain\Models\Projects\Page::class,
        App\Domain\Models\Projects\Module::class,
        App\Domain\Models\Projects\Component::class,
        App\Domain\Models\Projects\ComponentTemplate::class,
        App\Domain\Models\Projects\ControlTemplate::class,
        App\Domain\Models\Recipe::class,
        App\Domain\Models\Projects\ActionGroup::class,
        App\Domain\Models\Projects\Action::class,
        App\Domain\Models\Projects\StateField::class,
    ]

];