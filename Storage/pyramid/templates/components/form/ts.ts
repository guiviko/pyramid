import {Component, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'selectorName0',
    templateUrl: 'htmlName0',
    styleUrls: ['styleName0']
})
export class className0 {
    @Output() formUpdates = new EventEmitter<any>();
}