<?php

namespace App\Http\Requests{namespaces};

use App\Http\Requests\Request;

class {className} extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }
}