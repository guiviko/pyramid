import {ActionReducer, Action} from '@ngrx/store';

export interface stateName0 {
}

export const initialState = {
};

export const reducerName0: ActionReducer<stateName0> = (state: stateName0 = initialState, action: Action) => {
    switch (action.type) {
        default:
            return state;
    }
};
