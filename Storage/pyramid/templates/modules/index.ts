import {NgModule} from '@angular/core';
import {SharedModule} from "sharedModuleLevels0sharedModule/shared.module";
import {RouterModule} from "@angular/router";
import {componentName0} from './componentImport0';

import {routes} from './name0.routes';

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        componentName0
    ]
})
export class className0 {

}