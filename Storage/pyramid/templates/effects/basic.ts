import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import {Observable} from "rxjs";
import {AppState} from "../reducers/index";
import {Store} from "@ngrx/store";
import {Name0Actions} from "../actions/name0.actions";
import {Name0Service} from "../services/name0.service";

@Injectable()
export class Name0Effects {
    constructor(private actions$: Actions,
                private store$: Store<AppState>,
                private name0Service: Name0Service,
                private name0Actions: Name0Actions
    ) { }
}
