<?php

return [

    'icon' => 'television',

    'config' => [
        'groups' => [
            [
                'title' => 'General',
                'fields' => [
                    [
                        'label' => 'Name',
                        'type' => 'text',
                        'validations' => [],
                        'value' => 'Name'
                    ]
                ]
            ]
        ]
    ],

    'blockAreas' => [
        [
            'id' => 1,
            'blockMap' => [
                'nextId' => 3,
                1 => [
                    'id' => 1,
                    'isRoot' => true,
                    'parentId' => null,
                    'blocks' => [2],
                    'flexBasis' => 100,
                    'width' => 100,
                    'height' => 100,
                    'flexDirection' => 'row',
                    'type' => 'block',
                    'separatorDirection' => '',
                    'isSelected' => false,
                    'marginTop' => 0,
                    'marginBottom' => 0,
                    'marginLeft' => 0,
                    'marginRight' => 0,
                    'content' => [
                        'component' => null
                    ]
                ],
                2 => [
                    'id' => 2,
                    'isRoot' => false,
                    'parentId' => 1,
                    'blocks' => [],
                    'flexBasis' => 100,
                    'width' => 100,
                    'height' => 100,
                    'flexDirection' => 'row',
                    'type' => 'block',
                    'separatorDirection' => '',
                    'isSelected' => false,
                    'marginTop' => 0,
                    'marginBottom' => 0,
                    'marginLeft' => 0,
                    'marginRight' => 0,
                    'content' => [
                        'component' => null
                    ]
                ]
            ]
        ]
    ],

    'metadata' => []

];